package com.client.cpq.uitests.pageobjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.colt.common.utils.DriverManagerUtil;


public class Siebel_Objects {
	
	//Siebel Login Page
	@FindBy(xpath = "//input[@name='SWEUserName']") public WebElement userNameTxb;	//Username
	@FindBy(xpath = "//input[@name='SWEPassword']") public WebElement passWordTxb;	//Password
	@FindBy(xpath = "//a[text()='Login']") public WebElement loginBtn;	//Login Button
	@FindBy(xpath = "//a[text()='Accounts']") public WebElement accountsTab;	//Accounts Tab
	@FindBy(xpath = "//button[@data-display='Edit Layout']") public WebElement editLayoutBtn;	//Edit Layout Button
	
//	Siebel Home Page
	@FindBy(xpath = "//span[@title='Customer Orders']//parent::a") public WebElement customerOrderTab;
	@FindBy(xpath = "//input[contains(@aria-label,'Siebel Order')]") public WebElement siebelOrderTxb;
	@FindBy(xpath = "//button[@title='Search:Go']") public WebElement goButton;
	@FindBy(xpath = "//button[@title='Line Items:New Service Order']") public WebElement newServiceOrderBtn;
	
	
//	Customer Order
	@FindBy(xpath = "//input[contains(@aria-labelledby,'Delivery_Channel')]//following-sibling::span") public WebElement delvieryChannelLst;
	@FindBy(xpath = "//li[contains(@data-lovtype,'Delivery Channel')]//parent::ul") public WebElement delvieryChannelSubLst;
	@FindBy(xpath = "//input[contains(@aria-labelledby,'Sales_Channel')]//following-sibling::span") public WebElement salesChannelLst;
	@FindBy(xpath = "//li[contains(@data-lovtype,'Sales Channel')]//parent::ul") public WebElement salesChannelSubLst;	
	@FindBy(xpath = "//input[@aria-labelledby='Opportunity_Number_Label']") public WebElement oppurtunityLabelTxb;	
	@FindBy(xpath = "//input[@aria-labelledby='COLT_Contract_Label']//following-sibling::span[@aria-label='Selection Field']") public WebElement contractLookup;	
	@FindBy(xpath = "//input[@aria-labelledby='PopupQueryCombobox_Label']//following-sibling::span") public WebElement popUpFilterQueryLst;	
	@FindBy(xpath = "//li[contains(@data-lovtype,'PopupQueryCombobox')]//parent::ul") public WebElement popUpFilterQuerySubLst;	
	@FindBy(xpath = "//input[contains(@aria-labelledby,'PopupQuerySrchspec')]") public WebElement popUpQueryTxb;	
	@FindBy(xpath = "//button[@title='Pick Contract:Query']") public WebElement popUpContractQuerySearchBtn;	
	@FindBy(xpath = "//input[@aria-labelledby='COLT_Contract_Label']") public WebElement contractLabelTxb;	
	@FindBy(xpath = "//input[@name='Agreement_Number']") public WebElement contractIDTxb;
	@FindBy(xpath = "//input[@name='COLT_Contracting_City']//following-sibling::span") public WebElement contractCityLst;	
	@FindBy(xpath = "//li[contains(@data-lovtype,'COLT Contract Pick Applet:COLT Contracting City')]//parent::ul") public WebElement contractCitySubLst;	
	@FindBy(xpath = "//td[contains(@id,'Contracting_City')]") public WebElement contractCityCell;
	@FindBy(xpath = "//div[@class='siebui-popup-btm']//..//button[contains(@title,'OK')]") public WebElement popUpOKBtn;
	@FindBy(xpath = "//td[@class='siebui-popup-filter']//child::span[@class='siebui-popup-button']//child::button[@data-display='Go']") public WebElement popUpGOBtn;	
	@FindBy(xpath = "//table[contains(@summary,'Pick ')]") public WebElement pickPopUpTable;
	
	
//	Service Order
	@FindBy(xpath = "//span[@title='Service Order']//parent::a") public WebElement serviceOrderTab;
	@FindBy(xpath = "//input[@aria-labelledby='QuerySrchSpec_Label']") public WebElement enterServiceOrderTxb;
	@FindBy(xpath = "descendant::button[@title='Service Order List:Go'][2]") public WebElement serviceOrderSearchBtn;
	@FindBy(xpath = "//a[contains(text(),'Click to Show Full info')]") public WebElement showFullInfoLnk;
	
//	OrderSubType Entry
	@FindBy(xpath = "//input[@aria-labelledby='COLT_Order_Item_Sub_Type_Label']//following-sibling::span") public WebElement orderSubTypeLookup;
	@FindBy(xpath = "//button[@title='Service Order Sub Type:New']") public WebElement orderSubTypeNewBtn;
	@FindBy(xpath = "//input[@name='COLT_Order_Item_Sub_Type']//following-sibling::span") public WebElement orderSubTypeLst;
	@FindBy(xpath = "//li[contains(@data-lovtype,'Order Item Sub Type')]//parent::ul") public WebElement orderSubTypeSubLst;
	@FindBy(xpath = "//button[contains(@title,':OK')]") public WebElement orderSubTypeOKBtn;
	@FindBy(xpath = "//input[@aria-labelledby='Order_Processing_User_Label']//following-sibling::span") public WebElement orderProcessingUserLookup;
	@FindBy(xpath = "//input[@aria-labelledby='COLT_Network_Reference_Label']") public WebElement networkReferenceTxb;
	@FindBy(xpath = "//input[@aria-label='Promotion Code']") public WebElement promotionCodeTxb;
	@FindBy(xpath = "//input[@aria-labelledby='COLT_Technical_Contact_Full_Name_Label']//following-sibling::span[contains(@class,'siebui-icon')]") public WebElement techValLookup;
	@FindBy(xpath = "//input[@aria-labelledby='COLT_Existing_Capacity_Lead_Time_Code_Label']//following-sibling::span") public WebElement existingLeadTimePrimaryLst;
	
	
//	Save Service Order
	@FindBy(xpath = "//span[contains(@class,'-infopanel-moredetails-')]//child::a[contains(normalize-space(.),'Click here to save your ')]") public WebElement clickHereToSaveLnk;
	@FindBy(xpath = "//div[@id='colt-site-left']//child::div[contains(@class,'loading-panel colt-noedit-panel')]") public WebElement saveLeftPanel;
	@FindBy(xpath = "//div[contains(@class,'loading-panel colt-noedit-panel')]") public WebElement saveMainPanel;
	
//	middle applet entries
	@FindBy(xpath = "//span[contains(text(),'OSS Platform Flag')]//parent::div[contains(@class,'colt-attr')]//child::span[contains(@class,'applet-list')]") public WebElement ossPlatformFlagLsb;
	@FindBy(xpath = "//span[contains(text(),'Sub Sea Cable System (Worker Path)')]//parent::div[contains(@class,'colt-attr')]//child::span[contains(@class,'applet-list')]") public WebElement subSeaCablePathLst;
	@FindBy(xpath = "//span[contains(text(),'Router Technology')]//parent::div[contains(@class,'colt-attr')]//child::span[contains(@class,'applet-list')]") public WebElement routerTechnologyLsb;
	@FindBy(xpath = "//span[contains(text(),'Layer 3 Resilience')]//parent::div[contains(@class,'colt-attr')]//child::span[contains(@class,'applet-list')]") public WebElement layer3ResilienceLsb;
	
//	Site Address Selection
	@FindBy(xpath = "//div[@id='colt-site-left']//span[@title='Select a site']") public WebElement siteAddressAEndLookup;
	@FindBy(xpath = "//div[@id='colt-site-right']//span[@title='Select a site']") public WebElement siteAddressBEndLookup;
	@FindBy(xpath = "//input[@name='streetName']") public WebElement streetNameTxb;
	@FindBy(xpath = "//input[@name='country']") public WebElement countryTxb;
	@FindBy(xpath = "//input[@name='cityTown']") public WebElement cityTownTxb;
	@FindBy(xpath = "//input[@name='postalZipCode']") public WebElement postalZipCodeTxb;
	@FindBy(xpath = "//input[@name='premisesNumber']") public WebElement premisesTxb;
	@FindBy(xpath = "//div[contains(@id,'-site-selection-btn')]//child::button[text()='Search']") public WebElement searchAddressBtn;
	@FindBy(xpath = "//table[contains(@id,'siteselection-result-table')]") public WebElement searchResultTable;
	@FindBy(xpath = "//button[contains(text(),'Pick Address')]") public WebElement pickAddressBtn;
	@FindBy(xpath = "//button[contains(text(),'Pick Building')]") public WebElement pickBuildingBtn;
	@FindBy(xpath = "//button[contains(text(),'Pick Site')]") public WebElement pickSiteBtn;
	@FindBy(xpath = "//span[@class='ui-dialog-title' and contains(text(),'Site Selection')]") public WebElement siteSelectionHeader;
	@FindBy(xpath = "//button[contains(text(),'Building Not Found')]") public WebElement buildingNotFoundBtn;
	@FindBy(xpath = "//button[contains(text(),'Site Not Found')]") public WebElement siteNotFoundBtn;
	
//	Service Party and Site Contact
	@FindBy(xpath = "//div[@id='colt-site-left']//a[text()='Show full info']") public WebElement showFullInfoAendLnk;
	@FindBy(xpath = "//div[@id='colt-site-right']//a[text()='Show full info']") public WebElement showFullInfoBendLnk;
	@FindBy(xpath = "//div[@id='colt-site-left']//span[contains(text(),'Service Party')]//following-sibling::span[contains(@class,'applet-list-pick')]") public WebElement servicePartyLookup_A_End;
	@FindBy(xpath = "//div[@id='colt-site-right']//span[contains(text(),'Service Party')]//following-sibling::span[contains(@class,'applet-list-pick')]") public WebElement servicePartyLookup_B_End;
	@FindBy(xpath = "//div[@id='colt-site-left']//span[contains(text(),'Site Contact')]//following-sibling::span[contains(@class,'applet-list-pick')]") public WebElement siteContactLookup_A_End;
	@FindBy(xpath = "//div[@id='colt-site-right']//span[contains(text(),'Site Contact')]//following-sibling::span[contains(@class,'applet-list-pick')]") public WebElement siteContactLookup_B_End;
	@FindBy(xpath = "//span[text()='Addresses & Contacts']") public WebElement addressContactLookup;
	@FindBy(xpath = "//span[text()='Delivery Team Country']//following-sibling::input") public WebElement deliveryCountryTxb;
	@FindBy(xpath = "//span[text()='Delivery Team City']//following-sibling::input") public WebElement deliveryCityTxb;
	@FindBy(xpath = "//div[contains(@class,'colt-fullinfo-wrapper')]//..//button[@title='Close']") public WebElement closePopUpBtn;
	
	
//	A site details entry
	@FindBy(xpath = "//div[@id='colt-site-left']//span[text()='Third Party Access Provider']//following-sibling::span[contains(@class,'siebui-icon-dropdown')]") public WebElement thirdPartyAccessProviderALst;
	@FindBy(xpath = "//div[@id='colt-site-left']//span[text()='Shelf ID']//following-sibling::input") public WebElement shelfIDATxb;
	@FindBy(xpath = "//div[@id='colt-site-left']//span[text()='Slot ID']//following-sibling::input") public WebElement slotIDATxb;
	@FindBy(xpath = "//div[@id='colt-site-left']//span[text()='Physical Port ID']//following-sibling::input") public WebElement physicalportIDATxb;
	@FindBy(xpath = "//div[@id='colt-site-left']//span[text()='Install Time']//following-sibling::span[contains(@class,'siebui-icon-dropdown')]") public WebElement installationTimeALst;
	@FindBy(xpath = "//div[@id='colt-site-left']//span[text()='Fibre Type']//following-sibling::span[contains(@class,'siebui-icon-dropdown')]") public WebElement fibreTypeLst;
	
//	B site details entry
	@FindBy(xpath = "//div[@id='colt-site-right']//span[text()='Third Party Access Provider']//following-sibling::span[contains(@class,'siebui-icon-dropdown')]") public WebElement thirdPartyAccessProviderBLst;
	@FindBy(xpath = "//div[@id='colt-site-right']//span[text()='Shelf ID']//following-sibling::input") public WebElement shelfIDBTxb;
	@FindBy(xpath = "//div[@id='colt-site-right']//span[text()='Slot ID']//following-sibling::input") public WebElement slotIDBTxb;
	@FindBy(xpath = "//div[@id='colt-site-right']//span[text()='Physical Port ID']//following-sibling::input") public WebElement physicalportIDBTxb;
	@FindBy(xpath = "//div[@id='colt-site-right']//span[text()='VLAN Tag ID']//following-sibling::input") public WebElement vlanTagIDBTxb;
	@FindBy(xpath = "//div[@id='colt-site-right']//span[text()='Install Time']//following-sibling::span[contains(@class,'siebui-icon-dropdown')]") public WebElement installationTimeBLst;
	
//	Order dates Link
	@FindBy(xpath = "//a[contains(text(),'Order Dates')]") public WebElement orderDatesLnk;
	@FindBy(xpath = "//input[@aria-label='Order Signed Date']//following-sibling::span") public WebElement orderSignedDateElem;
	@FindBy(xpath = "//input[@aria-labelledby='Actual_Delivery_Date_Label']//following-sibling::span") public WebElement coltActualDateElem;
	@FindBy(xpath = "//input[@aria-labelledby='COLT_Order_Received_Date_Label']//following-sibling::span") public WebElement orderRecievedDateElem;
	@FindBy(xpath = "//input[@aria-label='Colt Promise Date']//following-sibling::span[@id='cpd_date_icon']") public WebElement coltPromiseDateElem;
	@FindBy(xpath = "//input[@aria-labelledby='Customer_Requested_Date1_Label']//following-sibling::span") public WebElement customerRequestedDateElem;
	@FindBy(xpath = "//span[contains(text(),'Customer Milestones')]") public WebElement customerMilestonesTxb;
	@FindBy(xpath = "//button[contains(@class,'datepicker') and text()='Now']") public WebElement pickNowFromDate;
	@FindBy(xpath = "//button[contains(@class,'datepicker') and text()='Done']") public WebElement pickDoneFromDate;
	@FindBy(xpath = "//button[text()='Accept Proposal']") public WebElement acceptProposalBtn;
	
	
//	Billing Entry
	@FindBy(xpath = "//a[contains(text(),'Billing')]") public WebElement billingLnk;
	@FindBy(xpath = "//input[contains(@aria-labelledby,'COLT_Customer_Reference')]") public WebElement billCustRefOrderTxb;
	@FindBy(xpath = "//input[@aria-labelledby='COLTBillingStartDate_Label']//following-sibling::span") public WebElement billStartDateElem;
	@FindBy(xpath = "//button[contains(@title,'Refresh')]") public WebElement refreshBtn;
	
//	Order Status Validation
	@FindBy(xpath = "//input[@aria-label='Order Status']//following-sibling::span") public WebElement orderStatusLst;
	@FindBy(xpath = "//li[contains(@data-lovtype,'Status')]//parent::ul") public WebElement orderStatusSubLst;
	@FindBy(xpath = "//input[@data-queue='BDWREVW' and @type='checkbox']") public WebElement BDWREVW_Checkbox;
	@FindBy(xpath = "//button[@id='TriggerTR']") public WebElement triggerTRBtn;
	@FindBy(xpath = "//button[text()='Get Reference']") public WebElement getReferenceBtn;
	@FindBy(xpath = "//span[text()='Circuit Reference']//following-sibling::input") public WebElement circuitReferenceTxb;
	@FindBy(xpath = "//select[@aria-label='Third Level View Bar']") public WebElement bottomChildLst;
	@FindBy(xpath = "//a[contains(text(),'Installation and Test')]") public WebElement installationAndTestLnk;
	@FindBy(xpath = "//input[@aria-label='Primary Testing Method']//following-sibling::span") public WebElement primaryTestingMethodLst;
	@FindBy(xpath = "//button[@title='Service Orders:Manual Validation']") public WebElement manualValidationBtn;
	@FindBy(xpath = "//button[text()='Continue']") public WebElement continueBtn;
	@FindBy(xpath = "//input[@class='workflow-checkbox']") public WebElement workflowCbx;
	@FindBy(xpath = "//button[text()='OK']") public WebElement statusValidationOKBtn;
	
	
	
	
	
	
	
	
	

	
	
	
	
//	alert popup
	@FindBy(xpath = "//div[@id='_sweview_popup']") public WebElement alertPopUp;
	@FindBy(xpath = "//button[@id='btn-accept']") public WebElement alertPopUpOKBtn;	
	
	
	
	
	
	public Siebel_Objects() {  
		WebDriver driver = DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
		PageFactory.initElements(driver, this);
	}
}

