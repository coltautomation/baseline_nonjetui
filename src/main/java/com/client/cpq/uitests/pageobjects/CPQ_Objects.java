package com.client.cpq.uitests.pageobjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.colt.common.utils.DriverManagerUtil;


public class CPQ_Objects {
	
	//C4C Login Page
	@FindBy(xpath = "//input[@id='userId']") public WebElement userNameTxb;	//Username
	@FindBy(xpath = "//input[@name='Ecom_Password']") public WebElement passWordTxb;	//Password
//	@FindBy(xpath = "//button[text()='Login ']") public WebElement loginBtn;	//Login Button
	@FindBy(xpath = "//button[@type='submit']") public WebElement loginBtn;	//Login Button
	@FindBy(xpath = "//button[contains(text(),'Login Back To Colt Online')]") public WebElement LoginBackToColtOnlineBtn;	//LoginBackToColtOnlineBtn
	@FindBy(xpath = "//button[@title='Click to Personalize / Adapt']") public WebElement verifyEditLnk; //Edit Link in Home page
	
//	C4C Logout Page
	@FindBy(xpath = "//div[contains(@id,('header-user-image'))]") public WebElement c4cUserImgLnk;
	@FindBy(xpath = "//bdi[text()='Sign Out']") public WebElement c4cSignOutBtn;
	@FindBy(xpath = "//button//bdi[text()='Yes']") public WebElement c4cAcceptSignoutBtn;
	@FindBy(xpath = "//button[@id='LOGIN_LINK']") public WebElement c4cLogOnBtn;

//	CPQ Login page
	@FindBy(xpath = "//input[@id='username']") public WebElement cpqUserNameTxb;
	@FindBy(xpath = "//label[text()='Password:']//following-sibling::input") public WebElement cpqPassWordTxb;
	@FindBy(xpath = "//a[text()='Log in']") public WebElement cpqLoginBtn;	

//	C4CMain Page

	@FindBy(xpath = "//span[@title='Customers']") public WebElement customerLst;	//Customer dropdown
	@FindBy(xpath = "//a[@title='Accounts']") public WebElement accountsTab;	//Accounts Option
	@FindBy(xpath = "//*[contains(@title,'Please wait')]") public WebElement c4cLoader;	//c4c Loader
	@FindBy(xpath = "//span[@title='Sales']") public WebElement salesLst;
	@FindBy(xpath = "//a[@title='Quotes']") public WebElement quotesTab;
	@FindBy(xpath = "//span[text()='Quote']//parent::a//parent::li//following-sibling::li[1]") public WebElement plTab;
	
//	Search Quote and Opportunities
	@FindBy(xpath = "//span[contains(@id,'variantManagement-trigger-img')]") public WebElement selectViewDn;	//Select View Dropdown
	@FindBy(xpath = "//li[contains(text(),'All')]") public WebElement allLst;		//All Value in dropdown
	@FindBy(xpath = "//div[@aria-label='Select all rows'][1]") public WebElement defaultCkb;		//to verify Check box is enbled
	@FindBy(xpath = "//span[contains(@id,'searchButton-img')]") public WebElement searchIcon;	//Search Icon
	@FindBy(xpath = "//input[@type='search']") public WebElement searchTxb;	//Search Input Box
	@FindBy(xpath = "//div[@title='Search']") public WebElement searchBtn;	//Search Button or Icon
	
//	Oppurtunity Screen
	@FindBy(xpath = "//div[text()='Opportunities']") public WebElement OpportunitiesLnk;	//Oppurtunity Link
	@FindBy(xpath = "//bdi[text()='New']") public WebElement opportunityNewBtn;	//New Button
	@FindBy(xpath = "//span[@title='Name']//following-sibling::div//..//input") public WebElement NameTxb;	//New Button
	@FindBy(xpath = "//span[@title='Offer Type']//following-sibling::div//child::span") public WebElement OfferTypeLst;	//New Button
	@FindBy(xpath = "//span[@title='Primary Programme']//following-sibling::div//child::span") public WebElement PrimaryProgramLst;
	@FindBy(xpath = "//span[@title='Primary Attribute']//following-sibling::div//child::span") public WebElement PrimaryAttributeLst;
	@FindBy(xpath = "//span[@title='Opportunity Currency']//following-sibling::div//child::span") public WebElement OpportunityCurencyLst;
	@FindBy(xpath = "//div[contains(@id,'button')]//button[@title='More']") public WebElement saveMoreLnk;
	@FindBy(xpath = "//bdi[text()='Save and Open']") public WebElement saveOpenLnk;
	@FindBy(xpath = "//div[@data-help-id='messageBar-headerBar']//..//span[contains(@data-help-id,'messageBar')]") public WebElement headerBar;
	@FindBy(xpath = "//span[@title='Opportunity ID']//following-sibling::div//child::span") public WebElement oppurtunityIDBx;
	@FindBy(xpath = "//bdi[text()='More']//parent::span//parent::span//parent::button") public WebElement moreLnk;
	@FindBy(xpath = "//button[@title='Expand']") public WebElement expandLnk;
	@FindBy(xpath = "//button[@title='Collapse']") public WebElement collapseLnk;
	@FindBy(xpath = "//button[@title='Edit']") public WebElement editOpportunityLnk;
	@FindBy(xpath = "//span[@title='Sales Unit']//following-sibling::div//..//input") public WebElement salesUnitTxb;	
	@FindBy(xpath = "//span[@title='Legal complexity']//following-sibling::div//child::span") public WebElement legalComplexityLnk;
	@FindBy(xpath = "//span[@title='Technical complexity']//following-sibling::div//child::span") public WebElement techComplexityLnk;
	@FindBy(xpath = "//descendant::bdi[text()='Save'][2]") public WebElement saveBtn;
	
//	SE Engagement Screen in C4C
	@FindBy(xpath = "//div[text()='Engagement']") public WebElement EngagementLnk;
	@FindBy(xpath = "//button[@title='Edit']") public WebElement editEngagementLnk;
	@FindBy(xpath = "//div[@title='Request SE']") public WebElement engageSECbx;
	@FindBy(xpath = "//div[@title='Consultant']") public WebElement consultantCbx;
	@FindBy(xpath = "//span[contains(@title,'SE Engagement')]//following-sibling::div//child::span") public WebElement seEngagementLst;
	@FindBy(xpath = "//ul[@role='listbox']") public WebElement defaultListElem;
	@FindBy(xpath = "//bdi[text()='Save']") public WebElement saveC4CBtn;
	
	
	
	
//	Quotes Screen
	@FindBy(xpath = "//div[text()='Quotes']") public WebElement quotesLnk;
	@FindBy(xpath = "//bdi[text()='Add']//parent::span//parent::span//parent::button") public WebElement addQuoteBtn;
	@FindBy(xpath = "//bdi[contains(text(),'Action')]") public WebElement actionBtn;	//New Button
	@FindBy(xpath = "//bdi[text()='Edit']") public WebElement EditBtn;	//New Button
	@FindBy(xpath = "//bdi[text()='View']") public WebElement ViewBtn;	//New Button
	@FindBy(xpath = "//a[@id='save']") public WebElement save4cBtn;	//New Button
	@FindBy(xpath = "//a[@id='update']") public WebElement update4cBtn;	//New Button
	@FindBy(xpath = "//bdi[text()='Copy']//parent::span") public WebElement copyQuoteC4CBtn;
	@FindBy(xpath = "//bdi[text()='Proceed']//parent::span") public WebElement proceedC4CBtn;
	
// Quotes MultiPuropse
	@FindBy(xpath = "//*[text()='Quote Currency']/parent::*/parent::*/div/div/div/div/input") public WebElement quoteCurrencyDBtn;
	@FindBy(xpath = "//*[text()='Legal Complexity']/parent::*/parent::*/div/div/div/div/input") public WebElement legalComplexityDBtn;
	@FindBy(xpath = "//*[text()='Technical Complexity']/parent::*/parent::*/div/div/div/div/input") public WebElement techicalComplexityDBtn;
	@FindBy(xpath = "//bdi[text()='Proceed']/parent::*/parent::*/parent::*") public WebElement proceedBtn;
	@FindBy(xpath = "//div[contains(@id,'ojChoiceId_quoteType_t')]//a") public WebElement quoteTypeBtn;
	@FindBy(xpath = "//li//div[text()='Multi-quote']") public WebElement multiTypeBtn;
	@FindBy(xpath = "//input[@value='Multi-Quote']//parent::span") public WebElement multiQuoteCbx;
	@FindBy(xpath = "//span[text()='Download Quote']") public WebElement downloadQuote;
	@FindBy(xpath = "//span[text()='Download Quote Excel-External']") public WebElement downloadQuoteExternal;
	@FindBy(xpath = "//span[text()='Proceed to Quote']") public WebElement procedToQuoteBtn;
	@FindBy(xpath = "//label[text()='Account Details']") public WebElement accountDetailsLnk;
	@FindBy(xpath = "//input[@id='oCN_t']") public WebElement ocnTxt;
	@FindBy(xpath = "//input[@id='oCN_t' and @readonly='readonly']") public WebElement ocnReadTxt;
	@FindBy(xpath = "//div[@id='oj-select-choice-pricingSegment_t']") public WebElement priceSegmentDrp;
	@FindBy(xpath = "//td[@id='cancel_configuration']") public WebElement cancelConfiguration;
	
//	CPQ Home Page
	@FindBy(xpath = "//button[@name='return_to_c4c']") public WebElement returnToC4CBtn;
	@FindBy(xpath = "//input[contains(@id,'quoteName')]") public WebElement quoteNameTxb;
	@FindBy(xpath = "//descendant::input[contains(@id,'quoteType')][2]") public WebElement quoteTypeElem;
//	@FindBy(xpath = "//div[contains(@id,'oj-select-choice-quoteType_t')]") public WebElement quoteTypeElem;
	@FindBy(xpath = "//descendant::input[contains(@id,'quoteID')]") public WebElement quoteIDElem;
	@FindBy(xpath = "//descendant::input[contains(@id,'status_t')]") public WebElement quoteStageElem;
	@FindBy(xpath = "//descendant::input[contains(@id,'statusNew')]") public WebElement quoteStatusElem;
	@FindBy(xpath = "//span[text()='SE Rev']") public WebElement seRevLnk;
	@FindBy(xpath = "//div[@id='lockCreateScreen' and not(@style='display: none;')]") public WebElement cpqLoadMask;
	@FindBy(xpath = "//button[@name='configuration_complete,_send_to_sales']") public WebElement confCompleteSendSalesBtn;
	
//	Adding Product
	@FindBy(xpath = "//span[text()='Add Product']") public WebElement addProductBtn;
	@FindBy(xpath = "//a[text()='CPE Solutions']") public WebElement cpeSolutionLnk;
	@FindBy(xpath = "//a[text()='CPE Solutions Service']") public WebElement cpeServiceLnk;
	@FindBy(xpath = "//a[text()='CPE Solutions Site']") public WebElement cpeSiteLnk;
	@FindBy(xpath = "//a[text()='Ethernet']") public WebElement ethernetLnk;
	@FindBy(xpath = "//a[text()='Ethernet Line']") public WebElement ethernetLineLnk;
	@FindBy(xpath = "//a[text()='Ethernet Hub']") public WebElement ethernetHubLnk;
	@FindBy(xpath = "//a[text()='Ethernet Spoke']") public WebElement ethernetSpokeLnk;
	@FindBy(xpath = "//a[text()='Optical']") public WebElement opticalLnk;
	@FindBy(xpath = "//a[text()='Wave']") public WebElement waveLnk;
	@FindBy(xpath = "//a[text()='IP Access']") public WebElement IPAccessLnk;
	@FindBy(xpath = "//a[text()='Colt IP Access']") public WebElement ColtIpAccessLnk;
	@FindBy(xpath = "//a[text()='Colt IP Domain']") public WebElement ColtIpDomainLnk;
	@FindBy(xpath = "//a[text()='Colt IP Guardian']") public WebElement ColtIpGuardianLnk;
	@FindBy(xpath = "//a[text()='Colt Managed Virtual Firewall']") public WebElement ColtManagedVirtualFirewallLnk;
	@FindBy(xpath = "//a[text()='Colt Managed Dedicated Firewall']") public WebElement ColtManagedDedicatedFirewallLnk;
	@FindBy(xpath = "//a[text()='Start']") public WebElement ColtIpAccessStartLnk;
	@FindBy(xpath = "//a[text()='Professional Services']") public WebElement professionalServicesLnk;
	@FindBy(xpath = "//a[text()='Professional Services - Data']") public WebElement professionalServicesDataLnk;
	@FindBy(xpath = "//a[text()='Professional Services - Voice']") public WebElement professionalServicesVoiceLnk;
	@FindBy(xpath = "//a[text()='Data - Consultancy']") public WebElement dataConsultancyLnk;
	@FindBy(xpath = "//a[text()='Data - Project Management']") public WebElement dataProjectManagement;
	@FindBy(xpath = "//a[text()='Data - Service Management']") public WebElement dataServiceManagement;
	@FindBy(xpath = "//a[text()='Voice - Consultancy']") public WebElement voiceConsultancyLnk;
	@FindBy(xpath = "//a[text()='Voice - Project Management']") public WebElement voiceProjectManagement;
	@FindBy(xpath = "//a[text()='Voice - Service Management']") public WebElement voiceServiceManagement;
	@FindBy(xpath = "//a[text()='Data']") public WebElement dataLink;
	@FindBy(xpath = "//a[text()='VPN']") public WebElement vpnLink;
	@FindBy(xpath = "//a[text()='VPN Data']") public WebElement vpnDataLink;
	@FindBy(xpath = "//a[text()='VPN Network']") public WebElement vpnNetworkLink;
	
//	Delete the Product
	@FindBy(xpath = "//oj-button[@title='Remove']") public WebElement deleteProductBtn;
	@FindBy(xpath = "//div[@class='oj-dialog-footer']//..//span[text()='OK']") public WebElement okdialogButton;
	
//	IPAddon Products
	@FindBy(xpath = "//select[@id='countryIPD']//following-sibling::button[@title='Show All Items']") public WebElement ipDomainCountryLst;
	@FindBy(xpath = "descendant::ul[@role='listbox'][1]") public WebElement ipAddOnCountrySubLst;	
	@FindBy(xpath = "//input[@id='iPAccessServiceReference']") public WebElement ipAccessServiceReferenceTxb;	
	@FindBy(xpath = "//select[contains(@id,'domainOrderTypeGeneric')]") public WebElement domainTypeGenericLst;
	@FindBy(xpath = "//select[@name='topLevelDomainGenericPickList']") public WebElement topLevelDomainLst;
	@FindBy(xpath = "//input[@id='domainName-0']") public WebElement ipDomainNameTxb;
	@FindBy(xpath = "//select[@name='serviceBandwidthIpGuardian']") public WebElement ipGuardianBandwidthLst;
	@FindBy(xpath = "//select[@name='customerType']") public WebElement customerTypeLst;
	@FindBy(xpath = "//select[@id='countryIPG']//following-sibling::button[@title='Show All Items']") public WebElement ipGuardianCountryLst;
	@FindBy(xpath = "//input[@id='iPAccessServiceReferenceipguardian']") public WebElement ipGuardianServiceReferenceTxb;
	@FindBy(xpath = "//select[@id='countryIPMVF']//following-sibling::button[@title='Show All Items']") public WebElement mvfCountryLst;
	@FindBy(xpath = "//select[@name='serviceBandwidthIPMVF']") public WebElement mvfBandwidthLst;
	@FindBy(xpath = "//input[@id='iPAccessServiceReferenceIPMVF']") public WebElement mvfServiceReferenceTxb;
	@FindBy(xpath = "//select[@id='managedDedicatedFirewallCountryAEnd']") public WebElement mdfCountryLst;
	@FindBy(xpath = "//select[@name='serviceBandwidthIPMDF']") public WebElement mdfBandwidthLst;
	@FindBy(xpath = "//input[@id='iPAccessServiceReferenceIPMDF']") public WebElement mdfServiceReferenceTxb;	
	
	
//	Bulk Upload Products
	@FindBy(xpath = "//span[text()='Bulk Upload']") public WebElement bulkUploadLnk;
	@FindBy(xpath = "//input[@value='Add New Line Items to Quote']") public WebElement bulkAddNewLineBtn;
	@FindBy(xpath = "//input[@value='Upload New File']") public WebElement bulkUploadNewFlieBtn;
	@FindBy(xpath = "//input[@id='fileInput']") public WebElement bulkfileInputBtn;
	@FindBy(xpath = "//input[@value='Upload']") public WebElement bulkUploadBtn;
	@FindBy(xpath = "//div[text()='Please wait,File Upload in Progress....']") public WebElement bulkPleaseWaitText;
	@FindBy(xpath = "//span[text()='Uploaded']") public WebElement bulkUploadedTxt;
	@FindBy(xpath = "//span[text()='Details']") public WebElement bulkDetailsTxt;
	@FindBy(xpath = "//button[text()='Refresh']") public WebElement bulkRefreshBtn;
	@FindBy(xpath = "//button[text()=' Check Connectivity']") public WebElement bulkCheckConnectivityBtn;
	@FindBy(xpath = "//span[contains(text()=' Connectivity Check in')]") public WebElement bulkConnectivityTxt;
	@FindBy(xpath = "//input[@id='fromAend']//preceding-sibling::span[.='ONNET'  and not(contains(@disabled,'disabled'))]") public WebElement bulkOnnetACheckRdb;
	@FindBy(xpath = "//input[@id='fromBend']//preceding-sibling::span[.='ONNET'  and not(contains(@disabled,'disabled'))]") public WebElement bulkOnnetBCheckRdb;
	@FindBy(xpath = "//span[text()='Select All']//preceding-sibling::input") public WebElement bulkSelectAllCbx;
	@FindBy(xpath = "//button[text()='Add To Quote']") public WebElement bulkAddToQuoteBtn;
	@FindBy(xpath = "//button[text()='Pending']") public WebElement bulkPendingBtn;
	@FindBy(xpath = "//button[text()='Continue To Quote']") public WebElement bulkContinueToQuoteBtn;
	
//	Professional Services Configuration
	@FindBy(xpath = "//select[@id='serviceType']") public WebElement serviceTypeLst;
	@FindBy(xpath = "//input[@id='effortEstimationInManDays']") public WebElement effortEstimationManDaysTxb;	
	@FindBy(xpath = "//div[@id='attribute-grossNonRecurringChargesNRC']//div[@class='attribute-field-container']//span") public WebElement grosssNRCElem;
	@FindBy(xpath = "//select[@id='packageType']") public WebElement packageTypeLst;
	@FindBy(xpath = "//input[@id='resourceRequired_PM']") public WebElement resourceRequired;
	@FindBy(xpath = "//select[@id='contractTermInYears']") public WebElement contractTermInYears;
	
//	CPE Solutions Service Cofiguration pagein CPQ
	@FindBy(xpath = "//select[@id='networkTopology']") public WebElement networkTopologyLst;
	@FindBy(xpath = "//label[@id='networkTopology_label']") public WebElement networkTopologyLabel;
	@FindBy(xpath = "//span[text()='CPE Site Configuration']") public WebElement cpeSiteConfigurationLnk;
	@FindBy(xpath = "//select[@id='cPESolutionService']") public WebElement cpeSolutionServiceLst;
	@FindBy(xpath = "//label[text()='Select' and @for='selectServiceRef_0']") public WebElement defSelectServiceBtn;
	@FindBy(xpath = "//span[text()='RELATED NETWORK PRODUCT']") public WebElement relatedNetworkProductLnk;
	@FindBy(xpath = "//select[@id='relatedNetworkProductReferenceType']") public WebElement relatedNetworkProductLst;
	@FindBy(xpath = "//span[text()='CPE SITE ADDRESS']") public WebElement cpeSiteAddressLnk;
	@FindBy(xpath = "//input[@id='relatedNetworkProductReference']") public WebElement relatedNetworkReferenceTxb;
	@FindBy(xpath = "//select[@id='hubType']") public WebElement hubTypeLst;
	@FindBy(xpath = "//input[@id='hubReferenceID']") public WebElement hubReferenceTxb;
	@FindBy(xpath = "//input[@id='siteAddressHubExisting']") public WebElement hubAddressTxb;
	@FindBy(xpath = "//descendant::img[@title='Search for an Address'][1]") public WebElement hubSearchImgLnk;
	@FindBy(xpath = "//descendant::img[@title='Search for an Address'][2]") public WebElement spokeSearchImgLnk;
	
//	Vpn Network
	@FindBy(xpath = "//div[@id='oj-select-choice-networkType']//a") public WebElement vpnSelectChoiceNetworkLnk;
	@FindBy(xpath = "//oj-progress[contains(@title,'Processing')]") public WebElement vpnLoadBarPorcess;
	@FindBy(xpath = "//span[text()='ADD']") public WebElement vpnAddBTN;
	@FindBy(xpath = "(//span[@class='oj-treeview-item-text'])[2]") public WebElement vpnTreeLink;
	@FindBy(xpath = "//span[text()='Add-Ons']") public WebElement vpnAddOnLink;
	@FindBy(xpath = "//input[@name='outsideBusinessInstallationHoursVPN']") public WebElement vpnOusideBusinessCbx;
	@FindBy(xpath = "//input[@name='longLiningVPN']") public WebElement vpnLongLiningCbx;
	
	
	
	
//	Vpn Address
	@FindBy(xpath = "//input[contains(@id,'siteAddressSearchVPN')]") public WebElement vpnSiteAddressTxb;
	@FindBy(xpath = "//div[@id='searchButton']//img") public WebElement vpnSiteSearchImg;
	@FindBy(xpath = "//span[text()='Next']") public WebElement vpnNextButton;
	@FindBy(xpath = "//span[text()='Site Configuration']") public WebElement vpnsiteConfiguartionLink;
	@FindBy(xpath = "//div[@id='oj-select-choice-linkTypeVPN']//a") public WebElement vpnlinkTypeLink;
	
//	Vpn Site configuration
	@FindBy(xpath = "//div[@id='oj-select-choice-contractTermSiteDetails']//a") public WebElement vpnContractYearLnk;
	@FindBy(xpath = "//div[@id='oj-select-choice-serviceBandwidthSiteDetails']//a") public WebElement vpnServiceBandLnk;
	@FindBy(xpath = "//div[@id='oj-select-choice-resilienceSiteDetails']//a") public WebElement vpnResilienceLnk;
	
//	Vpn Add product
	@FindBy(xpath = "//div[@id='oj-select-choice-existingCapacityLeadTime_VPN']//a") public WebElement vpnLeadTimeLink;
	@FindBy(xpath = "//div[@id='oj-select-choice-siteCabinetType_VPN']//a") public WebElement vpnCabinetTypeLnk;
	@FindBy(xpath = "//input[contains(@id,'siteCabinetID_VPN')]") public WebElement vpnCabinetIdTxb;
	@FindBy(xpath = "//div[@id='oj-select-choice-siteCustomerSitePOPStatus_VPN']//a") public WebElement vpnPopLink;
	@FindBy(xpath = "//div[@id='oj-select-choice-sitePresentationInterface_VPN']//a") public WebElement vpnSitePresentationLink;
	@FindBy(xpath = "//div[@id='oj-select-choice-siteConnectorType_VPN']//a") public WebElement vpnSiteConnectorLink;
	@FindBy(xpath = "//input[contains(@id,'siteID_VPN')]") public WebElement vpnSiteIdTxb;
	@FindBy(xpath = "//oj-button[@name='update']") public WebElement vpnUpdateBtn;
	@FindBy(xpath = "//oj-button[@name='start_over']//following-sibling::oj-button") public WebElement vpnSaveToQuoteBtn;
	@FindBy(xpath = "(//span[@class='oj-treeview-item-text'])[1]") public WebElement vpnNetworkTreeLink;
	
//	Vpn Dual Entry
	@FindBy(xpath = "//span[text()='Add-Ons']") public WebElement vpnAddONLink;
	@FindBy(xpath = "//div[@id='oj-select-choice-exploreCheckboxActionsDualEntryVPN']//a") public WebElement vpnDualEntryLink;
	@FindBy(xpath = "//input[@name='closeWindowForManualRequestDE']") public WebElement vpnExploreClsBtn;
	@FindBy(xpath = "//oj-button[@name='save']") public WebElement vpnSaveBtn;
	
//	Vpn Offnet Entry
	@FindBy(xpath = "//label[contains(@for,'connectivityCheckActions0')]") public WebElement vpnOffNetCheckBtn;
	@FindBy(xpath = "//label[contains(@for,'connectivityCheckActions3')]") public WebElement vpnManualCheckBtn;
	@FindBy(xpath = "//div[@id='oj-select-choice-raiseManualRequestVPN']//a") public WebElement vpnExploreRequestVPN;
	@FindBy(xpath = "//input[contains(@name,'closeWindowForMR')]") public WebElement vpnExploreOffCloseBtn;
	@FindBy(xpath = "//span[@class='oj-radiocheckbox-icon']//input[@name='contractTerm1Year-0']") public WebElement vpnOffnetRadioBtn;
	
//	Vpn DSL Entry
	@FindBy(xpath = "//label[contains(@for,'connectivityCheckActions1')]") public WebElement vpnOffNetDSLCheckBtn;
	
//	Address selection
	@FindBy(xpath = "//input[@id='siteAddressAEnd']") public WebElement siteAAddressTxb;
	@FindBy(xpath = "//input[@id='siteAddressBEnd']") public WebElement siteBAddressTxb;
	@FindBy(xpath = "//input[@id='siteTelephoneNumberAEnd']") public WebElement siteAPhoneNumberTxb;
	@FindBy(xpath = "//input[@id='siteTelephoneNumberBEnd']") public WebElement siteBPhoneNumberTxb;
	@FindBy(xpath = "//img[@title='Search for an Address']") public WebElement siteSearchImg;
	@FindBy(xpath = "//div[@id='siteAddressAEnd_message']") public WebElement siteAddressMessage;
	@FindBy(xpath = "//span[text()='Site Address Details']") public WebElement siteAddressDetailsLnk;
	@FindBy(xpath = "//input[@id='premiseNumberAEnd']") public WebElement premiseNumberAEndTxb;
	@FindBy(xpath = "//input[@id='streetNameAEnd']") public WebElement streetNameAEndTxb;
	@FindBy(xpath = "//input[@id='cityAEnd']") public WebElement cityAEndTxb;
	@FindBy(xpath = "//input[@id='countryAEnd']") public WebElement countryAEndTxb;
	@FindBy(xpath = "//input[@id='postCodeAEnd']") public WebElement postCodeAEndTxb;
	@FindBy(xpath = "//label[text()='Site Address']") public WebElement siteAddressLabel;
	@FindBy(xpath = "//div[@id='searchButtonAEnd']//img") public WebElement siteASearchImg;
	@FindBy(xpath = "//div[@id='searchButtonBEnd']//img") public WebElement siteBSearchImg;
	@FindBy(xpath = "//img//following-sibling::span[contains(text(),'Connected via Colt')]") public WebElement connectedViaColtElem;
	@FindBy(xpath = "//div//span[contains(text(),'Could not find Onnet and Automated Nearnet Connectivity')]") public WebElement connectionNotFound;
	@FindBy(xpath = "//div//span[contains(text(),'Datacenter Connected via Colt')]") public WebElement dataCenterConnectedElem;
	@FindBy(xpath = "//div//span[contains(text(),'Automated Nearnet')]") public WebElement automatedNearnetElem;
	@FindBy(xpath = "//td[text()='Next']") public WebElement nextBtn;
	
//	Onnet Dual Entry entries under faetures link
	@FindBy(xpath = "//span[text()='Features']") public WebElement featuresLnk;
	@FindBy(xpath = "//select[@id='exploreCheckboxActionsEndDE']") public WebElement onnetManualRequestLst;
	@FindBy(xpath = "//input[@value='REVALIDATE']") public WebElement reValidateRBtn;
	@FindBy(xpath = "//input[@value='RENEGOTIATE']") public WebElement reNegotiateRBtn;	
	@FindBy(xpath = "//span[text()='Revalidate']") public WebElement exploreReValidateBtn;	
	@FindBy(xpath = "//span[text()='Renegotiate']") public WebElement exploreReNegotiateBtn;
//	A-End Features
	@FindBy(xpath = "//input[@name='outsideBusinessHoursInstallationAEnd']") public WebElement outsideBusinessHoursInstallationAEndCbx;	
	@FindBy(xpath = "//input[@name='longLiningAEnd']") public WebElement longLiningAEndCbx;	
	@FindBy(xpath = "//input[@name='internalCablingAEnd']") public WebElement internalCablingAEndCbx;
//	B_End Features
	@FindBy(xpath = "//input[@name='outsideBusinessHoursInstallationBEnd']") public WebElement outsideBusinessHoursInstallationBEndCbx;	
	@FindBy(xpath = "//input[@name='longLiningBEnd']") public WebElement longLiningBEndCbx;	
	@FindBy(xpath = "//input[@name='internalCablingBEnd']") public WebElement internalCablingBEndCbx;	
	
	
//	Offnet and Nearnet Process
	@FindBy(xpath = "//label[@for='offNetDSLCCRequestAEnd_Leased_Line']") public WebElement offnetCheckAEndBtn;
	@FindBy(xpath = "//label[@for='offNetDSLCCRequestAEnd_MANUAL_ENGAGEMENT']") public WebElement manualEngagementAEndBtn;
	@FindBy(xpath = "//label[@for='offNetDSLCCRequestAEnd_DSL']") public WebElement dslCheckAEndBtn;
	@FindBy(xpath = "//label[@for='offNetDSLCCRequestBEnd_DSL']") public WebElement dslCheckBEndBtn;
	@FindBy(xpath = "//select[@id='exploreCheckboxActionsEndA']") public WebElement exploreActionsAEndLst;
	@FindBy(xpath = "//label[@for='offNetDSLCCRequestBEnd_Leased_Line']") public WebElement offnetCheckBEndBtn;
	@FindBy(xpath = "//label[@for='offNetDSLCCRequestBEnd_MANUAL_ENGAGEMENT']") public WebElement manualEngagementBEndBtn;
	@FindBy(xpath = "//select[@id='exploreCheckboxActionsEndB']") public WebElement exploreActionsBEndLst;
	@FindBy(xpath = "//span[text()='Priority']//preceding-sibling::input") public WebElement priorityTxb;
	@FindBy(xpath = "//span[@id='dualEntryStandard']//child::span[contains(@class,'CheckBox')]") public WebElement dualEntryCbx;
	@FindBy(xpath = "//span[text()='Get Quote']") public WebElement getQuoteBtn;
	@FindBy(xpath = "//div[contains(@id,'servicesDialog_')]//..//div[contains(@class,'dgrid-expando-icon ui-icon ui-icon-triangle')]") public WebElement expandArrowExploreIcn;
//	@FindBy(xpath = "//div[contains(@id,'servicesDialog_')]//..//div[contains(@class,'grid-expand')]") public WebElement expandArrowExploreIcn;	
	@FindBy(xpath = "//div[contains(@id,'servicesDialog_')]//..//div[contains(@class,'grid-expand')]//parent::td//following-sibling::td") public WebElement exploreRequestIDTdElem;
	@FindBy(xpath = "//span[contains(text(),'Request ID:')]") public WebElement requestIDElem;
	@FindBy(xpath = "//td[text()='CLOSE']") public WebElement exploreCloseBtn;
	@FindBy(xpath = "//span[text()='Submit']") public WebElement exploreSubmitBtn;
	@FindBy(xpath = "descendant::div[@id='offNetA']//input[@name='connRadioVarNameAEnd'][1]") public WebElement automatedOffnetDefAEndRadioBtn;
	@FindBy(xpath = "descendant::div[@id='offNetB']//input[@name='connRadioVarNameBEnd'][1]") public WebElement automatedOffnetDefBEndRadioBtn;
	@FindBy(xpath = "descendant::div[@id='dslNetA']//input[@name='connRadioVarNameAEnd'][1]") public WebElement automatedDSLDefAEndRadioBtn;
	@FindBy(xpath = "descendant::div[@id='dslNetB']//input[@name='connRadioVarNameBEnd'][1]") public WebElement automatedDSLDefBEndRadioBtn;
	@FindBy(xpath = "//td[text()='Waiting for 3rd Party']/following-sibling::td//*[contains(@name,'connRadioVarNameAEnd')]") public WebElement etherNetSpokeManualOffnet;
	@FindBy(xpath = "(//td[text()='ACTUAL COST'])[2]/following-sibling::td//*[contains(@name,'connRadioVarNameAEnd')]") public WebElement etherNetSpokeRevalidateOffnet;
	@FindBy(xpath = "//span[text()='Connection Type']//preceding-sibling::input") public WebElement connectionType;
	@FindBy(xpath = "//td[text()='3rd Party DSL']//preceding-sibling::td//input[contains(@id,'radioAEnd')]") public WebElement etherNetSpokeDSLOffnet;
	@FindBy(xpath = "//select[@id='incrementalReplacement_capex_interface']") public WebElement offnetWaveincrementalCapex;
	@FindBy(xpath = "//select[@id='incrementalReplacement_opex_interface']") public WebElement offnetWaveincrementalOpex;
	@FindBy(xpath = "//select[@id='frequency_interface']") public WebElement offnetWaveFrequency;
	
//	Offnet IP Access
	@FindBy(xpath = "//td[@id='offnetcheckAEnd']") public WebElement ipOffnetCheckAEndBtn;
	@FindBy(xpath = "//td[@id='manualEngagementAEnd']") public WebElement ipManualEngagementAEndBtn;
	@FindBy(xpath = "//select[@id='exploreManualEngagementCheckboxActionsAEnd']") public WebElement ipExploreActionsAEndLst;
	@FindBy(xpath = "//input[@id='closeButtonForAEnd_CLOSE']") public WebElement ipSupplierOnnetClose ;
	@FindBy(xpath = "//td[@id='dslcheckAEnd']") public WebElement ipDSLCheck ;
	

//	Partial Save Button
	@FindBy(xpath = "//input[@name='partialSave_SiteAddress']") public WebElement partialSaveAddressCbx;
	@FindBy(xpath = "//input[@name='markForPartialSaveSysConfig']") public WebElement ipPartialSaveAddressCbx;
	@FindBy(xpath = "//input[@name='partialSave_SiteDetails']") public WebElement partialSaveSiteCbx;
	@FindBy(xpath = "//input[@name='partialSave_Features']") public WebElement partialSaveFeaturesCbx;
	@FindBy(xpath = "//input[@name='partialSave_AdditionalTab']") public WebElement partialSaveAdditionalCbx;
	@FindBy(xpath = "//span[text()='Site Details']") public WebElement siteDetailsLnk;
	
//	Ethernet Line Override objects
	@FindBy(xpath = "//input[@name='overrideToULLAEnd']") public WebElement overrideToULLAEndCbx;
	@FindBy(xpath = "//textarea[@id='overrideReasonAEnd']") public WebElement overrideReasonAEndTxb;
	@FindBy(xpath = "//input[@name='overrideToULLBEnd']") public WebElement overrideToULLBEndCbx;
	@FindBy(xpath = "//textarea[@id='overrideReasonBEnd']") public WebElement overrideReasonBEndTxb;
	@FindBy(xpath = "//input[@name='overrideToULLAEnd']") public WebElement overrideToOnnetAEndCbx;
	@FindBy(xpath = "//input[@name='overrideToULLBEnd']") public WebElement overrideToOnnetBEndCbx;
	@FindBy(xpath = "//a[text()='Update']") public WebElement updateBtn;
	@FindBy(xpath = "//input[contains(@id,'InputtedIsCorrectAEnd')]") public WebElement AEndCorrectCbx;
	@FindBy(xpath = "//input[contains(@id,'InputtedIsCorrectBEnd')]") public WebElement BEndCorrectCbx;	
	
//	CPE Configuration
	@FindBy(xpath = "//span[text()='CPE CONFIGURATION']") public WebElement cpeConfigurationLnk;
	@FindBy(xpath = "//select[@id='connectionType']") public WebElement connectionTypeLst;
//	@FindBy(xpath = "//select[@id='serviceBandwidth']") public WebElement serviceBandwidthLst;
	@FindBy(xpath = "//select[contains(@id,'serviceBandwidth')]") public WebElement serviceBandwidthLst;
	@FindBy(xpath = "//select[@id='managedCPE']") public WebElement managedCPELst;
	@FindBy(xpath = "//select[@id='cPESolutionType']") public WebElement cpeSolutionTypeLst;
	@FindBy(xpath = "//label[text()='CPE Model']") public WebElement cpeModelLabel;
	@FindBy(xpath = "//select[contains(@id,'interface')]") public WebElement interfaceLst;
	@FindBy(xpath = "//select[contains(@id,'resiliencyServiceLevel')]") public WebElement reslianceType;
	@FindBy(xpath = "//select[contains(@id,'contractTermInYears')]") public WebElement contractYear;
	
	
//  Ip Access
	@FindBy(xpath = "//span[text()='L3 Resilience']") public WebElement ipL3ResilienceLnk;
	@FindBy(xpath = "//span[text()='Diversity']") public WebElement ipDiversityLnk;
	@FindBy(xpath = "//select[contains(@id,'contractTermAEnd')]") public WebElement ipContractTermLst;
	@FindBy(xpath = "//select[contains(@id,'billingTypeAEnd')]") public WebElement ipBillingTypeLst;
	@FindBy(xpath = "//select[contains(@id,'layer2ResilienceAEnd')]") public WebElement ipL2ReilienceLst;
	@FindBy(xpath = "//a[@id='next_model']") public WebElement cpeIPAcessNextBtn;
	@FindBy(xpath = "//div[@class='crumb active']//a/span[contains(text(),'Primary Connection')]") public WebElement ipPrimaryConnectionLnk;
	@FindBy(xpath = "//td[text()='APPROVED']") public WebElement ipApprovedTxt;
	@FindBy(xpath = "//span[text()='Site Addons']") public WebElement ipSiteAddonsLnk;
	@FindBy(xpath = "//span[text()='Service Addons']//parent::a//parent::div") public WebElement ipServiceAddonsLnk;
	@FindBy(xpath = "//select[@name='committedBandwidth']") public WebElement ipcomittedBandwidthLnk;
	@FindBy(xpath = "//select[@name='maximumBandwidth']") public WebElement ipmaximumBandwidthLnk;	
	
//  Ip Access Router Type Configuration
	@FindBy(xpath = "//select[contains(@id,'selectRouterTypeAEnd')]") public WebElement ipIPRouterTypeLst;
	@FindBy(xpath = "//select[@id='selectInterfaceTypeAEnd']") public WebElement ipSelectInterFaceLst;
	@FindBy(xpath = "//input[@name='natAEnd']") public WebElement ipNATCbx;
	@FindBy(xpath = "//input[@id='coltCPEIDAEnd']") public WebElement ipColtIdTbx;
	@FindBy(xpath = "//input[@id='costOfCPEAEnd']") public WebElement ipColtCostTbx;
	@FindBy(xpath = "//input[@id='routerModelAEnd']") public WebElement ipColtRouterModelLst;

//  IP Access BGP4 Type
	@FindBy(xpath = "//input[@name='bGP4FeedAEnd']") public WebElement ipBGP4FeedCbx;
	@FindBy(xpath = "//select[@id='bGP4FeedTypeAEnd']") public WebElement ipBGP4FeedTypeLst;
	@FindBy(xpath = "//select[@id='typeOfASAEnd']") public WebElement cpeBGPTypeAsEndLst;

//  IP Access SMTP Type
	@FindBy(xpath = "//input[@name='smtpAEnd']") public WebElement ipSMTPFeedCbx;
	
//  PRESENTATION INTERFACE
	@FindBy(xpath = "//select[@id='servicePresentationInterfaceType']") public WebElement ipServicePresenattionLst;
	@FindBy(xpath = "//select[@id='interfaceAEnd']") public WebElement ipinterfaceAEndLst;
	@FindBy(xpath = "//select[@id='connectorAEnd']") public WebElement ipConnectorLst;
	@FindBy(xpath = "//span[text()='PRESENTATION INTERFACE']") public WebElement ipPresentationInterFaceLst;

//  IP ADDRESSING
	@FindBy(xpath = "//select[@id='iPAddressingFormatAEnd']") public WebElement ipAddressingFormatLst;
	@FindBy(xpath = "//select[@id='iPv4AddressingTypeAEnd']") public WebElement ipAddressingTypeAEndLst;
	@FindBy(xpath = "//select[@id='numberOfIPv4Addresses']") public WebElement ipAddressingIPv4Lst;
	@FindBy(xpath = "//input[@name='carrierHotelCrossConnectAEnd']") public WebElement ipCarrierHotelLst;
	
//  L3 Resiliency Id
	@FindBy(xpath = "//select[@id='layer3Resiliency']") public WebElement ipL3ResilianceLnk;
	@FindBy(xpath = "//select[contains(@id,'backupBandwidth')]") public WebElement ipBackUpBandWidthLst;
	@FindBy(xpath = "//span[text()='IP Features']") public WebElement ipFeaturesLnk;
	
//  Diversity
	@FindBy(xpath = "//input[@name='diversity']") public WebElement ipDiversitySelector;
	
//	Service Adons
	@FindBy(xpath = "//input[@id='customerRequiredDate']") public WebElement ipCustomerRequired;
	
// 	Bespoke Feature
	@FindBy(xpath = "//input[@name='bespokeFeatureRequired']") public WebElement ipBeSpokeFeature;
	
// 	Ip Access Additional Product Data Primary
	@FindBy(xpath = "//select[@name='siteCabinetType']") public WebElement ipCabinetType;
	@FindBy(xpath = "//input[@id='siteCabinetID']") public WebElement ipCabinetID;
	@FindBy(xpath = "//select[@id='siteAccessTechnology']") public WebElement ipSiteAccessTechnology;
	@FindBy(xpath = "//label[contains(@id,'voiceBundledService_label')]") public WebElement voiceBundledServiceElem;
	
// 	Ip Access Additional Product Data Secondary
	@FindBy(xpath = "//select[@id='routerTechnology']") public WebElement ipRouterTechnology;
	@FindBy(xpath = "//select[@id='existingCapacityLeadTimeAEnd']") public WebElement ipexistingCapacityLeadTimeLst;
	@FindBy(xpath = "//input[@id='firstName']") public WebElement ipFirstName;
	@FindBy(xpath = "//input[@id='lastName']") public WebElement ipLastName;
	@FindBy(xpath = "//input[@id='contactNumber']") public WebElement ipContactNumber;
	@FindBy(xpath = "//input[@id='mobileNumber']") public WebElement ipMobileNumber;
	@FindBy(xpath = "//input[@id='email']") public WebElement ipEmail;
	@FindBy(xpath = "//input[@id='faxNumber']") public WebElement ipFaxNumber;
	@FindBy(xpath = "//input[@name='voiceBundledService']") public WebElement ipVoiceBundleService;
	@FindBy(xpath = "//span[text()='Secondary Site']") public WebElement ipSecondarySite;
	
		
//	Additional Product Data
	@FindBy(xpath = "//span[text()='Additional Product Data']") public WebElement addtionalProductDataLnk;
	@FindBy(xpath = "//select[@id='existingCapacityLeadTime_qto']") public WebElement existingCapacityLeadTimeLst;
	@FindBy(xpath = "//label[contains(text(),'Existing Capacity Lead Time')]']") public WebElement existingCapacityLeadTimeLabel;
	
//	AEnd Details Entry
	
	@FindBy(xpath = "//select[@name='siteType']") public WebElement siteTypeLst;	
	@FindBy(xpath = "//input[contains(@name,'siteCabinetIDAEndPrimary')]") public WebElement cabinetIDATxb;
	@FindBy(xpath = "//select[contains(@name,'siteCabinetTypeAEndPrimary')]") public WebElement cabinetTypeALst;
	@FindBy(xpath = "//select[@name='siteAccessTechnologyPrimaru_Qto']") public WebElement accessTechnologyALst;
	@FindBy(xpath = "//select[contains(@name,'siteCustomerSitePOPStatusPrimary')]") public WebElement customerPopStatusALst;
	@FindBy(xpath = "//input[@name='siteIdAEnd_Qto']") public WebElement siteIDATxb;
	@FindBy(xpath = "//select[contains(@name,'sitePresentationInterfacePrimary')]") public WebElement presentationInterfaceALst;
	@FindBy(xpath = "//select[@name='siteConnectorType']") public WebElement connectorTypeALst;
	@FindBy(xpath = "//select[@name='sitePortRole']") public WebElement portRoleALst;
	
//	BEnd Details Entry
	@FindBy(xpath = "//input[contains(@name,'siteCabinetIDBEndPrimary')]") public WebElement cabinetIDBTxb;
	@FindBy(xpath = "//select[contains(@name,'siteCabinetTypeBEndPrimary')]") public WebElement cabinetTypeBLst;
	@FindBy(xpath = "//select[@name='siteAccessTechnologyBendPrimary_Qto']") public WebElement accessTechnologyBLst;
	@FindBy(xpath = "//select[contains(@name,'siteCustomerSitePOPStatusBEndPrimary')]") public WebElement customerPopStatusBLst;
	@FindBy(xpath = "//input[@name='siteIdBPrimary_Qto']") public WebElement siteIDBTxb;
	@FindBy(xpath = "//select[contains(@name,'sitePresentationInterfaceBEndPrimary')]") public WebElement presentationInterfaceBLst;
	@FindBy(xpath = "//select[contains(@name,'siteConnectorTypeBEndPrimary')]") public WebElement connectorTypeBLst;
	@FindBy(xpath = "//select[contains(@name,'sitePortRoleBEndPrimary')]") public WebElement portRoleBLst;
	
//	Saving the Quote
	@FindBy(xpath = "//a[text()='Save to Quote']") public WebElement saveToQuoteBtn;
	@FindBy(xpath = "//div[contains(@class,'error-content')]") public WebElement errorContentCPQ;
	@FindBy(xpath = "//oj-button[@title='Reconfigure']") public WebElement reConfigureBtn;
	
//	Portfolio Pricing
	@FindBy(xpath = "//span[text()='Engage Portfolio Pricing']") public WebElement engagePortfolioPricingBtn;
	@FindBy(xpath = "//div[contains(@id,'portfolioTeamAssignment')]//child::a") public WebElement portfolioAssignmentLst;
	@FindBy(xpath = "//ul[contains(@id,'portfolioTeamAssignment')]") public WebElement portfolioAssignmentSubLst;
	@FindBy(xpath = "//span[text()='Assign Quote']") public WebElement assignQuoteBtn;
	@FindBy(xpath = "//span[text()='Refresh All Prices']") public WebElement refreshAllPricesBtn;
	@FindBy(xpath = "//span[text()='Send To Sales']") public WebElement sendToSalesBtn;	
	
//	Error Message CPQ
	@FindBy(xpath = "//div[contains(@class,'message-summary message-error')]") public WebElement cpqSummaryErrorMsgElem;
	@FindBy(xpath = "//div[contains(@class,'message-detail')]") public WebElement cpqErrorMsgElem;
	
//	Discounting
	@FindBy(xpath = "//span[text()='Quote']") public WebElement quoteLnk;
	@FindBy(xpath = "//input[@id='discountNRCPerc_t']") public WebElement basePriceNRCDiscountTxb;	
	@FindBy(xpath = "//input[@id='discountMRCPerc_t']") public WebElement basePriceMRCDiscountTxb;	
	@FindBy(xpath = "descendant::span[text()='Calculate Discount'][1]") public WebElement calculateDiscountBtn;	
	
//	Copy Quotes
	@FindBy(xpath = "//span[text()='Copy Line Items']//parent::span") public WebElement copyLineItemsBtn;
	@FindBy(xpath = "//input[contains(@aria-label,'Number Of Copies')]") public WebElement numberOfCopiesTxb;	
	@FindBy(xpath = "//div[@id='copy-lines-form']//span[text()='OK']//parent::span") public WebElement copyPromptOKBtn;
	
//	Option Quote
	@FindBy(xpath = "//div[@aria-labelledby='optionsQuote_t-label']") public WebElement optionQuoteRBtn;
	@FindBy(xpath = "//input[@id='numberOfOptions_t']") public WebElement noOfOptionsTxb;
	@FindBy(xpath = "//div[contains(@id,'selectPrimaryOptions')]//child::a") public WebElement selectPrimaryOptionsLst;
	@FindBy(xpath = "//ul[contains(@id,'selectPrimaryOptions')]") public WebElement selectPrimaryOptionsSubLst;
	@FindBy(xpath = "//input[@value='Discount']//parent::span") public WebElement discountCbx;
	@FindBy(xpath = "//span[text()='Calculate Discount']") public WebElement calculateLineLevelDiscountBtn;
	
//	Commercial Approval
	@FindBy(xpath = "//span[text()='Commercial Approval']//parent::a") public WebElement commercialValidationLnk;
//	@FindBy(xpath = "//a[contains(text(),'Approval')]//parent::li") public WebElement commercialValidationLnk;
	@FindBy(xpath = "//a[text()='Approval']") public WebElement approvalLnk;
	@FindBy(xpath = "//div[contains(text(),'VP Sales -')]") public WebElement VPSalesTxtElem;
	@FindBy(xpath = "//div[@id='approval-comment']//span[text()='Submit']") public WebElement VPSalesSubmitBtn;
	@FindBy(xpath = "//button[@title='Approve']") public WebElement approveDiscountBtn;
	@FindBy(xpath = "//button[@name='submit_to_approval']") public WebElement submitToApprovalBtn;
	@FindBy(xpath = "//a[@title='General Information']") public WebElement generalInformationLnk;
	
//	Legal and Technical Contact Info
	@FindBy(xpath = "//label[text()='Legal Contact Details']") public WebElement legalContactLnk;
	@FindBy(xpath = "//label[contains(@id,'technicalContactsDetails')]") public WebElement technicalContactLnk;
	@FindBy(xpath = "//input[@name='searchEmail']") public WebElement emailContactTxb;
	@FindBy(xpath = "//button[@id='searchButton']") public WebElement searchContactBtn;
	@FindBy(xpath = "//table[@id='contactList']//..//i[1]") public WebElement pickDefaultContactCbx;
	@FindBy(xpath = "//div[contains(@id,'legalGetContacts')]//child::input[@value='Get Contact Details']") public WebElement legalGetContactBtn;
	@FindBy(xpath = "//div[contains(@id,'technicalGetContacts')]//child::input[@value='Get Contact Details']") public WebElement technicalGetContactBtn;
	
//	Proactive Contact
	@FindBy(xpath = "//span[text()='Service Information']//parent::a") public WebElement serviceInformationLnk;
	@FindBy(xpath = "//input[contains(@id,'proactiveManagementContact_Qto_true')]") public WebElement proactiveContactCbx;
	@FindBy(xpath = "//select[@id='title_qto']") public WebElement pacTitleLst;
	@FindBy(xpath = "//input[@id='firstName_qto']") public WebElement pacFirstNameTxb;
	@FindBy(xpath = "//input[@id='lastName_Qto']") public WebElement pacLastNameTxb;
	@FindBy(xpath = "//input[@id='contactNumber_Qto']") public WebElement pacContactNumberTxb;
	@FindBy(xpath = "//input[@id='mobileNumber_qto']") public WebElement pacMobileNumberTxb;
	@FindBy(xpath = "//input[@id='email_Qto']") public WebElement pacEmailTxb;
	@FindBy(xpath = "//select[@id='correspondanceLanguage_qto']") public WebElement pacLanguageLst;
	@FindBy(xpath = "//select[@id='preferredContactMethod_qto']") public WebElement pacPreferredContactLst;
	
	
//	Technical Approval
	@FindBy(xpath = "//a[@title='Technical Approval']") public WebElement technicalApprovalLnk;
	@FindBy(xpath = "//input[@value='readyForTechnicalApproval']//parent::span") public WebElement readyForTechApprovalCbx;
	@FindBy(xpath = "//input[@value='customerCommittedInitiateFastSignClosureOfTheOpportunity']//parent::span") public WebElement fastSignInitiateCbx;
	@FindBy(xpath = "//button[@name='submit_for_technical_approval']") public WebElement submitTechApprovalBtn;
	@FindBy(xpath = "//div[@id='notificationsAndMessages_t']") public WebElement notificationBarCPQ;
	@FindBy(xpath = "//span[text()='Submit to CST Approval']") public WebElement submitCSTApprovalBtn;
	@FindBy(xpath = "//button[@name='add_internal_note']") public WebElement addInternalNoteBtn;
	@FindBy(xpath = "//textarea[contains(@id,'qtonotes')]") public WebElement quoteNotesTxb;
	@FindBy(xpath = "//span[text()='Add']") public WebElement addNotesBtn;
	@FindBy(xpath = "//span[text()='Approve']") public WebElement approveCSTBtn;
	@FindBy(xpath = "//div[contains(@id,'choice-selectOptionsTechApproval')]//child::a") public WebElement selectOptionsTechValLst;
	@FindBy(xpath = "//ul[contains(@id,'selectOptionsTechApproval')]") public WebElement selectOptionsTechValSubLst;
	@FindBy(xpath = "//span[text()='Proceed To Quote']") public WebElement proceedToQuoteBtn;
	
//	FastSign Process
	@FindBy(xpath = "//span[text()='Close Fast Sign Process']") public WebElement closeFastSignBtn;
	@FindBy(xpath = "//div[contains(@id,'reason')]//child::a") public WebElement fastSignReasonLst;
	@FindBy(xpath = "//ul[contains(@id,'reason')]") public WebElement fastSignReasonSubLst;
	@FindBy(xpath = "//input[@id='customerCommitmentProof_t']") public WebElement customerProofUploadTxb;
	
//	top page
	@FindBy(xpath = "//div[@id='requestSupportAttribute']") public WebElement topPageScrollElem;
	
//	switch to proxy user
	@FindBy(xpath = "//img[@title='Admin']") public WebElement adminBtn;
	@FindBy(xpath = "//a[text()='Internal Users']") public WebElement internalUsersLnk;
	@FindBy(xpath = "//a[contains(text(),'Manager')]") public WebElement orderToQuoteManagerLnk;
	@FindBy(xpath = "//img[@title='Proxy Logout']") public WebElement proxyLogoutBtn;
	@FindBy(xpath = "//a[text()='Transactions']") public WebElement transactionsLnk;
	@FindBy(xpath = "descendant::a[text()='Copy'][1]") public WebElement copyTransactionsLnk;

	@FindBy(xpath = "//img[@title='Log out']//parent::a") public WebElement cpqLogoutBtn;
	@FindBy(xpath = "//img[@alt='Proxy Logout']") public WebElement proxyLogoutRFSBtn;
	@FindBy(xpath = "//img[@title='Log out']") public WebElement cpqLogoutRFSBtn;
	@FindBy(xpath = "//a[@id='new_transaction']") public WebElement newTransactionBtn;
	@FindBy(xpath = "//button[contains(text(),'Track Tickets')]") public WebElement tractTicketsBtn;

//	line item grid table
	@FindBy(xpath = "//table[@aria-label='Line Item Grid']") public WebElement lineItemGridTable;
	@FindBy(xpath = "//input[@type='text']") public WebElement lineItemGridEditBox;
	
//	Billing Information
	@FindBy(xpath = "//input[@value='BCN' and @type='checkbox']//parent::span") public WebElement billingInfoCbx;
	@FindBy(xpath = "//input[@value='Order' and @type='checkbox']//parent::span") public WebElement orderDetailsCbx;
	@FindBy(xpath = "//button[@name='billing_information']") public WebElement billingInformationBtn;
	@FindBy(xpath = "//button[@name='apply']") public WebElement billingLookAapplyBtn;
	
//	Generate Proposal
	@FindBy(xpath = "//span[text()='Customer Signature']//parent::a") public WebElement customerSignatureLnk;
	@FindBy(xpath = "//span[text()='Customer Signature']") public WebElement generateProposalLnk;
	@FindBy(xpath = "//div[contains(@id,'select-choice-language')]//child::a") public WebElement proposalLanguageSelectedLst;
	@FindBy(xpath = "//div[contains(@data-oj-containerid,'language')]") public WebElement proposalLanguageLst;
	@FindBy(xpath = "//textarea[contains(@id,'proposalNotes')]") public WebElement proposalNotesTxb;
	@FindBy(xpath = "//span[text()='Generate Proposal']") public WebElement generateProposalBtn;
	@FindBy(xpath = "//div[contains(@id,'showMessageForProposalGeneratedDate_t')]") public WebElement verifyProposalConfElem;
	@FindBy(xpath = "//div[contains(@id,'select-choice-workflow_t')]//child::a") public WebElement selectWorkflowLst;
	@FindBy(xpath = "//div[contains(@data-oj-containerid,'workflow')]") public WebElement workflowLst;
	@FindBy(xpath = "//input[contains(@id,'toRecepient')]") public WebElement toRecepientLst;
	@FindBy(xpath = "//span[text()='Send Proposal']") public WebElement sendProposalBtn;
	@FindBy(xpath = "//span[contains(@id,'file_attachment')]//a") public WebElement fileAttachmentLnk;
	
//	Save C4C
	@FindBy(xpath = "//descendant::button[@name='save'][2]") public WebElement saveCPQBtn;
	@FindBy(xpath = "//a[@id='save']") public WebElement childSaveCPQBtn;
	@FindBy(xpath = "descendant::span[text()='Save'][3]") public WebElement pLSaveBtn;
	
	
//	Contact Information
	@FindBy(xpath = "//span[text()='Contact Information']") public WebElement contactInformationLnk;
	@FindBy(xpath = "//table[@aria-label='Additional Quote Information']") public WebElement additonalInfoTable;
	@FindBy(xpath = "//input[@value='copyContactDetailsFromLegalOrTechnicalOrdeingContact']//parent::span") public WebElement copyContactDetailsRadiobtn;
	@FindBy(xpath = "//input[@value='Ordering Contact']") public WebElement copyOrderingDetailsRadiobtn;
	@FindBy(xpath = "//input[@value='siteContactAEnd']") public WebElement siteContactRadiobtn;
	@FindBy(xpath = "//input[@value='technicalContactAEnd']") public WebElement technicalContactAEndRadiobtn;
	@FindBy(xpath = "//input[@value='electricianContactAEnd']") public WebElement electricianContactAEndRadiobtn;
	@FindBy(xpath = "//input[@value='siteContactBEnd']") public WebElement siteContactBEndRadiobtn;
	@FindBy(xpath = "//input[@value='technicalContactBEnd']") public WebElement technicalContactBEndRadiobtn;
	@FindBy(xpath = "//input[@value='electricianContactBEnd']") public WebElement electricianContactBEndRadiobtn;
	@FindBy(xpath = "//span[text()='Copy Contact']") public WebElement copyContactBtn;
	@FindBy(xpath = "//input[@id='selectJET-1']") public WebElement selectQuote;
	
//	Confirm order
	@FindBy(xpath = "//span[text()='Order']") public WebElement OrderLnk;
	@FindBy(xpath = "//div[contains(@id,'choice-quoteAction')]//child::a") public WebElement quoteActionLst;
	@FindBy(xpath = "//ul[contains(@id,'oj-listbox-results-quoteAction')]") public WebElement ActionLst;
	@FindBy(xpath = "//div[contains(@id,'reasonForStatus')]//child::a") public WebElement reasonStatusLst;
	@FindBy(xpath = "//ul[contains(@id,'reasonForStatus')]") public WebElement StatusLst;
	@FindBy(xpath = "//input[contains(@id,'acceptanceDocument')]") public WebElement acceptanceDocumentTxb;
	@FindBy(xpath = "//input[contains(@id,'customerSignedDate')]//parent::div//child::span[@title='Select Date.']") public WebElement customerSignedDateElem;
	@FindBy(xpath = "//a[contains(@class,'-selected')]") public WebElement defaultDateElem;	
	@FindBy(xpath = "//div[contains(@aria-labelledby,'pleaseConfirmThatTheCorrect')]//parent::div") public WebElement confirmAttachmentTgleBtn;
	@FindBy(xpath = "//button[@name='confirm_quote']") public WebElement confirmQuoteBtn;
	@FindBy(xpath = "//span[text()='Create Order']") public WebElement createOrderBtn;
	
	
	public CPQ_Objects() {  
		WebDriver driver = DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
		PageFactory.initElements(driver, this);
	}
}
