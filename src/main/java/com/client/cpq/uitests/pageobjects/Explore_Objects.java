package com.client.cpq.uitests.pageobjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.colt.common.utils.DriverManagerUtil;


public class Explore_Objects {
	
	//Explore Login Page
	@FindBy(xpath = "//input[@id='userId']") public WebElement userNameTxb;	//Username
	@FindBy(xpath = "//input[@name='Ecom_Password']") public WebElement passWordTxb;	//Password
	@FindBy(xpath = "//button[@type='submit']") public WebElement loginBtn;	//Login Button
	
	
//	Explore Main Page
	@FindBy(xpath = "//span[text()='Map View']") public WebElement mapViewBtn;
	@FindBy(xpath = "//span[text()='Fibre Planning Work Queue']") public WebElement fibrePlanningQueueBtn;
	@FindBy(xpath = "//span[text()='ONQT Work Queue']") public WebElement onqtWorkQueueBtn;	
	@FindBy(xpath = "descendant::span[text()='Actions'][3]") public WebElement actionsBtn;	
	@FindBy(xpath = "descendant::tr[@aria-label='Assign Request to Me '][2]") public WebElement assignRequestToMeLnk;
	@FindBy(xpath = "descendant::td[text()='Assign To Me'][2]") public WebElement assignToMeLnk;
	@FindBy(xpath = "//tr[@aria-label='Create Cost ']") public WebElement createCostLnk;	
	
//	Explore Offnet Cost popup
	@FindBy(xpath = "//span[text()='Carrier']//preceding-sibling::input") public WebElement carrierTxb;
	@FindBy(xpath = "//span[text()='Node']//preceding-sibling::input") public WebElement nodeTxb;
	@FindBy(xpath = "descendant::span[text()='MRC']//preceding-sibling::input[1]") public WebElement mrcTxb;
	@FindBy(xpath = "descendant::span[text()='NRC']//preceding-sibling::input[1]") public WebElement nrcTxb;
	@FindBy(xpath = "//span[text()='Term (Years)']//preceding-sibling::input") public WebElement termsTxb;
	@FindBy(xpath = "//span[text()='Currency']//preceding-sibling::input") public WebElement currenctTxb;
	@FindBy(xpath = "//span[text()='Price Type']//preceding-sibling::input") public WebElement priceTypeTxb;
	@FindBy(xpath = "//span[text()='Connector/Interface']//preceding-sibling::input") public WebElement connectorTxb;
	@FindBy(xpath = "//span[text()='Dual Entry from OLO']//preceding-sibling::input") public WebElement dualEntryFromOLOTxb;
	@FindBy(xpath = "//span[text()='Create Cost']") public WebElement createCostBtn;
	@FindBy(xpath = "//span[text()='Approve Quote']") public WebElement approveQuote;
	@FindBy(xpath = "//span[text()='Save Cost']") public WebElement saveCostBtn;
	
//	Nearnet Cost popup
	@FindBy(xpath = "//input[@name='primaryWidgetdistFromNetwork']") public WebElement distanceFromColtTxb;
	@FindBy(xpath = "(//label[text()='Quote Valid Up to (months)']/following::div/input)[2]") public WebElement quoteValidityUpToMonthTxb;
	@FindBy(xpath = "descendant::span[text()='currency']//preceding-sibling::input[1]") public WebElement nearnetCurrencyTxb;
	@FindBy(xpath = "descendant::span[text()='Add Cost'][1]") public WebElement addCostBtn;
	@FindBy(xpath = "//table[@id='dgrid_6-header']") public WebElement nearnetCostHeaderTable;
	@FindBy(xpath = "//div[contains(@id,'dgrid_6-row')]//child::table[@role='presentation']") public WebElement nearnetCostTable;
	@FindBy(xpath = "descendant::span[text()='Send to Sales'][1]") public WebElement exploreSendToSalesBtn;
	@FindBy(xpath = "//label[text()='Status']//following-sibling::span[@data-dojo-attach-point='statusNode']") public WebElement nearnetStatusElem;
	
//	Nearnet Secondary Entries
	@FindBy(xpath = "//input[@name='secondaryWidgetdistFromNetwork']") public WebElement distanceFromColtSecondaryTxb;
	@FindBy(xpath = "descendant::input[@name='secondaryWidgetopEx'][1]") public WebElement quoteValidityUpToMonthSecondaryTxb;
	@FindBy(xpath = "descendant::span[text()='currency'][2]//preceding-sibling::input") public WebElement nearnetCurrencySecondaryTxb;
	@FindBy(xpath = "//span[text()='Dual Entry Dig Cost']") public WebElement dualEntryDigCostTab;
	@FindBy(xpath = "descendant::span[text()='Add Cost'][2]") public WebElement addSecondaryCostBtn;
	@FindBy(xpath = "//table[@id='dgrid_7-header']") public WebElement nearnetCostHeaderSecondaryTable;
	@FindBy(xpath = "//div[contains(@id,'dgrid_7-row')]//child::table[@role='presentation']") public WebElement nearnetCostSecondaryTable;
	@FindBy(xpath = "descendant::span[text()='Add Cost']") public WebElement addHubSecondaryCostBtn;
	
	
//	Explore Logout
	@FindBy(xpath = "//div[@class='inTopBar']//child::span[contains(@class,'menuButton')]") public WebElement menuLnk;
	@FindBy(xpath = "//td[text()='Log Out']") public WebElement exploreLogoutLnk;
	
	
	
	
	
	
	public Explore_Objects() {  
		WebDriver driver = DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
		PageFactory.initElements(driver, this);
	}
}


