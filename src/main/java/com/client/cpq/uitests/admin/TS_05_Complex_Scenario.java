package com.client.cpq.uitests.admin;

import org.testng.annotations.Test;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import org.openqa.selenium.WebDriver;
import org.testng.SkipException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;
import com.qa.colt.extentreport.ExtentManager;
import com.qa.colt.extentreport.ExtentTestManager;
import com.colt.common.utils.dataminer;
import com.colt.common.utils.dateTimeUtil;
import com.colt.common.utils.CPQ_Library;
import com.colt.common.utils.Siebel_Library;
import com.colt.common.utils.DriverManagerUtil;
import com.colt.common.utils.Explore_Library;
import com.colt.common.utils.WebInteractUtil;
import com.client.cpq.uitests.pageobjects.CPQ_Objects;

public class TS_05_Complex_Scenario extends DriverManagerUtil{
	
	protected CPQ_Library CPQ_Library;	
	protected Explore_Library Explore_Library;
	protected Siebel_Library Siebel_Library;
	
	@BeforeMethod
	public void methodsetup() {		
		CPQ_Library = new CPQ_Library();
		Explore_Library = new Explore_Library();
		Siebel_Library = new Siebel_Library();
	}
	
	public void TearDown() {
        //Extentreports log and screenshot operations for failed tests.
        ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
//        DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().close();
        throw new SkipException("Skipping this test");
	}

	@SuppressWarnings("null")
	@Parameters({ "iScript","iSubScript" })
	@Test
	public void FirstTestCase (String iScript,String iSubScript) throws Exception {
		
		//declaring the source values
		String file_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_testdata.xlsx";
		String tfile_name = System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		String Sheet_Name = "TS_05_Complex_Scenario",sResult;	
		String Product_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		String Flow_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Flow_Type");
		String No_Of_Copies = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"No_Of_Copies");
		String UI_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"UI_Type");
		String Transactions_Range = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Transactions_Range");
		String[] Transactions_Split = Transactions_Range.split("\\|");
		String Set_Attempt = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Set_Attempt");
		String Prod_Code = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Prod_Code");
		int i = Integer.parseInt(Set_Attempt);
		String rTransaction = null;
				
//		Calling the Below method to perform C4C Login	
		sResult = CPQ_Library.CPQ_Login(file_name,"SalesUser");
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }	
		
////		calling the below method to navigate to quotes from c4c main page
//		CPQ_Library.navigateQuotesFromHomepage();
//		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
//		
////		calling the below method to search the quote from c4c
//		String Quote_ID = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Quote_ID");
//		sResult = CPQ_Library.searchQuoteC4C(Quote_ID);
//		if (sResult.equalsIgnoreCase("False")){ TearDown(); }	
////		String sProduct = Product_Name.replaceAll("(?!^)([A-Z])", " $1");	
	
//		Calling the Below method to Navigate to Accounts tab
		sResult = CPQ_Library.navigateAccountFromHomepage();
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		Calling the Below method to search an account
		sResult = CPQ_Library.searchAccount(file_name, Sheet_Name, iScript, iSubScript);
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		Calling the Below method to Create and Open the Opportunity
		String Opportunity_ID = CPQ_Library.createOpenOppurtunity(file_name, "Opportunity", iScript, iSubScript);
		if (Opportunity_ID.equalsIgnoreCase("False")){ TearDown(); }
		dataminer.fnsetcolvalue(file_name, Sheet_Name, iScript, iSubScript, "Opportunity_ID", Opportunity_ID);
		
//		Calling the below method to edit the opportunity
		sResult = CPQ_Library.editOpportunity(file_name, "Opportunity", iScript, iSubScript);
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		Calling the below method to add the quote
		String Quote_ID = CPQ_Library.addQuoteInC4C();
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		dataminer.fnsetcolvalue(file_name, Sheet_Name, iScript, iSubScript, "Quote_ID", Quote_ID);
		
//		Capturing Sales Transaction for Configuration
		String Sales_Config_StartTime = dateTimeUtil.fnGetCurrentTime();
			
//		Calling the below method to add the product in CPQ
		sResult = CPQ_Library.addProductCPQ(Product_Name, UI_Type, Transactions_Split[0], "Attempt_"+Integer.toString(i));
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		Calling the below method to enter the site address
		rTransaction = Transactions_Split[1] +"|"+ Transactions_Split[2]+"|"+ Transactions_Split[3];
		sResult = CPQ_Library.siteAddress(file_name, "Product_Configuration", iScript, iSubScript, UI_Type, rTransaction, "Attempt_"+Integer.toString(i));
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		Calling the below method to configure the product
		rTransaction = Transactions_Split[4] +"|"+ Transactions_Split[5]+"|"+ Transactions_Split[6]+"|"+ Transactions_Split[7]+"|"+ Transactions_Split[8]+"|"+ Transactions_Split[9]+"|"+ Transactions_Split[10]+"|"+ Transactions_Split[11]+"|"+ Transactions_Split[12];
		sResult = CPQ_Library.productConfiguration(file_name, "Product_Configuration", iScript, iSubScript, UI_Type, rTransaction, "Attempt_"+Integer.toString(i));
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		Saving the Product Entries
		sResult = CPQ_Library.updateSaveProductCPQ("SaveToQuote", UI_Type, Transactions_Split[13], "Attempt_"+Integer.toString(i));
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
		// Capturing End point of Transaction Capture
		String Sales_Config_EndTime = dateTimeUtil.fnGetCurrentTime();
		
		// Computing Difference between Transactions Capture
		String Sales_Config_TimeDiff = dateTimeUtil.fnGetElapsedTime(Sales_Config_StartTime, Sales_Config_EndTime);
		System.out.println("TimeDiff is "+Sales_Config_TimeDiff);
					
		//Entering the Values to the Data sheet
		dataminer.fnsetTransactionValue(tfile_name, UI_Type, Prod_Code+"_Sales_Config", "Attempt_"+Integer.toString(i), Sales_Config_TimeDiff);
		
//      Wave Product Flow for Manual Offnet
		if(Product_Name.equalsIgnoreCase("Wave")) {
			
//			calling the below method to verify quote stage and status
			sResult = CPQ_Library.verifyQuoteStage("Priced");
			if (sResult.equalsIgnoreCase("False")){ TearDown(); }
				
//			calling the below method to add Legal and Technical Contact Info
			sResult = CPQ_Library.SwitchCPQUser("SE_User", Quote_ID);
			if (sResult.equalsIgnoreCase("False")){ TearDown(); }
				
//			calling the below method reconfigure the product
			sResult = CPQ_Library.clickProductConfigurationBtn(Product_Name);
			if (sResult.equalsIgnoreCase("False")){ TearDown(); }
				
//			calling the below method to configure the product
			rTransaction = Transactions_Split[7]+"|"+ Transactions_Split[8]+"|"+ Transactions_Split[9]+"|"+ Transactions_Split[10]+"|"+ Transactions_Split[11]+"|"+ Transactions_Split[12];
			sResult = CPQ_Library.offnetandNearNetConfigurationWave(file_name, "Product_Configuration", iScript, iSubScript, UI_Type, rTransaction, "Attempt_"+Integer.toString(i));
			if (sResult.equalsIgnoreCase("False")){ TearDown(); }
			
//			calling the below method to update and save the product in cpq
			sResult = CPQ_Library.saveCPQ("Sub", "CST", UI_Type, "", "Attempt_"+Integer.toString(i));
			if (sResult.equalsIgnoreCase("False")){ TearDown(); }
			
		}
		

		if (Flow_Type.equalsIgnoreCase("ManualOffnet")) {
		
	//		calling the below method to verify quote stage and status
			sResult = CPQ_Library.verifyQuoteStage("Waiting for 3rd Party");
			if (sResult.equalsIgnoreCase("False")){ TearDown(); }
			
	//		calling the below method to verify quote stage and status
			sResult = CPQ_Library.logoutCPQ("Main");
			if (sResult.equalsIgnoreCase("False")){ TearDown(); }
			
	//		To Process Offnet Cost in Explore
			sResult = Explore_Library.offnetExploreOperations(file_name, Sheet_Name, iScript, iSubScript);
			if (sResult.equalsIgnoreCase("False")){ TearDown(); }
			
	//		Calling the Below method to perform C4C Login	
			sResult = CPQ_Library.CPQ_Login(file_name,"SalesUser");
			if (sResult.equalsIgnoreCase("False")){ TearDown(); }
			
	//		calling the below method to navigate to quotes from c4c main page
			CPQ_Library.navigateQuotesFromHomepage();
			if (sResult.equalsIgnoreCase("False")){ TearDown(); }
			
	//		calling the below method to search the quote from c4c
			sResult = CPQ_Library.searchQuoteC4C(Quote_ID);
			if (sResult.equalsIgnoreCase("False")){ TearDown(); }	
			
//			Clicking on Re-Configuring the Product
			sResult = CPQ_Library.editProductConfiguration(Product_Name, UI_Type, Transactions_Split[14], 1, "Attempt_"+Integer.toString(i));
			if (sResult.equalsIgnoreCase("False")){ TearDown(); }
			
	//		calling the below method to update and save the product in cpq
			sResult = CPQ_Library.saveCPQ("Sub", "CST", UI_Type, Transactions_Split[15], "Attempt_"+Integer.toString(i));
			if (sResult.equalsIgnoreCase("False")){ TearDown(); }
			
//			if (Product_Name.equalsIgnoreCase("ColtIpAccess")) {
////				Clicking on Re-Configuring the Product
//				sResult = CPQ_Library.editProductConfiguration(Product_Name, UI_Type, Transactions_Split[14], 1, "Attempt_"+Integer.toString(i));
//				if (sResult.equalsIgnoreCase("False")){ TearDown(); }
//				
////				calling the below method to update and save the product in cpq
//				sResult = CPQ_Library.saveCPQ("Sub", "CST", UI_Type, Transactions_Split[15], "Attempt_"+Integer.toString(i));
//				if (sResult.equalsIgnoreCase("False")){ TearDown(); }
//
//			}
			
		}
		
//		calling the below method to verify quote stage and status
		sResult = CPQ_Library.verifyQuoteStage("Priced");
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//	    calling the below method to move the quote status to Commercial Approval
		sResult = CPQ_Library.commercialApproval(UI_Type, Transactions_Split[16], "Attempt_"+Integer.toString(i));
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		calling the below method to verify quote stage and status
		sResult = CPQ_Library.verifyQuoteStage("Commercially Approved");
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }			
	
//		calling the below method to add Legal and Technical Contact Info
		sResult = CPQ_Library.addLegalTechnicalContacts(file_name, "Other_Items", iScript, iSubScript, "LegalContact", UI_Type, Transactions_Split[17], "Attempt_"+Integer.toString(i));
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		calling the below method to add Legal and Technical Contact Info
		sResult = CPQ_Library.addLegalTechnicalContacts(file_name, "Other_Items", iScript, iSubScript, "TechnicalContact", UI_Type, Transactions_Split[18], "Attempt_"+Integer.toString(i));
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		calling the below method to add Legal and Technical Contact Info
		sResult = CPQ_Library.submitForTechnicalApproval(UI_Type, Transactions_Split[19], "Attempt_"+Integer.toString(i));
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		calling the below method to verify quote stage and status
		sResult = CPQ_Library.verifyQuoteStage("SE Technical Review");
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		calling the below method to add Legal and Technical Contact Info
		sResult = CPQ_Library.SwitchCPQUser("SE_User", Quote_ID);
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		Capturing Sales Transaction for Configuration
		String SE_Config_StartTime = dateTimeUtil.fnGetCurrentTime();
		
//		Clicking on Re-Configuring the Product
		sResult = CPQ_Library.editProductConfiguration(Product_Name, UI_Type, Transactions_Split[20], 1, "Attempt_"+Integer.toString(i));
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		Saving the Product Entries	
		rTransaction = Transactions_Split[21] +"|"+ Transactions_Split[22];
		sResult = CPQ_Library.addProactiveContacts(file_name, "Other_Items", iScript, iSubScript, Product_Name, UI_Type, rTransaction, "Attempt_"+Integer.toString(i));
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
		// Capturing End point of Transaction Capture
		String SE_Config_EndTime = dateTimeUtil.fnGetCurrentTime();
		
		// Computing Difference between Transactions Capture
		String SE_Config_TimeDiff = dateTimeUtil.fnGetElapsedTime(SE_Config_StartTime, SE_Config_EndTime);
					
		//Entering the Values to the Data sheet
		dataminer.fnsetTransactionValue(tfile_name, UI_Type, Prod_Code+"_SE_Config", "Attempt_"+Integer.toString(i), SE_Config_TimeDiff);
		
//		calling the below method to verify quote stage and status
		sResult = CPQ_Library.technicalApprovalCPQ(UI_Type, Transactions_Split[23], "Attempt_"+Integer.toString(i));
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }	
		
//		calling the below method to verify quote stage and status
		sResult = CPQ_Library.verifyQuoteStage("CST Technical Review");
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }	
		
//		calling the below method to login to cpq as cst user
		sResult = CPQ_Library.SwitchCPQUser("CST_User", Quote_ID);
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		Capturing Sales Transaction for Configuration
		String CST_Config_StartTime = dateTimeUtil.fnGetCurrentTime();
		
//		Clicking on Re-Configuring the Product
		sResult = CPQ_Library.editProductConfiguration(Product_Name, UI_Type, Transactions_Split[24], 1, "Attempt_"+Integer.toString(i));
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		Additional Product data Entries
		switch (Product_Name) {
		
//			case "EthernetLine": case "Wave":
//				String Time1 = CPQ_Library.addtionalProductdataEntries(file_name, "A_End", iScript, iSubScript, Product_Name);
//				if (Time1.equalsIgnoreCase("False")){ TearDown(); }
//				
//				String Time2 = CPQ_Library.addtionalProductdataEntries(file_name, "B_End", iScript, iSubScript, Product_Name);
//				if (Time2.equalsIgnoreCase("False")){ TearDown(); }
//				 String TimeDiff = BigDecimal.valueOf(Double.parseDouble(Time1) + Double.parseDouble(Time2)).divide(BigDecimal.valueOf(1),3,RoundingMode.HALF_UP).toString();
//				//Entering the Values to the Data sheet
//				dataminer.fnsetTransactionValue(tfile_name, UI_Type, Transactions_Split[25], "Attempt_"+Integer.toString(i), TimeDiff);
//				break;
//				
//			case "EthernetHub": case "EthernetSpoke":
//				TimeDiff = CPQ_Library.addtionalProductdataEntries(file_name, "A_End", iScript, iSubScript, Product_Name);
//				if (TimeDiff.equalsIgnoreCase("False")){ TearDown(); }
//				//Entering the Values to the Data sheet
//				dataminer.fnsetTransactionValue(tfile_name, UI_Type, Transactions_Split[25], "Attempt_"+Integer.toString(i), TimeDiff);
//				break;
				
			case "ColtIpAccess":
				String TimeDiff = CPQ_Library.ipAccessAdditionalProductData(file_name, Sheet_Name, iScript, iSubScript);
				if (TimeDiff.equalsIgnoreCase("False")){ TearDown(); }
				//Entering the Values to the Data sheet
				dataminer.fnsetTransactionValue(tfile_name, UI_Type, Transactions_Split[25], "Attempt_"+Integer.toString(i), TimeDiff);
				break;
		}
		
//		Calling the below method to save the details
		sResult = CPQ_Library.saveCPQ("Sub", "CST", UI_Type, Transactions_Split[26], "Attempt_"+Integer.toString(i));
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
		// Capturing End point of Transaction Capture
		String CST_Config_EndTime = dateTimeUtil.fnGetCurrentTime();
		
		// Computing Difference between Transactions Capture
		String CST_Config_TimeDiff = dateTimeUtil.fnGetElapsedTime(CST_Config_StartTime, CST_Config_EndTime);
					
		//Entering the Values to the Data sheet
		dataminer.fnsetTransactionValue(tfile_name, UI_Type, Prod_Code+"_CST_Config", "Attempt_"+Integer.toString(i), CST_Config_TimeDiff);
		
//		Calling the below method to enter Line item notes
		String sProduct = Product_Name.replaceAll("(?!^)([A-Z])", " $1");
		String Leadtime_Days = dataminer.fngetcolvalue(file_name, "Other_Items", iScript, iSubScript,"Leadtime_Days");
		sResult = CPQ_Library.WebTableCellAction("Product", sProduct, "Lead Time (Days)*","Edit", Leadtime_Days);
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		Calling the below method to save the details
		sResult = CPQ_Library.saveCPQ("Main", "Sales", UI_Type, Transactions_Split[27], "Attempt_"+Integer.toString(i));
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		calling the below method to verify quote stage and status
		sResult = CPQ_Library.cstApprovalCPQ(UI_Type, Transactions_Split[28], "Attempt_"+Integer.toString(i));
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }	
		
//		calling the below method to verify quote stage and status
		sResult = CPQ_Library.logoutCPQ("Main");
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }	
		
//		Calling the Below method to perform C4C Login
		sResult = CPQ_Library.CPQ_Login(file_name,"SalesUser");
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		calling the below method to navigate to quotes from c4c main page
		CPQ_Library.navigateQuotesFromHomepage();
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		calling the below method to search the quote from c4c
		sResult = CPQ_Library.searchQuoteC4C(Quote_ID);
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		calling the below method to verify quote stage and status
		sResult = CPQ_Library.verifyQuoteStage("Approved");
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		Entering BCN values for CPE Solution Service
		rTransaction = Transactions_Split[29] +"|"+ Transactions_Split[30];
		String BCN = dataminer.fngetcolvalue(file_name, "Other_Items", iScript, iSubScript,"BCN").trim();
		sResult = CPQ_Library.addBillingInformation(sProduct, BCN, UI_Type, rTransaction, "Attempt_"+Integer.toString(i));
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		Calling the below method to generate and send proposal
		rTransaction = Transactions_Split[31] +"|"+ Transactions_Split[32] +"|"+ Transactions_Split[33];
		sResult = CPQ_Library.generateSendProposal(file_name, "Other_Items", iScript, iSubScript, UI_Type, rTransaction, "Attempt_"+Integer.toString(i));
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		/*
//		calling the below method to login to cpq as cst user
		sResult = CPQ_Library.contactInfoEntry(file_name, iScript, iSubScript, Product_Name);
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		Calling the below method to confirm the order
		rTransaction = Transactions_Split[34] +"|"+ Transactions_Split[35];
		sResult = CPQ_Library.confirmOrder(file_name, "Other_Items", iScript, iSubScript, UI_Type, rTransaction, "Attempt_"+Integer.toString(i));
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		calling the below method to verify quote stage and status
		sResult = CPQ_Library.verifyQuoteStage("Ordered");
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }
		
//		calling the below method to capture the service order
		sResult = CPQ_Library.captureServiceOrder(file_name, Sheet_Name, iScript, iSubScript);
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }	
		*/
//		calling the below method to verify quote stage and status
		sResult = CPQ_Library.logoutCPQ("Main");
		if (sResult.equalsIgnoreCase("False")){ TearDown(); }	
		
//		Terminating the Execution once it gets ended
        ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
        DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().close();
    	
	}
	
}




