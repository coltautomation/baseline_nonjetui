package com.colt.common.utils;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.Parameters;
import com.colt.common.utils.dataminer;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.qa.colt.extentreport.ExtentTestManager;
import com.client.cpq.uitests.pageobjects.Siebel_Objects;
import com.colt.common.utils.DriverManagerUtil;
import com.colt.common.utils.WebInteractUtil;

public class Siebel_Library {
	
	public WebDriver driver = DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
	
	public Siebel_Objects Siebel_Objects;

	public Siebel_Library() {
		Siebel_Objects = new Siebel_Objects();
	}
	
	public String Siebel_Operations(String file_name,String Sheet_Name,String iScript, String iSubScript, String Service_Order) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : Siebel_Operations
		Purpose     : This method will progress service order entries in siebel application
		Designer    : Vasantharaja C
		Created on  : 3rd August 2020 
		Input       : String file_name,String Sheet_Name,String iScript, String iSubScript, String UserType
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
//		Initializing the Variables
		String Product_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		String sResult;
		
//		Launching URL
		String Siebel_URL = dataminer.fngetconfigvalue(file_name, "Siebel_URL");
		WebInteractUtil.launchWebApp(Siebel_URL);
		
//		Calling the Below method to perform C4C Login	
		sResult = Siebel_Login(file_name);
		if (sResult.equalsIgnoreCase("False")){ return sResult; }
		
//		Splitting the Customer order from service order
		String[] sCustomer_Order = Service_Order.split("/");
		String Customer_Order = sCustomer_Order[1];
		
//		Calling the below function to search the customer order
		sResult = Search_CustomerOrder(Customer_Order, "Yes");
		if (sResult.equalsIgnoreCase("False")){ return sResult; }
		
//		Calling the below method to make the customer order entries
		sResult = CustomerOrder_Entry(file_name, "Customer_Service_Order", iScript, iSubScript);
		if (sResult.equalsIgnoreCase("False")){ return sResult; }
		
//		Calling the below function to search the Service order
		sResult = Search_ServiceOrder(Service_Order, "Yes");
		if (sResult.equalsIgnoreCase("False")){ return sResult; }
		
//		Calling the below function to make service order entries
		sResult = ServiceOrder_Entry(file_name, "Customer_Service_Order", iScript, iSubScript);
		if (sResult.equalsIgnoreCase("False")){ return sResult; }
		
//		fetching the network reference if the product name is Ethernet Hub
		if (Product_Name.equalsIgnoreCase("EthernetHub")) {
			sResult = Fetch_Network_Reference(file_name, Sheet_Name, iScript, iSubScript);
			if (sResult.equalsIgnoreCase("False")){ return sResult; }
		}
		
//		calling the below function to enter middle applet details
		sResult = MiddleApplet_Entry(file_name, "Customer_Service_Order", iScript, iSubScript);
		if (sResult.equalsIgnoreCase("False")){ return sResult; }
		
//		Calling the below function to enter site Operation
		if (!Product_Name.equalsIgnoreCase("EthernetSpoke")) {
			sResult = Site_Operation(file_name, "A_End", iScript, iSubScript);
			if (sResult.equalsIgnoreCase("False")){ return sResult; }
		}
		
		if (!Product_Name.equalsIgnoreCase("EthernetHub") && !Product_Name.equalsIgnoreCase("ColtIpAccess")) {
			sResult = Site_Operation(file_name, "B_End", iScript, iSubScript);
			if (sResult.equalsIgnoreCase("False")){ return sResult; }
		}
		
//		Calling the below method to enter and save order dates
		sResult = OrderDates_Entry("New");
		if (sResult.equalsIgnoreCase("False")){ return sResult; }
		
//		Calling the below method to enter the billing details
		sResult = Billing_Entry(file_name, "Other_Items", iScript, iSubScript);
		if (sResult.equalsIgnoreCase("False")){ return sResult; }	
		
//		Calling the below method to move order status to technical validation stage
		sResult = OrderStatus_Validation("Technical Validation");
		if (sResult.equalsIgnoreCase("False")){ return sResult; }
		
//		Calling the below method to enter techval lookup and existing capacity lead time
		sResult = TechVal_LeadTime_Entry(file_name, "Customer_Service_Order", iScript, iSubScript);
		if (sResult.equalsIgnoreCase("False")){ return sResult; }
		
//		Calling the below method to move order status to technical validation stage
		sResult = OrderStatus_Validation("Delivery");
		if (sResult.equalsIgnoreCase("False")){ return sResult; }	
		
//		Calling the below method to enter and save order dates
		sResult = OrderDates_Entry("Delivery");
		if (sResult.equalsIgnoreCase("False")){ return sResult; }
		
//		Calling the below method to generate circuit reference
		sResult = Generate_CircuitReference();
		if (sResult.equalsIgnoreCase("False")){ return sResult; }
		
//		Calling the below method to enter primary test method
		sResult = Primary_Test_Method_Entry(file_name, "Customer_Service_Order", iScript, iSubScript);
		if (sResult.equalsIgnoreCase("False")){ return sResult; }
		
//		Calling the below method to enter Manual Validation
		sResult = Manual_Validation();
		if (sResult.equalsIgnoreCase("False")){ return sResult; }
		
		return "True";
	}
	
	public String Siebel_Login(String file_name) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : CPQ_Login
		Purpose     : This method will progress the login of C4C application
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : String file_name,String Sheet_Name,String iScript, String iSubScript, String UserType
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
//		Initialize the Variable
		String Siebel_URL = dataminer.fngetconfigvalue(file_name, "Siebel_URL");
		String Siebel_Username = dataminer.fngetconfigvalue(file_name, "Siebel_Username");
		String Siebel_Password = dataminer.fngetconfigvalue(file_name, "Siebel_Password");
		
//		Entering the Credentials
		Waittilljquesryupdated();
		WebInteractUtil.isPresent(Siebel_Objects.userNameTxb,90);
		ExtentTestManager.getTest().log(LogStatus.PASS, "Siebel URL "+Siebel_URL+" launch is successfull");
		System.out.println("C4C URL "+Siebel_URL+" launch is successfull");
		
//		Performing the Login Operations
		WebInteractUtil.sendKeys(Siebel_Objects.userNameTxb, Siebel_Username);
		WebInteractUtil.sendKeys(Siebel_Objects.passWordTxb, Siebel_Password);
		WebInteractUtil.click(Siebel_Objects.loginBtn);
		Waittilljquesryupdated();
		
//		Verify Login Successsfull or not
		if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.editLayoutBtn, 120)) {
			Waittilljquesryupdated();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Login to Siebel application is successfull");
			System.out.println("Login to Siebel application is successfull");
		}
		else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Login to Siebel application Failed");
			System.out.println("Login to Siebel application Failed");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
		
	}
	
	public void Waittilljquesryupdated() throws InterruptedException, SocketTimeoutException {
		/*----------------------------------------------------------------------
		Method Name : Waittilljquesryupdated
		Purpose     : This method will return whether the driver steady state is completed ornot
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : None
		 ----------------------------------------------------------------------*/ 
//		JavascriptExecutor js = null;
		boolean Status = false;
		Thread.sleep(1000);
		JavascriptExecutor js = (JavascriptExecutor) DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
		for (int i=1; i<10; i++) {
			if (js == null) {
				Thread.sleep(250);
				js = (JavascriptExecutor) DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
				continue;
			} else {
				try {
					while(!(js.executeScript("return document.readyState").equals("complete")))
					{
						Thread.sleep(500);
					}
					Status = true;
					if (Status = true) { Thread.sleep(250); break; }
				} catch (Exception e) {
					continue;
				}
			}
		}	
	}
	
	public String Search_CustomerOrder(String Customer_Order, String sClick) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : Search_CustomerOrder
		Purpose     : This method will Search the Customer Order in Siebel
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : String Customer_Order
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		
		Waittilljquesryupdated();
		
//		Entering the Credentials
		if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.customerOrderTab,90)) {
			WebInteractUtil.click(Siebel_Objects.customerOrderTab);
			Waittilljquesryupdated();
			WebInteractUtil.sendKeys(Siebel_Objects.siebelOrderTxb,Customer_Order);
			WebInteractUtil.click(Siebel_Objects.goButton);
			Thread.sleep(2000);
			Waittilljquesryupdated();
			String sXpath = "//a[text()='"+Customer_Order+"']";
			if (WebInteractUtil.waitForElementToBeVisible(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(sXpath)), 60)) {
				if (sClick.equalsIgnoreCase("Yes")) {
					WebInteractUtil.click(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(sXpath)));
					Waittilljquesryupdated();
					WebInteractUtil.isPresent(Siebel_Objects.newServiceOrderBtn,60);
					Waittilljquesryupdated();
					WebInteractUtil.scrollIntoTop();
				}
				ExtentTestManager.getTest().log(LogStatus.PASS, "Search Customer Order "+Customer_Order+" in Siebel is successfull");
				System.out.println("Search Customer Order "+Customer_Order+" in Siebel is successfull");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Search Customer Order "+Customer_Order+" in Siebel is not successfull, Please Verify");
				System.out.println("Search Customer Order "+Customer_Order+" in Siebel is not successfull, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "customerOrderTab is not Visible, Please Verify");
			System.out.println("customerOrderTab is not Visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
			
		
		return "True";
	}
	
	public String CustomerOrder_Entry(String file_name,String Sheet_Name,String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : CustomerOrder_Entry
		Purpose     : This method will progress the Entry of Customer Order
		Designer    : Vasantharaja C
		Created on  : 28th May 2020 
		Input       : String file_name,String Sheet_Name,String iScript, String iSubScript, String UserType
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
//		Initialize the Variable
		String Delivery_Channel = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Delivery_Channel");
		String Sales_Channel = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Sales_Channel");
		String Contracting_City = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Contracting_City");
		String sResult;
		
		if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.newServiceOrderBtn,60)) {
				Waittilljquesryupdated();
				WebInteractUtil.scrollIntoTop();
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "newServiceOrderBtn in Customer Order Page is not visible, Please Verify");
			System.out.println("newServiceOrderBtn in Customer Order Page is not visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
//		Selecting Colt delivery Channel and sales channel value
		WebInteractUtil.selectByValueDIV(Siebel_Objects.delvieryChannelLst, Siebel_Objects.delvieryChannelSubLst, Delivery_Channel);
		WebInteractUtil.selectByValueDIV(Siebel_Objects.salesChannelLst, Siebel_Objects.salesChannelSubLst, Sales_Channel);
		
//		Selecting the Contracting City
		String Contract_ID = WebInteractUtil.getAttribute(Siebel_Objects.contractLabelTxb, "value");
		sResult = Select_Contracting_City(Contract_ID, Contracting_City);
		if (sResult.equalsIgnoreCase("False")){ return sResult;}
		
		//Saving the details
		Siebel_Objects.oppurtunityLabelTxb.sendKeys(Keys.chord(Keys.CONTROL + Keys.chord("s")));
		 //Validating if last click was not happened properly or not
		sResult = Validate_ErrorMsg(5);
		if (sResult.equalsIgnoreCase("False")){ return sResult;}
		
		ExtentTestManager.getTest().log(LogStatus.PASS, "Customer Order Details have been Entered Successfully");
		System.out.println("Customer Order Details have been Entered Successfully");
		
		
		return "True";
	}
	
	public String Validate_ErrorMsg(int TimeOut) throws IOException, InterruptedException {
	/*----------------------------------------------------------------------
	Method Name : Validate_ErrorMsg
	Purpose     : To make Validate the Error message
	Designer    : Vasantharaja C
	Created on  : 28th May 2020
	Input       : int TimeOut
	Output      : sResult
	 ----------------------------------------------------------------------*/
		String Ret_Val = "True",gText;
	
		int i;
		for (i=1;i<=TimeOut;i++) {
			try {
				if(Siebel_Objects.alertPopUp!= null){
					gText = Siebel_Objects.alertPopUp.getText();
					if (!gText.equals("")) {
						if (!gText.contains("another user")) {
							ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to Save Siebel Order due to the Error "+gText);
							System.out.println("Unable to Save Siebel Order due to the Error "+gText);
							ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
							return "False";
						} else {
							WebInteractUtil.click(Siebel_Objects.alertPopUpOKBtn);
							WebInteractUtil.waitForInvisibilityOfElement(Siebel_Objects.alertPopUpOKBtn, 50);
							return "True";
						}
					} else {
						return "True";
					}
				}
			}
			catch (Exception e) {
				Thread.sleep(1000);
				continue;
			}
		}

		return Ret_Val;
	}
	
	public String Select_Contracting_City(String Contract_ID, String Contracting_City) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : Select_Contracting_City
		Purpose     : This method will Search the Contracting City
		Designer    : Vasantharaja C
		Created on  : 28th May 2020 
		Input       : String Contracting_City
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		String sResult;
		Waittilljquesryupdated();
		
//		Entering the Credentials
		if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.contractLookup,90)) {
			WebInteractUtil.click(Siebel_Objects.contractLookup);
			WebInteractUtil.isPresent(Siebel_Objects.popUpFilterQueryLst,90);
			WebInteractUtil.selectByValueDIV(Siebel_Objects.popUpFilterQueryLst, Siebel_Objects.popUpFilterQuerySubLst, "Contract Id");
			Waittilljquesryupdated();
			WebInteractUtil.sendKeys(Siebel_Objects.popUpQueryTxb,Contract_ID);
			WebInteractUtil.click(Siebel_Objects.popUpContractQuerySearchBtn);
			WebInteractUtil.sendKeys(Siebel_Objects.contractIDTxb,Contract_ID);
			Siebel_Objects.contractIDTxb.sendKeys(Keys.chord(Keys.ENTER));
			Waittilljquesryupdated();
			WebInteractUtil.click(Siebel_Objects.contractCityCell);
			WebInteractUtil.selectByValueDIV(Siebel_Objects.contractCityLst, Siebel_Objects.contractCitySubLst, Contracting_City);
			Waittilljquesryupdated();
			WebInteractUtil.click(Siebel_Objects.popUpOKBtn);
//			WebInteractUtil.waitForInvisibilityOfElement(Siebel_Objects.popUpOKBtn,5);
			sResult = WaitElementInvisible(Siebel_Objects.popUpOKBtn,5);
			if (sResult.equalsIgnoreCase("False")){ return sResult;}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "contractLookup in Customer Order Page is not visible, Please Verify");
			System.out.println("contractLookup in Customer Order Page is not visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
	}
	
	public String WaitElementInvisible(WebElement sElement, int TimeOut) throws IOException, InterruptedException {
	/*----------------------------------------------------------------------
	Method Name : WaitElementInvisible
	Purpose     : To make execution wait till the element got invisible
	Designer    : Vasantharaja C
	Created on  :  28th May 2020
	Input       : file_name, sheet_name
	Output      : sResult
	 ----------------------------------------------------------------------*/
		int i;
		String Ret_Val = "True";
		for (i = 0; i <= TimeOut; i++) {
			try {
//				WebInteractUtil.waitForElementToBeVisible(sElement, 2);
				sElement.isDisplayed();
				Thread.sleep(1000);
				continue;
			} catch (Exception e) {
				break;
			}
		}
		if (i > TimeOut) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Element "+ sElement.toString() +" was still visible, please verify");
			System.out.println("Element "+ sElement.toString() +" was still visible, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}	
		return Ret_Val;
	}
	
	public String Search_ServiceOrder(String Service_Order, String sClick) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : Search_ServiceOrder
		Purpose     : This method will Search the Service Order in Siebel
		Designer    : Vasantharaja C
		Created on  : 28th May 2020 
		Input       : String Service_Order
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		
		Waittilljquesryupdated();
		
//		Entering the Credentials
		if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.serviceOrderTab,90)) {
			WebInteractUtil.click(Siebel_Objects.serviceOrderTab);
			Waittilljquesryupdated();
			WebInteractUtil.isEnabled(Siebel_Objects.enterServiceOrderTxb);
			Waittilljquesryupdated();
			WebInteractUtil.sendKeys(Siebel_Objects.enterServiceOrderTxb,Service_Order);
			WebInteractUtil.click(Siebel_Objects.serviceOrderSearchBtn);
			Waittilljquesryupdated();
			String sXpath = "//a[text()='"+Service_Order+"']";
			if (WebInteractUtil.waitForElementToBeVisible(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(sXpath)), 60)) {
				if (sClick.equalsIgnoreCase("Yes")) {
					WebInteractUtil.click(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(sXpath)));
					Waittilljquesryupdated();
					WebInteractUtil.isPresent(Siebel_Objects.showFullInfoLnk,90);
					Waittilljquesryupdated();
					WebInteractUtil.scrollIntoTop();
				}
				ExtentTestManager.getTest().log(LogStatus.PASS, "Search Service Order "+Service_Order+" in Siebel is successfull");
				System.out.println("Search Service Order "+Service_Order+" in Siebel is successfull");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Search Customer Order \"+Customer_Order+\" in Siebel is not successfull, Please Verify");
				System.out.println("Search Service Order "+Service_Order+" in Siebel is not successfull, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "serviceOrderTab is not Visible, Please Verify");
			System.out.println("serviceOrderTab is not Visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
	}
	
	public String ServiceOrder_Entry(String file_name,String Sheet_Name,String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : ServiceOrder_Entry
		Purpose     : This method will progress the Entry of Service Order
		Designer    : Vasantharaja C
		Created on  : 28th May 2020 
		Input       : String file_name,String Sheet_Name,String iScript, String iSubScript, String UserType
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
//		Initialize the Variable
		String Order_SybType = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Order_SybType");
		String Order_Processing_User = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Order_Processing_User");
		String sResult;
		
		Waittilljquesryupdated();
		
		if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.showFullInfoLnk,60)) {
			Waittilljquesryupdated();
			WebInteractUtil.scrollIntoTop();
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "showFullInfoLnk in Service Order Page is not visible, Please Verify");
			System.out.println("showFullInfoLnk in Service Order Page is not visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
//		Selecting the OrderSubType
		sResult = Order_SubType_Entry(Order_SybType);
		if (sResult.equalsIgnoreCase("False")){ return sResult;}
		
//		calling the below method to enter order processing user
		sResult = SelectLookUpVal(Siebel_Objects.orderProcessingUserLookup, "Login User",  Order_Processing_User, "Yes");
		if (sResult.equalsIgnoreCase("False")){ return sResult;}
		
		//Saving the details
		WebInteractUtil.click(Siebel_Objects.promotionCodeTxb);
		Siebel_Objects.promotionCodeTxb.sendKeys(Keys.chord(Keys.CONTROL + Keys.chord("s")));
		 //Validating if last click was not happened properly or not
		sResult = Validate_ErrorMsg(8);
		if (sResult.equalsIgnoreCase("False")){ return sResult;}
		
////		Calling the below function to save the service order
//		sResult = Save_ServiceOrder();
//		if (sResult.equalsIgnoreCase("False")){ return sResult;}
		
		return "True";
	}
	
	public String Order_SubType_Entry(String Order_SybType) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : Select_Contracting_City
		Purpose     : This method will select the order Subtype Entry
		Designer    : Vasantharaja C
		Created on  : 28th May 2020 
		Input       : String Contracting_City
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		String sResult;
		Waittilljquesryupdated();
		
//		Entering the Credentials
		if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.orderSubTypeLookup,90)) {
			WebInteractUtil.click(Siebel_Objects.orderSubTypeLookup);
			WebInteractUtil.isPresent(Siebel_Objects.orderSubTypeNewBtn,90);
			WebInteractUtil.click(Siebel_Objects.orderSubTypeNewBtn);
			WebInteractUtil.isPresent(Siebel_Objects.orderSubTypeLst,90);
			WebInteractUtil.selectByValueDIV(Siebel_Objects.orderSubTypeLst, Siebel_Objects.orderSubTypeSubLst, Order_SybType);
			Waittilljquesryupdated();
			WebInteractUtil.click(Siebel_Objects.orderSubTypeOKBtn);
			sResult = WaitElementInvisible(Siebel_Objects.orderSubTypeOKBtn,5);
			if (sResult.equalsIgnoreCase("False")){ return sResult;}
			ExtentTestManager.getTest().log(LogStatus.PASS, "orderSubType value has been selected as "+Order_SybType);
			System.out.println("orderSubType value has been selected as "+Order_SybType);
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "orderSubTypeLookup in Service Order Page is not visible, Please Verify");
			System.out.println("orderSubTypeLookup in Service Order Page is not visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
	}
	
	public String SelectLookUpVal(WebElement LookUpElem, String FilterValue,  String Value, String sClick) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : SelectLookUpVal
		Purpose     : This method will select the lookup value
		Designer    : Vasantharaja C
		Created on  : 28th May 2020 
		Input       : String Contracting_City
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		String sResult;
		Waittilljquesryupdated();
		
//		Entering the Credentials
		if (WebInteractUtil.waitForElementToBeVisible(LookUpElem,90)) {
			WebInteractUtil.click(LookUpElem);
			WebInteractUtil.isPresent(Siebel_Objects.popUpFilterQueryLst,90);
			WebInteractUtil.selectByValueDIV(Siebel_Objects.popUpFilterQueryLst, Siebel_Objects.popUpFilterQuerySubLst, FilterValue);
			Waittilljquesryupdated();
			WebInteractUtil.sendKeys(Siebel_Objects.popUpQueryTxb,Value);
			WebInteractUtil.click(Siebel_Objects.popUpGOBtn);
			Waittilljquesryupdated();
			if (sClick.equalsIgnoreCase("Yes")) {
				if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.pickPopUpTable,10)) {
					// find the row and click the same
					WebElement Click_Value = Siebel_Objects.pickPopUpTable.findElement(By.xpath("//tr/td[text()='" + Value + "'][1]"));
					WebInteractUtil.click(Click_Value);
					Waittilljquesryupdated();
//					Saving the element
					WebInteractUtil.click(Siebel_Objects.popUpOKBtn);
					sResult = WaitElementInvisible(Siebel_Objects.popUpOKBtn,5);
					if (sResult.equalsIgnoreCase("False")){ return sResult;}
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "pickPopUpTable in Customer/Service Order Page is not visible, Please Verify");
					System.out.println("pickPopUpTable in Customer/Service Order Page is not visible, Please Verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}
			} else {
				sResult = WaitElementInvisible(Siebel_Objects.popUpOKBtn,5);
				if (sResult.equalsIgnoreCase("False")){ return sResult;}
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "contractLookup in Customer Order Page is not visible, Please Verify");
			System.out.println("contractLookup in Customer Order Page is not visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
	}
	
	public String Save_ServiceOrder() throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : Service_Order
		Purpose     : To Save Service Order details
		Designer    : Vasantharaja C
		Created on  : 28th May 2020
		Input       : WebDriver driver, String pScreenshot
		Output      : sResult
		 ----------------------------------------------------------------------*/
		String sResult;
		if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.clickHereToSaveLnk,10)) {
			Waittilljquesryupdated();
			WebInteractUtil.click(Siebel_Objects.clickHereToSaveLnk);
			sResult = WaitElementInvisible(Siebel_Objects.clickHereToSaveLnk,60);
			if (sResult.equalsIgnoreCase("False")){ return sResult;}
			sResult = WaitElementInvisible(Siebel_Objects.saveMainPanel,40);
			if (sResult.equalsIgnoreCase("False")){ return sResult;}
			Waittilljquesryupdated();
		}
		
		return "True";
	}
	
	public String Fetch_Network_Reference(String file_name,String Sheet_Name,String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : Fetch_Network_Refernce
		Purpose     : This method will fetch the Network Reference
		Designer    : Vasantharaja C
		Created on  : 28th May 2020 
		Input       : String file_name,String Sheet_Name,String iScript, String iSubScript, String UserType
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		Waittilljquesryupdated();
		
		if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.networkReferenceTxb,60)) {
				Waittilljquesryupdated();
				String Network_Reference = WebInteractUtil.getAttribute(Siebel_Objects.networkReferenceTxb, "value");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Network Reference "+Network_Reference+" got generated");
				System.out.println("Network Reference "+Network_Reference+" got generated");
//				Passing the value to the Input Sheet
				dataminer.fnsetcolvalue(file_name, Sheet_Name, iScript, iSubScript, "Network_Reference", Network_Reference);
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "networkReferenceTxb in Service Order Page is not visible, Please Verify");
			System.out.println("networkReferenceTxb in Service Order Page is not visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
	return "True";
	}
	
	public String MiddleApplet_Entry(String file_name,String Sheet_Name,String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : MiddleApplet_Entry
		Purpose     : This method will enter the middle applet details
		Designer    : Vasantharaja C
		Created on  : 28th May 2020 
		Input       : String file_name,String Sheet_Name,String iScript, String iSubScript, String UserType
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initialize the Variable
		String OSS_Platform_Flag = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"OSS_Platform_Flag");
		String SubSea_Worker_Path = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"SubSea_Worker_Path");
		String Product_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		String Router_Technology = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Router_Technology");
		String Layer3_Resilience = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Layer3_Resilience");
		String sResult;
		
		Waittilljquesryupdated();
		
		if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.ossPlatformFlagLsb,60)) {
				Waittilljquesryupdated();
				WebInteractUtil.click(Siebel_Objects.ossPlatformFlagLsb);
				Waittilljquesryupdated();
				String sXpath = "//li[text()='"+OSS_Platform_Flag+"']";
				WebInteractUtil.click(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(sXpath)));
				Waittilljquesryupdated();
				if (Product_Name.equalsIgnoreCase("Wave")) {
					WebInteractUtil.click(Siebel_Objects.subSeaCablePathLst);
					Waittilljquesryupdated();
					sXpath = "//li[text()='"+SubSea_Worker_Path+"']";
					WebInteractUtil.click(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(sXpath)));
					Waittilljquesryupdated();
				}
				if (Product_Name.equalsIgnoreCase("ColtIpAccess")) {
					WebInteractUtil.click(Siebel_Objects.routerTechnologyLsb);
					Waittilljquesryupdated();
					sXpath = "//li[text()='"+Router_Technology+"']";
					WebInteractUtil.click(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(sXpath)));
					Waittilljquesryupdated();
					WebInteractUtil.click(Siebel_Objects.layer3ResilienceLsb);
					Waittilljquesryupdated();
					sXpath = "//li[text()='"+Layer3_Resilience+"']";
					WebInteractUtil.click(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(sXpath)));
					Waittilljquesryupdated();
				}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "networkReferenceTxb in Service Order Page is not visible, Please Verify");
			System.out.println("networkReferenceTxb in Service Order Page is not visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
//		Calling the below function to save the service order
		sResult = Save_ServiceOrder();
		if (sResult.equalsIgnoreCase("False")){ return sResult;}
		
		ExtentTestManager.getTest().log(LogStatus.PASS, "Middle Applet details have been entered Successfully");
		System.out.println("Middle Applet details have been entered Successfully");
		
		return "True";
	}
	
	public String Site_Operation(String file_name,String Sheet_Name,String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : Site_A_Operation
		Purpose     : This method will perform Site_A_Operation entry
		Designer    : Vasantharaja C
		Created on  : 28th May 2020 
		Input       : String file_name,String Sheet_Name,String iScript, String iSubScript, String UserType
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		String Product_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		String sResult;
		
//		Calling the below function to enter site address
		sResult = Site_Address_Entry(file_name, Sheet_Name, iScript, iSubScript);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		Calling the below function to select service party and site contact entry
		sResult = ServiceParty_SiteContact_Entry(file_name, Sheet_Name, iScript, iSubScript);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		Calling the below method to enter popup details in site info
		if (!Product_Name.equalsIgnoreCase("Wave")) {
			sResult = SiteInfoPopUpEntry(file_name, Sheet_Name, iScript, iSubScript);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
		}
		
//		Calling the below method to enter popup details in site info
		sResult = SiteDetailsEntry(file_name, Sheet_Name, iScript, iSubScript);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		return "True";
	}
	
	public String Site_Address_Entry(String file_name,String Sheet_Name,String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : Site_Address_Entry
		Purpose     : This method will enter the site address of the respective end
		Designer    : Vasantharaja C
		Created on  : 28th May 2020 
		Input       : String file_name,String Sheet_Name,String iScript, String iSubScript, String UserType
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initialize the Variable
		String Street_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Street_Name");
		String City = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"City");
		String Country = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Country");
		String Post_Code = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Post_Code");
		String Premises = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Premises");
		String sResult;
		WebElement selectLookup = null;
		Waittilljquesryupdated();
		
		switch (Sheet_Name) {
			case "A_End":
				selectLookup = Siebel_Objects.siteAddressAEndLookup;
				break;
			
			case "B_End":
				selectLookup = Siebel_Objects.siteAddressBEndLookup;
				break;		
		}
		
		if (WebInteractUtil.waitForElementToBeVisible(selectLookup,60)) {
			Waittilljquesryupdated();
			WebInteractUtil.click(selectLookup);
			Waittilljquesryupdated();
			if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.streetNameTxb,60)) {
				WebInteractUtil.sendKeys(Siebel_Objects.streetNameTxb, Street_Name);
				WebInteractUtil.sendKeys(Siebel_Objects.countryTxb, Country);
				WebInteractUtil.sendKeys(Siebel_Objects.cityTownTxb, City);
				WebInteractUtil.sendKeys(Siebel_Objects.postalZipCodeTxb, Post_Code);
				WebInteractUtil.sendKeys(Siebel_Objects.premisesTxb, Premises);
				WebInteractUtil.click(Siebel_Objects.searchAddressBtn);
				Waittilljquesryupdated();
				if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.searchResultTable,60)) {
					Waittilljquesryupdated();
					WebInteractUtil.click(Siebel_Objects.searchResultTable.findElement(By.xpath("descendant::tr/td[contains(span, '" + Street_Name + "')][1]")));
					Waittilljquesryupdated();
					WebInteractUtil.click(Siebel_Objects.pickAddressBtn);
					if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.buildingNotFoundBtn,60)) {
						Waittilljquesryupdated();
						WebInteractUtil.click(Siebel_Objects.searchResultTable.findElement(By.xpath("descendant::tr/td[contains(span, '" + Street_Name + "')][1]")));
						Waittilljquesryupdated();
						WebInteractUtil.click(Siebel_Objects.pickBuildingBtn);
						Waittilljquesryupdated();
						if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.siteNotFoundBtn,60)) {
							Waittilljquesryupdated();
							WebInteractUtil.click(Siebel_Objects.searchResultTable.findElement(By.xpath("descendant::tr/td[contains(text(),'Live')][1]")));
							Waittilljquesryupdated();
							WebInteractUtil.click(Siebel_Objects.pickSiteBtn);
							Waittilljquesryupdated();
							sResult = WaitElementInvisible(Siebel_Objects.siteSelectionHeader,40);
							if (sResult.equalsIgnoreCase("False")){ return sResult;}
						} else {
							ExtentTestManager.getTest().log(LogStatus.FAIL, "pickSiteBtn is not visible in Siebel Address Doctor, Please Verify");
							System.out.println("pickSiteBtn is not visible in Siebel Address Doctor, Please Verify");
							ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
							return "False";
						}
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "pickBuildingBtn is not visible in Siebel Address Doctor, Please Verify");
						System.out.println("pickBuildingBtn is not visible in Siebel Address Doctor, Please Verify");
						ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
						return "False";
					}
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "searchResultTable is not visible in Siebel Address Doctor, Please Verify");
					System.out.println("searchResultTable is not visible in Siebel Address Doctor, Please Verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "streetNameTxb is not visible in Siebel Address Doctor, Please Verify");
				System.out.println("streetNameTxb is not visible in Siebel Address Doctor, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, Sheet_Name+" address lookup is not visible in Service Order Page, Please Verify");
			System.out.println(Sheet_Name+" address lookup is not visible in Service Order Page, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		ExtentTestManager.getTest().log(LogStatus.PASS, Sheet_Name+" Address have been Entered in Siebel Successfully");
		System.out.println(Sheet_Name+" Address have been Entered in Siebel Successfully");
		return "True";
	}
	
	public String ServiceParty_SiteContact_Entry(String file_name,String Sheet_Name,String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : ServiceParty_SiteContact_Entry
		Purpose     : This method will enter the Service Party and Site Contact Details
		Designer    : Vasantharaja C
		Created on  : 28th May 2020 
		Input       : String file_name,String Sheet_Name,String iScript, String iSubScript, String UserType
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initialize the Variable
		String Site_Contact = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Site_Contact");
		String Service_Party = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Service_Party");
		String sResult;
		WebElement siteContactLookup = null, servicePartyLookup = null;
		
		Waittilljquesryupdated();
		
		switch (Sheet_Name) {
		case "A_End":
			servicePartyLookup = Siebel_Objects.servicePartyLookup_A_End;
			siteContactLookup = Siebel_Objects.siteContactLookup_A_End;
			break;
		
		case "B_End":
			servicePartyLookup = Siebel_Objects.servicePartyLookup_B_End;
			siteContactLookup = Siebel_Objects.siteContactLookup_B_End;
			break;		
		}
		
		if (WebInteractUtil.waitForElementToBeVisible(siteContactLookup,60)) {
			
//			calling the below method to enter Service Party Lookup
			sResult = SelectLookUpVal(servicePartyLookup, "Party Name",  Service_Party, "Yes");
			if (sResult.equalsIgnoreCase("False")){ return sResult;}
			
//			calling the below method to enter site contact
			sResult = SelectLookUpVal(siteContactLookup, "Last Name",  Site_Contact, "Yes");
			if (sResult.equalsIgnoreCase("False")){ return sResult;}	
			
//			Calling the below function to save the service order
			sResult = Save_ServiceOrder();
			if (sResult.equalsIgnoreCase("False")){ return sResult;}
				
			ExtentTestManager.getTest().log(LogStatus.PASS, "Service Party/Site Contact details have been entered Successfully");
			System.out.println("Service Party/Site Contact details have been entered Successfully");
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "siteContactLookup in Service Order Page is not visible, Please Verify");
			System.out.println("siteContactLookup in Service Order Page is not visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
	return "True";
	}
	
	public String SiteInfoPopUpEntry(String file_name,String Sheet_Name,String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : SiteInfoPopUpEntry
		Purpose     : This method will enter the pop up details in the site info
		Designer    : Vasantharaja C
		Created on  : 29th May 2020 
		Input       : String file_name,String Sheet_Name,String iScript, String iSubScript, String UserType
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initialize the Variable
		String Delivery_Country = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Delivery_Country");
		String Delivery_City = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Delivery_City");
		String sResult;
		WebElement showFullInfoLink = null;
		
		Waittilljquesryupdated();
		
		switch (Sheet_Name) {
		case "A_End":
			showFullInfoLink = Siebel_Objects.showFullInfoAendLnk;
			break;
		
		case "B_End":
			showFullInfoLink = Siebel_Objects.showFullInfoBendLnk;
			break;		
		}
		
		if (WebInteractUtil.waitForElementToBeVisible(showFullInfoLink,60)) {
			WebInteractUtil.click(showFullInfoLink);
			if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.addressContactLookup,60)) {
				Waittilljquesryupdated();
				WebInteractUtil.sendKeys(Siebel_Objects.deliveryCountryTxb,Delivery_Country);
				WebInteractUtil.sendKeysWithKeys(Siebel_Objects.deliveryCountryTxb,Delivery_Country,"Enter");
				Waittilljquesryupdated();
				WebInteractUtil.sendKeysWithKeys(Siebel_Objects.deliveryCityTxb,Delivery_City,"Enter");
				Waittilljquesryupdated();
				WebInteractUtil.click(Siebel_Objects.closePopUpBtn);
				sResult = WaitElementInvisible(Siebel_Objects.closePopUpBtn,40);
				if (sResult.equalsIgnoreCase("False")){ return sResult;}
				Waittilljquesryupdated();
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "showFullInfoLeftPanelLnk in Service Order Page is not visible, Please Verify");
				System.out.println("showFullInfoLeftPanelLnk in Service Order Page is not visible, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
			
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "showFullInfoLeftPanelLnk in Service Order Page is not visible, Please Verify");
			System.out.println("showFullInfoLeftPanelLnk in Service Order Page is not visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
//			Calling the below function to save the service order
			sResult = Save_ServiceOrder();
			if (sResult.equalsIgnoreCase("False")){ return sResult;}
				
			ExtentTestManager.getTest().log(LogStatus.PASS, "SiteInfoPopUpEntry details have been entered Successfully");
			System.out.println("SiteInfoPopUpEntry details have been entered Successfully");
		
			return "True";
	}
	
	public String SiteDetailsEntry(String file_name,String Sheet_Name,String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : SiteDetailsEntry
		Purpose     : This method will enter site details info
		Designer    : Vasantharaja C
		Created on  : 29th May 2020 
		Input       : String file_name,String Sheet_Name,String iScript, String iSubScript, String UserType
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initialize the Variable
		String ThirdParty_Provider = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"ThirdParty_Provider");
		String Shelf_ID = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Shelf_ID");
		String PhysicalPort_ID = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"PhysicalPort_ID");
		String Slot_ID = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Slot_ID");
		String VLAN_TagID = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"VLAN_TagID");
		String Installaton_Time = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Installaton_Time");
		String Product_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		String Flow_Type = dataminer.fngetcolvalue(file_name, "Customer_Service_Order", iScript, iSubScript,"Flow_Type");
		String Fibre_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Fibre_Type");
		String sResult, sXpath;
		
		Waittilljquesryupdated();
		
		switch (Sheet_Name) {
		case "A_End":
			
			if (Flow_Type.equalsIgnoreCase("Override") && WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.thirdPartyAccessProviderALst,5)) {
//				Third Party Access Provider Entry
				WebInteractUtil.scrollIntoView(Siebel_Objects.thirdPartyAccessProviderALst);
				Waittilljquesryupdated();
				WebInteractUtil.click(Siebel_Objects.thirdPartyAccessProviderALst);
				Waittilljquesryupdated();
				sXpath = "//li[text()='"+ThirdParty_Provider+"']";
				WebInteractUtil.click(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(sXpath)));
				Waittilljquesryupdated();
				WebInteractUtil.isPresent(Siebel_Objects.clickHereToSaveLnk, 45);
				Waittilljquesryupdated();
			}
			
			if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.shelfIDATxb,60)) {
				Waittilljquesryupdated();
				WebInteractUtil.sendKeys(Siebel_Objects.shelfIDATxb,Shelf_ID);
				WebInteractUtil.sendKeys(Siebel_Objects.slotIDATxb,Slot_ID);
				WebInteractUtil.sendKeys(Siebel_Objects.physicalportIDATxb,PhysicalPort_ID);
				if (Product_Name.equalsIgnoreCase("ColtIpAccess")) {
					WebInteractUtil.click(Siebel_Objects.fibreTypeLst);
					Waittilljquesryupdated();
					sXpath = "//li[text()='"+Fibre_Type+"']";
					WebInteractUtil.click(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(sXpath)));
					Waittilljquesryupdated();
				}
//				Installation Time Entry
				WebInteractUtil.scrollIntoView(Siebel_Objects.installationTimeALst);
				Waittilljquesryupdated();
				WebInteractUtil.click(Siebel_Objects.installationTimeALst);
				Waittilljquesryupdated();
				sXpath = "//li[text()='"+Installaton_Time+"']";
				WebInteractUtil.isEnabled(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(sXpath)));
				WebInteractUtil.click(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(sXpath)));
				Waittilljquesryupdated();
				WebInteractUtil.isPresent(Siebel_Objects.clickHereToSaveLnk, 45);
				Waittilljquesryupdated();
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "shelfIDATxb in Service Order Page is not visible, Please Verify");
				System.out.println("shelfIDATxb in Service Order Page is not visible, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
			break;
		
		case "B_End":
			
			if (Flow_Type.equalsIgnoreCase("Override") && WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.thirdPartyAccessProviderALst,5)) {
//				Third Party Access Provider Entry
				WebInteractUtil.scrollIntoView(Siebel_Objects.thirdPartyAccessProviderBLst);
				Waittilljquesryupdated();
				WebInteractUtil.click(Siebel_Objects.thirdPartyAccessProviderBLst);
				Waittilljquesryupdated();
				sXpath = "//li[text()='"+ThirdParty_Provider+"']";
				WebInteractUtil.click(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(sXpath)));
				Waittilljquesryupdated();
				WebInteractUtil.isPresent(Siebel_Objects.clickHereToSaveLnk, 45);
			}
			
			if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.shelfIDBTxb,60)) {
				WebInteractUtil.sendKeys(Siebel_Objects.shelfIDBTxb,Shelf_ID);
				WebInteractUtil.sendKeys(Siebel_Objects.slotIDBTxb,Slot_ID);
				WebInteractUtil.sendKeys(Siebel_Objects.physicalportIDBTxb,PhysicalPort_ID);
				if (Product_Name.equalsIgnoreCase("EthernetSpoke")) { WebInteractUtil.sendKeys(Siebel_Objects.vlanTagIDBTxb,VLAN_TagID); }
//				Installation Time Entry
				WebInteractUtil.scrollIntoView(Siebel_Objects.installationTimeBLst);
				Waittilljquesryupdated();
				WebInteractUtil.click(Siebel_Objects.installationTimeBLst);
				Waittilljquesryupdated();
				sXpath = "//li[text()='"+Installaton_Time+"']";
				WebInteractUtil.click(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(sXpath)));
				Waittilljquesryupdated();
				WebInteractUtil.isPresent(Siebel_Objects.clickHereToSaveLnk, 45);
				Waittilljquesryupdated();
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "shelfIDBTxb in Service Order Page is not visible, Please Verify");
				System.out.println("shelfIDBTxb in Service Order Page is not visible, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
			break;		
		}
		
//		Calling the below function to save the service order
		sResult = Save_ServiceOrder();
		if (sResult.equalsIgnoreCase("False")){ return sResult;}
			
		ExtentTestManager.getTest().log(LogStatus.PASS, "Site Details Entry have been entered Successfully");
		System.out.println("Site Details Entry have been entered Successfully");
	
		return "True";
	}
	
	public String OrderDates_Entry(String Orer_Type) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : OrderDates_Entry
		Purpose     : This method will enter the Order Dates Info
		Designer    : Vasantharaja C
		Created on  : 29th May 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		Waittilljquesryupdated();
		
		//Initialize the dates
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String date1 = dateFormat.format(date);
		String sResult;
		
		if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.orderDatesLnk,60)) {
			WebInteractUtil.click(Siebel_Objects.orderDatesLnk);
			Waittilljquesryupdated();
			switch(Orer_Type) {
				case "New":
					WebInteractUtil.click(Siebel_Objects.orderSignedDateElem);
					WebInteractUtil.click(Siebel_Objects.pickNowFromDate);
					WebInteractUtil.click(Siebel_Objects.pickDoneFromDate);
					Waittilljquesryupdated();
					WebInteractUtil.click(Siebel_Objects.orderRecievedDateElem);
					WebInteractUtil.click(Siebel_Objects.pickNowFromDate);
					WebInteractUtil.click(Siebel_Objects.pickDoneFromDate);
					Waittilljquesryupdated();
					break;
				case "Delivery":
					WebInteractUtil.click(Siebel_Objects.coltPromiseDateElem);
					Waittilljquesryupdated();
					WebInteractUtil.isEnabled(Siebel_Objects.acceptProposalBtn);
					WebInteractUtil.click(Siebel_Objects.acceptProposalBtn);
					Waittilljquesryupdated();
					break;
				case "Cease_Delivery":
					WebInteractUtil.click(Siebel_Objects.customerRequestedDateElem);
					WebInteractUtil.click(Siebel_Objects.pickNowFromDate);
					WebInteractUtil.click(Siebel_Objects.pickDoneFromDate);
					break;
			}
			
			//Saving the details
			WebInteractUtil.click(Siebel_Objects.promotionCodeTxb);
			Siebel_Objects.promotionCodeTxb.sendKeys(Keys.chord(Keys.CONTROL + Keys.chord("s")));
			 //Validating Error Messaage
			sResult = Validate_ErrorMsg(3);
			if (sResult.equalsIgnoreCase("False")){ return sResult;}
			
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order dates in Siebel have been saved successfully");
			System.out.println("Order dates in Siebel have been saved successfully");
			
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "orderDatesLnk in Siebel main Page is not visible, Please Verify");
			System.out.println("orderDatesLnk in Siebel main Page is not visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
	}
	
	public String Billing_Entry(String file_name,String Sheet_Name,String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : Billing_Entry
		Purpose     : This method will enter Billing_Entry details info
		Designer    : Vasantharaja C
		Created on  : 29th May 2020 
		Input       : String file_name,String Sheet_Name,String iScript, String iSubScript, String UserType
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initialize the Variable
		String Bill_Cust_Ref = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Bill_Cust_Ref");
		String sResult;
		
		Waittilljquesryupdated();
		
		//Initialize the dates
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String date1 = dateFormat.format(date);
		
		if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.billingLnk,60)) {
//			Third Party Access Provider Entry
			WebInteractUtil.click(Siebel_Objects.billingLnk);
			Waittilljquesryupdated();
			if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.refreshBtn,60)) {
				Waittilljquesryupdated();
				WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.showFullInfoLnk, 25);
				WebInteractUtil.click(Siebel_Objects.billCustRefOrderTxb);
				Waittilljquesryupdated();
				WebInteractUtil.sendKeys(Siebel_Objects.billCustRefOrderTxb, Bill_Cust_Ref);
				WebInteractUtil.click(Siebel_Objects.billStartDateElem);
				WebInteractUtil.click(Siebel_Objects.pickNowFromDate);
				WebInteractUtil.click(Siebel_Objects.pickDoneFromDate);
				Waittilljquesryupdated();
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "refreshBtn in Billing Page is not visible, Please Verify");
				System.out.println("refreshBtn in Billing Page is not visible, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}	
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "billingLnk in Service Order Page is not visible, Please Verify");
			System.out.println("billingLnk in Service Order Page is not visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
//			Calling the below function to save the service order
			sResult = Save_ServiceOrder();
			if (sResult.equalsIgnoreCase("False")){ return sResult;}
				
			ExtentTestManager.getTest().log(LogStatus.PASS, "Billing Entry have been entered Successfully");
			System.out.println("Billing Entry have been entered Successfully");
		
			return "True";
	}
	
	public String OrderStatus_Validation(String Order_Status) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : OrderStatus_Validation
		Purpose     : This method will perform OrderStatus_Validation
		Designer    : Vasantharaja C
		Created on  : 29th May 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		String sResult;
		Waittilljquesryupdated();
		
		if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.orderStatusLst,60)) {
			Waittilljquesryupdated();
			WebInteractUtil.click(Siebel_Objects.orderStatusLst);
			Waittilljquesryupdated();
			String sXpath = "//li[text()='"+Order_Status+"']";
			WebInteractUtil.click(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(sXpath)));
			Waittilljquesryupdated();
			if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.continueBtn, 3)) { WebInteractUtil.click(Siebel_Objects.continueBtn); Waittilljquesryupdated(); }
//			Validating Error Message
			sResult = Validate_ErrorMsg(5);
			if (sResult.equalsIgnoreCase("False")){ return sResult;}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "orderStatusLst in Service Order Page is not visible, Please Verify");
			System.out.println("orderStatusLst in Service Order Page is not visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}	
		
		if (Order_Status.equalsIgnoreCase("Technical Validation")) {
//			to check the checkbox if it is not selected
			if (!Siebel_Objects.workflowCbx.isSelected()) { WebInteractUtil.click(Siebel_Objects.workflowCbx); }
			if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.triggerTRBtn, 60)) {
				Waittilljquesryupdated();
				WebInteractUtil.click(Siebel_Objects.triggerTRBtn);
				Waittilljquesryupdated();
				 //Validating if last click was not happened properly or not
				sResult = Validate_ErrorMsg(15);
				if (sResult.equalsIgnoreCase("False")){ return sResult;}
//				Checking for Element Invisibility
				sResult = WaitElementInvisible(Siebel_Objects.triggerTRBtn,45);
				if (sResult.equalsIgnoreCase("False")){ return sResult;}
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "triggerTRBtn in Service Order Page is not visible, Please Verify");
				System.out.println("triggerTRBtn in Service Order Page is not visible, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		}
		
		if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.statusValidationOKBtn, 3)) {WebInteractUtil.click(Siebel_Objects.statusValidationOKBtn); }
		
		WebInteractUtil.click(Siebel_Objects.promotionCodeTxb);
		Waittilljquesryupdated();
		
		ExtentTestManager.getTest().log(LogStatus.PASS, "Order Status has been successfully Moved to "+Order_Status);
		System.out.println("Order Status has been successfully Moved to "+Order_Status);
		
		return "True";
	}
	
	public String TechVal_LeadTime_Entry(String file_name,String Sheet_Name,String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : TechVal_LeadTime_Entry
		Purpose     : This method will enter TechVal_LeadTime_Entry info
		Designer    : Vasantharaja C
		Created on  : 29th May 2020 
		Input       : String file_name,String Sheet_Name,String iScript, String iSubScript, String UserType
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initialize the Variable
		String Technical_Validation_User = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Technical_Validation_User");
		String LeadTime_Primary = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"LeadTime_Primary");
		String sResult;
		
		Waittilljquesryupdated();
		
		if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.techValLookup,60)) {
//			calling the below method to enter site contact
			sResult = SelectLookUpVal(Siebel_Objects.techValLookup, "Login User",  Technical_Validation_User, "Yes");
			if (sResult.equalsIgnoreCase("False")){ return sResult;}	
			
//			Entering the leadtime
			WebInteractUtil.click(Siebel_Objects.existingLeadTimePrimaryLst);
			Waittilljquesryupdated();
			String sXpath = "//li[text()='"+LeadTime_Primary+"']";
			WebInteractUtil.click(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(sXpath)));
			Waittilljquesryupdated();
			
			//Saving the details
			WebInteractUtil.click(Siebel_Objects.promotionCodeTxb);
			Siebel_Objects.promotionCodeTxb.sendKeys(Keys.chord(Keys.CONTROL + Keys.chord("s")));
//			Validating if last click was not happened properly or not
			sResult = Validate_ErrorMsg(7);
			if (sResult.equalsIgnoreCase("False")){ return sResult;}
			
			ExtentTestManager.getTest().log(LogStatus.PASS, "Tech Val lookup and lead time entries have been entered Successfully");
			System.out.println("Tech Val lookup and lead time entries have been entered Successfully");
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "techValLookup in Service Order Page is not visible, Please Verify");
			System.out.println("techValLookup in Service Order Page is not visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
	}
	
	public String Generate_CircuitReference() throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : Generate_CircuitReference
		Purpose     : This method will generate the circuit reference
		Designer    : Vasantharaja C
		Created on  : 29th May 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		String sResult;
		
		if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.billingLnk,60)) {
//			Third Party Access Provider Entry
			WebInteractUtil.click(Siebel_Objects.billingLnk);
			Waittilljquesryupdated();
			if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.getReferenceBtn,60)) {
				Waittilljquesryupdated();
				WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.refreshBtn, 25);
				WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.showFullInfoLnk, 15);
				WebInteractUtil.click(Siebel_Objects.getReferenceBtn);
				Waittilljquesryupdated();
				WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.clickHereToSaveLnk, 75);
				Waittilljquesryupdated();
				if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.circuitReferenceTxb,60)) {
					Waittilljquesryupdated();
					String CircuitReference = WebInteractUtil.getAttribute(Siebel_Objects.circuitReferenceTxb, "value");
					ExtentTestManager.getTest().log(LogStatus.PASS, "Circuit Reference "+CircuitReference+" got Generated");
					System.out.println("Circuit Reference "+CircuitReference+" got Generated");
//					Calling the below function to save the service order
					sResult = Save_ServiceOrder();
					if (sResult.equalsIgnoreCase("False")){ return sResult;}
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "circuitReferenceTxb is not visible, Please Verify");
					System.out.println("circuitReferenceTxb is not visible, Please Verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}	
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "getReferenceBtn in Siebel main Page is not visible, Please Verify");
				System.out.println("getReferenceBtn in Siebel main Page is not visible, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}	
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "billingLnk in Service Order Page is not visible, Please Verify");
			System.out.println("billingLnk in Service Order Page is not visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
	}
	
	public String Primary_Test_Method_Entry(String file_name,String Sheet_Name,String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : Primary_Test_Method_Entry
		Purpose     : This method will enter Primary_Test_Method_Entry details info
		Designer    : Vasantharaja C
		Created on  : 29th May 2020 
		Input       : String file_name,String Sheet_Name,String iScript, String iSubScript, String UserType
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initialize the Variable
		String PrimaryTesting_Method = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"PrimaryTesting_Method");
		String sResult;
		
		Waittilljquesryupdated();
		
		if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.installationAndTestLnk,5)) {
			WebInteractUtil.click(Siebel_Objects.installationAndTestLnk);
			Waittilljquesryupdated();
		} else {
//			WebInteractUtil.selectByValue(Siebel_Objects.bottomChildLst, "Installation and Test");
			WebInteractUtil.selectByIndex(Siebel_Objects.bottomChildLst, 5);
			Waittilljquesryupdated();
		}
		
		if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.primaryTestingMethodLst,25)) {
			Waittilljquesryupdated();
			WebInteractUtil.scrollIntoView(Siebel_Objects.primaryTestingMethodLst);
			Waittilljquesryupdated();
			WebInteractUtil.click(Siebel_Objects.primaryTestingMethodLst);
			Waittilljquesryupdated();
			String sXpath = "//li[text()='"+PrimaryTesting_Method+"']";
			WebInteractUtil.click(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(sXpath)));
			Waittilljquesryupdated();
			sResult = Validate_ErrorMsg(5);
			if (sResult.equalsIgnoreCase("False")){ return sResult;}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "primaryTestingMethodLst is not visible, Please Verify");
			System.out.println("primaryTestingMethodLst is not visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		

		
		return "True";
	}
	
	public String Manual_Validation() throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : Manual_Validation
		Purpose     : This method will trigger the manual validation 
		Designer    : Vasantharaja C
		Created on  : 29th May 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		String sResult;
		
		if (WebInteractUtil.waitForElementToBeVisible(Siebel_Objects.manualValidationBtn,60)) {
//			Third Party Access Provider Entry
			WebInteractUtil.click(Siebel_Objects.manualValidationBtn);
			Waittilljquesryupdated();
			sResult = Validate_ErrorMsg(5);	if (sResult.equalsIgnoreCase("False")){ return sResult;}
			WebInteractUtil.click(Siebel_Objects.promotionCodeTxb);
			Waittilljquesryupdated();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Manual Validation has been triggered with no reported missing entries");
			System.out.println("Manual Validation has been triggered with no reported missing entries");
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "manualValidationBtn in Service Order Page is not visible, Please Verify");
			System.out.println("manualValidationBtn in Service Order Page is not visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
	}
}
