package com.colt.common.utils;

import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.Parameters;
import com.colt.common.utils.dataminer;
import com.relevantcodes.extentreports.LogStatus;
import com.qa.colt.extentreport.ExtentManager;
import com.qa.colt.extentreport.ExtentTestManager;
import com.client.cpq.uitests.pageobjects.CPQ_Objects;
import com.client.cpq.uitests.pageobjects.Explore_Objects;
import com.colt.common.utils.DriverManagerUtil;
import com.colt.common.utils.WebInteractUtil;
import com.colt.common.utils.Explore_Library;

public class CPQ_Library {
	
	public WebDriver driver = DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
	
	public CPQ_Objects CPQ_Objects;
	public Explore_Objects Explore_Objects;

	public CPQ_Library() {
		CPQ_Objects = new CPQ_Objects();
		Explore_Objects = new Explore_Objects();
	}
	
	public String CPQ_Login(String file_name, String UserType) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : CPQ_Login
		Purpose     : This method will progress the login of C4C application
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : String file_name, String UserType
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
//		Initialize the Variable		
		String Environment = dataminer.fngetconfigvalue(file_name, "Environment");
		String C4C_URL = dataminer.fngetconfigvalue(file_name, "C4C_URL");
		String C4C_Username = dataminer.fngetconfigvalue(file_name, "Sales_User");
		String C4C_Password = dataminer.fngetconfigvalue(file_name, "C4C_Password");
		
//		Launching URL
		WebInteractUtil.launchWebApp(C4C_URL);	
		Waittilljquesryupdated();
		WaitforC4Cloader();
		
//		re-Login again if the login was not carry forwarded with cache
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.verifyEditLnk, 3)) { logOutC4C(); }
		
//		Entering the Credentials
		WebInteractUtil.isPresent(CPQ_Objects.userNameTxb,90);
		ExtentTestManager.getTest().log(LogStatus.PASS, "C4C URL "+C4C_URL+" launch is successfull");
		System.out.println("C4C URL "+C4C_URL+" launch is successfull");
		
//		Performing the Login Operations
		WebInteractUtil.sendKeys(CPQ_Objects.userNameTxb, C4C_Username);
		WebInteractUtil.sendKeys(CPQ_Objects.passWordTxb, C4C_Password);
		WebInteractUtil.click(CPQ_Objects.loginBtn);
		Waittilljquesryupdated();
		
		if (Environment.equalsIgnoreCase("RFS")) {
			Waittilljquesryupdated();
			WebInteractUtil.launchWebApp(C4C_URL);
			Waittilljquesryupdated();
		}
		
//		Verify Login Successsfull or not
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.verifyEditLnk, 75)) {
			Waittilljquesryupdated();
			ExtentTestManager.getTest().log(LogStatus.PASS, "C4C URL application Login with "+UserType+" is successfull");
			System.out.println("C4C URL application Login with "+UserType+" is successfull");
		}
		else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Login to C4C application "+C4C_URL+" Failed");
			System.out.println("Login to C4C application "+C4C_URL+" Failed");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
		
	}
	
	public String logOutC4C() throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : logOutC4C
		Purpose     : This method will logout C4C
		Designer    : Vasantharaja C
		Created on  : 17th July 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.c4cUserImgLnk,90)) {
			Waittilljquesryupdated();
			WebInteractUtil.click(CPQ_Objects.c4cUserImgLnk);
			WebInteractUtil.isPresent(CPQ_Objects.c4cSignOutBtn,30);
			WebInteractUtil.click(CPQ_Objects.c4cSignOutBtn);
			Waittilljquesryupdated();
			WebInteractUtil.isPresent(CPQ_Objects.c4cAcceptSignoutBtn,30);
			WebInteractUtil.scrollIntoView(CPQ_Objects.c4cAcceptSignoutBtn);
			WebInteractUtil.click(CPQ_Objects.c4cAcceptSignoutBtn);
			WaitforC4Cloader();
			WebInteractUtil.isPresent(CPQ_Objects.c4cLogOnBtn,30);
			WebInteractUtil.click(CPQ_Objects.c4cLogOnBtn);
			Waittilljquesryupdated();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Logout from C4C was Successfull");
			System.out.println("Logout from C4C was Successfull");
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "c4cUserImgLnk is not found in C4C Home Page");
			System.out.println("c4cUserImgLnk is not found in C4C Home Page");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
	
		
		return "True";
	}
	
	public String navigateAccountFromHomepage() throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : navigateAccountFromHomepage
		Purpose     : This method will Navigate to Accounts tab from C4C main page
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		retriving the Driver
//		WebDriver driver = DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.customerLst,90)) {
			Waittilljquesryupdated();
			WebInteractUtil.click(CPQ_Objects.customerLst);
			WebInteractUtil.isPresent(CPQ_Objects.accountsTab,90);
			WebInteractUtil.click(CPQ_Objects.accountsTab);
			Waittilljquesryupdated();
			Waittilljquesryupdated();
			WebInteractUtil.isPresent(CPQ_Objects.searchIcon,90);
			WaitforC4Cloader();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Navigation to Accounts tab was Successfull");
			System.out.println("Navigation to Accounts tab was Successfull");
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Customer tab is not found in C4C Home Page");
			System.out.println("Customer tab is not found in C4C Home Page");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
			
		return "True";
	}
	
	public String navigateQuotesFromHomepage() throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : navigateQuotesFromHomepage
		Purpose     : This method will Navigate to QQuotes tab from C4C main page
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 

		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.salesLst,90)) {
			Waittilljquesryupdated();
			WebInteractUtil.click(CPQ_Objects.salesLst);
			WebInteractUtil.isPresent(CPQ_Objects.quotesTab,90);
			WebInteractUtil.click(CPQ_Objects.quotesTab);
			WaitforC4Cloader();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Navigation to Quotes tab was Successfull");
			System.out.println("Navigation to Quotes tab was Successfull");
			Waittilljquesryupdated();
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Sales tab is not found in C4C Home Page");
			System.out.println("Sales tab is not found in C4C Home Page");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
			
		return "True";
	}
	
	public String searchQuoteC4C(String Quote_ID) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : navigateAccountFromHomepage
		Purpose     : This method will Navigate to Accounts tab from C4C main page
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		String Environment = dataminer.fngetconfigvalue(System.getProperty("user.dir")+"\\TestData\\CPQ_testdata.xlsx", "Environment");
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.searchIcon,60)) {
			Waittilljquesryupdated();
			WaitforC4Cloader();
			Waittilljquesryupdated();
			WebInteractUtil.pause(1000);
			WebInteractUtil.scrollIntoView(CPQ_Objects.searchIcon);
			WebInteractUtil.click(CPQ_Objects.searchIcon);
			WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.searchTxb,60);
			if (WebInteractUtil.sendKeys(CPQ_Objects.searchTxb, Quote_ID)) {
				WebInteractUtil.click(CPQ_Objects.searchBtn);
				WaitforC4Cloader();
				WebDriver driver = DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
				Waittilljquesryupdated();
//				if (Environment.equalsIgnoreCase("Test2")) { WebInteractUtil.scrollIntoView(CPQ_Objects.defaultCkb); }
				WebInteractUtil.isEnabled(driver.findElement(By.xpath("//descendant::a[text()='"+Quote_ID+"']")));
				WebInteractUtil.click(driver.findElement(By.xpath("//descendant::a[text()='"+Quote_ID+"']")));
				WebInteractUtil.pause(1000);
				WaitforC4Cloader();
				WebInteractUtil.isEnabled(CPQ_Objects.actionBtn);
				WebInteractUtil.scrollIntoView(CPQ_Objects.actionBtn);
				WaitforC4Cloader();
				WebInteractUtil.click(CPQ_Objects.actionBtn);
				WebInteractUtil.pause(1000);
				WebInteractUtil.scrollIntoView(CPQ_Objects.EditBtn);
				WebInteractUtil.click(CPQ_Objects.EditBtn);
//				WebInteractUtil.scrollIntoView(CPQ_Objects.ViewBtn);
//				WebInteractUtil.click(CPQ_Objects.ViewBtn);
				WebInteractUtil.isPresent(CPQ_Objects.quoteNameTxb, 180);
				waitForpageloadmask();
				Waittilljquesryupdated();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Quote Search with QuoteID '"+Quote_ID+"' is successfull in C4C");
				System.out.println("Quote Search with QuoteID '"+Quote_ID+"' is successfull in C4C");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Quote Search Textbox is not found in C4C Home Page");
				System.out.println("Quote Search Textbox is not found in C4C Home Page");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "searchIcon is not found in Quote Search Page in C4C");
			System.out.println("searchIcon is not found in Quote Search Page in C4C");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
	}
	
	public void waitForpageloadmask() throws InterruptedException
	/*----------------------------------------------------------------------
	Method Name : waitForpageloadmask
	Purpose     : This method will wait until the mask element disappears in CPQ page
	Designer    : Vasantharaja C
	Created on  : 1st April 2020 
	Input       : None
	Output      : None
	 ----------------------------------------------------------------------*/ 
	{    
		boolean Status = false;
		Thread.sleep(1000);
		for (int j=0; j < 10; j++) {
			try {
				WebDriver driver = DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
				try {
					  for(int i=0;i<=20;i++) {
						  while(driver.findElement(By.xpath("//div[@id='lockCreateScreen' and not(@style='display: none;')]")).isDisplayed()) {
//								Thread.sleep(1000);
								Status = true;
	//							System.out.println("Load Mask Displayed");
						  }
					  }
				} catch(Exception e) {
					Thread.sleep(200);
					Waittilljquesryupdated();
	//				System.out.println("Load Mask Not Displayed");
	//				System.out.println(e.getMessage().toString());
				}
			} catch (SocketTimeoutException e1) {
				continue;
			}
			if (Status = true) { break; }
		} 
		
		
//		Thread.sleep(300);
	}
	
	public void Waittilljquesryupdated() throws InterruptedException, SocketTimeoutException {
		/*----------------------------------------------------------------------
		Method Name : Waittilljquesryupdated
		Purpose     : This method will return whether the driver steady state is completed ornot
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : None
		 ----------------------------------------------------------------------*/ 
//		JavascriptExecutor js = null;
		boolean Status = false;
		Thread.sleep(750);
		JavascriptExecutor js = (JavascriptExecutor) DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
		for (int i=1; i<10; i++) {
			if (js == null) {
				Thread.sleep(250);
				js = (JavascriptExecutor) DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
				continue;
			} else {
				try {
					while(!(js.executeScript("return document.readyState").equals("complete")))
					{
	//					System.out.println("dom state is" +(js.executeScript("return document.readyState")));
//						Thread.sleep(500);
					}
					Status = true;
					if (Status = true) { Thread.sleep(250); break; }
				} catch (Exception e) {
					continue;
				}
			}
		}	
	}
	
	public String searchAccount(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : navigateAccountFromHomepage
		Purpose     : This method will Navigate to Accounts tab from C4C main page
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String OCN_Number = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"OCN_Number");
		String Account_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Account_Name");
		int i;
		
//		Searching the Account
//		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.selectViewDn,60)) {
//			WebInteractUtil.scrollIntoView(CPQ_Objects.defaultCkb);
//		WebInteractUtil.pause(2000);
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.defaultCkb,60)) {
			WebInteractUtil.scrollIntoView(CPQ_Objects.defaultCkb);
			WebInteractUtil.pause(2000);
			WebInteractUtil.click(CPQ_Objects.selectViewDn);
			WebInteractUtil.pause(1000);
			WebInteractUtil.click(CPQ_Objects.allLst);
			WaitforC4Cloader();
			WebInteractUtil.scrollIntoView(CPQ_Objects.searchIcon);
			WaitforC4Cloader();
			WebInteractUtil.click(CPQ_Objects.searchIcon);
			if (WebInteractUtil.sendKeys(CPQ_Objects.searchTxb, OCN_Number)) {
				WebInteractUtil.click(CPQ_Objects.searchBtn);
				WaitforC4Cloader();
				WebDriver driver = DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
				WebInteractUtil.scrollIntoView(CPQ_Objects.defaultCkb);
				WebInteractUtil.click(driver.findElement(By.xpath("//descendant::a[text()='"+Account_Name+"']")));
				WaitforC4Cloader();
				ExtentTestManager.getTest().log(LogStatus.PASS, "OCN Search with Account name '"+Account_Name+"' is successfull");
				System.out.println("OCN Search with Account name '"+Account_Name+"' is successfull");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Search OCN Textbox is not found in C4C Home Page");
				System.out.println("Search OCN Textbox is not found in C4C Home Page");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Select View Dropdown is not found in C4C Home Page");
			System.out.println("Select View Dropdown is not found in C4C Home Page");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
	}
	
	public void WaitforC4Cloader() throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : WaitforC4Cloader
		Purpose     : This method will wait C4C application while loading page occurs
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : None
		 ----------------------------------------------------------------------*/ 
		int i = 1;
		Thread.sleep(1000);   
		WebDriver driver = DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
          try {
        	  while (driver.findElement(By.xpath("//*[contains(@title,'Please wait')]")).isDisplayed()) {   
        		  if (i > 60) { break; }
                      Thread.sleep(1000);
//                      System.out.println("Waiting C4C Page to load");
                      i = i+1;
                } 
          } catch(Exception e) {
        	  Waittilljquesryupdated();
//              System.out.println("C4C Page loaded fully, proceeding further");
          }
	}
	
	public void WaitforCPQloader() throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : WaitforCPQloader
		Purpose     : This method will wait CPQ application while loading page occurs
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : None
		 ----------------------------------------------------------------------*/ 
		int i = 1;
		Thread.sleep(1000);   
		WebDriver driver = DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
          try {
        	  while (driver.findElement(By.xpath("//*[@id='loading-mask']")).isDisplayed()) {   
        		  if (i > 60) { break; }
                      Thread.sleep(1000);
                      System.out.println("Waiting CPQ Page to load");
                      i = i+1;
                } 
          } catch(Exception e) {
        	  Waittilljquesryupdated();
//            System.out.println("CPQ Page loaded fully, proceeding further");
          }
	}
	
	public String createOpenOppurtunity(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : createOppurtunity
		Purpose     : This method is to create new oppurtunity
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : String file_name, String sheet_name, String iScript, String iSubScript
		Output      : Oppurtunity
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Name");
		String Offer_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Offer_Type");
		String Primary_Programme = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Primary_Programme");
		String Primary_Attribute = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Primary_Attribute");
		String Opportunity_Currency = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Opportunity_Currency");
		
//		Initializing the driver
//		WebDriver driver = DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
		
//		Creating an Opportunity
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.OpportunitiesLnk,90)) {
			WaitforC4Cloader();
			WebInteractUtil.click(CPQ_Objects.OpportunitiesLnk);
			WaitforC4Cloader();
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.OpportunitiesLnk,60)) {
				WebInteractUtil.click(CPQ_Objects.opportunityNewBtn);
				WaitforC4Cloader();
				WebInteractUtil.pause(2000);
				WaitforC4Cloader();
				WebInteractUtil.isEnabled(CPQ_Objects.NameTxb);
				WebInteractUtil.sendKeys(CPQ_Objects.NameTxb, Name);
				WebInteractUtil.click(CPQ_Objects.OfferTypeLst);
				WebInteractUtil.ClickonElementByString("//li[normalize-space(.)='"+Offer_Type+"']", 30);
				WebInteractUtil.click(CPQ_Objects.PrimaryProgramLst);
				WebInteractUtil.ClickonElementByString("//li[normalize-space(.)='"+Primary_Programme+"']", 30);
				WebInteractUtil.click(CPQ_Objects.PrimaryAttributeLst);
				WebInteractUtil.ClickonElementByString("//li[normalize-space(.)='"+Primary_Attribute+"']", 30);
				WebInteractUtil.click(CPQ_Objects.OpportunityCurencyLst);
				WebInteractUtil.ClickonElementByString("//li[normalize-space(.)='"+Opportunity_Currency+"']", 30);
				System.out.println("Creating Opportunity details have been entered");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Opportunity New Button is not found in C4C Page");
				System.out.println("Opportunity New Button is not found in C4C Page");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Opportunity Link is not found in C4C Page");
			System.out.println("Opportunity Link is not found in C4C Page");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
//		Saving the details and opening the page
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.saveMoreLnk,60)) {
			WebInteractUtil.scrollIntoView(CPQ_Objects.saveMoreLnk);
			WebInteractUtil.click(CPQ_Objects.saveMoreLnk);
			WebInteractUtil.pause(1000);
			WebInteractUtil.scrollIntoView(CPQ_Objects.saveOpenLnk);
			WebInteractUtil.click(CPQ_Objects.saveOpenLnk);
			WebInteractUtil.pause(3000);
			WaitforC4Cloader();
			WebInteractUtil.pause(3000);
			Waittilljquesryupdated();
//			Fetching the Opportunity ID and printing the same in the report as well as data sheet
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.oppurtunityIDBx,60)) {
				String Opportunity_ID = CPQ_Objects.oppurtunityIDBx.getAttribute("title");
				System.out.println("Oppotunity ID is "+Opportunity_ID);
				ExtentTestManager.getTest().log(LogStatus.PASS, "Opportunity ID "+Opportunity_ID+" has been Created");
				return Opportunity_ID;
			} else {
				WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.headerBar,120);
				String sText = CPQ_Objects.headerBar.getText();
				if (sText.contains("Error")) {
					ExtentTestManager.getTest().log(LogStatus.PASS, "Opportunity is not created due to the error "+sText);
					System.out.println("Opportunity is not created due to the error "+sText);
				}
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Opportunity ID is not created, please verify");
			System.out.println("Opportunity Link is not found in C4C Page");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
	
		return "True";
	}
	
	public String editOpportunity(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : editOpportunity
		Purpose     : This method is to create new oppurtunity
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : String file_name, String sheet_name, String iScript, String iSubScript
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String Legal_Complexity = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Legal_Complexity");
		String Technical_Complexity = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Technical_Complexity");
		String Sales_Unit = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Sales_Unit");
		String Environment = dataminer.fngetconfigvalue(file_name, "Environment");
		
		Waittilljquesryupdated();
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.moreLnk,120)) {
//		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.expandLnk,120)) {
			WaitforC4Cloader();
    		WebInteractUtil.click(CPQ_Objects.moreLnk);
			WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.editOpportunityLnk,120);
			WebInteractUtil.click(CPQ_Objects.editOpportunityLnk);
			WaitforC4Cloader();
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.legalComplexityLnk,120)) {
				if (Environment.equalsIgnoreCase("RFS")) {
					WebInteractUtil.isEnabled(CPQ_Objects.salesUnitTxb);
					WebInteractUtil.sendKeys(CPQ_Objects.salesUnitTxb, Sales_Unit);
					WaitforC4Cloader();
					CPQ_Objects.salesUnitTxb.sendKeys(Keys.chord(Keys.TAB));
					WaitforC4Cloader();
				}
//				WebInteractUtil.scrollIntoView(CPQ_Objects.legalComplexityLnk);
				WebInteractUtil.clickByJS(CPQ_Objects.legalComplexityLnk);
				WebInteractUtil.ClickonElementByString("//li[normalize-space(.)='"+Legal_Complexity+"']", 30);
//				WebInteractUtil.scrollIntoView(CPQ_Objects.techComplexityLnk);
				WebInteractUtil.clickByJS(CPQ_Objects.techComplexityLnk);
				WebInteractUtil.ClickonElementByString("//li[normalize-space(.)='"+Technical_Complexity+"']", 30);
				WebInteractUtil.pause(1500);
				WebInteractUtil.scrollIntoView(CPQ_Objects.saveBtn);
				WebInteractUtil.click(CPQ_Objects.saveBtn);
				WaitforC4Cloader();
				WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.headerBar,120);
//				WebInteractUtil.scrollIntoTop();
//				WebInteractUtil.click(CPQ_Objects.collapseLnk);
				String sText = CPQ_Objects.headerBar.getText();
				if (sText.contains("saved")) {
					ExtentTestManager.getTest().log(LogStatus.PASS, "Opportunity edit operation is success with the message "+sText);
					System.out.println("Opportunity edit operation is success with the message "+sText);
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to Edit Oppurtunity due to "+sText);
					System.out.println("Unable to Edit Oppurtunity due to "+sText);
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Edit Opportunity page is not visible, please verify");
				System.out.println("Edit Opportunity page is not visible, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to click on Edit Oppurtunity Link, please verify");
			System.out.println("Unable to click on Edit Oppurtunity Link, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
	}

	public String addQuoteInC4C() throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : addQuoteInC4C
		Purpose     : This method is to add a quote from oppurtunity tab
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.quotesLnk,75)) {
			WebInteractUtil.click(CPQ_Objects.quotesLnk);
			for (int i = 1; i < 3; i++) { WaitforC4Cloader(); }
			WebInteractUtil.isEnabled(CPQ_Objects.addQuoteBtn);
			WebInteractUtil.scrollIntoView(CPQ_Objects.addQuoteBtn);
			WaitforC4Cloader();
			WebInteractUtil.click(CPQ_Objects.addQuoteBtn);
			WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.quoteNameTxb, 240);
			WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.seRevLnk, 120);
			WebInteractUtil.isPresent(CPQ_Objects.quoteTypeElem, 180);
			WaitforCPQloader();
			String quoteType = CPQ_Objects.quoteTypeElem.getAttribute("value");
			String quoteID = CPQ_Objects.quoteIDElem.getAttribute("value");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Quote ID is generated "+quoteID);
			System.out.println("Quote ID is generated "+quoteID);
			if (quoteType.equals("Standard")) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Quote Type is Standard in CPQ");
				return quoteID;
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Quote Type is not Standard, Please Verify");
				System.out.println("Unable to click on Quotes Link, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
			
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to click on Quotes Link, please verify");
			System.out.println("Unable to click on Quotes Link, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
	}
	
	public String deleteProductCPQ(String ProductName, int iRow ) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : deleteProductCPQ
		Purpose     : This method is to delete the product from CPQ
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String sResult, sProduct_Name;
//		Selecting the product
		sProduct_Name = ProductName.replaceAll("(?!^)([A-Z])", " $1");
		WebInteractUtil.scrollIntoBottom();
		waitForpageloadmask();
		
//		Clicking the WebTable
		sResult = MultiLineWebTableCellAction("Product", sProduct_Name, null,"Click", null, iRow);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		waitForpageloadmask();
		
//		clicking on Reconfiguration link
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.deleteProductBtn, 45)) {
			WebInteractUtil.click(CPQ_Objects.deleteProductBtn);
			waitForpageloadmask();
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.okdialogButton, 6)) {
				WebInteractUtil.click(CPQ_Objects.okdialogButton);
	    		for (int i = 1; i < 3; i++) { waitForpageloadmask(); }
			} else {
//				Clicking the WebTable
				sResult = MultiLineWebTableCellAction("Product", sProduct_Name, null,"Click", null, iRow);
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
				waitForpageloadmask();
				WebInteractUtil.isPresent(CPQ_Objects.deleteProductBtn, 5);
				WebInteractUtil.click(CPQ_Objects.deleteProductBtn);
				waitForpageloadmask();
				WebInteractUtil.isPresent(CPQ_Objects.okdialogButton, 5);
				WebInteractUtil.click(CPQ_Objects.okdialogButton);
				for (int i = 1; i < 3; i++) { waitForpageloadmask(); }
			}
    		
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "deleteProductBtn is not visible in CPQ, please verify");
			System.out.println("deleteProductBtn is not visible in CPQ, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
	
		return "True";
	}
	
	public String addProductCPQ(String ProductName, String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : addQuoteInC4C
		Purpose     : This method is to add a quote from oppurtunity tab
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		String StartTime = null;
		String tfile_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.addProductBtn, 180)) {
			WebInteractUtil.click(CPQ_Objects.addProductBtn);
			WebInteractUtil.pause(1500);
			switch (ProductName)  {
				case "CpeSolutionsSite":
					WebInteractUtil.isPresent(CPQ_Objects.dataLink, 60);
					WebInteractUtil.click(CPQ_Objects.dataLink);
					WebInteractUtil.isPresent(CPQ_Objects.cpeSolutionLnk, 180);
					WebInteractUtil.click(CPQ_Objects.cpeSolutionLnk);
					WebInteractUtil.scrollIntoView(CPQ_Objects.cpeSolutionLnk);
					WebInteractUtil.isPresent(CPQ_Objects.cpeSiteLnk, 180);
					WebInteractUtil.click(CPQ_Objects.cpeSiteLnk);
					break;
				case "EthernetLine":
					WebInteractUtil.isPresent(CPQ_Objects.dataLink, 60);
					WebInteractUtil.click(CPQ_Objects.dataLink);
					WebInteractUtil.isPresent(CPQ_Objects.ethernetLnk, 180);
					WebInteractUtil.click(CPQ_Objects.ethernetLnk);
					WebInteractUtil.scrollIntoView(CPQ_Objects.ethernetLineLnk);
					WebInteractUtil.isPresent(CPQ_Objects.ethernetLineLnk, 180);
					WebInteractUtil.click(CPQ_Objects.ethernetLineLnk);
					break;
				case "EthernetHub":
					WebInteractUtil.isPresent(CPQ_Objects.dataLink, 60);
					WebInteractUtil.click(CPQ_Objects.dataLink);
					WebInteractUtil.isPresent(CPQ_Objects.ethernetLnk, 180);
					WebInteractUtil.click(CPQ_Objects.ethernetLnk);
					WebInteractUtil.scrollIntoView(CPQ_Objects.ethernetHubLnk);
					WebInteractUtil.isPresent(CPQ_Objects.ethernetHubLnk, 180);
					WebInteractUtil.click(CPQ_Objects.ethernetHubLnk);
					break;
				case "EthernetSpoke":
					WebInteractUtil.isPresent(CPQ_Objects.dataLink, 60);
					WebInteractUtil.click(CPQ_Objects.dataLink);
					WebInteractUtil.isPresent(CPQ_Objects.ethernetLnk, 180);
					WebInteractUtil.click(CPQ_Objects.ethernetLnk);
					WebInteractUtil.scrollIntoView(CPQ_Objects.ethernetSpokeLnk);
					WebInteractUtil.isPresent(CPQ_Objects.ethernetSpokeLnk, 180);
					WebInteractUtil.click(CPQ_Objects.ethernetSpokeLnk);
					break;
				case "Wave":
					WebInteractUtil.isPresent(CPQ_Objects.dataLink, 60);
					WebInteractUtil.click(CPQ_Objects.dataLink);
					WebInteractUtil.isPresent(CPQ_Objects.opticalLnk, 180);
					WebInteractUtil.click(CPQ_Objects.opticalLnk);
					WebInteractUtil.scrollIntoView(CPQ_Objects.waveLnk);
					WebInteractUtil.isPresent(CPQ_Objects.waveLnk, 180);
					WebInteractUtil.click(CPQ_Objects.waveLnk);
					break;
				case "ColtIpAccess":
					WebInteractUtil.isPresent(CPQ_Objects.dataLink, 60);
					WebInteractUtil.click(CPQ_Objects.dataLink);
					WebInteractUtil.isPresent(CPQ_Objects.IPAccessLnk, 180);
					WebInteractUtil.click(CPQ_Objects.IPAccessLnk);
					WebInteractUtil.scrollIntoView(CPQ_Objects.ColtIpAccessLnk);
					WebInteractUtil.isPresent(CPQ_Objects.ColtIpAccessLnk, 30);
					WebInteractUtil.click(CPQ_Objects.ColtIpAccessLnk);
					break;
			}
			
			// Capturing Start point of Transaction Capture
			StartTime = dateTimeUtil.fnGetCurrentTime();
			
			waitForpageloadmask();
			if (!ProductName.equalsIgnoreCase("ColtIpAccess")) { 
				WebInteractUtil.switchToFrame("siteAddressLink");
				waitForpageloadmask();
				WebInteractUtil.switchToDefaultFrame();
				waitForpageloadmask();
			} else {
				waitForpageloadmask();
				WebInteractUtil.isPresent(CPQ_Objects.ColtIpAccessStartLnk, 90); 
				for (int i = 1; i<= 2; i++) {waitForpageloadmask(); }
			}
			
			// Capturing End point of Transaction Capture
			String EndTime = dateTimeUtil.fnGetCurrentTime();
			
			// Computing Difference between Transactions Capture
			String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
			System.out.println("TimeDiff is "+TimeDiff);
			
//			Entering the Values to the Data sheet
			dataminer.fnsetTransactionValue(tfile_name, UI_Type, TransactionID, ColName, TimeDiff);
			
			if (ProductName.equalsIgnoreCase("ColtIpAccess")) { 
				WebInteractUtil.click(CPQ_Objects.ColtIpAccessStartLnk); 
				waitForpageloadmask();
			}
			
			ExtentTestManager.getTest().log(LogStatus.PASS, "Product "+ProductName+" successfully added in CPQ Journey");
			System.out.println("Product "+ProductName+" successfully added in CPQ Journey");
			
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Add Product Button is not visible in CPQ, please verify");
			System.out.println("Add Product Button is not visible in CPQ, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return StartTime;
	}
	
	public String cpeServiceConfiguration(String file_name, String Sheet_Name, String iScript, String iSubScript) throws InterruptedException, IOException {
		/*----------------------------------------------------------------------
		Method Name : cpeServiceConfiguration
		Purpose     : This method is to configure CPQ product in CPQ
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String Network_Topology = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Network_Topology");
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.networkTopologyLst, 180)) {
			WebInteractUtil.scrollIntoView(CPQ_Objects.networkTopologyLst);
			WebInteractUtil.clickByAction(CPQ_Objects.networkTopologyLst);
			WebInteractUtil.pause(500);
			WebInteractUtil.selectByValue(CPQ_Objects.networkTopologyLst, Network_Topology);
			waitForpageloadmask();
			WebInteractUtil.scrollIntoView(CPQ_Objects.saveToQuoteBtn);
			WebInteractUtil.click(CPQ_Objects.saveToQuoteBtn);
			WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.quoteNameTxb, 240);
			waitForpageloadmask();
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "CPE Solution Config page is not visible in CPQ, please verify");
			System.out.println("CPE Solution Config page is not visible in CPQ, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
	}
	
	public String cpeSiteConfiguration(String file_name, String Sheet_Name, String iScript, String iSubScript, String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : cpeServiceConfiguration
		Purpose     : This method is to configure CPQ product in CPQ
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String CPE_Service_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"CPE_Service_Type");
		String Related_Network_Reference = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Related_Network_Reference");
		String Network_Reference_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Network_Reference_Type");
		String sResult;
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.cpeSiteConfigurationLnk, 180)) {
			waitForpageloadmask();
			WebInteractUtil.isPresent(CPQ_Objects.cpeSolutionServiceLst, 180);
			WebInteractUtil.scrollIntoView(CPQ_Objects.cpeSolutionServiceLst);
			WebInteractUtil.selectByValue(CPQ_Objects.cpeSolutionServiceLst, CPE_Service_Type);
			waitForpageloadmask();
			WebInteractUtil.isPresent(CPQ_Objects.defSelectServiceBtn, 180);
			WebInteractUtil.click(CPQ_Objects.defSelectServiceBtn);
			waitForpageloadmask();
//			WebInteractUtil.isPresent(CPQ_Objects.relatedNetworkProductLnk, 180);
//			WebInteractUtil.click(CPQ_Objects.relatedNetworkProductLnk);
			WebInteractUtil.isPresent(CPQ_Objects.relatedNetworkProductLst, 180);
			WebInteractUtil.selectByValue(CPQ_Objects.relatedNetworkProductLst, Network_Reference_Type);
			waitForpageloadmask();
			WebInteractUtil.isPresent(CPQ_Objects.relatedNetworkReferenceTxb, 180);
			WebInteractUtil.sendKeys(CPQ_Objects.relatedNetworkReferenceTxb, Related_Network_Reference);
			WebInteractUtil.click(CPQ_Objects.relatedNetworkProductLst);
			waitForpageloadmask();
			
//			calling the below method to enter the site address
//			sResult = siteAddress(file_name, "A_End", iScript, iSubScript);
//			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
//			calling the belo method to enter the cpe configuration details
			sResult = cpeConfiguration(file_name, Sheet_Name, iScript, iSubScript);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
//			calling the below method to enter the additional product data
			sResult = addtionalProductdata(file_name, "A_End", iScript, iSubScript, UI_Type, TransactionID, ColName);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "CPE Site Config page is not visible in CPQ, please verify");
			System.out.println("CPE Site Config page is not visible in CPQ, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
	}
	
	public String siteAddress(String file_name, String Sheet_Name, String iScript, String iSubScript, String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : siteAddress
		Purpose     : This method will add the address to the CPQ
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		String Product_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		String Flow_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Flow_Type");
		String sResult;
		String[] sTransactionsID = TransactionID.split("\\|");
		String tfile_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		
//		waiting for the page to get loaded
		for (int i = 1; i < 3; i++) { waitForpageloadmask(); }
//		Initializing the driver
		WebDriver driver = DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
		
		switch (Product_Name) {
		
			case "EthernetLine": case "EthernetHub": case "Wave":
				
//				Site A Address Entry
				if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.siteAAddressTxb, 120)) {
//					Reading the Address from the data sheet
					String SiteA_Address = dataminer.fngetcolvalue(file_name, "A_End", iScript, iSubScript,"SiteA_Address");
					String SiteA_Address_Type = dataminer.fngetcolvalue(file_name, "A_End", iScript, iSubScript,"SiteA_Address_Type");
					String Phone_Number = dataminer.fngetcolvalue(file_name, "A_End", iScript, iSubScript,"Phone_Number");
					waitForpageloadmask();
					WebInteractUtil.scrollIntoView(CPQ_Objects.siteAAddressTxb);
					WebInteractUtil.sendKeysByJS(CPQ_Objects.siteAAddressTxb, SiteA_Address);
					WebInteractUtil.click(CPQ_Objects.siteASearchImg);
					// Capturing Start point of Transaction Capture
					String StartTime = dateTimeUtil.fnGetCurrentTime();
					WebInteractUtil.pause(2500);
					waitForpageloadmask();
					//By executing a java script
					driver.switchTo().frame("siteAddressLink");
					//Validate address type
					String EndTime = addressTypeConfiguration(SiteA_Address,SiteA_Address_Type);
					if (EndTime.equalsIgnoreCase("False")) { return "False"; }
								
					// Computing Difference between Transactions Capture
					String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
					System.out.println("TimeDiff is "+TimeDiff);
								
					//Entering the Values to the Data sheet
					dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[0], ColName, TimeDiff);
					
//					Entering Site Contact Number if the flow type is Manual/Automated DSL
					if (Flow_Type.equalsIgnoreCase("ManualDSL") || Flow_Type.equalsIgnoreCase("ManualOLODSL")) { 
						WebInteractUtil.sendKeys(CPQ_Objects.siteAPhoneNumberTxb, Phone_Number); 
					}
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Site A address text box is not visible in CPQ, please verify");
					System.out.println("Site A address text box is not visible in CPQ, please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}
				
//				Site B Address Entry
				if (!Product_Name.equalsIgnoreCase("EthernetHub")) {
//					Reading the SiteAddress details
					String SiteB_Address = dataminer.fngetcolvalue(file_name, "B_End", iScript, iSubScript,"SiteB_Address");
					String SiteB_Address_Type = dataminer.fngetcolvalue(file_name, "B_End", iScript, iSubScript,"SiteB_Address_Type");
					String Phone_Number = dataminer.fngetcolvalue(file_name, "B_End", iScript, iSubScript,"Phone_Number");
					if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.siteBAddressTxb, 120)) {
						waitForpageloadmask();
						WebInteractUtil.scrollIntoView(CPQ_Objects.siteBAddressTxb);
						WebInteractUtil.sendKeysByJS(CPQ_Objects.siteBAddressTxb, SiteB_Address);
						WebInteractUtil.click(CPQ_Objects.siteBSearchImg);
						
						// Capturing Start point of Transaction Capture
						String StartTime = dateTimeUtil.fnGetCurrentTime();
						
						WebInteractUtil.pause(2500);
						waitForpageloadmask();
						//By executing a java script
						driver.switchTo().frame("siteAddressLink");
						//Validate address type
						String EndTime = addressTypeConfiguration(SiteB_Address,SiteB_Address_Type);
						if (EndTime.equalsIgnoreCase("False")) { return "False"; }
									
						// Computing Difference between Transactions Capture
						String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
						System.out.println("TimeDiff is "+TimeDiff);
									
						//Entering the Values to the Data sheet
						dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[1], ColName, TimeDiff);
						
						if (Flow_Type.equalsIgnoreCase("ManualDSL") || Flow_Type.equalsIgnoreCase("ManualOLODSL")) { 
							WebInteractUtil.sendKeys(CPQ_Objects.siteBPhoneNumberTxb, Phone_Number); 
						}
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Site B address text box is not visible in CPQ, please verify");
						System.out.println("Site B address text box is not visible in CPQ, please verify");
						ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
						return "False";
					}
				}
				break;
				
			case "EthernetSpoke":
				
//				Configuring Hub part if the network type is existing
				String SiteA_Address = dataminer.fngetcolvalue(file_name, "A_End", iScript, iSubScript,"SiteA_Address");
				String SiteB_Address = dataminer.fngetcolvalue(file_name, "B_End", iScript, iSubScript,"SiteB_Address");
				String Hub_Type = dataminer.fngetcolvalue(file_name, "B_End", iScript, iSubScript,"Hub_Type");
				String Hub_Reference = dataminer.fngetcolvalue(file_name, "B_End", iScript, iSubScript,"Hub_Reference");
				if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.hubTypeLst, 45)) {
					String SiteA_Address_Type = dataminer.fngetcolvalue(file_name, "A_End", iScript, iSubScript,"SiteA_Address_Type");
					WebInteractUtil.selectByValue(CPQ_Objects.hubTypeLst, Hub_Type);
					waitForpageloadmask();
					WebInteractUtil.sendKeys(CPQ_Objects.hubReferenceTxb, Hub_Reference);
					waitForpageloadmask();
					WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.hubAddressTxb, 120);
					WebInteractUtil.sendKeysByJS(CPQ_Objects.hubAddressTxb, SiteA_Address);
					WebInteractUtil.pause(1500);
					WebInteractUtil.click(CPQ_Objects.hubSearchImgLnk);
					// Capturing Start point of Transaction Capture
					String StartTime = dateTimeUtil.fnGetCurrentTime();
					WebInteractUtil.pause(2500);
					waitForpageloadmask();
					//By executing a java script
					driver.switchTo().frame("siteAddressLink");
					//Validate Address
					String EndTime = addressTypeConfiguration(SiteA_Address,SiteA_Address_Type);
					if (EndTime.equalsIgnoreCase("False")) {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Site A address text box is not visible in CPQ, please verify");
						System.out.println("Site A address text box is not visible in CPQ, please verify");
						ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
						return "False";
					}
					// Computing Difference between Transactions Capture
					String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
					System.out.println("TimeDiff is "+TimeDiff);
								
					//Entering the Values to the Data sheet
					dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[0], ColName, TimeDiff);
					
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "HubType Listbox for Spoke Product is not visible in CPQ, please verify");
					System.out.println("HubType Listbox for Spoke Product is not visible in CPQ, please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}
				
//				Configuring Spoke Part
				if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.siteAAddressTxb, 45)) {
					String SiteB_Address_Type = dataminer.fngetcolvalue(file_name, "B_End", iScript, iSubScript,"SiteB_Address_Type");
					String Phone_Number = dataminer.fngetcolvalue(file_name, "A_End", iScript, iSubScript,"Phone_Number");
					WebInteractUtil.sendKeysByJS(CPQ_Objects.siteAAddressTxb, SiteB_Address);
					WebInteractUtil.click(CPQ_Objects.spokeSearchImgLnk);
					// Capturing Start point of Transaction Capture
					String StartTime = dateTimeUtil.fnGetCurrentTime();
					WebInteractUtil.pause(4000);
					waitForpageloadmask();
					//By executing a java script
					driver.switchTo().frame("siteAddressLink");
					//Validate Address
					String EndTime = addressTypeConfiguration(SiteB_Address,SiteB_Address_Type);
					if (Flow_Type.equalsIgnoreCase("ManualDSL") || Flow_Type.equalsIgnoreCase("ManualOLODSL")) { 
						WebInteractUtil.sendKeys(CPQ_Objects.siteAPhoneNumberTxb, Phone_Number); 
					}
					if (EndTime.equalsIgnoreCase("False")) { 
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Site B address text box is not visible in CPQ, please verify");
						System.out.println("Site B address text box is not visible in CPQ, please verify");
						ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
						return "False";
					}
					
					// Computing Difference between Transactions Capture
					String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
					System.out.println("TimeDiff is "+TimeDiff);
								
					//Entering the Values to the Data sheet
					dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[1], ColName, TimeDiff);
					
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "HubType Listbox for Spoke Product is not visible in CPQ, please verify");
					System.out.println("HubType Listbox for Spoke Product is not visible in CPQ, please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}
				break;
			
				
				
			case "ColtIpAccess":
				
//				Site A Address Entry
				if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.siteAAddressTxb, 120)) {
//					Reading the Address from the data sheet
					SiteA_Address = dataminer.fngetcolvalue(file_name, "A_End", iScript, iSubScript,"SiteA_Address");
					String SiteA_Address_Type = dataminer.fngetcolvalue(file_name, "A_End", iScript, iSubScript,"SiteA_Address_Type");
					String Phone_Number = dataminer.fngetcolvalue(file_name, "A_End", iScript, iSubScript,"Phone_Number");
					WebInteractUtil.scrollIntoView(CPQ_Objects.siteAAddressTxb);
					WebInteractUtil.sendKeysByJS(CPQ_Objects.siteAAddressTxb, SiteA_Address);
					WebInteractUtil.click(CPQ_Objects.siteASearchImg);
					
					// Capturing Start point of Transaction Capture
					String StartTime = dateTimeUtil.fnGetCurrentTime();
					
					WebInteractUtil.pause(4000);
					waitForpageloadmask();
					//By executing a java script
					driver.switchTo().frame("siteAddressLink");
					//Validate address type
					String EndTime = addressTypeConfiguration(SiteA_Address,SiteA_Address_Type);
					if (Flow_Type.equalsIgnoreCase("ManualDSL") || Flow_Type.equalsIgnoreCase("ManualOLODSL")) { 
						WebInteractUtil.sendKeys(CPQ_Objects.siteAPhoneNumberTxb, Phone_Number); 
					}
					if (EndTime.equalsIgnoreCase("False")) { return "False"; }
								
					// Computing Difference between Transactions Capture
					String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
					System.out.println("TimeDiff is "+TimeDiff);
								
					//Entering the Values to the Data sheet
					dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[0], ColName, TimeDiff);
					
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Site A address text box is not visible in CPQ, please verify");
					System.out.println("Site A address text box is not visible in CPQ, please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}
				break;
		}
		
		//Partial Save of the Operation
		if (!Product_Name.equalsIgnoreCase("ColtIpAccess")) {
			if (WebInteractUtil.isPresent(CPQ_Objects.partialSaveAddressCbx, 120)) {
				waitForpageloadmask();
				WebInteractUtil.click(CPQ_Objects.nextBtn);
				
				// Capturing Start point of Transaction Capture
				String StartTime = dateTimeUtil.fnGetCurrentTime();
				
				for (int i = 1; i < 3; i++) {waitForpageloadmask(); }
				//By executing a java script
				driver.switchTo().frame("siteAddressLink");
				waitForpageloadmask();
				 // Switch back to first browser window
				driver.switchTo().defaultContent();
				
				// Capturing End point of Transaction Capture
				String EndTime = dateTimeUtil.fnGetCurrentTime();
							
				// Computing Difference between Transactions Capture
				String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
				System.out.println("TimeDiff is "+TimeDiff);
							
				//Entering the Values to the Data sheet
				dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[2], ColName, TimeDiff);
				
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Next Button is not visible in CPQ, please verify");
				System.out.println("Next Button is not visible in CPQ, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		}
		if (Product_Name.equalsIgnoreCase("ColtIpAccess")) {
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipPartialSaveAddressCbx, 120)) {
				waitForpageloadmask();
				WebInteractUtil.click(CPQ_Objects.cpeIPAcessNextBtn);
				
				// Capturing Start point of Transaction Capture
				String StartTime = dateTimeUtil.fnGetCurrentTime();
				
				waitForpageloadmask();
				
				if (Flow_Type.equalsIgnoreCase("ManualDSL") || Flow_Type.equalsIgnoreCase("ManualOLODSL")) { 
					WebInteractUtil.click(CPQ_Objects.cpeIPAcessNextBtn);
					waitForpageloadmask();
				}
				
				// Capturing End point of Transaction Capture
				String EndTime = dateTimeUtil.fnGetCurrentTime();
							
				// Computing Difference between Transactions Capture
				String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
				System.out.println("TimeDiff is "+TimeDiff);
							
				//Entering the Values to the Data sheet
				dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[2], ColName, TimeDiff);
			}
			else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Next Button is not visible in CPQ, please verify");
				System.out.println("Next Button is not visible in CPQ, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		}
		
		return "True";
		
	}
	
	public String addressTypeConfiguration(String Site_Address, String Site_Address_Type) throws IOException, InterruptedException {
	/*----------------------------------------------------------------------
	Method Name : addressTypeConfiguration
	Purpose     : This method will validate the address entires in Mapper
	Designer    : Kashyap
	Created on  : 23rd June 2020 
	Input       : String Site_Address, String Site_Address_Type
	Output      : True/False
	 ----------------------------------------------------------------------*/ 
		String EndTime = null;
		
		switch (Site_Address_Type.toUpperCase()) {
		
		case "ONNET": case "MANUALDSL": case "AUTOMATEDDSL": case "MANUALOLODSL": case "ONNETDUALENTRY":
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.connectedViaColtElem, 180)) {
				waitForpageloadmask();
				
				// Capturing End point of Transaction Capture
				EndTime = dateTimeUtil.fnGetCurrentTime();
				
				WebInteractUtil.scrollIntoView(CPQ_Objects.connectedViaColtElem);
				// Switch back to first browser window
				driver.switchTo().defaultContent();
				ExtentTestManager.getTest().log(LogStatus.PASS,"Address " + Site_Address + " selected/entered successfully");
				System.out.println("Address " + Site_Address + " selected/entered successfully");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL,"Address " + Site_Address + " not visible in Address doctor, Please verify");
				System.out.println("Address " + Site_Address + " not visible in Address doctor, Please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
				}
			break;
		

		case "MANUALOFFNET": case "AUTOMATEDOFFNET": 
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.connectedViaColtElem, 180)) {
				waitForpageloadmask();
				
				// Capturing End point of Transaction Capture
				EndTime = dateTimeUtil.fnGetCurrentTime();
				
				WebInteractUtil.scrollIntoView(CPQ_Objects.connectedViaColtElem);
				// Switch back to first browser window
				driver.switchTo().defaultContent();
				ExtentTestManager.getTest().log(LogStatus.PASS,"Address " + Site_Address + " selected/entered successfully");
				System.out.println("Address " + Site_Address + " selected/entered successfully");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL,"Address " + Site_Address + " not visible in Address doctor, Please verify");
				System.out.println("Address " + Site_Address + " not visible in Address doctor, Please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
				}
				break;
				
		case "OLOCOMBINATION": 
			
			int i = 0;
			
			for (i=1; i<60; i++) {
				if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.connectedViaColtElem, 1)) {
					break;
				}
				
				if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.connectionNotFound, 1)) {
					break;
				}
				
				if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.automatedNearnetElem, 1)) {
					break;
				}
				
				if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.dataCenterConnectedElem, 1)) {
					break;
				}
				
				Thread.sleep(1000);
				continue;
			}
			
			if (i >=60) {
				ExtentTestManager.getTest().log(LogStatus.FAIL,"Address " + Site_Address + " not visible in Address doctor, Please verify");
				System.out.println("Address " + Site_Address + " not visible in Address doctor, Please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
			
			waitForpageloadmask();
			
			// Capturing End point of Transaction Capture
			EndTime = dateTimeUtil.fnGetCurrentTime();
			// Switch back to first browser window
			driver.switchTo().defaultContent();
			ExtentTestManager.getTest().log(LogStatus.PASS,"Address " + Site_Address + " selected/entered successfully");
			System.out.println("Address " + Site_Address + " selected/entered successfully");
			break;			
		}
		return EndTime;
	}
	
	public String cpeConfiguration(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : cpeConfiguration
		Purpose     : This method is to configure CPE features in CPQ
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String Connection_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Connection_Type");
		String Service_Bandwidth = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Service_Bandwidth");
		String Managed_CPE = dataminer.fngetcolvalue(file_name, "A_End", iScript, iSubScript,"Managed_CPE");
		String CPE_Solution_Type = dataminer.fngetcolvalue(file_name, "A_End", iScript, iSubScript,"CPE_Solution_Type");
		
//		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.cpeConfigurationLnk, 180)) {
//			WebInteractUtil.scrollIntoView(CPQ_Objects.cpeConfigurationLnk);
//			WebInteractUtil.clickByJS(CPQ_Objects.cpeConfigurationLnk);
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.connectionTypeLst,75)) {
			WebInteractUtil.selectByValue(CPQ_Objects.connectionTypeLst, Connection_Type);
			WebInteractUtil.click(CPQ_Objects.cpeModelLabel);
			waitForpageloadmask();
			WebInteractUtil.selectByValue(CPQ_Objects.serviceBandwidthLst, Service_Bandwidth);
			WebInteractUtil.click(CPQ_Objects.cpeModelLabel);
			waitForpageloadmask();
			WebInteractUtil.selectByValue(CPQ_Objects.managedCPELst, Managed_CPE);
			WebInteractUtil.click(CPQ_Objects.cpeModelLabel);
			waitForpageloadmask();
			WebInteractUtil.selectByValue(CPQ_Objects.cpeSolutionTypeLst, CPE_Solution_Type);
			WebInteractUtil.click(CPQ_Objects.cpeModelLabel);
			waitForpageloadmask();
			
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "CPE configuration  address Link is not visible in CPQ, please verify");
			System.out.println("CPE address link is not visible in CPQ, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		ExtentTestManager.getTest().log(LogStatus.PASS, "CPE Features have been added successfully");
		System.out.println("CPE Features have been added successfully");
		ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
		return "True";
		
	}
	
	public String offnetConfiguration(String file_name, String Sheet_Name, String iScript, String iSubScript, String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : offnetConfiguration
		Purpose     : This method is to configure offnet features of the respective iterations
		Designer    : Vasantharaja C
		Created on  : 24th June 2020 
		Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String Product_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		String Flow_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Flow_Type");
		String sResult;
		String[] sTransactionsID = TransactionID.split("\\|");
		
		switch (Flow_Type.toUpperCase()) {
				
			case "MANUALOFFNET": case "MANUALDSL":
				
//				Entering A-End details
				sResult = offnetEntries(file_name, "A_End", iScript, iSubScript, UI_Type, sTransactionsID[0] +"|"+ sTransactionsID[1]+"|"+ sTransactionsID[2], ColName);
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
				
//				Entering B-End details
				if (Product_Name.equalsIgnoreCase("EthernetLine")) {
					sResult = offnetEntries(file_name, "B_End", iScript, iSubScript, UI_Type, sTransactionsID[3] +"|"+ sTransactionsID[4]+"|"+ sTransactionsID[5], ColName);
					if (sResult.equalsIgnoreCase("False")){ return "False"; }
				}
				break;
				
			case "AUTOMATEDOFFNET":
				
//				Entering A-End details
				sResult = automatedOffnetEntries(file_name, "A_End", iScript, iSubScript);
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
				
//				Entering B-End details
				if (Product_Name.equalsIgnoreCase("EthernetLine")) {
					sResult = automatedOffnetEntries(file_name, "B_End", iScript, iSubScript);
					if (sResult.equalsIgnoreCase("False")){ return "False"; }
				}
				break;
		}
		
		
		return "True";
		
	}
	
	public String offnetandNearNetConfigurationWave(String file_name, String Sheet_Name, String iScript, String iSubScript, String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : offnetConfigurationWave
		Purpose     : This method is to configure offnet features of the respective iterations
		Designer    : Kashyap D
		Created on  : 24th June 2020 
		Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String Flow_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Flow_Type");
		String Product_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		String sResult;
		String[] sTransactionsID = TransactionID.split("\\|");
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.siteDetailsLnk, 120)) {
			WebInteractUtil.click(CPQ_Objects.siteDetailsLnk);
			waitForpageloadmask();
		}
		
		switch (Flow_Type.toUpperCase()) {
				
			case "MANUALOFFNET":				
				
//				Entering A-End details
				sResult = offnetEntriesWave(file_name, "A_End", iScript, iSubScript, UI_Type, sTransactionsID[0] +"|"+ sTransactionsID[1]+"|"+ sTransactionsID[2], ColName);
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
				
//				Entering B-End details
				if (Product_Name.equalsIgnoreCase("EthernetLine")) {
					sResult = offnetEntriesWave(file_name, "B_End", iScript, iSubScript, UI_Type, sTransactionsID[3] +"|"+ sTransactionsID[4]+"|"+ sTransactionsID[5], ColName);
					if (sResult.equalsIgnoreCase("False")){ return "False"; }
				}
				break;				
		}
		
		waitForpageloadmask();
		if(WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.offnetWaveincrementalCapex,75)) {
			WebInteractUtil.scrollIntoView(CPQ_Objects.offnetWaveincrementalCapex);
			WebInteractUtil.selectByValue(CPQ_Objects.offnetWaveincrementalCapex, "Incremental");
			waitForpageloadmask();
			WebInteractUtil.selectByValue(CPQ_Objects.offnetWaveincrementalOpex, "Incremental");
			waitForpageloadmask();
			WebInteractUtil.selectByValue(CPQ_Objects.offnetWaveFrequency, "Monthly");
			waitForpageloadmask();
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "offnet Incremental is still not visible in CPQ even after click, please verify");
			System.out.println("offnet Incremental is still not visible in CPQ even after click, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
	}
	
	public String offnetEntries(String file_name, String Sheet_Name, String iScript, String iSubScript, String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : offnetEntries
		Purpose     : This method is to configure offnet features of the respective iterations
		Designer    : Vasantharaja C
		Created on  : 24th June 2020 
		Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String Product_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		String Explore_Options = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Explore_Options");	
		String Priority = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Priority");
		String Connection_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Connection_Type");
		WebElement offnetCheckBtn = null; WebElement manualEngagementBtn = null; WebElement exploreOptionsLsb = null;
		
		String[] sTransactionsID = TransactionID.split("\\|");
		String tfile_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		String StartTime = null, EndTime = null, TimeDiff = null;
		
		switch (Sheet_Name) {
		
		case "A_End":
			offnetCheckBtn = CPQ_Objects.offnetCheckAEndBtn;
			manualEngagementBtn = CPQ_Objects.manualEngagementAEndBtn;
			exploreOptionsLsb = CPQ_Objects.exploreActionsAEndLst;
			break;
			
		case "B_End":
			offnetCheckBtn = CPQ_Objects.offnetCheckBEndBtn;
			manualEngagementBtn = CPQ_Objects.manualEngagementBEndBtn;
			exploreOptionsLsb = CPQ_Objects.exploreActionsBEndLst;
			break;
		}
		if(Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("EthernetHub")||
				Product_Name.equalsIgnoreCase("EthernetSpoke")&&!Product_Name.equalsIgnoreCase("ColtIpAccess")&&
				!Product_Name.equalsIgnoreCase("Wave")&&!Product_Name.equalsIgnoreCase("VPNNetwork")) {
			if (WebInteractUtil.waitForElementToBeVisible(offnetCheckBtn,75)) {
				WebInteractUtil.scrollIntoView(offnetCheckBtn);
				WebInteractUtil.click(offnetCheckBtn);
				// Capturing Start point of Transaction Capture
				StartTime = dateTimeUtil.fnGetCurrentTime();
				waitForpageloadmask();
				// Capturing End point of Transaction Capture
				EndTime = dateTimeUtil.fnGetCurrentTime();
							
				// Computing Difference between Transactions Capture
				TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
				System.out.println("TimeDiff is "+TimeDiff);
				
				//Entering the Values to the Data sheet
				dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[0], ColName, TimeDiff);
				
				WebInteractUtil.click(manualEngagementBtn);
				// Capturing Start point of Transaction Capture
				StartTime = dateTimeUtil.fnGetCurrentTime();
				waitForpageloadmask();
				// Capturing End point of Transaction Capture
				EndTime = dateTimeUtil.fnGetCurrentTime();
							
				// Computing Difference between Transactions Capture
				TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
				System.out.println("TimeDiff is "+TimeDiff);
				
				//Entering the Values to the Data sheet
				dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[1], ColName, TimeDiff);
				if (WebInteractUtil.waitForElementToBeVisible(exploreOptionsLsb,75)) {
					WebInteractUtil.isEnabled(exploreOptionsLsb);
					waitForpageloadmask();
					WebInteractUtil.selectByText(exploreOptionsLsb, Explore_Options);
					// Capturing Start point of Transaction Capture
					StartTime = dateTimeUtil.fnGetCurrentTime();
	//				WebInteractUtil.selectByValue(exploreOptionsLsb, Explore_Options);
					WebInteractUtil.switchToFrame("exploreEngagementComponent");
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "offnet check button is still visible in CPQ even after click, please verify");
					System.out.println("offnet check button is still visible in CPQ even after click, please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "offnetCheckBtn is not visible in Sitedetails page in CPQ, please verify");
				System.out.println("offnetCheckBtn is not visible in Sitedetails page in CPQ, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		}
		
		if (Product_Name.equalsIgnoreCase("ColtIpAccess")) {
			
			offnetCheckBtn = CPQ_Objects.ipOffnetCheckAEndBtn;
			manualEngagementBtn = CPQ_Objects.ipManualEngagementAEndBtn;
			exploreOptionsLsb = CPQ_Objects.ipExploreActionsAEndLst;
			
			if (WebInteractUtil.waitForElementToBeVisible(offnetCheckBtn,75)) {
				WebInteractUtil.scrollIntoView(offnetCheckBtn);
				WebInteractUtil.click(offnetCheckBtn);
				// Capturing Start point of Transaction Capture
				StartTime = dateTimeUtil.fnGetCurrentTime();
				waitForpageloadmask();
				// Capturing End point of Transaction Capture
				EndTime = dateTimeUtil.fnGetCurrentTime();
				// Computing Difference between Transactions Capture
				TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
				System.out.println("TimeDiff is "+TimeDiff);
				//Entering the Values to the Data sheet
				dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[0], ColName, TimeDiff);
				WebInteractUtil.click(manualEngagementBtn);
				// Capturing Start point of Transaction Capture
				StartTime = dateTimeUtil.fnGetCurrentTime();
				waitForpageloadmask();
				// Capturing End point of Transaction Capture
				EndTime = dateTimeUtil.fnGetCurrentTime();
				// Computing Difference between Transactions Capture
				TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
				System.out.println("TimeDiff is "+TimeDiff);
				//Entering the Values to the Data sheet
				dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[1], ColName, TimeDiff);
				if (WebInteractUtil.waitForElementToBeVisible(exploreOptionsLsb,75)) {
					WebInteractUtil.isEnabled(exploreOptionsLsb);
					waitForpageloadmask();
					WebInteractUtil.selectByText(exploreOptionsLsb, Explore_Options);
					// Capturing Start point of Transaction Capture
					StartTime = dateTimeUtil.fnGetCurrentTime();
	//				WebInteractUtil.selectByValue(exploreOptionsLsb, Explore_Options);
					WebInteractUtil.switchToFrame("exploreEngagementComponent");
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "offnet check button is still visible in CPQ even after click, please verify");
					System.out.println("offnet check button is still visible in CPQ even after click, please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "offnetCheckBtn is not visible in Sitedetails page in CPQ, please verify");
				System.out.println("offnetCheckBtn is not visible in Sitedetails page in CPQ, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		}
		
		if(!Product_Name.equalsIgnoreCase("Wave")) {
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.priorityTxb,75)) {
			waitForpageloadmask();
			// Capturing End point of Transaction Capture
			EndTime = dateTimeUtil.fnGetCurrentTime();
						
			// Computing Difference between Transactions Capture
			TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
			System.out.println("TimeDiff is "+TimeDiff);
			
			//Entering the Values to the Data sheet
			dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[2], ColName, TimeDiff);
			WebInteractUtil.sendKeysWithKeys(CPQ_Objects.priorityTxb, Priority, "Enter");
			waitForpageloadmask();
			if (Connection_Type.equalsIgnoreCase("DualEntry")) { WebInteractUtil.click(CPQ_Objects.dualEntryCbx); }
			WebInteractUtil.click(CPQ_Objects.getQuoteBtn);
			int k = 1;
			for (k=1; k < 10; k++) {
				try {
					WebDriver driver = DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
					driver.switchTo().alert().accept();
					WebInteractUtil.pause(2000);
					break;
				} catch (Exception e) {
					Thread.sleep(2000);
					continue;
				}
			}
			if (k >=10) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Alert was not visible in OLO popup, please verify");
				System.out.println("Alert was not visible in OLO popup, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
			WebInteractUtil.click(CPQ_Objects.expandArrowExploreIcn);
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.requestIDElem, 60)) {
				WebInteractUtil.isPresent(CPQ_Objects.requestIDElem, 30);
				String fullVal = CPQ_Objects.requestIDElem.getText();
				String[] bits = fullVal.split(":");
				String Request_ID = bits[bits.length-1].trim();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+Sheet_Name);
				System.out.println("Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+Sheet_Name);
//				Passing this request ID to testdata sheet
				dataminer.fnsetcolvalue(file_name, Sheet_Name, iScript, iSubScript, "Request_ID", Request_ID);
				WebInteractUtil.switchToDefaultFrame();
				if(Product_Name.equalsIgnoreCase("VPNNetwork")) {WebInteractUtil.click(CPQ_Objects.vpnExploreOffCloseBtn);WaitforVpnloader();}
				else{WebInteractUtil.click(CPQ_Objects.exploreCloseBtn);}
				waitForpageloadmask();
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "requestID in OLO Cost Summary Table is not generated in Explore, please verify");
				System.out.println("requestID in OLO Cost Summary Table is not generated in Explore, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "offnet check button is still visible in CPQ even after click, please verify");
			System.out.println("offnet check button is still visible in CPQ even after click, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
			}
		}
		
		if(Product_Name.equalsIgnoreCase("EthernetSpoke")) {
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.etherNetSpokeManualOffnet, 60)) {
				WebInteractUtil.click(CPQ_Objects.etherNetSpokeManualOffnet);
			}
			else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "EtherNetSpoke Radio button is not visible in CPQ even after Offnet, please verify");
				System.out.println("EtherNetSpoke Radio button is not visible in CPQ even after Offnet, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		}
		
		return "True";
		
	}
	
	public String automatedOffnetEntries(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : automatedOffnetEntries
		Purpose     : This method is to configure automated offnet features of the respective iterations
		Designer    : Vasantharaja C
		Created on  : 24th June 2020 
		Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String Product_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");		
		WebElement offnetCheckBtn = null; WebElement defOffnetRadioBtn = null; WebElement exploreOptionsLsb = null;
		
		switch (Sheet_Name) {
		
		case "A_End":
			offnetCheckBtn = CPQ_Objects.offnetCheckAEndBtn;
			defOffnetRadioBtn = CPQ_Objects.automatedOffnetDefAEndRadioBtn;
			break;
			
		case "B_End":
			offnetCheckBtn = CPQ_Objects.offnetCheckBEndBtn;
			defOffnetRadioBtn = CPQ_Objects.automatedOffnetDefBEndRadioBtn;
			break;
		}

		if(Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("EthernetHub")||
				Product_Name.equalsIgnoreCase("EthernetSpoke")&&!Product_Name.equalsIgnoreCase("ColtIpAccess")
				&&!Product_Name.equalsIgnoreCase("VPNNetwork")) {
		if (WebInteractUtil.waitForElementToBeVisible(offnetCheckBtn,75)) {
			WebInteractUtil.scrollIntoView(offnetCheckBtn);
			WebInteractUtil.click(offnetCheckBtn);
			waitForpageloadmask();
			WebInteractUtil.click(defOffnetRadioBtn);
			waitForpageloadmask();
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "offnetCheckBtn is not visible in Sitedetails page in CPQ, please verify");
			System.out.println("offnetCheckBtn is not visible in Sitedetails page in CPQ, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
			}
		}	
			
		if (Product_Name.equalsIgnoreCase("ColtIpAccess")) {
				offnetCheckBtn = CPQ_Objects.ipOffnetCheckAEndBtn;
		if (WebInteractUtil.waitForElementToBeVisible(offnetCheckBtn,75)) {
			WebInteractUtil.scrollIntoView(offnetCheckBtn);
			WebInteractUtil.click(offnetCheckBtn);
			waitForpageloadmask();
			if(WebInteractUtil.isPresent(CPQ_Objects.ipSupplierOnnetClose,20)) {
				WebInteractUtil.click(CPQ_Objects.ipSupplierOnnetClose);
				waitForpageloadmask();
			}
			WebInteractUtil.click(defOffnetRadioBtn);
			waitForpageloadmask();
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "offnetCheckBtn is not visible in Sitedetails page in CPQ, please verify");
			System.out.println("offnetCheckBtn is not visible in Sitedetails page in CPQ, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
			}
		}	
		
		if (Product_Name.equalsIgnoreCase("VPNNetwork")) {
			offnetCheckBtn = CPQ_Objects.vpnOffNetCheckBtn;
			defOffnetRadioBtn = CPQ_Objects.vpnOffnetRadioBtn;
		if (WebInteractUtil.waitForElementToBeVisible(offnetCheckBtn,75)) {
			WebInteractUtil.scrollIntoView(offnetCheckBtn);
			WebInteractUtil.click(offnetCheckBtn);
			WaitforVpnloader();
			WebInteractUtil.click(defOffnetRadioBtn);
			WaitforVpnloader();
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "VPn offnetCheckBtn is not visible in Sitedetails page in CPQ, please verify");
			System.out.println("Vpn offnetCheckBtn is not visible in Sitedetails page in CPQ, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
	}	
		return "True";
		
	}
	
	public String addtionalProductdata(String file_name, String Sheet_Name, String iScript, String iSubScript, String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : cpeConfiguration
		Purpose     : This method is to configure CPE features in CPQ
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String Product_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		String Existing_Capacity_Lead_Time = dataminer.fngetcolvalue(file_name, "Product_Configuration", iScript, iSubScript,"Existing_Capacity_Lead_Time");
		String sResult;
		
		
		
		switch (Product_Name) {
		
			case "CPESolutions":
				if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.addtionalProductDataLnk, 180)) {
					WebInteractUtil.scrollIntoView(CPQ_Objects.addtionalProductDataLnk);
					WebInteractUtil.clickByJS(CPQ_Objects.addtionalProductDataLnk);
					WebInteractUtil.isEnabled(CPQ_Objects.existingCapacityLeadTimeLst);
					waitForpageloadmask();
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "CPE configuration  address Link is not visible in CPQ, please verify");
					System.out.println("CPE address link is not visible in CPQ, please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}
				sResult = addtionalProductdataEntries(file_name, Sheet_Name, iScript, iSubScript, Product_Name);
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
				break;
				
			case "EthernetLine": case "EthernetHub": case "EthernetSpoke": case "Wave":
				
//				Entering A-End details
				if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.addtionalProductDataLnk, 180)) {
					WebInteractUtil.scrollIntoView(CPQ_Objects.addtionalProductDataLnk);
					WebInteractUtil.clickByJS(CPQ_Objects.addtionalProductDataLnk);
					WebInteractUtil.isEnabled(CPQ_Objects.existingCapacityLeadTimeLst);
					waitForpageloadmask();
					WebInteractUtil.selectByValue(CPQ_Objects.existingCapacityLeadTimeLst, Existing_Capacity_Lead_Time);
					waitForpageloadmask();
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "CPE configuration  address Link is not visible in CPQ, please verify");
					System.out.println("CPE address link is not visible in CPQ, please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}
				
//				sResult = addtionalProductdataEntries(file_name, "A_End", iScript, iSubScript, Product_Name);
//				if (sResult.equalsIgnoreCase("False")){ return "False"; }
//				
////				Entering B-End details
//				if (Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("Wave")) {
//					sResult = addtionalProductdataEntries(file_name, "B_End", iScript, iSubScript, Product_Name);
//					if (sResult.equalsIgnoreCase("False")){ return "False"; }
//					break;
//				}
				break;
				
			case "ColtIpAccess":
				sResult = ipAccessConfiguration(file_name, Sheet_Name, iScript, iSubScript, UI_Type, TransactionID, ColName);
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
				break;
		}
		
		
		return "True";
		
	}
	
	public String addtionalProductdataEntries(String file_name, String Sheet_Name, String iScript, String iSubScript, String Product_Name ) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : addtionalProductdataEntries
		Purpose     : This method is to add the additional product data entries
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String Existing_Capacity_Lead_Time = dataminer.fngetcolvalue(file_name, "Product_Configuration", iScript, iSubScript,"Existing_Capacity_Lead_Time");
		String Cabinet_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Cabinet_Type");
		String Site_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Site_Type");
		String Presentation_Interface = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Presentation_Interface");
		String Cabinet_ID = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Cabinet_ID");
		String Connector_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Connector_Type");
		String Access_Technology = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Access_Technology");
		String Customer_Pop_Status = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Customer_Pop_Status");
		String Site_ID = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Site_ID");
		String Port_Role = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Port_Role");
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.addtionalProductDataLnk, 180)) {
			WebInteractUtil.scrollIntoView(CPQ_Objects.addtionalProductDataLnk);
			WebInteractUtil.click(CPQ_Objects.addtionalProductDataLnk);
			WebInteractUtil.isEnabled(CPQ_Objects.existingCapacityLeadTimeLst);
			waitForpageloadmask();
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Additional product data Link is not visible in CPQ, please verify");
			System.out.println("Additional product data Link is not visible in CPQ, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		// Capturing Start point of Transaction Capture
		String StartTime = dateTimeUtil.fnGetCurrentTime();
		
		switch (Product_Name) {
		
			case "CPESolutions":
							
				WebInteractUtil.selectByValue(CPQ_Objects.existingCapacityLeadTimeLst, Existing_Capacity_Lead_Time);
				waitForpageloadmask();
				WebInteractUtil.selectByValue(CPQ_Objects.cabinetTypeALst, Cabinet_Type);
				waitForpageloadmask();
				WebInteractUtil.selectByValue(CPQ_Objects.siteTypeLst, Site_Type);
				waitForpageloadmask();
				WebInteractUtil.selectByValue(CPQ_Objects.presentationInterfaceALst, Presentation_Interface);
				waitForpageloadmask();
				WebInteractUtil.sendKeys(CPQ_Objects.cabinetIDATxb, Cabinet_ID);
				waitForpageloadmask();
				WebInteractUtil.selectByValue(CPQ_Objects.connectorTypeALst, Connector_Type);
				waitForpageloadmask();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Additional Product data's for CPE Soultions have been entered successfully");
				System.out.println("Additional Product data's have been added successfully");
				break;
				
			case "EthernetLine": case "EthernetHub": case "EthernetSpoke": case "Wave":
				
				switch (Sheet_Name) {
				
					case "A_End": 
						
						WebInteractUtil.selectByValue(CPQ_Objects.existingCapacityLeadTimeLst, Existing_Capacity_Lead_Time);
						waitForpageloadmask();
						WebInteractUtil.sendKeys(CPQ_Objects.cabinetIDATxb, Cabinet_ID);
						WebInteractUtil.selectByValue(CPQ_Objects.cabinetTypeALst, Cabinet_Type);
						waitForpageloadmask();
						WebInteractUtil.selectByValue(CPQ_Objects.accessTechnologyALst, Access_Technology);
						waitForpageloadmask();
						WebInteractUtil.selectByValue(CPQ_Objects.customerPopStatusALst, Customer_Pop_Status);
						waitForpageloadmask();
						WebInteractUtil.sendKeys(CPQ_Objects.siteIDATxb, Site_ID);
						WebInteractUtil.selectByValue(CPQ_Objects.presentationInterfaceALst, Presentation_Interface);
						waitForpageloadmask();
						WebInteractUtil.selectByValue(CPQ_Objects.connectorTypeALst, Connector_Type);
						waitForpageloadmask();
						if (!Product_Name.equalsIgnoreCase("EthernetSpoke")) {
							WebInteractUtil.selectByValue(CPQ_Objects.portRoleALst, Port_Role);
							waitForpageloadmask();
						}
						ExtentTestManager.getTest().log(LogStatus.PASS, "Additional Product data's for A-End have been entered successfully");
						System.out.println("Additional Product data's for A-End have been added successfully");
						break;
						
					case "B_End": 
						
						WebInteractUtil.selectByValue(CPQ_Objects.existingCapacityLeadTimeLst, Existing_Capacity_Lead_Time);
						waitForpageloadmask();
						WebInteractUtil.sendKeys(CPQ_Objects.cabinetIDBTxb, Cabinet_ID);
						WebInteractUtil.selectByValue(CPQ_Objects.cabinetTypeBLst, Cabinet_Type);
						waitForpageloadmask();
						WebInteractUtil.selectByValue(CPQ_Objects.accessTechnologyBLst, Access_Technology);
						waitForpageloadmask();
						WebInteractUtil.selectByValue(CPQ_Objects.customerPopStatusBLst, Customer_Pop_Status);
						waitForpageloadmask();
						WebInteractUtil.sendKeys(CPQ_Objects.siteIDBTxb, Site_ID);
						WebInteractUtil.selectByValue(CPQ_Objects.presentationInterfaceBLst, Presentation_Interface);
						waitForpageloadmask();
						WebInteractUtil.selectByValue(CPQ_Objects.connectorTypeBLst, Connector_Type);
						waitForpageloadmask();
						WebInteractUtil.selectByValue(CPQ_Objects.portRoleBLst, Port_Role);
						waitForpageloadmask();
						ExtentTestManager.getTest().log(LogStatus.PASS, "Additional Product data's for B-End have been entered successfully");
						System.out.println("Additional Product data's for B-End have been added successfully");
						break;
				}
			}
		
		// Capturing End point of Transaction Capture
		String EndTime = dateTimeUtil.fnGetCurrentTime();
					
		// Computing Difference between Transactions Capture
		String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
		System.out.println("TimeDiff is "+TimeDiff);
		
		return TimeDiff;
	}
	
	public String saveTOQuoteCPQ() throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : saveTOQuoteCPQ
		Purpose     : This method is to save the quote in CPQ
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
				
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.saveToQuoteBtn, 180)) {
			WebInteractUtil.scrollIntoView(CPQ_Objects.saveToQuoteBtn);
			waitForpageloadmask();
			WebInteractUtil.clickByJS(CPQ_Objects.saveToQuoteBtn);
			waitForpageloadmask();
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.errorContentCPQ, 5)) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "CPQ configuration save got failed due to the error "+CPQ_Objects.errorContentCPQ.getText());
				System.out.println("CPQ configuration save got failed due to the error "+CPQ_Objects.errorContentCPQ.getText());
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Save to Quote button is not clicked in CPQ, please verify");
			System.out.println("Save to Quote button is not clicked in CPQ, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		return "True";
	}
	
	public String verifyQuoteStage(String Quote_Stage) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : verifyQuoteStage
		Purpose     : This method is to Verify the Quote Stage status
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		int i, Iter;
		DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
		
		if (Quote_Stage.equals("Ordered")) { Iter = 50; } else { Iter = 2; };
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.quoteStageElem, 60)) {
			WaitforCPQloader();
			String quoteStage = CPQ_Objects.quoteStageElem.getAttribute("value");
			for (i = 1; i <= Iter; i++) {
				if (quoteStage.equals(Quote_Stage)) {
					ExtentTestManager.getTest().log(LogStatus.PASS, "Quote stage is set to "+quoteStage);
					System.out.println("Quote stage is set to "+quoteStage);
					break;
				} else {
					Thread.sleep(10000);
					WaitforCPQloader();
					WebInteractUtil.click(CPQ_Objects.quoteStageElem);
					quoteStage = CPQ_Objects.quoteStageElem.getAttribute("value");
					WebInteractUtil.scrollIntoTop();
					continue;
				}
			}
			if (i > Iter) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Quote stage is still set as "+quoteStage+ " but the Expected status is "+Quote_Stage);
				System.out.println("Quote stage is still set as "+quoteStage+ " but the Expected status is "+Quote_Stage);
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Quote Type is not Standard, Please Verify");
			System.out.println("Unable to click on Quotes Link, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		return "True";
	}
	
	public String engagePPT(String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : discountingProcess
		Purpose     : This method is to progress the discounting section
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		String tfile_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.engagePortfolioPricingBtn, 60)) {			
			waitForpageloadmask();
			WebInteractUtil.click(CPQ_Objects.engagePortfolioPricingBtn);
			
			// Capturing Start point of Transaction Capture
			String StartTime = dateTimeUtil.fnGetCurrentTime();
			
			waitForpageloadmask();
			
			// Capturing End point of Transaction Capture
			String EndTime = dateTimeUtil.fnGetCurrentTime();
						
			// Computing Difference between Transactions Capture
			String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
			System.out.println("TimeDiff is "+TimeDiff);
						
			//Entering the Values to the Data sheet
			dataminer.fnsetTransactionValue(tfile_name, UI_Type, TransactionID, ColName, TimeDiff);
			
			String sResult = validateCPQErrorMsg();
			if (sResult.equalsIgnoreCase("False")) { return "False"; }
			
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "engagePortfolioPricingBtn is not Visible in CPQ Page, Please Verify");
			System.out.println("engagePortfolioPricingBtn is not Visible in CPQ Page, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
	
		
		return "True";
	}
	
	public String assignPPT(String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : assignPPT
		Purpose     : This method is to progress the discounting section
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		String PPT_UserName = dataminer.fngetconfigvalue(System.getProperty("user.dir")+"\\TestData\\CPQ_testdata.xlsx", "PPT_UserName");
		String tfile_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.plTab, 60)) {			
			WebInteractUtil.click(CPQ_Objects.plTab);
			waitForpageloadmask();
			WebInteractUtil.selectByValueDIV(CPQ_Objects.portfolioAssignmentLst, CPQ_Objects.portfolioAssignmentSubLst, PPT_UserName);
			waitForpageloadmask();
			WebInteractUtil.click(CPQ_Objects.assignQuoteBtn);
			
			// Capturing Start point of Transaction Capture
			String StartTime = dateTimeUtil.fnGetCurrentTime();
			
			waitForpageloadmask();
			
			// Capturing End point of Transaction Capture
			String EndTime = dateTimeUtil.fnGetCurrentTime();
						
			// Computing Difference between Transactions Capture
			String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
			System.out.println("TimeDiff is "+TimeDiff);
						
			//Entering the Values to the Data sheet
			dataminer.fnsetTransactionValue(tfile_name, UI_Type, TransactionID, ColName, TimeDiff);
			
			String sResult = validateCPQErrorMsg();
			if (sResult.equalsIgnoreCase("False")) { return "False"; }
			
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "engagePortfolioPricingBtn is not Visible in CPQ Page, Please Verify");
			System.out.println("engagePortfolioPricingBtn is not Visible in CPQ Page, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
	
		return "True";
	}
	
	public String pptPricingEntries(String file_name, String Sheet_Name, String iScript, String iSubScript, String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : pptPricingEntries
		Purpose     : This method is to make a pricing entries
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Reading the values from excel sheet
		String PPT_UserName = dataminer.fngetconfigvalue(System.getProperty("user.dir")+"\\TestData\\CPQ_testdata.xlsx", "PPT_UserName");
		String tfile_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		String sResult = null;
		String NRR_Price = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"NRR_Price");
		String MRR_Price = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"MRR_Price");
		
		String[] sTransactionsID = TransactionID.split("\\|");
		
//		sResult = WebTableCellAction("Stage", "Waiting for Portfolio Pricing", "NRR (gross)","Edit", NRR_Price);
//		if (sResult.equalsIgnoreCase("False")){ return "False"; }
//		waitForpageloadmask();
//		
////		Saving the details
//		sResult = saveCPQ("Main", UI_Type, "", ColName);
//		if (sResult.equalsIgnoreCase("False")){ return "False"; }
//		
//		sResult = WebTableCellAction("Stage", "Waiting for Portfolio Pricing", "MRR (gross)","Edit", MRR_Price);
//		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		Saving the details
		sResult = saveCPQ("PLTab", "Sales",UI_Type, sTransactionsID[0], ColName);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		WebInteractUtil.isPresent(CPQ_Objects.refreshAllPricesBtn, 40);
		WebInteractUtil.click(CPQ_Objects.refreshAllPricesBtn);
		
		// Capturing Start point of Transaction Capture
		String StartTime = dateTimeUtil.fnGetCurrentTime();
		waitForpageloadmask();
		
		// Capturing End point of Transaction Capture
		String EndTime = dateTimeUtil.fnGetCurrentTime();
		for (int i = 1; i < 3; i++) {waitForpageloadmask(); }
					
		// Computing Difference between Transactions Capture
		String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
		System.out.println("TimeDiff is "+TimeDiff);
					
		//Entering the Values to the Data sheet
		dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[1], ColName, TimeDiff);
		
		WebInteractUtil.isPresent(CPQ_Objects.sendToSalesBtn, 40);
		WebInteractUtil.click(CPQ_Objects.sendToSalesBtn);
		
		// Capturing Start point of Transaction Capture
		StartTime = dateTimeUtil.fnGetCurrentTime();
		waitForpageloadmask();
		WebInteractUtil.waitForInvisibilityOfElement(CPQ_Objects.sendToSalesBtn, 75);
		
		// Capturing End point of Transaction Capture
		EndTime = dateTimeUtil.fnGetCurrentTime();
					
		// Computing Difference between Transactions Capture
		TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
		System.out.println("TimeDiff is "+TimeDiff);
					
		//Entering the Values to the Data sheet
		dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[2], ColName, TimeDiff);
		
		return "True";
	}
	
	public String discountingProcess(String file_name, String Sheet_Name, String iScript, String iSubScript, String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : discountingProcess
		Purpose     : This method is to progress the discounting section
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		String Discount_Percentage = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Discount_Percentage");
		String tfile_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.quoteLnk, 60)) {
			WaitforCPQloader();
			WebInteractUtil.click(CPQ_Objects.quoteLnk);
			WaitforCPQloader();
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.basePriceNRCDiscountTxb, 60)) {
				WaitforCPQloader();
				WebInteractUtil.sendKeys(CPQ_Objects.basePriceNRCDiscountTxb, Discount_Percentage);
				WebInteractUtil.sendKeys(CPQ_Objects.basePriceMRCDiscountTxb, Discount_Percentage);
				WaitforCPQloader();
				WebInteractUtil.click(CPQ_Objects.calculateDiscountBtn);
				// Capturing Start point of Transaction Capture
				String StartTime = dateTimeUtil.fnGetCurrentTime();
				WaitforCPQloader();
				// Capturing End point of Transaction Capture
				String EndTime = dateTimeUtil.fnGetCurrentTime();
							
				// Computing Difference between Transactions Capture
				String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
				System.out.println("TimeDiff is "+TimeDiff);
							
				//Entering the Values to the Data sheet
				dataminer.fnsetTransactionValue(tfile_name, UI_Type, TransactionID, ColName, TimeDiff);
				ExtentTestManager.getTest().log(LogStatus.PASS, "Discount with "+Discount_Percentage+" percentage has set properly for the Quote");
				System.out.println("Discount with "+Discount_Percentage+" percentage has set properly for the Quote");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "basePriceNRCDiscountTxb is not Visible in CPQ Page, Please Verify");
				System.out.println("basePriceNRCDiscountTxb is not Visible in CPQ Page, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "quoteLnk is not Visible in CPQ Page, Please Verify");
			System.out.println("quoteLnk is not Visible in CPQ Page, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
	
		return "True";
		
	}
	
	public String copyQuote(String file_name, String Sheet_Name, String iScript, String iSubScript, String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : copyQuote
		Purpose     : This method is to copy quotes according to its types
		Designer    : Vasantharaja C
		Created on  : 21st July 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing Variable
		String Copy_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Copy_Type");
		String Product_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		String No_Of_Copies = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"No_Of_Copies");
		String QuoteID = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Quote_ID");
		String tfile_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		
		String sResult = null;
		String sProduct;
		if(Product_Name.equalsIgnoreCase("VPNNetwork")) {sProduct="VPN Network";}else {sProduct= Product_Name.replaceAll("(?!^)([A-Z])", " $1");}
		
		switch (Copy_Type.toUpperCase()) {
		
		case "LINEITEM":
			
//			Clicking on the rows from the table
			sResult = MultiLineWebTableCellAction("Product", sProduct, null,"Click", null, 1);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.copyLineItemsBtn, 60)) {
				WebInteractUtil.click(CPQ_Objects.copyLineItemsBtn);
				waitForpageloadmask();
				WebInteractUtil.isPresent(CPQ_Objects.numberOfCopiesTxb,30);
				WebInteractUtil.sendKeys(CPQ_Objects.numberOfCopiesTxb, No_Of_Copies);
				waitForpageloadmask();
				WebInteractUtil.click(CPQ_Objects.copyPromptOKBtn);
				// Capturing Start point of Transaction Capture
				String StartTime = dateTimeUtil.fnGetCurrentTime();
				ExtentTestManager.getTest().log(LogStatus.PASS, "LineItem Copy of the product "+Product_Name+" was successfull");
				System.out.println("LineItem Copy of the product "+Product_Name+" was successfull");
				waitForpageloadmask();
				waitForpageloadmask();
				// Capturing End point of Transaction Capture
				String EndTime = dateTimeUtil.fnGetCurrentTime();
							
				// Computing Difference between Transactions Capture
				String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
				System.out.println("TimeDiff is "+TimeDiff);
							
				//Entering the Values to the Data sheet
				dataminer.fnsetTransactionValue(tfile_name, UI_Type, TransactionID, ColName, TimeDiff);
				break;
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "copyLineItemsBtn is not Visible in CPQ, Please Verify");
				System.out.println("copyLineItemsBtn is not Visible in CPQ, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
			

		case "TRANSACTION":
			
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.adminBtn, 60)) {
				WebInteractUtil.click(CPQ_Objects.adminBtn);
				Waittilljquesryupdated();
				WebInteractUtil.isPresent(CPQ_Objects.transactionsLnk,30);
				WebInteractUtil.click(CPQ_Objects.transactionsLnk);
				Waittilljquesryupdated();
				FluentWait<WebDriver> fluentWait = new FluentWait<>(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get()) 
			    		.withTimeout(120, TimeUnit.SECONDS)
			    		.pollingEvery(1000, TimeUnit.MILLISECONDS)
			    		.ignoring(NoSuchElementException.class);
						fluentWait.until(ExpectedConditions.elementToBeClickable(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//a[text()='"+QuoteID+"']//parent::div//parent::td//preceding-sibling::td//input[@type='checkbox']"))));
	    		DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//a[text()='"+QuoteID+"']//parent::div//parent::td//preceding-sibling::td//input[@type='checkbox']")).click();				
				WebInteractUtil.click(CPQ_Objects.copyTransactionsLnk);
				break;
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "adminBtn is not Visible in CPQ, Please Verify");
				System.out.println("adminBtn is not Visible in CPQ, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
			
		case "C4C":
//			Calling the below method to return from cpq to c4c
			sResult = returnC4CFromCPQ();
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.quotesLnk, 60)) {
				WebInteractUtil.click(CPQ_Objects.quotesLnk);
				WaitforC4Cloader();
				Waittilljquesryupdated();
				WaitforC4Cloader();
				WebInteractUtil.isPresent(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//descendant::a[text()='"+QuoteID+"']")), 30);
				WebInteractUtil.click(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//descendant::a[text()='"+QuoteID+"']")));
				WebInteractUtil.pause(1000);
				WaitforC4Cloader();
				WebInteractUtil.isEnabled(CPQ_Objects.actionBtn);
				WebInteractUtil.scrollIntoView(CPQ_Objects.actionBtn);
				WaitforC4Cloader();
				WebInteractUtil.click(CPQ_Objects.actionBtn);
				Waittilljquesryupdated();
				WebInteractUtil.click(CPQ_Objects.copyQuoteC4CBtn);
				Waittilljquesryupdated();
				WebInteractUtil.click(CPQ_Objects.proceedC4CBtn);
				break;
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "quotesLnk is not Visible in C4C, Please Verify");
				System.out.println("quotesLnk is not Visible in C4C, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
			
		}
		
//		Capturing Qupte Ref
		if (Copy_Type.equalsIgnoreCase("TRANSACTION") || Copy_Type.equalsIgnoreCase("C4C")) {
			WebInteractUtil.isPresent(CPQ_Objects.quoteNameTxb, 240);
			WebInteractUtil.isPresent(CPQ_Objects.seRevLnk, 120);
			WebInteractUtil.isPresent(CPQ_Objects.quoteTypeElem, 180);
			WaitforCPQloader();
			Waittilljquesryupdated();
			String quoteID = CPQ_Objects.quoteIDElem.getAttribute("value");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Copy Quote ID is generated "+quoteID);
			System.out.println("Copy Quote ID is generated "+quoteID);
			dataminer.fnsetcolvalue(file_name, Sheet_Name, iScript, iSubScript, "Copy_Quote_ID", quoteID);
			
		}
		
		return "True";
		
	}
	
	public String commercialApproval(String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : commercialApproval
		Purpose     : This method is to move the quote to commercial approval
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		String Click_Submit = "False";
		String tfile_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.commercialValidationLnk, 60)) {
			waitForpageloadmask();
			WebInteractUtil.click(CPQ_Objects.commercialValidationLnk);
			waitForpageloadmask();
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.VPSalesTxtElem, 3)) { Click_Submit = "True"; }
			WebInteractUtil.click(CPQ_Objects.submitToApprovalBtn);
			// Capturing Start point of Transaction Capture
			String StartTime = dateTimeUtil.fnGetCurrentTime();
			waitForpageloadmask();
			WebInteractUtil.waitForInvisibilityOfElement(CPQ_Objects.submitToApprovalBtn, 60);
			
			// Capturing End point of Transaction Capture
			String EndTime = dateTimeUtil.fnGetCurrentTime();
			
			// Computing Difference between Transactions Capture
			String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
			System.out.println("TimeDiff is "+TimeDiff);
						
			//Entering the Values to the Data sheet
			dataminer.fnsetTransactionValue(tfile_name, UI_Type, TransactionID, ColName, TimeDiff);
						
			String sResult = validateCPQErrorMsg();
			if (sResult.equalsIgnoreCase("False")) { return "False"; }
			WebInteractUtil.click(CPQ_Objects.generalInformationLnk);
			waitForpageloadmask();
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Commercial Validation link is not Visible, Please Verify");
			System.out.println("Commercial Validation link is not Visible, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		return "True";
	}
	
	public String quoteDiscountGovernanceApproval() throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : quoteDiscountGovernanceApproval
		Purpose     : This method is to approve the quote on governance approval
		Designer    : Vasantharaja C
		Created on  : 15th July 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.commercialValidationLnk, 60)) {
			WaitforCPQloader();
			WebInteractUtil.click(CPQ_Objects.commercialValidationLnk);
			WaitforCPQloader();
			WebInteractUtil.click(CPQ_Objects.approveDiscountBtn);
			WebInteractUtil.waitForInvisibilityOfElement(CPQ_Objects.approveDiscountBtn, 25);
			WaitforCPQloader();
			Waittilljquesryupdated();
			String sResult = validateCPQErrorMsg();
			if (sResult.equalsIgnoreCase("False")) { return "False"; }
			WebInteractUtil.click(CPQ_Objects.generalInformationLnk);
			WaitforCPQloader();
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Commercial Validation link is not Visible, Please Verify");
			System.out.println("Commercial Validation link is not Visible, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		return "True";
	}
	
	public String validateCPQErrorMsg() throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : commercialApproval
		Purpose     : This method is to move the quote to commercial approval
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.cpqSummaryErrorMsgElem, 2)) {
			String Main_Err = CPQ_Objects.cpqSummaryErrorMsgElem.getText().trim();
			String Sub_Err = CPQ_Objects.cpqErrorMsgElem.getText().trim();
    		ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to Process the quote due to the Error: "+Main_Err+" and "+Sub_Err);
			System.out.println("Unable to Process the quote due to the Error: "+Main_Err+" and "+Sub_Err);
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
    	} 
		
		return "True";
		
	}
	
	public String addProactiveContacts(String file_name, String Sheet_Name, String iScript, String iSubScript, String Product_Name, String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : addPoractiveContacts
		Purpose     : This method is to add Proactive Contacts
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/
		
//		Initializing the Variable
		String tfile_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		String Title = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Title");
		String First_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"First_Name");
		String Last_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Last_Name");
		String Contact_Number = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Contact_Number");
		String Mobile_Number = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Mobile_Number");
		String Preferred_Contact = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Preferred_Contact");
		String pac_Language = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"pac_Language");
		String To_Reciepient = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"To_Reciepient");
		String[] sTransactionsID = TransactionID.split("\\|");
		String sResult = null; String StartTime = null;
		
		 if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.addtionalProductDataLnk,75)) {
			 WebInteractUtil.click(CPQ_Objects.addtionalProductDataLnk);
			 waitForpageloadmask();
			 if (!Product_Name.equalsIgnoreCase("ColtIpAccess")) {
				 WebInteractUtil.isPresent(CPQ_Objects.proactiveContactCbx,45);
				 CPQ_Objects.proactiveContactCbx.sendKeys(Keys.SPACE);
			 } else {
				 waitForpageloadmask();
				 WebInteractUtil.click(CPQ_Objects.serviceInformationLnk);
				 waitForpageloadmask();
			 }
			// Capturing Start point of Transaction Capture
			 StartTime = dateTimeUtil.fnGetCurrentTime();
			 waitForpageloadmask();
		 } else {
	        	ExtentTestManager.getTest().log(LogStatus.FAIL, "addtionalProductDataLnk is not Visible, Please Verify");
				System.out.println("addtionalProductDataLnk is not Visible, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
	      }
		 
		 if (!Product_Name.equalsIgnoreCase("ColtIpAccess")) {
			 
			 WebInteractUtil.isPresent(CPQ_Objects.pacTitleLst,45);
			 WebInteractUtil.selectByValue(CPQ_Objects.pacTitleLst, Title);
			 waitForpageloadmask();
			 WebInteractUtil.sendKeys(CPQ_Objects.pacFirstNameTxb, First_Name);
			 WebInteractUtil.sendKeys(CPQ_Objects.pacLastNameTxb, Last_Name);
			 WebInteractUtil.sendKeys(CPQ_Objects.pacContactNumberTxb, Contact_Number);
			 WebInteractUtil.sendKeys(CPQ_Objects.pacMobileNumberTxb, Mobile_Number);
			 WebInteractUtil.sendKeys(CPQ_Objects.pacEmailTxb, To_Reciepient);
			 WebInteractUtil.selectByValue(CPQ_Objects.pacLanguageLst, pac_Language);
			 waitForpageloadmask();
			 WebInteractUtil.selectByValue(CPQ_Objects.pacPreferredContactLst, Preferred_Contact);
			 waitForpageloadmask();
		} else {
			
			WebInteractUtil.isPresent(CPQ_Objects.ipFirstName,45);
			WebInteractUtil.sendKeys(CPQ_Objects.ipFirstName,First_Name);
			waitForpageloadmask();
			WebInteractUtil.sendKeys(CPQ_Objects.ipLastName,Last_Name);
			waitForpageloadmask();
			WebInteractUtil.sendKeys(CPQ_Objects.ipContactNumber,Contact_Number);
			waitForpageloadmask();
			WebInteractUtil.sendKeys(CPQ_Objects.ipMobileNumber,Mobile_Number);
			waitForpageloadmask();
			WebInteractUtil.sendKeys(CPQ_Objects.ipEmail,To_Reciepient);
			waitForpageloadmask();
		}
		 
		// Capturing End point of Transaction Capture
		String EndTime = dateTimeUtil.fnGetCurrentTime();
		 			
		// Computing Difference between Transactions Capture
		String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
		System.out.println("TimeDiff is "+TimeDiff);
		 			
		//Entering the Values to the Data sheet
		dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[0], ColName, TimeDiff);
		 
//		Saving the Product Entries	
		sResult = updateSaveProductCPQ("Save", UI_Type, sTransactionsID[1], ColName);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		 
		
		return "True";
	}
	
	public String addLegalTechnicalContacts(String file_name, String Sheet_Name, String iScript, String iSubScript,String Contact_Type, String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : commercialApproval
		Purpose     : This method is to move the quote to commercial approval
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String Legal_Contact = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Legal_Contact");
		String Technical_Contact = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Technical_Contact");
		String tfile_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		
//		Initializing the driver
		WebDriver driver = DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
		waitForpageloadmask();
		
//      Setting up the instance of previous window handle
        String WindowHandle = driver.getWindowHandle();
        int i = 0; String sContact = null;
        
        switch (Contact_Type) {
        	case "LegalContact": 
        		WebInteractUtil.scrollIntoView(CPQ_Objects.legalContactLnk);
        		WebInteractUtil.click(CPQ_Objects.legalContactLnk);
        		waitForpageloadmask();
        		WebInteractUtil.click(CPQ_Objects.legalGetContactBtn);
        		sContact = Legal_Contact;
        		break;
        	case "TechnicalContact":
        		WebInteractUtil.scrollIntoView(CPQ_Objects.technicalContactLnk);
        		WebInteractUtil.click(CPQ_Objects.technicalContactLnk);
        		waitForpageloadmask();
        		WebInteractUtil.click(CPQ_Objects.technicalGetContactBtn);
        		sContact = Technical_Contact;
        		break;
        }
        
//      Capturing the new window handle
        for (String WindowHandleAfter : driver.getWindowHandles()) {
              driver.switchTo().window(WindowHandleAfter);
        }
        
//      Selecting the values from the contact
        if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.emailContactTxb,75)) {
        	waitForpageloadmask();
        	WebInteractUtil.click(CPQ_Objects.emailContactTxb);
        	WebInteractUtil.sendKeys(CPQ_Objects.emailContactTxb, sContact);
            WebInteractUtil.click(CPQ_Objects.searchContactBtn);
            WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.pickDefaultContactCbx, 120);
            waitForpageloadmask();
            WebInteractUtil.scrollIntoView(CPQ_Objects.pickDefaultContactCbx);
            WebInteractUtil.click(CPQ_Objects.pickDefaultContactCbx);
            
         // Capturing Start point of Transaction Capture
            String StartTime = dateTimeUtil.fnGetCurrentTime();
            
            waitForpageloadmask();
//            waiting till the driver gets closed
            try {
            	FluentWait<WebDriver> fluentWait = new FluentWait<>(driver) 
					.withTimeout(120, TimeUnit.SECONDS)
    		        .pollingEvery(1000, TimeUnit.MILLISECONDS)
    		        .ignoring(NoSuchElementException.class);
    				fluentWait.until(ExpectedConditions.invisibilityOf(CPQ_Objects.searchContactBtn));
            } catch(Exception e) {
            	if (e.toString().contains("no such window")) {
            		System.out.println("Legal/Technical contact window closed");          		
            	} else {
            		ExtentTestManager.getTest().log(LogStatus.FAIL, "Error in Selecting Legal/Technical Contact lookup");
        			System.out.println("Error in Selecting Legal/Technical Contact lookup");
        			driver.close();
        			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
        			return "False";
            	}                
            }
            
         // Capturing End point of Transaction Capture
    		String EndTime = dateTimeUtil.fnGetCurrentTime();
    					
    		// Computing Difference between Transactions Capture
    		String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
    		System.out.println("TimeDiff is "+TimeDiff);
    					
    		//Entering the Values to the Data sheet
    		dataminer.fnsetTransactionValue(tfile_name, UI_Type, TransactionID, ColName, TimeDiff);
    		
            ExtentTestManager.getTest().log(LogStatus.PASS, "Legal/Technical Contact has been selected as "+sContact);
			System.out.println("Legal/Technical Contact has been selected as "+sContact);
        } else {
        	ExtentTestManager.getTest().log(LogStatus.FAIL, "Legal/Technical Contact lookup is not Visible, Please Verify");
			System.out.println("Legal/Technical Contact lookup is not Visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
        }
       
        // Switch back to first browser window
        driver.switchTo().window(WindowHandle);
        WaitforCPQloader();
		return "True";
	}
	
	public String submitForTechnicalApproval(String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : submitForTechnicalApproval
		Purpose     : This method is to submit the quote for Technical approval
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		String tfile_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		String StartTime = null, EndTime = null, TimeDiff = null;
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.technicalApprovalLnk, 60)) {
			WaitforCPQloader();
			WebInteractUtil.scrollIntoView(CPQ_Objects.technicalApprovalLnk);
//			WebInteractUtil.scrollIntoTop();
			WaitforCPQloader();
			WebInteractUtil.click(CPQ_Objects.technicalApprovalLnk);
			WaitforCPQloader();
			WebInteractUtil.scrollIntoView(CPQ_Objects.readyForTechApprovalCbx);
			WaitforCPQloader();
			WebInteractUtil.click(CPQ_Objects.readyForTechApprovalCbx);
			WaitforCPQloader();
			WebInteractUtil.scrollIntoView(CPQ_Objects.submitTechApprovalBtn);
			WaitforCPQloader();
			WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.submitTechApprovalBtn, 60);
			WebInteractUtil.click(CPQ_Objects.submitTechApprovalBtn);
			
			// Capturing Start point of Transaction Capture
			StartTime = dateTimeUtil.fnGetCurrentTime();
			WaitforCPQloader();
			WebInteractUtil.waitForInvisibilityOfElement(CPQ_Objects.submitTechApprovalBtn, 180);
			
			// Capturing End point of Transaction Capture
			EndTime = dateTimeUtil.fnGetCurrentTime();
						
			// Computing Difference between Transactions Capture
			TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
			System.out.println("TimeDiff is "+TimeDiff);
						
			//Entering the Values to the Data sheet
			dataminer.fnsetTransactionValue(tfile_name, UI_Type, TransactionID, ColName, TimeDiff);
			
			String sResult = validateCPQErrorMsg();
			if (sResult.equalsIgnoreCase("False")) { return "False"; }
			
			WebInteractUtil.scrollIntoView(CPQ_Objects.generalInformationLnk);
			WaitforCPQloader();
			WebInteractUtil.click(CPQ_Objects.generalInformationLnk);
			WaitforCPQloader();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Quote is set for Technical Approval with message "+CPQ_Objects.notificationBarCPQ.getText());
			System.out.println("Quote is set for Technical Approval with message "+CPQ_Objects.notificationBarCPQ.getText());
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Technical Approval link is not Visible, Please Verify");
			System.out.println("Commercial Approval link is not Visible, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		return "True";
	}
	
	public String SwitchCPQUser(String sUser, String QuoteID) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : proxyUserSwitchCPQ
		Purpose     : This method is to switch the respective user in CPQ Login
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		String file_name = System.getProperty("user.dir")+"\\TestData\\CPQ_testdata.xlsx";
		String CPQ_URL = dataminer.fngetconfigvalue(file_name, "CPQ_URL");
		String Username = dataminer.fngetconfigvalue(file_name, sUser);
		String Password = dataminer.fngetconfigvalue(file_name, "C4C_Password");

//		New Functionality
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.cpqLogoutBtn,75)) {	
			WebInteractUtil.click(CPQ_Objects.cpqLogoutBtn);
			Waittilljquesryupdated();
			WebInteractUtil.launchWebApp(CPQ_URL);
			Waittilljquesryupdated();
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.userNameTxb,90)) {
				Waittilljquesryupdated();
				WebInteractUtil.sendKeys(CPQ_Objects.userNameTxb, Username);
				WebInteractUtil.sendKeys(CPQ_Objects.passWordTxb, Password);
				WebInteractUtil.click(CPQ_Objects.loginBtn);
				Waittilljquesryupdated();
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "CPQ Logout was not Successfull, Please Verify");
				System.out.println("CPQ Logout was not Successfull, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "cpqLogoutBtn Button is not Visible, Please Verify");
			System.out.println("cpqLogoutBtn link is not Visible, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
////		Old Functionality
//		switch (sUser) {
//			case "CST_User":
//				WebInteractUtil.click(CPQ_Objects.proxyLogoutBtn);
//				Waittilljquesryupdated();
//				break;
//		}
//		
//		if (!sUser.equalsIgnoreCase("CST_User")) {	
//			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.adminBtn,75)) {	
//				WebInteractUtil.click(CPQ_Objects.adminBtn);
//				Waittilljquesryupdated();
//				if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.internalUsersLnk,90)) {
//					Waittilljquesryupdated();
//					WebInteractUtil.click(CPQ_Objects.internalUsersLnk);
//					Waittilljquesryupdated();
//				} else {
//					ExtentTestManager.getTest().log(LogStatus.FAIL, "CPQ Logout was not Successfull, Please Verify");
//					System.out.println("CPQ Logout was not Successfull, Please Verify");
//					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
//					return "False";
//				}
//			} else {
//				ExtentTestManager.getTest().log(LogStatus.FAIL, "cpqLogoutBtn Button is not Visible, Please Verify");
//				System.out.println("cpqLogoutBtn link is not Visible, please verify");
//				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
//				return "False";
//			}
//		}
//		
//		if (WebInteractUtil.waitForElementToBeVisible(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//a[text()='"+Username+"']//parent::td//following-sibling::td//child::a//child::img[@alt='Proxy Login']")),90)) {
//			Waittilljquesryupdated();
//			WebInteractUtil.click(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//a[text()='"+Username+"']//parent::td//following-sibling::td//child::a//child::img[@alt='Proxy Login']")));
//			Waittilljquesryupdated();
//		} else {
//			ExtentTestManager.getTest().log(LogStatus.FAIL, "CPQ Logout was not Successfull, Please Verify");
//			System.out.println("CPQ Logout was not Successfull, Please Verify");
//			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
//			return "False";
//		}
		
		
//		Usual Functionality
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.orderToQuoteManagerLnk,125)) {	
			WebInteractUtil.click(CPQ_Objects.orderToQuoteManagerLnk);
			Waittilljquesryupdated();
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "orderToQuoteManager link is not Visible, Please Verify");
			System.out.println("orderToQuoteManager link is not Visible, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
//		clicking on Quote Link	
		try {
			WebInteractUtil.isPresent(CPQ_Objects.newTransactionBtn, 60);
			WebInteractUtil.scrollIntoView(CPQ_Objects.newTransactionBtn);
			Waittilljquesryupdated();
			FluentWait<WebDriver> fluentWait = new FluentWait<>(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get()) 
    		.withTimeout(120, TimeUnit.SECONDS)
    		.pollingEvery(1000, TimeUnit.MILLISECONDS)
    		.ignoring(NoSuchElementException.class);
    		fluentWait.until(ExpectedConditions.elementToBeClickable(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//a[text()='"+QuoteID+"']"))));
    		DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//a[text()='"+QuoteID+"']")).click();
    		WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.quoteNameTxb, 240);
			WebInteractUtil.isPresent(CPQ_Objects.quoteTypeElem, 180);
			waitForpageloadmask();
		} catch(Exception e) {
    		ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to click Quote ID "+QuoteID+", Please verify");
			System.out.println("Unable to click Quote ID "+QuoteID+", Please verify"+e.toString());
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";              
        }
		return "True";
	}
	
	public String technicalApprovalCPQ(String UI_Type, String TransactionID, String ColName	) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : technicalApprovalCPQ
		Purpose     : This method is to approve the quote as se user
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		String tfile_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		String StartTime = null, EndTime = null, TimeDiff = null;
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.technicalApprovalLnk, 60)) {
			waitForpageloadmask();
			WebInteractUtil.scrollIntoView(CPQ_Objects.technicalApprovalLnk);
			WebInteractUtil.click(CPQ_Objects.technicalApprovalLnk);
			waitForpageloadmask();
			WebInteractUtil.scrollIntoView(CPQ_Objects.submitCSTApprovalBtn);
			waitForpageloadmask();
			WebInteractUtil.click(CPQ_Objects.submitCSTApprovalBtn);
			
			// Capturing Start point of Transaction Capture
			StartTime = dateTimeUtil.fnGetCurrentTime();
			waitForpageloadmask();
			WebInteractUtil.waitForInvisibilityOfElement(CPQ_Objects.submitCSTApprovalBtn, 120);
			
			// Capturing End point of Transaction Capture
			EndTime = dateTimeUtil.fnGetCurrentTime();
						
			// Computing Difference between Transactions Capture
			TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
			System.out.println("TimeDiff is "+TimeDiff);
						
			//Entering the Values to the Data sheet
			dataminer.fnsetTransactionValue(tfile_name, UI_Type, TransactionID, ColName, TimeDiff);
			
			String sResult = validateCPQErrorMsg();
			if (sResult.equalsIgnoreCase("False")) { return "False"; }
			waitForpageloadmask();
			WebInteractUtil.click(CPQ_Objects.generalInformationLnk);
			waitForpageloadmask();						
			ExtentTestManager.getTest().log(LogStatus.PASS, "SE user approved the quote with message "+CPQ_Objects.notificationBarCPQ.getText());
			System.out.println("SE user approved the quote with message "+CPQ_Objects.notificationBarCPQ.getText());
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Technical Approval link is not Visible, Please Verify");
			System.out.println("Commercial Approval link is not Visible, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		return "True";
	}
	
	public String cstApprovalCPQ(String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : cstApprovalCPQ
		Purpose     : This method is to approve the quote as cst user
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		String tfile_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		String StartTime = null, EndTime = null, TimeDiff = null;
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.technicalApprovalLnk, 60)) {
			waitForpageloadmask();
			WebInteractUtil.scrollIntoView(CPQ_Objects.technicalApprovalLnk);
			WebInteractUtil.click(CPQ_Objects.technicalApprovalLnk);
			waitForpageloadmask();
//			WebInteractUtil.scrollIntoView(CPQ_Objects.addInternalNoteBtn);
			WebInteractUtil.scrollIntoView(CPQ_Objects.approveCSTBtn);
			WebInteractUtil.click(CPQ_Objects.approveCSTBtn);
			
			// Capturing Start point of Transaction Capture
			StartTime = dateTimeUtil.fnGetCurrentTime();
			
			waitForpageloadmask();
			WebInteractUtil.waitForInvisibilityOfElement(CPQ_Objects.approveCSTBtn, 120);
			// Capturing End point of Transaction Capture
			EndTime = dateTimeUtil.fnGetCurrentTime();
						
			// Computing Difference between Transactions Capture
			TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
			System.out.println("TimeDiff is "+TimeDiff);
						
			//Entering the Values to the Data sheet
			dataminer.fnsetTransactionValue(tfile_name, UI_Type, TransactionID, ColName, TimeDiff);
			
			WebInteractUtil.waitForInvisibilityOfElement(CPQ_Objects.addInternalNoteBtn, 180);
			waitForpageloadmask();
			String sResult = validateCPQErrorMsg();
			if (sResult.equalsIgnoreCase("False")) { return "False"; }
			WebInteractUtil.scrollIntoView(CPQ_Objects.generalInformationLnk);
			WebInteractUtil.click(CPQ_Objects.generalInformationLnk);
			waitForpageloadmask();						
			ExtentTestManager.getTest().log(LogStatus.PASS, "CST user approved the quote with message "+CPQ_Objects.notificationBarCPQ.getText());
			System.out.println("CST user approved the quote with message "+CPQ_Objects.notificationBarCPQ.getText());
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Technical Approval link is not Visible, Please Verify");
			System.out.println("Technical Approval link is not Visible, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		return "True";
	}
	
	public String logoutCPQ(String LogoutType) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : proxyUserLogout
		Purpose     : This method is to logout form CPQ for the proxy user
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		String Environment = dataminer.fngetconfigvalue(System.getProperty("user.dir")+"\\TestData\\CPQ_testdata.xlsx", "Environment");
		WebElement logOutBtn = null;
		WebElement proxyLogOutBtn = null;
		
		switch (Environment) {
		case "RFS": case "PRD":
			logOutBtn = CPQ_Objects.cpqLogoutRFSBtn;
			proxyLogOutBtn = CPQ_Objects.proxyLogoutRFSBtn;
			break;
		
		case "TEST2": case "TEST1":
			logOutBtn = CPQ_Objects.cpqLogoutBtn;
			proxyLogOutBtn = CPQ_Objects.proxyLogoutBtn;
			break;		
		}
		
		if (LogoutType.equals("Proxy")) {
			if (WebInteractUtil.waitForElementToBeVisible(proxyLogOutBtn, 60)) {
				WebInteractUtil.click(proxyLogOutBtn);
				WebInteractUtil.pause(2500);
				waitForpageloadmask();
				System.out.println("Proxy User Logout was successfull");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Proxy User Logout Button was not Visible, Please Verify");
				System.out.println("Proxy User Logout Button was not Visible, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		} else if (LogoutType.equals("Main")) {
			if (LogoutType.equals("Main")) {
				Waittilljquesryupdated();
				WebInteractUtil.click(logOutBtn);
//				if (Environment.equalsIgnoreCase("Test2")) {
//					WebInteractUtil.isEnabled(logOutBtn);
//				}
				Waittilljquesryupdated();
				System.out.println("CPQ User Logout was successfull");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "CPQ Logout Button was not Visible, Please Verify");
				System.out.println("CPQ logout Button was not Visible, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		}
		
		return "True";
	}
	
	public String WebTableCellAction(String refColumn, String rowRef, String actColumn, String ActionType, String ActionValue) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : WebTableCellAction
		Purpose     : This method is to logout form CPQ for the proxy user
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		int row_number = 0; String tXpath = null, Row_Val;
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.lineItemGridTable, 35)) {
			WebInteractUtil.scrollIntoView(CPQ_Objects.lineItemGridTable);
			String sXpath = CPQ_Objects.lineItemGridTable.toString();
			tXpath = sXpath.substring(sXpath.indexOf("xpath:")+"xpath:".length(), sXpath.indexOf("]]")+1).trim();
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "lineItemGridTable was not visible in CPQ Main Page, Please Verify");
			System.out.println("lineItemGridTable was not visible in CPQ Main Page, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
//		getting the row and column number
		List<WebElement> rows = CPQ_Objects.lineItemGridTable.findElements(By.tagName("tr"));
		List<WebElement> columns = rows.get(0).findElements(By.tagName("th"));
		int tot_row = rows.size();
		int tot_col = columns.size();
//		System.out.println("Total Column size is "+tot_col);
		int iCol, iRow, rColumn_number = 0, aColumn_number = 0;
		
		//Reading the column headers of table and set the column number with use of reference
		for(iCol = 1; iCol <= tot_col-1; iCol++){
			String Col_Val = columns.get(iCol).getAttribute("title").trim();
			if (Col_Val.equals(refColumn)){ 
				rColumn_number = iCol+1; 
//				System.out.println("ref Column number is "+rColumn_number); 
				break; 
			}
		}
		
//		Returns the function of reference column number
		if (iCol >= tot_col) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Column Name "+refColumn+" is not found in the webtable, Please Verify ");
			System.out.println("Column Name "+refColumn+" is not found in the webtable, Please Verify ");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		
		if (!ActionType.equals("Click")) {
			//Reading the actual column number ff table and set the column number with use of reference
			for(iCol = 1; iCol <= tot_col-1; iCol++){
				String Col_Val = columns.get(iCol).getAttribute("title").trim();
				if (Col_Val.contains(actColumn)){ 
					aColumn_number = iCol+1; 
//					System.out.println("act Column number is "+aColumn_number); 
					break; 
				}
			}
		
//			Returns the function of column names are not matched
			if (iCol >= tot_col) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Column Name "+actColumn+" is not found in the webtable, Please Verify ");
				System.out.println("Column Name "+actColumn+" is not found in the webtable, Please Verify ");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		}
		
		//Taking the row value
		WebElement rowValue = null;
		for(iRow =1; iRow < tot_row-1; iRow++){
			try {
				if (refColumn.equals("Stage")) {
					rowValue = driver.findElement(By.xpath(tXpath+"/tbody/tr["+iRow+"]/td["+rColumn_number+"]//child::span"));
				} else {
					rowValue = driver.findElement(By.xpath(tXpath+"/tbody/tr["+iRow+"]/td["+rColumn_number+"]//child::span[contains(@data-bind,'text')]"));
				}
	//			WebElement rowValue = driver.findElement(By.xpath(tXpath+"/tbody/tr["+iRow+"]/td["+rColumn_number+"]//child::span[contains(@data-bind,'text')]"));
				Row_Val = rowValue.getAttribute("title").trim();
				if (Row_Val.equalsIgnoreCase(rowRef)){ 
					row_number = iRow; 
	//				System.out.println("Row number is "+row_number);
					break;
				}
			} catch (Exception e) {
				continue;
			}
			
		}
		
//		Returns the function if rows names are not matched
		if (iRow >= tot_row-1) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Row Key "+rowRef+" is not found in the webtable, Please Verify ");
			System.out.println("Row Key "+rowRef+" is not found in the webtable, Please Verify ");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		waitForpageloadmask();
		
		WebElement Cell; String sOut = null;
		switch (ActionType) {
			case "Edit":
				Cell = driver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]"));
				WebInteractUtil.scrollIntoView(Cell);
				WebInteractUtil.click(Cell);
				WebElement edit_Box = driver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]//input"));
//				WebElement edit_Box = driver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]"));
				WebInteractUtil.clear(edit_Box);
				WebInteractUtil.click(edit_Box);
				Thread.sleep(2000);
				WebInteractUtil.sendKeys(edit_Box, ActionValue);
				sOut = "True";
				break;
			case "Select":
				Cell = driver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]"));
				WebInteractUtil.scrollIntoView(Cell);
				Cell.click();
				WebElement dropdown = driver.findElement(By.xpath("//descendant::ul[@role='listbox']"));
				List<WebElement> options = dropdown.findElements(By.tagName("li"));
				for (WebElement option : options)
				{
				    if (option.getText().equals(ActionValue))
				    {
				        option.click(); // click the desired option
				        break;
				    }
				}
				sOut = "True";
				break;
			case "Store":
				Cell = driver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]//child::span"));
				WebInteractUtil.scrollIntoView(Cell);
				sOut = Cell.getAttribute("title");
				break;
				
			case "Click":
				Cell = driver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+rColumn_number+"]"));
				WebInteractUtil.scrollIntoView(Cell);
				WebInteractUtil.click(Cell);
				sOut = "True";
				break;
		}
		
		return sOut;
	}
	
	public String MultiLineWebTableCellAction(String refColumn, String rowRef, String actColumn, String ActionType, String ActionValue, int RowNumber) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : MultiLineWebTableCellAction
		Purpose     : This method is to pick
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		int row_number = 0; String tXpath = null, Row_Val;
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.lineItemGridTable, 35)) {
			WebInteractUtil.scrollIntoView(CPQ_Objects.lineItemGridTable);
			String sXpath = CPQ_Objects.lineItemGridTable.toString();
			tXpath = sXpath.substring(sXpath.indexOf("xpath:")+"xpath:".length(), sXpath.indexOf("]]")+1).trim();
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "lineItemGridTable was not visible in CPQ Main Page, Please Verify");
			System.out.println("lineItemGridTable was not visible in CPQ Main Page, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
//		getting the row and column number
		List<WebElement> rows = CPQ_Objects.lineItemGridTable.findElements(By.tagName("tr"));
		List<WebElement> columns = rows.get(0).findElements(By.tagName("th"));
		int tot_row = rows.size();
		int tot_col = columns.size();
//		System.out.println("Total Column size is "+tot_col);
		int iCol, iRow, rColumn_number = 0, aColumn_number = 0;
		
		//Reading the column headers of table and set the column number with use of reference
		for(iCol = 1; iCol <= tot_col-1; iCol++){
			String Col_Val = columns.get(iCol).getAttribute("title").trim();
			if (Col_Val.equals(refColumn)){ 
				rColumn_number = iCol+1; 
//				System.out.println("ref Column number is "+rColumn_number); 
				break; 
			}
		}
		
//		Returns the function of reference column number
		if (iCol >= tot_col) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Column Name "+refColumn+" is not found in the webtable, Please Verify ");
			System.out.println("Column Name "+refColumn+" is not found in the webtable, Please Verify ");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		
		if (!ActionType.equalsIgnoreCase("Click") && !ActionType.equalsIgnoreCase("GetRow")) {
			//Reading the actual column number ff table and set the column number with use of reference
			for(iCol = 1; iCol <= tot_col-1; iCol++){
				String Col_Val = columns.get(iCol).getAttribute("title").trim();
				if (Col_Val.contains(actColumn)){ 
					aColumn_number = iCol+1; 
//					System.out.println("act Column number is "+aColumn_number); 
					break; 
				}
			}
		
//			Returns the function of column names are not matched
			if (iCol >= tot_col) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Column Name "+actColumn+" is not found in the webtable, Please Verify ");
				System.out.println("Column Name "+actColumn+" is not found in the webtable, Please Verify ");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		}
		
		//Taking the row value
		WebElement rowValue = null; 
		for(iRow = RowNumber; iRow < tot_row-1; iRow++){
			try {
				if (refColumn.equalsIgnoreCase("Product")) {
					rowValue = driver.findElement(By.xpath(tXpath+"/tbody/tr["+iRow+"]/td["+rColumn_number+"]//child::span[contains(@data-bind,'text')]"));
				} else {
					rowValue = driver.findElement(By.xpath(tXpath+"/tbody/tr["+iRow+"]/td["+rColumn_number+"]//child::span"));
				}
				Row_Val = rowValue.getAttribute("title").trim();
				if (Row_Val.equalsIgnoreCase(rowRef)){ 
					row_number = iRow; 
					break;
				}
			} catch (Exception e) {
				continue;
			}
		}
		
//		Returns the function if rows names are not matched
		if (iRow >= tot_row-1) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Row Key "+rowRef+" is not found in the webtable, Please Verify ");
			System.out.println("Row Key "+rowRef+" is not found in the webtable, Please Verify ");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
//		waitForpageloadmask();
		
		WebElement Cell; String sOut = null;
		switch (ActionType) {
			case "Edit":
				Cell = driver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]"));
				WebInteractUtil.scrollIntoView(Cell);
				WebInteractUtil.click(Cell);
				WebElement edit_Box = driver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]//input"));
				WebInteractUtil.sendKeysByJS(edit_Box, ActionValue);
				sOut = "True";
				break;
			case "Select":
				Cell = driver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]"));
				WebInteractUtil.scrollIntoView(Cell);
				Waittilljquesryupdated();
				Cell.click();
				WebElement dropdown = driver.findElement(By.xpath("//descendant::ul[@role='listbox']"));
				List<WebElement> options = dropdown.findElements(By.tagName("li"));
				for (WebElement option : options) {
				    if (option.getText().equals(ActionValue)) {
				        option.click(); // click the desired option
				        break;
				    }
				}
				sOut = "True";
				break;
			case "Store":
				Cell = driver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]//child::span"));
				WebInteractUtil.scrollIntoView(Cell);
				sOut = Cell.getAttribute("title");
				break;
				
			case "Click":
				Cell = driver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+rColumn_number+"]"));
//				WebInteractUtil.scrollIntoView(Cell);
				WebInteractUtil.click(Cell);
				sOut = "True";
				break;
				
			case "GetRow":
				sOut = Integer.toString(row_number);
				break;
		}
		
		return sOut;
	}
	
	public String addBillingInformation(String ProductName, String BCN_ID, String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : addBillingInformation
		Purpose     : This method is to add the billing information for the relavent product
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String sResult;
		String Environment = dataminer.fngetconfigvalue(System.getProperty("user.dir")+"\\TestData\\CPQ_testdata.xlsx", "Environment");
		
		String[] sTransactionsID = TransactionID.split("\\|");
		String tfile_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		String StartTime = null, EndTime = null, TimeDiff = null;
		
		if(ProductName.equalsIgnoreCase("Colt Ip Domain")) {ProductName="IP Domain";}
		if(ProductName.equalsIgnoreCase("Colt Ip Guardian")) {ProductName="IP Guardian";}
		if(ProductName.equalsIgnoreCase("Colt Managed Virtual Firewall")) {ProductName="IP Managed Virtual Firewall";}
		if(ProductName.equalsIgnoreCase("Colt Managed Dedicated Firewall")) {ProductName="IP Managed Dedicated Firewall";}
		
//		Clicking on the rows from the table
		sResult = WebTableCellAction("Product", ProductName, null,"Click", null);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		Initializing the driver
		WebDriver driver = DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
        
//      Clicking the Billing Information Button
		WebInteractUtil.scrollIntoView(CPQ_Objects.billingInformationBtn);
		WebInteractUtil.clickByJS(CPQ_Objects.billingInformationBtn);   
		// Capturing Start point of Transaction Capture
		StartTime = dateTimeUtil.fnGetCurrentTime();
        
//      Selecting the values from the contact
        if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.billingLookAapplyBtn, 75)) {
        	
        	// Capturing End point of Transaction Capture
        	EndTime = dateTimeUtil.fnGetCurrentTime();
        				
        	// Computing Difference between Transactions Capture
        	TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
        	System.out.println("TimeDiff is "+TimeDiff);
        				
        	//Entering the Values to the Data sheet
        	dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[0], ColName, TimeDiff);
        	
        	WebInteractUtil.scrollIntoView(CPQ_Objects.billingLookAapplyBtn);
        	for (int i = 1; i < 3; i++) {waitForpageloadmask(); }
            try {
            	FluentWait<WebDriver> fluentWait = new FluentWait<>(driver) 
					.withTimeout(120, TimeUnit.SECONDS)
    		        .pollingEvery(1000, TimeUnit.MILLISECONDS)
    		        .ignoring(NoSuchElementException.class);
    				fluentWait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//span[@title='"+BCN_ID+"']//parent::td//preceding-sibling::td//child::input[contains(@id,'sELECT')]"))));	
    				WebElement CheckBox = driver.findElement(By.xpath("//span[@title='"+BCN_ID+"']//parent::td//preceding-sibling::td//child::input[contains(@id,'sELECT')]"));
    				CheckBox.isEnabled();
    				CheckBox.sendKeys(Keys.SPACE);   
    				waitForpageloadmask();
            } catch(Exception e) {
            		ExtentTestManager.getTest().log(LogStatus.FAIL, "Error in selecting BCN from Billing lookup "+e.toString());
        			System.out.println("Error in selecting BCN from Billing lookup "+e.toString());
        			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
        			return "False";             
            }
        	WebInteractUtil.click(CPQ_Objects.billingLookAapplyBtn);
        	
        	// Capturing Start point of Transaction Capture
    		StartTime = dateTimeUtil.fnGetCurrentTime();
    		
        	WebInteractUtil.waitForInvisibilityOfElement(CPQ_Objects.billingLookAapplyBtn,120);
        	
        	// Capturing End point of Transaction Capture
        	EndTime = dateTimeUtil.fnGetCurrentTime();
        				
        	// Computing Difference between Transactions Capture
        	TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
        	System.out.println("TimeDiff is "+TimeDiff);
        				
        	//Entering the Values to the Data sheet
        	dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[1], ColName, TimeDiff);
        	
        	waitForpageloadmask();

        } else {
        	ExtentTestManager.getTest().log(LogStatus.FAIL, "Billing Information lookup is not Visible, Please Verify");
			System.out.println("Billing Information lookup is not Visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
        }
        
//      clicking the Billing Information Checkbox
		WebInteractUtil.scrollIntoView(CPQ_Objects.billingInfoCbx);
		WebInteractUtil.isEnabled(CPQ_Objects.billingInfoCbx);
		WebInteractUtil.click(CPQ_Objects.billingInfoCbx);
		for (int i = 1; i < 3; i++) { waitForpageloadmask(); }
        
//		Verifying the nrcBCN selected successfully or not
		String refColName = null;
		if (Environment.equalsIgnoreCase("PRD")) { refColName = "BCN"; } else { refColName = "BCN NRC"; }
	    String nrcBCN = WebTableCellAction("Product", ProductName, refColName,"Store", null).trim();
	    if (nrcBCN.equals(BCN_ID)) {
	        ExtentTestManager.getTest().log(LogStatus.PASS, "nrcBCN "+BCN_ID+" Successfully Selected for the product "+ProductName);
			System.out.println("nrcBCN "+BCN_ID+" Successfully Selected for the product "+ProductName);
	    } else {
	    	ExtentTestManager.getTest().log(LogStatus.FAIL, "nrcBCN "+BCN_ID+" not Selected for the product "+ProductName);
			System.out.println("nrcBCN "+BCN_ID+" not Selected for the product "+ProductName);
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
	    }
        
//	  Verifying the mrcBCN selected successfully or not
	  if (Environment.equalsIgnoreCase("PRD")) { refColName = "BCN"; } else { refColName = "BCN MRC"; }
      String mrcBCN = WebTableCellAction("Product", ProductName, refColName,"Store", null).trim();
      if (mrcBCN.equals(BCN_ID)) {
	        ExtentTestManager.getTest().log(LogStatus.PASS, "mrcBCN "+BCN_ID+" Successfully Selected for the product "+ProductName);
			System.out.println("mrcBCN "+BCN_ID+" Successfully Selected for the product "+ProductName);
      } else {
      		ExtentTestManager.getTest().log(LogStatus.FAIL, "mrcBCN "+BCN_ID+" not Selected for the product "+ProductName);
			System.out.println("mrcBCN "+BCN_ID+" not Selected for the product "+ProductName);
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
      }
        
//      Unchecking the Billing Information Checkbox
		WebInteractUtil.scrollIntoView(CPQ_Objects.billingInfoCbx);
		WebInteractUtil.click(CPQ_Objects.billingInfoCbx);    
		waitForpageloadmask();
		return "True";
	}
	
	public String generateSendProposal(String file_name, String Sheet_Name, String iScript, String iSubScript, String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : generateProposal
		Purpose     : This method is to generate proposal
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String Proposal_Language = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Proposal_Language");
		String Proposal_Notes = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Proposal_Notes");
		String Select_Workflow = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Select_Workflow");
		String To_Reciepient = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"To_Reciepient");
		String Product_Name = dataminer.fngetcolvalue(file_name, "Product_Configuration", iScript, iSubScript,"Product_Name");
		String Container_NRC = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Container_NRC");
		String Container_MRC = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Container_MRC");
		String Journey_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Journey_Type");
		
		String[] sTransactionsID = TransactionID.split("\\|");
		String tfile_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		String StartTime = null, EndTime = null, TimeDiff = null;
		
		String sResult;  WebElement genratePropsalLink;
		if(Journey_Type.contains("Container")) {genratePropsalLink=CPQ_Objects.customerSignatureLnk;}
		else {genratePropsalLink=CPQ_Objects.approvalLnk;}
		
		waitForpageloadmask();
		if (WebInteractUtil.waitForElementToBeVisible(genratePropsalLink, 25)) {
			WebInteractUtil.isPresent(CPQ_Objects.customerSignatureLnk, 25);
			WebInteractUtil.click(CPQ_Objects.customerSignatureLnk);
			waitForpageloadmask();
		} else {
			waitForpageloadmask();
			WebInteractUtil.isPresent(CPQ_Objects.generateProposalLnk, 25);
			WebInteractUtil.click(CPQ_Objects.generateProposalLnk);
			waitForpageloadmask();
		}
		
		if	(WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.proposalLanguageSelectedLst, 60)) {
			WebInteractUtil.selectByValueDIV(CPQ_Objects.proposalLanguageSelectedLst, CPQ_Objects.proposalLanguageLst, Proposal_Language);
			waitForpageloadmask();
			WebInteractUtil.sendKeys(CPQ_Objects.proposalNotesTxb,Proposal_Notes);
			WebInteractUtil.click(CPQ_Objects.generateProposalBtn);
			
			// Capturing Start point of Transaction Capture
			StartTime = dateTimeUtil.fnGetCurrentTime();
			
			waitForpageloadmask();
			
			// Capturing End point of Transaction Capture
			EndTime = dateTimeUtil.fnGetCurrentTime();
						
			// Computing Difference between Transactions Capture
			TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
			System.out.println("TimeDiff is "+TimeDiff);
						
			//Entering the Values to the Data sheet
			dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[0], ColName, TimeDiff);
			
			for (int i = 1; i < 2; i++) { waitForpageloadmask(); }
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.verifyProposalConfElem, 90)) {
				waitForpageloadmask();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Proposal got generated which to be send to customer");
				System.out.println("Proposal got generated which to be send to customer");
				WebInteractUtil.selectByValueDIV(CPQ_Objects.selectWorkflowLst, CPQ_Objects.workflowLst, Select_Workflow);
				waitForpageloadmask();
				WebInteractUtil.isPresent(CPQ_Objects.toRecepientLst, 60);
				WebInteractUtil.sendKeys(CPQ_Objects.toRecepientLst, To_Reciepient);
				
////				Calling the below method to save the details
//				sResult = saveCPQ("Main", "s", "s", "s");
//				if (sResult.equalsIgnoreCase("False")){ return "False"; }
//				waitForpageloadmask();
				
//				calling the below method to download the proposal
				String File_Name = CPQ_Objects.fileAttachmentLnk.getText();
				WebInteractUtil.click(CPQ_Objects.fileAttachmentLnk);
				// Capturing Start point of Transaction Capture
				StartTime = dateTimeUtil.fnGetCurrentTime();
				if (!File_Name.contains(".pdf")) { File_Name = File_Name+".pdf"; }
				
//				Checking if the file is existing or not and printing the file location into the testdata sheet
				EndTime = isFileDownloaded(System.getProperty("user.dir")+"\\src\\Data\\Downloads", File_Name);
				if (EndTime.equalsIgnoreCase("False")){ return "False"; }
				dataminer.fnsetcolvalue(file_name, Sheet_Name, iScript, iSubScript, "Proposal_Path", System.getProperty("user.dir")+"\\src\\Data\\Downloads\\"+File_Name);
		
				// Capturing End point of Transaction Capture
				EndTime = dateTimeUtil.fnGetCurrentTime();
							
				// Computing Difference between Transactions Capture
				TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
				System.out.println("TimeDiff is "+TimeDiff);
							
				//Entering the Values to the Data sheet
				dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[2], ColName, TimeDiff);
				
//				Sending proposal to the customer
				WebInteractUtil.click(CPQ_Objects.sendProposalBtn);
				
				// Capturing Start point of Transaction Capture
				StartTime = dateTimeUtil.fnGetCurrentTime();
				
				for (int i = 1; i < 2; i++) { waitForpageloadmask(); }
				
				// Capturing End point of Transaction Capture
				EndTime = dateTimeUtil.fnGetCurrentTime();
							
				// Computing Difference between Transactions Capture
				TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
				System.out.println("TimeDiff is "+TimeDiff);
							
				//Entering the Values to the Data sheet
				dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[1], ColName, TimeDiff);
				
				for (int i = 1; i < 2; i++) { waitForpageloadmask(); }
				sResult = validateCPQErrorMsg();
				if (sResult.equalsIgnoreCase("False")) { return "False"; }
				ExtentTestManager.getTest().log(LogStatus.PASS, "Proposal got Submited to customer");
				System.out.println("Proposal got Submited to customer");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Generate Proposal Link is not visible, Please verify");
				System.out.println("Generate Proposal Link is not visible, Please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "proposalLanguageSelectedLst is not visible, Please verify");
			System.out.println("proposalLanguageSelectedLst is not visible, Please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		return "True";
	}
	
	public String isFileDownloaded(String Download_Path, String File_Name) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : isFileDownloaded
		Purpose     : This method is to check whether the file is available in the directory or not
		Designer    : Vasantharaja C
		Created on  : 19th July 2020 
		Input       : String Download_Path, String File_Name
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variables
		int j = 0; int i = 0;
		String EndTime = null;
		
		for (j = 0; j < 10; j++) {
			File dir = new File(Download_Path);     
			File[] dir_contents = dir.listFiles();           
			for (i = 0; i < dir_contents.length; i++) {         
				if (dir_contents[i].getName().equals(File_Name)) {
					// Capturing End point of Transaction Capture
					EndTime = dateTimeUtil.fnGetCurrentTime();
					System.out.println("File name "+File_Name+" is available under the path "+Download_Path);
					ExtentTestManager.getTest().log(LogStatus.PASS, "File name "+File_Name+" is available under the path "+Download_Path);
					break;
				}
			}
			if (i >= dir_contents.length) {
				Thread.sleep(3000);
				continue;
			} else {
				break;
			}
		}
		
		if (j >=10) {
			System.out.println("File name "+File_Name+" is not available under the path "+Download_Path);
			ExtentTestManager.getTest().log(LogStatus.PASS, "File name "+File_Name+" is not available under the path "+Download_Path+" , please verify");
			return "False";
		}
		
		
		return EndTime;
		
	}
	
	public String deleteFile(String File_Name) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : deleteFile
		Purpose     : This method is to delete the file which downloaded earlier
		Designer    : Vasantharaja C
		Created on  : 19th July 2020 
		Input       : String Download_Path, String File_Name
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variables
		int i = 0;
		File_Name = File_Name.split("\\\\")[File_Name.split("\\\\").length-1].trim();
		String Download_Path = System.getProperty("user.dir")+"\\src\\Data\\Downloads";
		File dir = new File(Download_Path);     
		File[] dir_contents = dir.listFiles();           
		for (i = 0; i < dir_contents.length; i++) {         
			if (dir_contents[i].getName().equals(File_Name)) {
				dir_contents[i].delete();
				Thread.sleep(2000);
				System.out.println("File name "+File_Name+" was deleted under the path "+Download_Path);
				ExtentTestManager.getTest().log(LogStatus.PASS, "File name "+File_Name+" was deleted under the path "+Download_Path);
				break;
			}
		}		
		return "True";
		
	}
	
	public String saveCPQ(String ButtonType, String UserType, String UI_Type, String Transaction_ID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : saveCPQ
		Purpose     : This method is to Save CPQ records
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		WebElement sButton = null;
		String file_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		String StartTime = null; String EndTime = null;
		waitForpageloadmask();
		
		switch (ButtonType) {
		
			case "Main":
				sButton = CPQ_Objects.saveCPQBtn;
				break;
			case "Sub":
				sButton = CPQ_Objects.childSaveCPQBtn;
				break;
			case "PLTab":
				sButton = CPQ_Objects.pLSaveBtn;
				break;
		}
		
		if (UserType.equalsIgnoreCase("CST")) {
			WebInteractUtil.isPresent(CPQ_Objects.update4cBtn, 35);
			WebInteractUtil.click(CPQ_Objects.update4cBtn);
			for (int i = 1; i < 3; i++) {waitForpageloadmask(); }
			// Capturing Start point of Transaction Capture
			StartTime = dateTimeUtil.fnGetCurrentTime();
		}
		
		if (WebInteractUtil.waitForElementToBeVisible(sButton, 60)) {
//			WebInteractUtil.scrollIntoView(sButton);
			waitForpageloadmask();
			WebInteractUtil.click(sButton);
			
			if (!Transaction_ID.equals("") && !UserType.equalsIgnoreCase("CST")) {
				// Capturing Start point of Transaction Capture
				StartTime = dateTimeUtil.fnGetCurrentTime();
			}
			for (int i = 1; i < 2; i++) { waitForpageloadmask(); }
			if (!Transaction_ID.equals("")) {
				// Capturing End point of Transaction Capture
				EndTime = dateTimeUtil.fnGetCurrentTime();
				
				// Computing Difference between Transactions Capture
				String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
				System.out.println("TimeDiff is "+TimeDiff);
				
	//			Entering the Values to the Data sheet
				dataminer.fnsetTransactionValue(file_name, UI_Type, Transaction_ID, ColName, TimeDiff);
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Save Button in CPQ is not visible, Please verify");
			System.out.println("Save Button in CPQ is not visible, Please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
	}
	
	public String addContactInformationCPQ(String refColumn, String rowRef, String actColumn, String ActionType, String ActionValue) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : addContactInformationCPQ
		Purpose     : This method is to add the contact information for CPQ
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		String tXpath, Row_Val;
		int row_number = 0;
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.additonalInfoTable, 60)) {
			Waittilljquesryupdated();
			String sXpath = CPQ_Objects.additonalInfoTable.toString();
			tXpath = sXpath.substring(sXpath.indexOf("xpath:")+"xpath:".length(), sXpath.indexOf("]]")+1).trim();
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Contact Information Link in CPQ is not visible, Please verify");
			System.out.println("Contact Information Link in CPQ is not visible, Please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
//		getting the row and column number
		List<WebElement> rows = CPQ_Objects.additonalInfoTable.findElements(By.tagName("tr"));
		List<WebElement> columns = rows.get(0).findElements(By.tagName("th"));
		int tot_row = rows.size();
		int tot_col = columns.size();
//		System.out.println("Total Column size is "+tot_col);
		int iCol, iRow, rColumn_number = 0, aColumn_number = 0;
		
		//Reading the column headers of table and set the column number with use of reference
		for(iCol = 1; iCol <= tot_col-1; iCol++){
			String Col_Val = columns.get(iCol).getAttribute("title").trim();
			if (Col_Val.equals(refColumn)){ 
				rColumn_number = iCol+1; 
//				System.out.println("ref Column number is "+rColumn_number); 
				break; 
			}
		}
		
//		Returns the function of reference column number
		if (iCol > tot_col) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Column Name "+refColumn+" is not found in the webtable, Please Verify ");
			System.out.println("Column Name "+refColumn+" is not found in the webtable, Please Verify ");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		
		if (!ActionType.equals("Click")) {
			//Reading the actual column number ff table and set the column number with use of reference
			for(iCol = 1; iCol <= tot_col-1; iCol++){
				String Col_Val = columns.get(iCol).getAttribute("title").trim();
				if (Col_Val.equals(actColumn)){ 
					aColumn_number = iCol+1; 
//					System.out.println("act Column number is "+aColumn_number); 
					break; 
				}
			}
		
//			Returns the function of column names are not matched
			if (iCol > tot_col) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Column Name "+actColumn+" is not found in the webtable, Please Verify ");
				System.out.println("Column Name "+actColumn+" is not found in the webtable, Please Verify ");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		}
		
		//Taking the row value
		for(iRow =1; iRow <= tot_row-1; iRow++){
			WebElement rowValue = driver.findElement(By.xpath(tXpath+"/tbody/tr["+iRow+"]/td["+rColumn_number+"]//child::span"));
			Row_Val = rowValue.getAttribute("title").trim();
			if (Row_Val.equals(rowRef)){ 
				row_number = iRow; 
//				System.out.println("Row number is "+row_number);
				break;
			}
		}
		
//		Returns the function if rows names are not matched
		if (iCol > tot_col) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Row Name "+rowRef+" is not found in the webtable, Please Verify ");
			System.out.println("Row Name "+rowRef+" is not found in the webtable, Please Verify ");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		WebElement Cell; String sOut;
		switch (ActionType) {
			case "Edit":
				Cell = driver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]"));
				WebInteractUtil.scrollIntoView(Cell);
				for (int i = 1; i < 3; i++) { waitForpageloadmask(); }
				WebInteractUtil.click(Cell);
				WebElement edit_Box = driver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]//input"));
				WebInteractUtil.sendKeys(edit_Box, ActionValue);
				sOut = "True";
				break;
			case "Select":
				Cell = driver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]"));
				WebInteractUtil.scrollIntoView(Cell);
				Cell.click();
				WebElement dropdown = driver.findElement(By.xpath("//descendant::ul[@role='listbox']"));
				List<WebElement> options = dropdown.findElements(By.tagName("li"));
				for (WebElement option : options)
				{
				    if (option.getText().equals(ActionValue))
				    {
				        option.click(); // click the desired option
				        break;
				    }
				}
				sOut = "True";
				break;
			case "Store":
				Cell = driver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]//child::span"));
				WebInteractUtil.scrollIntoView(Cell);
				sOut = Cell.getAttribute("title");
				break;
				
			case "Click":
				Cell = driver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+rColumn_number+"]"));
				WebInteractUtil.scrollIntoView(Cell);
				WebInteractUtil.click(Cell);
				sOut = "True";
				break;
		}
		
		
		return "True";
	}
	
	public String contactInfoMultiLineEntry(String file_name, String Sheet_Name, String iScript, String iSubScript, String Product_Name) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : contactInfoEntry
		Purpose     : This method is to add the contact information for CPQ
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : String file_name, String iScript, String iSubScript, String Product_Name
		Output      : True/False
		 ----------------------------------------------------------------------*/ 	
		String sResult;
		String sProduct = null;
		
//		Initializing the Variable
		String No_Of_Copies = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"No_Of_Copies");

		if (Product_Name.contains("Data -")||Product_Name.contains("Voice -")) { sProduct = Product_Name; } else { sProduct = Product_Name.replaceAll("(?!^)([A-Z])", " $1"); }
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.contactInformationLnk, 60)) {
			WebInteractUtil.click(CPQ_Objects.contactInformationLnk);
			for (int i = 1; i < 5; i++) { waitForpageloadmask(); }
			Waittilljquesryupdated();
			WebInteractUtil.isPresent(CPQ_Objects.additonalInfoTable, 25);
			Waittilljquesryupdated();
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Contact Information Link in CPQ is not visible, Please verify");
			System.out.println("Contact Information Link in CPQ is not visible, Please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
//		Picking the Row Number of Each products
		String Rows[] = new String[Integer.parseInt(No_Of_Copies)+2];
		String QuoteRefs[] = new String[Integer.parseInt(No_Of_Copies)+2];
		String Row_Value = null; int j = 0; int Temp_Row = 1;
		for (j = 1; j <= Integer.parseInt(No_Of_Copies)+2; j++) {
			Row_Value = MultiLineWebTableCellAction("Product", Product_Name.replaceAll("(?!^)([A-Z])", " $1"), null,"GetRow", null, Temp_Row);
			Rows[j-1] = Row_Value;
			QuoteRefs[j-1] = MultiLineWebTableCellAction("Product", Product_Name.replaceAll("(?!^)([A-Z])", " $1"), "Quote Line Item Id","Store", null, Integer.parseInt(Row_Value)).trim();
			Temp_Row = Integer.parseInt(Row_Value)+1;
		}
		
		WebInteractUtil.scrollIntoView(CPQ_Objects.additonalInfoTable);
		
		for (int k = 1; k <= Integer.parseInt(No_Of_Copies)+2; k++) {
			
//			Calling the below function to enter A company name
			String Company_Name = dataminer.fngetcolvalue(file_name, "Other_Items", iScript, iSubScript,"Company_Name");
			sResult = addContactInformationCPQ("Line Item ID", QuoteRefs[k-1], "Company Name A End","Edit", Company_Name);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			waitForpageloadmask();
			
//			Calling the below function to enter B company name
			if (Product_Name.equals("EthernetLine") || Product_Name.equals("Wave")) {
				sResult = addContactInformationCPQ("Line Item ID", QuoteRefs[k-1], "Company Name B End","Edit", Company_Name);
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
				waitForpageloadmask();
				
//				Calling the below method to save the details
				sResult = saveCPQ("Main", "Sales", "Dummy", "", "Attempt_");
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
			}
		}		
		return "True";
		
	}
	
	public String navigateContactInfoTab() throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : navigateContactInfoTab
		Purpose     : This method is to navigate the contact info
		Designer    : Vasantharaja C
		Created on  : 21st July 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.contactInformationLnk, 60)) {
			WebInteractUtil.click(CPQ_Objects.contactInformationLnk);
			for (int i = 1; i < 3; i++) { waitForpageloadmask(); }
			Waittilljquesryupdated();
			WebInteractUtil.isPresent(CPQ_Objects.additonalInfoTable, 25);
			Waittilljquesryupdated();
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Contact Information Link in CPQ is not visible, Please verify");
			System.out.println("Contact Information Link in CPQ is not visible, Please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
		
	}
	
	public String contactInfoEntry(String file_name, String iScript, String iSubScript, String Product_Name) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : contactInfoEntry
		Purpose     : This method is to add the contact information for CPQ
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : String file_name, String iScript, String iSubScript, String Product_Name
		Output      : True/False
		 ----------------------------------------------------------------------*/ 	
		String sResult;
		String sProduct = null;

		if (Product_Name.contains("Data -")||Product_Name.contains("Voice -")) { sProduct = Product_Name; } else { sProduct = Product_Name.replaceAll("(?!^)([A-Z])", " $1"); }
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.contactInformationLnk, 60)) {
			WebInteractUtil.click(CPQ_Objects.contactInformationLnk);
			for (int i = 1; i < 3; i++) { waitForpageloadmask(); }
			Waittilljquesryupdated();
			WebInteractUtil.isPresent(CPQ_Objects.additonalInfoTable, 25);
			Waittilljquesryupdated();
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Contact Information Link in CPQ is not visible, Please verify");
			System.out.println("Contact Information Link in CPQ is not visible, Please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
//		Calling the below method to fetch the quote ID
		String QuoteLineItem = WebTableCellAction("Product", sProduct, "Quote Line Item Id","Store", null);
		if (QuoteLineItem.equalsIgnoreCase("False")){ return "False"; }
		
//		Calling the below function to enter A company name
		String Company_Name = dataminer.fngetcolvalue(file_name, "Other_Items", iScript, iSubScript,"Company_Name");
		sResult = addContactInformationCPQ("Line Item ID", QuoteLineItem, "Company Name A End","Edit", Company_Name);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		Calling the below function to enter B company name
		if (Product_Name.equals("EthernetLine") || Product_Name.equals("Wave")) {
			sResult = addContactInformationCPQ("Line Item ID", QuoteLineItem, "Company Name B End","Edit", Company_Name);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
		}
		
		return "True";
		
	}
	
	public String confirmOrder(String file_name, String Sheet_Name, String iScript, String iSubScript, String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : confirmOrder
		Purpose     : This method is to Confirm the Order
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String Quote_Action = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Quote_Action");
		String Status_Reason = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Status_Reason");
		String Proposal_Path = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Proposal_Path");
		
		String[] sTransactionsID = TransactionID.split("\\|");
		String tfile_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		String StartTime = null, EndTime = null, TimeDiff = null;
		
//		Initializing Objects		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.OrderLnk, 60)) {
			waitForpageloadmask();
			WebInteractUtil.click(CPQ_Objects.OrderLnk);
			waitForpageloadmask();
			WebInteractUtil.isPresent(CPQ_Objects.quoteActionLst, 30);
			Waittilljquesryupdated();
			WebInteractUtil.selectByValueDIV(CPQ_Objects.quoteActionLst, CPQ_Objects.ActionLst, Quote_Action);
			waitForpageloadmask();
			WebInteractUtil.isEnabled(CPQ_Objects.reasonStatusLst);
			WebInteractUtil.selectByValueDIV(CPQ_Objects.reasonStatusLst, CPQ_Objects.StatusLst, Status_Reason);
			waitForpageloadmask();
			//Adding the attachments
			CPQ_Objects.acceptanceDocumentTxb.sendKeys(Proposal_Path);
			waitForpageloadmask();
//			Clicking Customer request date using action
    		WebInteractUtil.clickByAction(CPQ_Objects.customerSignedDateElem);
    		waitForpageloadmask();
    		WebInteractUtil.clickByAction(CPQ_Objects.defaultDateElem);
    		waitForpageloadmask();
    		WebInteractUtil.isPresent(CPQ_Objects.confirmAttachmentTgleBtn, 40);
    		WebInteractUtil.clickByAction(CPQ_Objects.confirmAttachmentTgleBtn);
    		waitForpageloadmask();
    		WebInteractUtil.click(CPQ_Objects.confirmQuoteBtn);
    		
    		// Capturing Start point of Transaction Capture
    		StartTime = dateTimeUtil.fnGetCurrentTime();
    		
    		waitForpageloadmask();
    		
    		// Capturing End point of Transaction Capture
    		EndTime = dateTimeUtil.fnGetCurrentTime();
    					
    		// Computing Difference between Transactions Capture
    		TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
    		System.out.println("TimeDiff is "+TimeDiff);
    					
    		//Entering the Values to the Data sheet
    		dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[0], ColName, TimeDiff);
    		
    		for (int i = 1; i < 2; i++) { waitForpageloadmask(); }
    		if (WebInteractUtil.waitForInvisibilityOfElement(CPQ_Objects.confirmQuoteBtn, 120)) {
    			ExtentTestManager.getTest().log(LogStatus.PASS, "Quote has been Confirmed with the message "+CPQ_Objects.notificationBarCPQ.getText());
    			System.out.println("Quote has been Confirmed with the message "+CPQ_Objects.notificationBarCPQ.getText());
    			waitForpageloadmask();
    			Waittilljquesryupdated();
    			WebInteractUtil.click(CPQ_Objects.createOrderBtn);
        		// Capturing Start point of Transaction Capture
        		StartTime = dateTimeUtil.fnGetCurrentTime();
        		for (int i = 1; i < 3; i++) { waitForpageloadmask(); }
        		
        		// Capturing End point of Transaction Capture
        		EndTime = dateTimeUtil.fnGetCurrentTime();
        					
        		// Computing Difference between Transactions Capture
        		TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
        		System.out.println("TimeDiff is "+TimeDiff);
        					
        		//Entering the Values to the Data sheet
        		dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[1], ColName, TimeDiff);
        		
    			String sResult = validateCPQErrorMsg();
    			if (sResult.equalsIgnoreCase("False")) { return "False"; }
    			if (WebInteractUtil.waitForInvisibilityOfElement(CPQ_Objects.createOrderBtn, 120)) {
        			ExtentTestManager.getTest().log(LogStatus.PASS, "Order has been created "+CPQ_Objects.notificationBarCPQ.getText());
        			System.out.println("Order has been created "+CPQ_Objects.notificationBarCPQ.getText());
        			waitForpageloadmask();
        			WebInteractUtil.clickByJS(CPQ_Objects.generalInformationLnk);
        			waitForpageloadmask();
        		} else {
        			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order not Created with the message "+CPQ_Objects.notificationBarCPQ.getText());
        			System.out.println("Order not Created with the message "+CPQ_Objects.notificationBarCPQ.getText());
        			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
        			return "False";
        		}
    		} else {
    			ExtentTestManager.getTest().log(LogStatus.FAIL, "Quote not Confirmed with the message "+CPQ_Objects.notificationBarCPQ.getText());
    			System.out.println("Quote not Confirmed with the message "+CPQ_Objects.notificationBarCPQ.getText());
    			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
    			return "False";
    		}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "OrderLnk Button in CPQ is not visible, Please verify");
			System.out.println("OrderLnk Button in CPQ is not visible, Please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
	}
	
	public String overrideOnnetUllFibre(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException, SocketTimeoutException {
		/*----------------------------------------------------------------------
		Method Name : paratialSaveC4C
		Purpose     : This method is to Override Onnet or ULL Fibre connector type
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String Product_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		String A_End_Override = dataminer.fngetcolvalue(file_name, "A_End", iScript, iSubScript,"A_End_Override");
		String B_End_Override = dataminer.fngetcolvalue(file_name, "B_End", iScript, iSubScript,"B_End_Override");
		String Override_Reason = dataminer.fngetcolvalue(file_name, "A_End", iScript, iSubScript,"Override_Reason");
		
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.siteDetailsLnk, 60)) {
			waitForpageloadmask();
//			WebInteractUtil.clickByAction(CPQ_Objects.siteDetailsLnk);
			WebInteractUtil.click(CPQ_Objects.siteDetailsLnk);
			waitForpageloadmask();
			switch (Product_Name) {
				case "EthernetLine": case "EthernetHub": case "EthernetSpoke": case "Wave":
					
					if (A_End_Override.equals("ULL")) {
						CPQ_Objects.overrideToULLAEndCbx.sendKeys(Keys.SPACE);
						waitForpageloadmask();
						WebInteractUtil.sendKeys(CPQ_Objects.overrideReasonAEndTxb, Override_Reason);
						CPQ_Objects.AEndCorrectCbx.sendKeys(Keys.SPACE);
						waitForpageloadmask();
					} else if (A_End_Override.equals("Onnet")) {
						CPQ_Objects.overrideToOnnetAEndCbx.sendKeys(Keys.SPACE);
						waitForpageloadmask();
						WebInteractUtil.sendKeys(CPQ_Objects.overrideReasonAEndTxb, Override_Reason);
						CPQ_Objects.AEndCorrectCbx.sendKeys(Keys.SPACE);
						waitForpageloadmask();
					}
					
					if (!Product_Name.equalsIgnoreCase("EthernetHub") && !Product_Name.equalsIgnoreCase("EthernetSpoke")) {
						if (B_End_Override.equals("ULL")) {
							CPQ_Objects.overrideToULLBEndCbx.sendKeys(Keys.SPACE);
							waitForpageloadmask();
							WebInteractUtil.sendKeys(CPQ_Objects.overrideReasonBEndTxb, Override_Reason);
							CPQ_Objects.BEndCorrectCbx.sendKeys(Keys.SPACE);
							waitForpageloadmask();
						} else if (A_End_Override.equals("Onnet")) {
							CPQ_Objects.overrideToOnnetBEndCbx.sendKeys(Keys.SPACE);
							waitForpageloadmask();
							WebInteractUtil.sendKeys(CPQ_Objects.overrideReasonBEndTxb, Override_Reason);
							CPQ_Objects.BEndCorrectCbx.sendKeys(Keys.SPACE);
							waitForpageloadmask();
						}
						break;
					}
			}
			
//			Saving the Quote
//			WebInteractUtil.clickByJS(CPQ_Objects.updateBtn);
			WebInteractUtil.click(CPQ_Objects.updateBtn);
			for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
			WebInteractUtil.click(CPQ_Objects.saveToQuoteBtn);
			for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.returnToC4CBtn, 120)) {
				waitForpageloadmask();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Override Functionality is Successfull");
				System.out.println("Override Functionality is Successfull");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "returnToC4CBtn in CPQ is not visible, Please verify");
				System.out.println("returnToC4CBtn in CPQ is not visible, Please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "siteDetailsLnk in CPQ is not visible, Please verify");
			System.out.println("siteDetailsLnk in CPQ is not visible, Please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
	}
	
	public String returnC4CFromCPQ() throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : returnC4CFromCPQ
		Purpose     : This method is to return C4C from CPQ Interface
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.returnToC4CBtn, 60)) {
			waitForpageloadmask();
			WebInteractUtil.click(CPQ_Objects.returnToC4CBtn);
			for (int i = 1; i < 3; i++) {WaitforC4Cloader(); }
			WebInteractUtil.isPresent(CPQ_Objects.EngagementLnk, 120);
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.EngagementLnk, 60)) {
				WaitforC4Cloader();
				WebInteractUtil.scrollIntoView(CPQ_Objects.EngagementLnk);
				for (int i = 1; i < 2; i++) {WaitforC4Cloader(); }			
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "EngagementLnk in C4C is not visible, Please verify");
				System.out.println("EngagementLnk in C4C is not visible, Please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "returnToC4CBtn in CPQ is not visible, Please verify");
			System.out.println("returnToC4CBtn in CPQ is not visible, Please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
	}
	
	public String editPSEngagement(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : editPSEngagement
		Purpose     : This method is to assign PS engagement to the quote
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String PS_Engagement = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"PS_Engagement");
		String Engagement_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Engagement_Type");
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.EngagementLnk, 120)) {
			WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.EngagementLnk,60);
			WebInteractUtil.isEnabled(CPQ_Objects.EngagementLnk);
			WaitforC4Cloader();
			WebInteractUtil.click(CPQ_Objects.EngagementLnk);	
			for (int i = 1; i < 2; i++) {WaitforC4Cloader(); }
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.editEngagementLnk, 75)) {
				WaitforC4Cloader();
				WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.editEngagementLnk,60);
				WebInteractUtil.click(CPQ_Objects.editEngagementLnk);
				WaitforC4Cloader();
				switch (Engagement_Type) {
				case "SalesEngineer":
					WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.engageSECbx,60);
		    		WebInteractUtil.click(CPQ_Objects.engageSECbx);
					WaitforC4Cloader();
					WebInteractUtil.click(CPQ_Objects.seEngagementLst);
					WaitforC4Cloader();
					WebInteractUtil.ClickonElementByString("//li[normalize-space(.)='"+PS_Engagement+"']", 30);
					break;
				case "ConsultancyEngagement":
					WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.consultantCbx,60);
		    		WebInteractUtil.click(CPQ_Objects.consultantCbx);
					break;
				}
				WebInteractUtil.scrollIntoView(CPQ_Objects.saveC4CBtn);
				WebInteractUtil.click(CPQ_Objects.saveC4CBtn);
				WaitforC4Cloader();
				WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.headerBar,120);
				String sText = CPQ_Objects.headerBar.getText();
				if (sText.contains("saved")) {
					WaitforC4Cloader();
					ExtentTestManager.getTest().log(LogStatus.PASS, "SE/Consultant User Engaged to the Quote with message "+sText);
					System.out.println("SE/Consultant User Engaged to the Quote with message "+sText);
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to Edit Oppurtunity due to "+sText);
					System.out.println("Unable to Edit Oppurtunity due to "+sText);
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Failed to Click on editPSEngagementLnk in C4C, Please verify");
				System.out.println("Failed to Click on editPSEngagementLnk in C4C, Please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
			
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "psEngagementLnk in C4C is not visible, Please verify");
			System.out.println("psEngagementLnk in C4C is not visible, Please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
	}
	
	public String editProductConfiguration(String Product_Name, String UI_Type, String Transaction_ID, int iRow, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : editProductConfiguration
		Purpose     : This method is to edit the product configuration
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String sResult;
		String sProduct_Name = null;
		String tfile_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		
		if (Product_Name.contains("Data -")||Product_Name.contains("Voice -")) {
			sProduct_Name = Product_Name;
		} else {
			sProduct_Name = Product_Name.replaceAll("(?!^)([A-Z])", " $1");
		}
		
		sResult = MultiLineWebTableCellAction("Product", sProduct_Name, null,"Click", null, iRow);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		Thread.sleep(1500);
		
//		clicking on Reconfiguration link
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.reConfigureBtn, 120)) {
			waitForpageloadmask();
    		WebInteractUtil.click(CPQ_Objects.reConfigureBtn);
    		
			// Capturing Start point of Transaction Capture
			String StartTime = dateTimeUtil.fnGetCurrentTime();
			WebInteractUtil.pause(2000);
			waitForpageloadmask();
			
			if (Product_Name.equalsIgnoreCase("ColtIpAccess")) {
				waitForpageloadmask();
				WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.voiceBundledServiceElem,90);
			} else {
				WebInteractUtil.switchToFrame("siteAddressLink");
				waitForpageloadmask();
				waitForpageloadmask();
				WebInteractUtil.switchToDefaultFrame();
			}
			
			// Capturing End point of Transaction Capture
			String EndTime = dateTimeUtil.fnGetCurrentTime();
			
			// Computing Difference between Transactions Capture
			String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
			System.out.println("TimeDiff is "+TimeDiff);
			
//			Entering the Values to the Data sheet
			dataminer.fnsetTransactionValue(tfile_name, UI_Type, Transaction_ID, ColName, TimeDiff);
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "reConfigureBtn in CPQ is not visible, Please verify");
			System.out.println("reConfigureBtn in CPQ is not visible, Please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
	}
	
	public String clickProductConfigurationBtn(String sProduct_Name) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : clickProductConfigurationBtn
		Purpose     : This method is to click the product config button
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : String Product_Name
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String sResult, Product_Name;
//		Selecting the product
//		Product_Name = Product_Name.replaceAll("(?!^)([A-Z])", " $1");
		if (sProduct_Name.contains("Data -")||sProduct_Name.contains("Voice -")){ Product_Name = sProduct_Name; } 
		else if(sProduct_Name.equalsIgnoreCase("VPNNetwork")) {Product_Name="VPN Network";}
		else { Product_Name = sProduct_Name.replaceAll("(?!^)([A-Z])", " $1"); }
		
		sResult = WebTableCellAction("Product", Product_Name, null,"Click", null);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		clicking on Reconfiguration link
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.reConfigureBtn, 120)) {
			waitForpageloadmask();
    		WebInteractUtil.clickByAction(CPQ_Objects.reConfigureBtn);
    		waitForpageloadmask();
    		if (!Product_Name.equals("CPE Solutions Site")&&!Product_Name.equals("Colt Ip Access") && !sProduct_Name.contains("Data -") && !sProduct_Name.contains("Voice -")
    				&&!sProduct_Name.contains("VPNNetwork")) {
	    		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.siteDetailsLnk, 120)) {
	    			waitForpageloadmask();
	    		} else {
	    			ExtentTestManager.getTest().log(LogStatus.FAIL, "siteDetailsLnk in CPQ is not visible, Please verify");
	    			System.out.println("siteDetailsLnk in CPQ is not visible, Please verify");
	    			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
	    			return "False";
	    		}
    		} else if(Product_Name.equals("Colt Ip Access")) {
    			for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
    			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipL3ResilienceLnk, 120)) {
    				WebInteractUtil.click(CPQ_Objects.ipPrimaryConnectionLnk);
    				for (int i = 1; i < 4; i++) {waitForpageloadmask(); }
    				WebInteractUtil.click(CPQ_Objects.cpeIPAcessNextBtn);
    				for (int i = 1; i < 3; i++) {waitForpageloadmask();}
    				waitForpageloadmask();
	    		} else {
	    			ExtentTestManager.getTest().log(LogStatus.FAIL, "L3 Resilience in CPQ is not visible, Please verify");
	    			System.out.println("L3 Resilience in CPQ is not visible, Please verify");
	    			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
	    			return "False";
	    		}
    		}else if(Product_Name.equals("VPNNetwork")) {
    			WaitforVpnloader();
    			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.vpnNetworkTreeLink, 120)) {
    				ExtentTestManager.getTest().log(LogStatus.PASS, "Vpn Network in CPQ is visible, Please verify");
    			}else {
    	    		ExtentTestManager.getTest().log(LogStatus.FAIL, "Vpn Network in CPQ is not visible, Please verify");
    	    		System.out.println("Vpn Network in CPQ is not visible, Please verify");
    	    		ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
    	    		return "False";
    			}
    		}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "reConfigureBtn in CPQ is not visible, Please verify");
			System.out.println("reConfigureBtn in CPQ is not visible, Please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}

		return "True";
	
	}
	
	public String productConfiguration(String file_name, String Sheet_Name, String iScript, String iSubScript, String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : productConfiguration
		Purpose     : This method is to configure the product
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String sResult;
		String Product_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		String Flow_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Flow_Type");
		String Discount_Applicable = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Discount_Applicable");
		String[] sTransactionsID = TransactionID.split("\\|");
		
//		calling the below entry to enter the site details
		String rTransaction = sTransactionsID[0] +"|"+ sTransactionsID[1]+"|"+ sTransactionsID[2];
		sResult = siteDetailEntry(file_name, Sheet_Name, iScript, iSubScript, UI_Type, rTransaction, ColName);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		switch (Flow_Type.toUpperCase()) {	
			case "MANUALOFFNET": case "AUTOMATEDOFFNET":
				rTransaction = sTransactionsID[3] +"|"+ sTransactionsID[4]+"|"+ sTransactionsID[5]+"|"+sTransactionsID[6] +"|"+ sTransactionsID[7]+"|"+ sTransactionsID[8];
				sResult = offnetConfiguration(file_name, Sheet_Name, iScript, iSubScript, UI_Type, rTransaction, ColName);
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
				break;
		}
		
		if(Product_Name.equalsIgnoreCase("ColtIpAccess")) {
			sResult = routerTypeConfiguration(file_name, Sheet_Name, iScript, iSubScript);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
		}
		
//		Calling the below method if the discounts are applicable
		if (!Product_Name.equalsIgnoreCase("ColtIpAccess") && Flow_Type.equalsIgnoreCase("Onnet")) {
			sResult = selectCPQProductFeatures(file_name, Sheet_Name, iScript, iSubScript, "New" , UI_Type, sTransactionsID[3], ColName);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
		}
		
//		calling the below entry to add additional product data entries
		sResult = addtionalProductdata(file_name, Sheet_Name, iScript, iSubScript, UI_Type, TransactionID, ColName);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		return "True";
	}
	
	public String selectCPQProductFeatures(String file_name, String Sheet_Name, String iScript, String iSubScript, String Feature_Type, String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : selectCPQProductFeatures
		Purpose     : This method is to select the applicable product features for discounting process
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String sResult;
		String Product_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		String tfile_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		String StartTime = null, EndTime = null;
		
		switch (Product_Name.toUpperCase()) {
			
		case "ETHERNETLINE": case "ETHERNETSPOKE": case "ETHERNETHUB": case "WAVE":
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.featuresLnk, 120)) {
				WebInteractUtil.click(CPQ_Objects.featuresLnk);
				waitForpageloadmask();
				
//				switch (Feature_Type) {
//					case "New":
//						WebInteractUtil.isPresent(CPQ_Objects.outsideBusinessHoursInstallationAEndCbx, 30);
//						CPQ_Objects.outsideBusinessHoursInstallationAEndCbx.sendKeys(Keys.SPACE);
//						break;
//					case "Mod":
//						WebInteractUtil.isPresent(CPQ_Objects.longLiningAEndCbx, 30);
//						CPQ_Objects.longLiningAEndCbx.sendKeys(Keys.SPACE);
//						break;
//				}
				
				WebInteractUtil.isPresent(CPQ_Objects.outsideBusinessHoursInstallationAEndCbx, 30);
				CPQ_Objects.outsideBusinessHoursInstallationAEndCbx.sendKeys(Keys.SPACE);
				
				// Capturing Start point of Transaction Capture
				StartTime = dateTimeUtil.fnGetCurrentTime();
				
				waitForpageloadmask();
				
				WebInteractUtil.isPresent(CPQ_Objects.longLiningAEndCbx, 30);
				CPQ_Objects.longLiningAEndCbx.sendKeys(Keys.SPACE);
				waitForpageloadmask();
				
				// Capturing End point of Transaction Capture
				EndTime = dateTimeUtil.fnGetCurrentTime();
							
				// Computing Difference between Transactions Capture
				String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
				System.out.println("TimeDiff is "+TimeDiff);
							
				//Entering the Values to the Data sheet
				dataminer.fnsetTransactionValue(tfile_name, UI_Type, TransactionID, ColName, TimeDiff);
					
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "featuresLnk is not visible in CPQ, please verify");
				System.out.println("featuresLnk is not visible in CPQ, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
			break;
			
		case "COLTIPACCESS":
			if (Feature_Type.equals("Mod")) {
				WebInteractUtil.isPresent(CPQ_Objects.ipServiceAddonsLnk, 60);
				WebInteractUtil.click(CPQ_Objects.ipServiceAddonsLnk);
				waitForpageloadmask();
			} else {
				WebInteractUtil.isPresent(CPQ_Objects.ipSiteAddonsLnk, 60);
				WebInteractUtil.click(CPQ_Objects.ipSiteAddonsLnk);
				waitForpageloadmask();
			}
				
			switch (Feature_Type) {
				case "New":
					WebInteractUtil.click(CPQ_Objects.outsideBusinessHoursInstallationAEndCbx);
					break;
				case "Mod":
					WebInteractUtil.click(CPQ_Objects.longLiningAEndCbx);
					break;
			}
				
			// Capturing Start point of Transaction Capture
			StartTime = dateTimeUtil.fnGetCurrentTime();
			
			waitForpageloadmask();
			
			// Capturing End point of Transaction Capture
			EndTime = dateTimeUtil.fnGetCurrentTime();
						
			// Computing Difference between Transactions Capture
			String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
			System.out.println("TimeDiff is "+TimeDiff);
						
			//Entering the Values to the Data sheet
			dataminer.fnsetTransactionValue(tfile_name, UI_Type, TransactionID, ColName, TimeDiff);
			break;
		}
		return "True";
	}
	
	public String partialSaveCPQ(String UI_Type, String Transaction_ID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : partialSaveCPQ
		Purpose     : This method is to Partial Save the CPQ Entries
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/
		
		String file_name = System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		String sResult;
		waitForpageloadmask();
		String[] sTransactionsID = Transaction_ID.split("\\|");
		
		//Update the product before Save
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.partialSaveAdditionalCbx, 120)) {
			waitForpageloadmask();
			CPQ_Objects.partialSaveAdditionalCbx.sendKeys(Keys.SPACE);
			
//			 Capturing Start point of Transaction Capture
			String StartTime = dateTimeUtil.fnGetCurrentTime();
			
			for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
			
			// Capturing End point of Transaction Capture
			String EndTime = dateTimeUtil.fnGetCurrentTime();
			
			// Computing Difference between Transactions Capture
			String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
			System.out.println("TimeDiff is "+TimeDiff);
			
//			Entering the Values to the Data sheet
			dataminer.fnsetTransactionValue(file_name, UI_Type, sTransactionsID[0], ColName, TimeDiff);
			
//			Calling the below method to save the details
			sResult = updateSaveProductCPQ("SaveToQuote", UI_Type, sTransactionsID[1], ColName);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }

		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Update Button is not visible in CPQ, please verify");
			System.out.println("Update Button is not visible in CPQ, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		
		return "True";
		
	}
	
	public String updateSaveProductCPQ(String SaveType, String UI_Type, String Transaction_ID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : updateSaveProductCPQ
		Purpose     : This method is to update and save the cpq entries
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/
		
		String file_name = System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		String sResult;
		waitForpageloadmask();
		
		//Update the product before Save
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.update4cBtn, 120)) {
			waitForpageloadmask();
			WebInteractUtil.click(CPQ_Objects.update4cBtn);
			for (int i = 1; i < 3; i++) {waitForpageloadmask(); }
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Update Button is not visible in CPQ, please verify");
			System.out.println("Update Button is not visible in CPQ, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}	
		
		
		switch (SaveType.toUpperCase()) {
		
			case "SAVETOQUOTE":
				//Saving the Quote Details
				if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.saveToQuoteBtn, 120)) {
					waitForpageloadmask();
					WebInteractUtil.click(CPQ_Objects.saveToQuoteBtn);
					// Capturing Start point of Transaction Capture
					String StartTime = dateTimeUtil.fnGetCurrentTime();
					for (int i = 1; i < 3; i++) {waitForpageloadmask(); }
					WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.returnToC4CBtn, 90);
					for (int i = 1; i < 3; i++) {waitForpageloadmask(); }
					// Capturing End point of Transaction Capture
					String EndTime = dateTimeUtil.fnGetCurrentTime();
					
					// Computing Difference between Transactions Capture
					String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
					System.out.println("TimeDiff is "+TimeDiff);
					
//					Entering the Values to the Data sheet
					dataminer.fnsetTransactionValue(file_name, UI_Type, Transaction_ID, ColName, TimeDiff);
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "saveToQuoteBtn Button is not visible in CPQ, please verify");
					System.out.println("saveToQuoteBtn Button is not visible in CPQ, please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}
				break;
				
			case "SAVE":
	//			Calling the below method to save the details
				sResult = saveCPQ("Sub", "Sales",UI_Type, Transaction_ID, ColName);
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
				break;
		}
		return "True";
	}
	
	
	public String siteDetailEntry(String file_name, String Sheet_Name, String iScript, String iSubScript, String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : siteDetail
		Purpose     : This method is to add the entries for site details
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String sResult;
		String Service_Bandwidth = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Service_Bandwidth");
		String Product_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		String Resilience=dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Resilience");
		String Contract_Term_year = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Contract_Term_Year");
		String Billing_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Billing_Type");
		String VPN_Link_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"VPN_Link_Type");
		String Maximum_Bandwidth = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Maximum_Bandwidth");
		String[] sTransactionsID = TransactionID.split("\\|");
		String StartTime = null, EndTime = null;
		String tfile_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		
		switch(Product_Name) {
		
		case "EthernetLine": case "EthernetHub": case "Wave": case "EthernetSpoke":
//			calling the below method to perform site details operation
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.siteDetailsLnk, 120)) {
	    		WebInteractUtil.click(CPQ_Objects.siteDetailsLnk);
	    		waitForpageloadmask();
	    		
	    		//Service BandWidth
				if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.serviceBandwidthLst, 120)) {
					waitForpageloadmask();
					if (Product_Name.equalsIgnoreCase("Wave")) {
						String Interface = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Interface");
						WebInteractUtil.isEnabled(CPQ_Objects.interfaceLst);
						WebInteractUtil.selectByValue(CPQ_Objects.interfaceLst, Interface);
						waitForpageloadmask();
		    		}
					WebInteractUtil.selectByValue(CPQ_Objects.serviceBandwidthLst, Service_Bandwidth);
					
					// Capturing Start point of Transaction Capture
					StartTime = dateTimeUtil.fnGetCurrentTime();
					waitForpageloadmask();
					
					// Capturing End point of Transaction Capture
					EndTime = dateTimeUtil.fnGetCurrentTime();
								
					// Computing Difference between Transactions Capture
					String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
					System.out.println("TimeDiff is "+TimeDiff);
								
					//Entering the Values to the Data sheet
					dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[0], ColName, TimeDiff);
					
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "serviceBandwidthLst in CPQ is not visible, Please verify");
					System.out.println("serviceBandwidthLst in CPQ is not visible, Please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				} 
				
				//Contract Term
				if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.contractYear, 120)) {
					if (Product_Name.equalsIgnoreCase("Wave")) {
						String Interface = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Interface");
						WebInteractUtil.isEnabled(CPQ_Objects.interfaceLst);
						WebInteractUtil.selectByValue(CPQ_Objects.interfaceLst, Interface);
						waitForpageloadmask();
		    		}
					WebInteractUtil.selectByValue(CPQ_Objects.contractYear, Contract_Term_year);
					// Capturing Start point of Transaction Capture
					StartTime = dateTimeUtil.fnGetCurrentTime();
					
					waitForpageloadmask();
					
					// Capturing End point of Transaction Capture
					EndTime = dateTimeUtil.fnGetCurrentTime();
								
					// Computing Difference between Transactions Capture
					String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
					System.out.println("TimeDiff is "+TimeDiff);
								
					//Entering the Values to the Data sheet
					dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[1], ColName, TimeDiff);
					
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Contract Term Year in CPQ is not visible, Please verify");
					System.out.println("Contract Term Year in CPQ is not visible, Please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}
				
				//Resliance Type
				if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.reslianceType, 120)) {
					waitForpageloadmask();
					if (Product_Name.equalsIgnoreCase("Wave")) {
						String Interface = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Interface");
						WebInteractUtil.isEnabled(CPQ_Objects.interfaceLst);
						WebInteractUtil.selectByValue(CPQ_Objects.interfaceLst, Interface);
						waitForpageloadmask();
		    		}
					WebInteractUtil.selectByValue(CPQ_Objects.reslianceType, Resilience);
					// Capturing Start point of Transaction Capture
					StartTime = dateTimeUtil.fnGetCurrentTime();
					
					waitForpageloadmask();
					
					// Capturing End point of Transaction Capture
					EndTime = dateTimeUtil.fnGetCurrentTime();
								
					// Computing Difference between Transactions Capture
					String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
					System.out.println("TimeDiff is "+TimeDiff);
								
					//Entering the Values to the Data sheet
					dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[2], ColName, TimeDiff);
					
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Resliance type in CPQ is not visible, Please verify");
					System.out.println("Resliance type in CPQ is not visible, Please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}  
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "siteDetailsLnk in CPQ is not visible, Please verify");
				System.out.println("siteDetailsLnk in CPQ is not visible, Please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			} 
			break;
			
		case "ColtIpAccess":
			
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.serviceBandwidthLst, 120)) {
				waitForpageloadmask();
				WebInteractUtil.selectByValue(CPQ_Objects.serviceBandwidthLst, Service_Bandwidth);
				
				// Capturing Start point of Transaction Capture
				StartTime = dateTimeUtil.fnGetCurrentTime();
				
				waitForpageloadmask();
				
				// Capturing End point of Transaction Capture
				EndTime = dateTimeUtil.fnGetCurrentTime();
							
				// Computing Difference between Transactions Capture
				String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
				System.out.println("TimeDiff is "+TimeDiff);
							
				//Entering the Values to the Data sheet
				dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[0], ColName, TimeDiff);
				
				WebInteractUtil.selectByValue(CPQ_Objects.ipContractTermLst, Contract_Term_year);
				
				// Capturing Start point of Transaction Capture
				StartTime = dateTimeUtil.fnGetCurrentTime();
				
				waitForpageloadmask();
				
				// Capturing End point of Transaction Capture
				EndTime = dateTimeUtil.fnGetCurrentTime();
							
				// Computing Difference between Transactions Capture
				TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
				System.out.println("TimeDiff is "+TimeDiff);
							
				//Entering the Values to the Data sheet
				dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[1], ColName, TimeDiff);
				
				WebInteractUtil.selectByValue(CPQ_Objects.ipL2ReilienceLst, Resilience);
				
				// Capturing Start point of Transaction Capture
				StartTime = dateTimeUtil.fnGetCurrentTime();
				
				waitForpageloadmask();
				
				// Capturing End point of Transaction Capture
				EndTime = dateTimeUtil.fnGetCurrentTime();
							
				// Computing Difference between Transactions Capture
				TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
				System.out.println("TimeDiff is "+TimeDiff);
							
				//Entering the Values to the Data sheet
				dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[2], ColName, TimeDiff);
				
				WebInteractUtil.isPresent(CPQ_Objects.ipBillingTypeLst, 20);
				WebInteractUtil.selectByValue(CPQ_Objects.ipBillingTypeLst, Billing_Type);
				waitForpageloadmask();
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Primary Connection tab in CPQ is not visible, Please verify");
				System.out.println("Primary Connection tab in CPQ is not visible, Please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
			break;
			
		case "VPNNetwork":
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.vpnsiteConfiguartionLink, 120)) {
				WebInteractUtil.click(CPQ_Objects.vpnsiteConfiguartionLink);
				waitForpageloadmask();
				WebInteractUtil.click(CPQ_Objects.vpnlinkTypeLink);
				WebInteractUtil.ClickonElementByString("//li[normalize-space(.)='"+VPN_Link_Type+"']", 30);
				WaitforVpnloader();
				
				WebInteractUtil.click(CPQ_Objects.vpnContractYearLnk);
				WebInteractUtil.ClickonElementByString("//li[normalize-space(.)='"+Contract_Term_year+"']", 30);
				WaitforVpnloader();
				
				WebInteractUtil.click(CPQ_Objects.vpnServiceBandLnk);
				WebInteractUtil.ClickonElementByString("//li[normalize-space(.)='"+Service_Bandwidth+"']", 30);
				WaitforVpnloader();
				
				WebInteractUtil.click(CPQ_Objects.vpnResilienceLnk);
				WebInteractUtil.ClickonElementByString("//li[normalize-space(.)='"+Resilience+"']", 30);
				WaitforVpnloader();
				
				ExtentTestManager.getTest().log(LogStatus.PASS, "Vpn Product Configuration is done, Please verify");
				System.out.println("Vpn Product Configuration is done, Please verify");
				waitForpageloadmask();
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Primary Connection tab in CPQ is not visible, Please verify");
				System.out.println("Primary Connection tab in CPQ is not visible, Please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
			break;
		}
		
		return "True";
	}
	
	public String configCompleteSendToSales() throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : productConfiguration
		Purpose     : This method is to click config product and send to sales
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : NA
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.confCompleteSendSalesBtn, 120)) {
			waitForpageloadmask();
			WebInteractUtil.clickByAction(CPQ_Objects.confCompleteSendSalesBtn);
			waitForpageloadmask();
			if (WebInteractUtil.waitForInvisibilityOfElement(CPQ_Objects.confCompleteSendSalesBtn, 120)) {
				waitForpageloadmask();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Configuration complete and quote sent to sales user");
				System.out.println("Configuration complete and quote sent to sales user");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Configuration cannot be completed, please verify");
				System.out.println("Configuration cannot be completed, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			} 
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "confCompleteSendSalesBtn in CPQ is not visible, Please verify");
			System.out.println("confCompleteSendSalesBtn in CPQ is not visible, Please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		} 
		
		return "True";
		
	}
	
	public String captureServiceOrder(String file_name, String Sheet_Name, String iScript,String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : captureServiceOrder
		Purpose     : This method is to capture the service order from CPQ
		Designer    : Vasantharaja C
		Created on  : 22nd April 2020 
		Input       : NA
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
//		Reading the runtime data values
		String Product_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.orderDetailsCbx, 75)) {
			WebInteractUtil.scrollIntoView(CPQ_Objects.orderDetailsCbx);
			waitForpageloadmask();
			WebInteractUtil.clickByAction(CPQ_Objects.orderDetailsCbx);
			for (int i = 1; i < 3; i++) { waitForpageloadmask(); }
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "orderDetailsCbx in CPQ is not visible, Please verify");
			System.out.println("orderDetailsCbx in CPQ is not visible, Please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
//      capturing the service order for the respective product
		if (Product_Name.equalsIgnoreCase("CPESolutions")) {			
			String sProduct = "CPE Solutions Service|CPE Solutions Site";
			String sField = "CPE_Service_Order|CPE_Site_Order";
		    String[] Product_Split = sProduct.split("\\|");
		    String[] Field_Name = sField.split("\\|");
		    for (int i=0; i < Product_Split.length; i++)
		    {
		    	String Service_Order = WebTableCellAction("Product", Product_Split[i], "Service Order No","Store", null).trim();
				if (Service_Order.equalsIgnoreCase("False")){ return "False"; }
				//Printing the order reference
				System.out.println("Service Order for the product "+Product_Split[i]+" is "+Service_Order);
				ExtentTestManager.getTest().log(LogStatus.PASS, "Service Order for the product "+Product_Split[i]+" is "+Service_Order);
//				Exporting Service Order values to the testdata sheet
				dataminer.fnsetcolvalue(file_name, Sheet_Name, iScript, iSubScript, Field_Name[i], Service_Order);
		    }
			
		} else {
//			String sProduct = Product_Name.replaceAll("(?!^)([A-Z])", " $1");
			String sProduct = null;
			if (Product_Name.contains("Data -")||Product_Name.contains("Voice -")) {sProduct = Product_Name;}
			else if(Product_Name.equalsIgnoreCase("ColtIpDomain")) {sProduct="IP Domain";}
			else if(Product_Name.equalsIgnoreCase("ColtIpGuardian")) {sProduct="IP Guardian";}
			else if(Product_Name.equalsIgnoreCase("ColtManagedVirtualFirewall")) {sProduct="IP Managed Virtual Firewall";}
			else if(Product_Name.equalsIgnoreCase("ColtManagedDedicatedFirewall")) {sProduct="IP Managed Virtual Firewall";}
			else {sProduct = Product_Name.replaceAll("(?!^)([A-Z])", " $1");}
			
			String Service_Order = WebTableCellAction("Product", sProduct, "Service Order No","Store", null).trim();
			if (Service_Order.equalsIgnoreCase("False")){ return "False"; }
//			Printing the order reference
			System.out.println("Service Order for the product "+sProduct+" is "+Service_Order);
			ExtentTestManager.getTest().log(LogStatus.PASS, "Service Order for the product "+sProduct+" is "+Service_Order);
//			Exporting Service Order values to the testdata sheet
			dataminer.fnsetcolvalue(file_name, Sheet_Name, iScript, iSubScript, "Service_Order", Service_Order);
		}
		
		
//      Unchecking the Order Details Checkbox
		WebInteractUtil.scrollIntoView(CPQ_Objects.orderDetailsCbx);
		WebInteractUtil.clickByAction(CPQ_Objects.orderDetailsCbx);    
		waitForpageloadmask();
		
		return "True";
	}
	
	public String retriveServiceOrder(String Product_Name, int RowNumber) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : retriveServiceOrder
		Purpose     : This method is to return the service order from CPQ
		Designer    : Vasantharaja C
		Created on  : 17th July 2020 
		Input       : NA
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.orderDetailsCbx, 75)) {
			WebInteractUtil.scrollIntoView(CPQ_Objects.orderDetailsCbx);
			waitForpageloadmask();
			WebInteractUtil.clickByAction(CPQ_Objects.orderDetailsCbx);
			for (int i = 1; i < 3; i++) { waitForpageloadmask(); }
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "orderDetailsCbx in CPQ is not visible, Please verify");
			System.out.println("orderDetailsCbx in CPQ is not visible, Please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
//      capturing the service order for the respective product
		String sProduct = null;
		if (Product_Name.contains("Data -")||Product_Name.contains("Voice -")) { sProduct = Product_Name; } else { sProduct = Product_Name.replaceAll("(?!^)([A-Z])", " $1"); }
		String Service_Order = MultiLineWebTableCellAction("Product", sProduct, "Service Order No","Store", null, RowNumber).trim();
		if (Service_Order.equalsIgnoreCase("False")){ return "False"; }
//		Printing the order reference
		System.out.println("Service Order for the product "+sProduct+" is "+Service_Order);
		ExtentTestManager.getTest().log(LogStatus.PASS, "Service Order for the product "+sProduct+" is "+Service_Order);
	
		
//      Unchecking the Order Details Checkbox
		WebInteractUtil.scrollIntoView(CPQ_Objects.orderDetailsCbx);
		WebInteractUtil.clickByAction(CPQ_Objects.orderDetailsCbx);    
		waitForpageloadmask();
		
		return Service_Order;
	}
	
	public String addHubrefernce(String Product_Name) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : addQuoteInC4C
		Purpose     : This method is to add a quote from oppurtunity tab
		Designer    : Kashyap D
		Created on  : 24th June 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
//		Initializing the Variable
		String product=Product_Name.replaceAll("(?!^)([A-Z])", " $1");
		String HubId;
		switch(Product_Name) {
		
		case "EthernetSpoke":{
			HubId  = WebTableCellAction("Product", product, "Hub ID","Store", null).trim();
			if(HubId.length()>0) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Hub ID is avaliable");
				return HubId;
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Hub Refrence ID is not avaliable, Please Verify");
				System.out.println("Hub Refrence ID is not avaliable, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		}
		case "EthernetHub":{
			HubId  = WebTableCellAction("Product", product, "Hub ID","Store", null).trim();
			if(HubId.length()>0) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Hub ID is avaliable");
				return HubId;
				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Hub Refrence ID is not avaliable, Please Verify");
					System.out.println("Hub Refrence ID is not avaliable, please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}
			}
		}
		return "True";
	}	
	
	public String ipAccessConfiguration(String file_name,String Sheet_Name, String iScript, String iSubScript, String UI_Type, String TransactionID, String ColName) throws InterruptedException, IOException, ParseException {
		/*----------------------------------------------------------------------
		Method Name : addtionalProductdataEntries
		Purpose     : This method is to add the IP Access configuration data Entries
		Designer    : Kashyap D
		Created on  : 25th June 2020 
		Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		
//    Initializing the Variable
		String sResult;
		
		//Site Addons
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipSiteAddonsLnk, 120)) {
			WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.cpeIPAcessNextBtn, 90);
			WebInteractUtil.click(CPQ_Objects.cpeIPAcessNextBtn);
			for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
			WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipL3ResilianceLnk, 90);
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Site Addons is not avaliable, Please Verify");
			System.out.println("Site Addons is not avaliable, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		
//		L3 Resiliance
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipL3ResilianceLnk, 90)) {
			//L3 Resilience configuration
			sResult = ipAccessL3Resilience(file_name, Sheet_Name, iScript, iSubScript, UI_Type, TransactionID, ColName);
			if (sResult.equalsIgnoreCase("False")) {return "False";}
			WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipDiversitySelector, 60);
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "L3 Resliance Configuration is not completed, Please Verify");
			System.out.println("L3 Resliance Configuration is not completed, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
//		 Diversity
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipDiversitySelector, 120)) {
			WebInteractUtil.scrollIntoView(CPQ_Objects.cpeIPAcessNextBtn);
			WebInteractUtil.click(CPQ_Objects.cpeIPAcessNextBtn);
			for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
			WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipCustomerRequired, 90);
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Diversity type is not avaliable, Please Verify");
			System.out.println("Diversity type is not avaliable, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
			}
		
//		Service Addons
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipCustomerRequired, 120)) {
			WebInteractUtil.scrollIntoView(CPQ_Objects.cpeIPAcessNextBtn);
			WebInteractUtil.click(CPQ_Objects.cpeIPAcessNextBtn);
			for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
			WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipBeSpokeFeature, 90);
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Service Addons type is not avaliable, Please Verify");
			System.out.println("Service Addons type is not avaliable, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
			}
		
//		BeSpoke
		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipBeSpokeFeature, 120)) {
			WebInteractUtil.scrollIntoView(CPQ_Objects.cpeIPAcessNextBtn);
			WebInteractUtil.click(CPQ_Objects.cpeIPAcessNextBtn);
			for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
			WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipCabinetType, 90);
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Bespoke type is not avaliable, Please Verify");
			System.out.println("Bespoke type is not avaliable, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
			}
		
//		Additional Product Data
		sResult = ipAccessAdditionalProductData(file_name, Sheet_Name, iScript, iSubScript);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipCabinetType, 120)) {
//			sResult = ipAccessAdditionalProductData(file_name, Sheet_Name, iScript, iSubScript);
//			if (sResult.equalsIgnoreCase("False")){ return "False"; }	
//			
//		}else {
//			ExtentTestManager.getTest().log(LogStatus.FAIL, "Additional Product Data is not avaliable, Please Verify");
//			System.out.println("Additional Product Data is not avaliable, please verify");
//			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
//			return "False";
//		}
		
		return sResult;
	}
			
		public String routerTypeConfiguration(String file_name,String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
			/*----------------------------------------------------------------------
			Method Name : routerTypeConfiguration
			Purpose     : This method is to select the Router Type and add data entries
			Designer    : Kashyap D
			Created on  : 25th June 2020 
			Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
			Output      : True/False
			 ----------------------------------------------------------------------*/ 
//			Initializing the Variable
			String sResult;
			String Router_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Router_Type");
			String BG4Feed_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"BG4Feed_Type");
			String BG4Feed_Type_AS = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"BG4Feed_Type_AS");
			String Presentation_Interface = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Presentation_Interface");
			String InterFace = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"InterFace");
			String Connector = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Connector");
			String IP_Addressing_Format = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"IP_Addressing_Format");
			String IP_Addressing_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"IP_Addressing_Type");
			String IP4V_Address = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"IP4V_Address");
			
			//IPFeature
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipL3ResilienceLnk, 120)) {
				WebInteractUtil.click(CPQ_Objects.cpeIPAcessNextBtn);
				waitForpageloadmask();
				WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipIPRouterTypeLst, 90);
				//Update the product before Save
				
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Ip Feature type is not avaliable, Please Verify");
				System.out.println("Ip Feature type is not avaliable, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
						
		switch(Router_Type) {
			
			case "Customer Provided Router":
				//Router Type
				if(WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipIPRouterTypeLst, 30)) {
					WebInteractUtil.selectByValue(CPQ_Objects.ipIPRouterTypeLst, Router_Type);
					for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, Router_Type+" in CPQ is not visible, Please verify");
					System.out.println(Router_Type+" in CPQ is not visible, Please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}	
				break;
				
			case "Default Managed Router (no NAT Support)":
				if(WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipIPRouterTypeLst, 30)) {
					WebInteractUtil.selectByValue(CPQ_Objects.ipIPRouterTypeLst, Router_Type);
					for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
					if(WebInteractUtil.waitForInvisibilityOfElement(CPQ_Objects.ipNATCbx,20)) {
						ExtentTestManager.getTest().log(LogStatus.INFO,"NAT is disabled in CPQ is not visible, Please verify");
					}else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "NAT is Enabled in CPQ is not visible, Please verify");
						System.out.println("NAT is Enabled in CPQ is not visible, Please verify");
						ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
						return "False";
					}
				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, Router_Type+" in CPQ is not visible, Please verify");
					System.out.println(Router_Type+" in CPQ is not visible, Please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}	
				break;
				
			case "Default Managed Router (Supports NAT)":
				if(WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipIPRouterTypeLst, 30)) {
					WebInteractUtil.selectByValue(CPQ_Objects.ipIPRouterTypeLst, Router_Type);
					for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
//					Check the NAT 
					if(WebInteractUtil.isVerify(CPQ_Objects.ipNATCbx)) {
						CPQ_Objects.ipNATCbx.sendKeys(Keys.SPACE);
						waitForpageloadmask();
						ExtentTestManager.getTest().log(LogStatus.INFO,"NAT is Enabled in CPQ is not visible, Please verify");
					}else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "NAT is disabled in CPQ is not visible, Please verify");
						System.out.println("NAT is disabled in CPQ is not visible, Please verify");
						ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
						return "False";
					}
				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, Router_Type+" in CPQ is not visible, Please verify");
					System.out.println(Router_Type+" in CPQ is not visible, Please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}	
				break;
				
			case "Default Managed Router (Supports Full BGP / NAT)":
				if(WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipIPRouterTypeLst, 30)) {
					WebInteractUtil.selectByValue(CPQ_Objects.ipIPRouterTypeLst, Router_Type);
					for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
					if(WebInteractUtil.isVerify(CPQ_Objects.ipNATCbx)) {
						CPQ_Objects.ipNATCbx.sendKeys(Keys.SPACE);
						waitForpageloadmask();
						ExtentTestManager.getTest().log(LogStatus.INFO,"NAT is Enabled in CPQ is not visible, Please verify");
					}else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "NAT is disabled in CPQ is not visible, Please verify");
						System.out.println("NAT is Enabled in CPQ is not visible, Please verify");
						ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
						return "False";
					}
				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, Router_Type+" in CPQ is not visible, Please verify");
					System.out.println(Router_Type+" in CPQ is not visible, Please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}	
				break;
				
			case "Default Unmanaged Router (no NAT Support)":
				if(WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipIPRouterTypeLst, 30)) {
					WebInteractUtil.selectByValue(CPQ_Objects.ipIPRouterTypeLst, Router_Type);
					for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
					if(WebInteractUtil.waitForInvisibilityOfElement(CPQ_Objects.ipNATCbx,20)) {
						ExtentTestManager.getTest().log(LogStatus.INFO,"NAT is Disabled in CPQ is not visible, Please verify");
					}else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "NAT is Enabled in CPQ is not visible, Please verify");
						System.out.println("NAT is Enabled in CPQ is not visible, Please verify");
						ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
						return "False";
					}
				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, Router_Type+" in CPQ is not visible, Please verify");
					System.out.println(Router_Type+" in CPQ is not visible, Please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}	
				break;
				
			case "Default Unmanaged Router (Supports NAT)":
				if(WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipIPRouterTypeLst, 30)) {
					WebInteractUtil.selectByValue(CPQ_Objects.ipIPRouterTypeLst, Router_Type);
					for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
					if(WebInteractUtil.isVerify(CPQ_Objects.ipNATCbx)) {
						CPQ_Objects.ipNATCbx.sendKeys(Keys.SPACE);
						ExtentTestManager.getTest().log(LogStatus.INFO,"NAT is Enabled in CPQ is not visible, Please verify");
					}else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "NAT is disabled in CPQ is not visible, Please verify");
						System.out.println("NAT is Enabled in CPQ is not visible, Please verify");
						ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
						return "False";
					}
				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, Router_Type+" in CPQ is not visible, Please verify");
					System.out.println(Router_Type+" in CPQ is not visible, Please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}	
				break;
			
			case "Default Unmanaged Router (Supports Full BGP / NAT)":
				if(WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipIPRouterTypeLst, 30)) {
					WebInteractUtil.selectByValue(CPQ_Objects.ipIPRouterTypeLst, Router_Type);
					for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
					if(WebInteractUtil.isVerify(CPQ_Objects.ipNATCbx)) {
						CPQ_Objects.ipNATCbx.sendKeys(Keys.SPACE);
						ExtentTestManager.getTest().log(LogStatus.INFO,"NAT is Enabled in CPQ is not visible, Please verify");
					}else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "NAT is disabled in CPQ is not visible, Please verify");
						System.out.println("NAT is Enabled in CPQ is not visible, Please verify");
						ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
						return "False";
					}
				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, Router_Type+" in CPQ is not visible, Please verify");
					System.out.println(Router_Type+" in CPQ is not visible, Please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}	
				break;
			
			
			case "Specific Managed COLT Router": case "Specific Unmanaged COLT Router":
				if(WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipIPRouterTypeLst, 30)) {
					WebInteractUtil.selectByValue(CPQ_Objects.ipIPRouterTypeLst, Router_Type);
					for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
					
					//ColtId
					if(WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipColtIdTbx, 30)) {
						WebInteractUtil.sendKeys(CPQ_Objects.ipColtIdTbx, "C1234");
						for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
					}else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to Enter Colt ID in CPQ is not visible, Please verify");
						System.out.println("Unable to Enter Colt ID in CPQ is not visible, Please verify");
						ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
						return "False";
					}
					
					//Cost of CPE
					if(WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipColtCostTbx, 30)) {
						WebInteractUtil.clear(CPQ_Objects.ipColtCostTbx);
						waitForpageloadmask();
						WebInteractUtil.sendKeys(CPQ_Objects.ipColtCostTbx, "100");
						for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
					}else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to Enter Colt Cost in CPQ is not visible, Please verify");
						System.out.println("Unable to Enter Colt Cost ID in CPQ is not visible, Please verify");
						ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
						return "False";
					}
					
					//Router Model Name
					if(WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipColtRouterModelLst, 30)) {
						WebInteractUtil.sendKeys(CPQ_Objects.ipColtRouterModelLst, "Automate Router");
						for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
					}else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to Enter Colt Router Name in CPQ is not visible, Please verify");
						System.out.println("Unable to Enter Colt Router Name in CPQ is not visible, Please verify");
						ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
						return "False";
					}
				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, Router_Type+" in CPQ is not visible, Please verify");
					System.out.println(Router_Type+" in CPQ is not visible, Please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
					}	
					break;
				}
			
//			BGP4
			if(WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipBGP4FeedCbx, 30)) {
				CPQ_Objects.ipBGP4FeedCbx.sendKeys(Keys.SPACE);
				for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
				WebInteractUtil.selectByValue(CPQ_Objects.ipBGP4FeedTypeLst, BG4Feed_Type);
				waitForpageloadmask();
				WebInteractUtil.selectByValue(CPQ_Objects.cpeBGPTypeAsEndLst, BG4Feed_Type_AS);
				waitForpageloadmask();
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "BGP4Feed in CPQ is not visible, Please verify");
				System.out.println(Router_Type+" BGP4Feed in CPQ is not visible, Please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
				
//			SMTP
			if(WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipSMTPFeedCbx, 30)) {
				CPQ_Objects.ipSMTPFeedCbx.sendKeys(Keys.SPACE);
				waitForpageloadmask();
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "SMTP in CPQ is not visible, Please verify");
				System.out.println(Router_Type+" SMTP in CPQ is not visible, Please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
				
//			PRESENTATION INTERFACE
			if(WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipinterfaceAEndLst, 30)) {
				//interFace type
				WebInteractUtil.selectByValue(CPQ_Objects.ipinterfaceAEndLst, InterFace);
				for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
				//Connector
				WebInteractUtil.selectByValue(CPQ_Objects.ipConnectorLst, Connector);
				for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
				
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Presentation Interface in CPQ is not visible, Please verify");
				System.out.println(Router_Type+" Presentation Interface in CPQ is not visible, Please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
				
//			IP ADDRESSING
			if(WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipAddressingFormatLst, 30)) {
				//Ip Addressing Format
				WebInteractUtil.selectByValue(CPQ_Objects.ipAddressingFormatLst, IP_Addressing_Format);
				for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
				//Ip Addressing Type A
				WebInteractUtil.selectByValue(CPQ_Objects.ipAddressingTypeAEndLst, IP_Addressing_Type);
				for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
				//IP4V Address
				WebInteractUtil.selectByValue(CPQ_Objects.ipAddressingIPv4Lst, IP4V_Address);
				for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
				WebInteractUtil.scrollIntoView(CPQ_Objects.cpeIPAcessNextBtn);
				WebInteractUtil.click(CPQ_Objects.cpeIPAcessNextBtn);
				for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
				WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipSiteAddonsLnk, 120);
				
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Ip Addressing Interface in CPQ is not visible, Please verify");
				System.out.println(Router_Type+" IP Addressing in CPQ is not visible, Please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
				
			
			return "True";
		}
		
		public String ipAccessAdditionalProductData(String file_name,String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException, ParseException {
			/*----------------------------------------------------------------------
			Method Name : ipAccessAdditionalProductData
			Purpose     : This method is to add additional product data entries
			Designer    : Kashyap D
			Created on  : 25th June 2020 
			Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
			Output      : True/False
			 ----------------------------------------------------------------------*/ 
//			Initializing the Variable
			String sResult;
			String Site_Cabinet_Type = dataminer.fngetcolvalue(file_name, "A_End", iScript, iSubScript,"Cabinet_Type");
			String Site_Cabinet_Id = dataminer.fngetcolvalue(file_name, "A_End", iScript, iSubScript,"Site_ID");
			String Site_Access_Technology = dataminer.fngetcolvalue(file_name, "A_End", iScript, iSubScript,"Access_Technology");
			String Router_Technology = dataminer.fngetcolvalue(file_name, "A_End", iScript, iSubScript,"Router_Technology");
			String Existing_Capacity_Lead_Time = dataminer.fngetcolvalue(file_name, "Product_Configuration", iScript, iSubScript,"Existing_Capacity_Lead_Time");

			if (WebInteractUtil.isPresent(CPQ_Objects.addtionalProductDataLnk, 120)) {
				WebInteractUtil.click(CPQ_Objects.addtionalProductDataLnk);
				waitForpageloadmask();
			}
			// Capturing Start point of Transaction Capture
			String StartTime = dateTimeUtil.fnGetCurrentTime();
			
			//Additional Data Primary Site
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipCabinetType, 120)) {
				WebInteractUtil.selectByValue(CPQ_Objects.ipCabinetType, Site_Cabinet_Type);
				waitForpageloadmask();
				WebInteractUtil.sendKeys(CPQ_Objects.ipCabinetID,Site_Cabinet_Id);
				waitForpageloadmask();
				WebInteractUtil.selectByValue(CPQ_Objects.ipSiteAccessTechnology, Site_Access_Technology);
				waitForpageloadmask();
				WebInteractUtil.scrollIntoView(CPQ_Objects.cpeIPAcessNextBtn);
				WebInteractUtil.click(CPQ_Objects.cpeIPAcessNextBtn);
				waitForpageloadmask();
//				WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipRouterTechnology, 120);
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Additional Data Primary Site type is not avaliable, Please Verify");
				System.out.println("Additional Data Primary Site type is not avaliable, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
				}
		
			
			//Additional Service Information
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipRouterTechnology, 20)) {
				WebInteractUtil.selectByValue(CPQ_Objects.ipRouterTechnology, Router_Technology);
				waitForpageloadmask();
				WebInteractUtil.selectByValue(CPQ_Objects.ipexistingCapacityLeadTimeLst, Existing_Capacity_Lead_Time);
				waitForpageloadmask();
				WebInteractUtil.click(CPQ_Objects.cpeIPAcessNextBtn);
				for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
				WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipVoiceBundleService, 120);
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Additional Data Secondary Site type is not avaliable, Please Verify");
				System.out.println("Additional Data Secondary Site type is not avaliable, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
			
			// Capturing End point of Transaction Capture
			String EndTime = dateTimeUtil.fnGetCurrentTime();
						
			// Computing Difference between Transactions Capture
			String TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
			System.out.println("TimeDiff is "+TimeDiff);
			
			
			return TimeDiff;
			
		}
		
		public String ipAccessL3Resilience(String file_name, String Sheet_Name, String iScript, String iSubScript, String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException {
			/*----------------------------------------------------------------------
			Method Name : ipAccessL3Resilience
			Purpose     : This method is to Update the L3Resilience and respective L3 details. 
			Designer    : Kashyap D
			Created on  : 25th June 2020 
			Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
			Output      : True/False
			 ----------------------------------------------------------------------*/ 
//			Initializing the Variable
			String sResult;
			String L3_Resilience_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"L3_Resilience_Type");
			
			switch(L3_Resilience_Type.toUpperCase()) {
			
				case "NO RESILIENCE":{
					WebInteractUtil.selectByValue(CPQ_Objects.ipL3ResilianceLnk, L3_Resilience_Type);
					waitForpageloadmask();
					WebInteractUtil.click(CPQ_Objects.cpeIPAcessNextBtn);
					for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
					break;
					}
				
				case "DUAL ACCESS UNMANAGED":{
					WebInteractUtil.selectByValue(CPQ_Objects.ipL3ResilianceLnk, L3_Resilience_Type);
					waitForpageloadmask();
					WebInteractUtil.click(CPQ_Objects.cpeIPAcessNextBtn);
					for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
					sResult=ipAccessL3Configuration(file_name, Sheet_Name, iScript, iSubScript,L3_Resilience_Type, UI_Type, TransactionID, ColName);
					if (sResult.equalsIgnoreCase("False")) { return "False"; }
					break;
					}
				
				case "DUAL ACCESS PRIMARY & BACKUP":{
					WebInteractUtil.selectByValue(CPQ_Objects.ipL3ResilianceLnk, L3_Resilience_Type);
					waitForpageloadmask();
					WebInteractUtil.click(CPQ_Objects.cpeIPAcessNextBtn);
					for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
					sResult=ipAccessL3Configuration(file_name, Sheet_Name, iScript, iSubScript,L3_Resilience_Type, UI_Type, TransactionID, ColName);
					if (sResult.equalsIgnoreCase("False")) { return "False"; }
					break;
					}
				
				case "DUAL ACCESS LOAD SHARED":{
					WebInteractUtil.selectByValue(CPQ_Objects.ipL3ResilianceLnk, L3_Resilience_Type);
					waitForpageloadmask();
					WebInteractUtil.click(CPQ_Objects.cpeIPAcessNextBtn);
					for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
					sResult=ipAccessL3Configuration(file_name, Sheet_Name, iScript, iSubScript,L3_Resilience_Type, UI_Type, TransactionID, ColName);
					if (sResult.equalsIgnoreCase("False")) { return "False"; }
					break;
					}
				
				case "MULTI-ISP PRIMARY & BACKUP":{
					WebInteractUtil.selectByValue(CPQ_Objects.ipL3ResilianceLnk, L3_Resilience_Type);
					waitForpageloadmask();
					WebInteractUtil.click(CPQ_Objects.cpeIPAcessNextBtn);
					for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
					sResult=ipAccessL3Configuration(file_name, Sheet_Name, iScript, iSubScript,L3_Resilience_Type, UI_Type, TransactionID, ColName);
					if (sResult.equalsIgnoreCase("False")) { return "False"; }
					break;
					}
				
				case "DSL BACKUP":{
					WebInteractUtil.selectByValue(CPQ_Objects.ipL3ResilianceLnk, L3_Resilience_Type);
					waitForpageloadmask();
					WebInteractUtil.click(CPQ_Objects.cpeIPAcessNextBtn);
					for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
					sResult=ipAccessL3Configuration(file_name, Sheet_Name, iScript, iSubScript,L3_Resilience_Type, UI_Type, TransactionID, ColName);
					if (sResult.equalsIgnoreCase("False")) { return "False"; }
					break;
					}
			}
			return "True";
		}
		
		public String ipAccessL3Configuration(String file_name, String Sheet_Name, String iScript, String iSubScript,String L3_Resilience_Type, String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException {
			/*----------------------------------------------------------------------
			Method Name : ipAccessL3Configuration
			Purpose     : This method is to Update the L3Resilience configuration. 
			Designer    : Kashyap D
			Created on  : 25th June 2020 
			Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
			Output      : True/False
			 ----------------------------------------------------------------------*/ 
//			Initializing the Variable
			String sResult;
			String BackUp_Bandwidth = dataminer.fngetcolvalue(file_name, "B_End", iScript, iSubScript,"IP_Backup_BandWidth");
			
			
//			Site B Address Entry
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.siteAAddressTxb, 120)) {
//				Reading the Address from the data sheet
				String SiteB_Address = dataminer.fngetcolvalue(file_name, "B_End", iScript, iSubScript,"SiteB_Address");
				String SiteB_Address_Type = dataminer.fngetcolvalue(file_name, "B_End", iScript, iSubScript,"SiteB_Address_Type");
				waitForpageloadmask();
				WebInteractUtil.scrollIntoView(CPQ_Objects.siteAAddressTxb);
				WebInteractUtil.sendKeysByJS(CPQ_Objects.siteAAddressTxb, SiteB_Address);
				WebInteractUtil.click(CPQ_Objects.siteASearchImg);
				WebInteractUtil.pause(4000);
				waitForpageloadmask();
				//By executing a java script
				driver.switchTo().frame("siteAddressLink");
				//Validate address type
				sResult = addressTypeConfiguration(SiteB_Address,SiteB_Address_Type);
				if (sResult.equalsIgnoreCase("False")) { return "False"; }
				WebInteractUtil.click(CPQ_Objects.cpeIPAcessNextBtn);
				for (int i = 1; i < 3; i++) {waitForpageloadmask(); }
				//By executing a java script
				if(L3_Resilience_Type.equalsIgnoreCase("Multi-ISP Primary & Backup")) {
					WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipFeaturesLnk, 120);
				}else {WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipBackUpBandWidthLst, 20);}
				
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Site A address text box is not visible in CPQ, please verify");
				System.out.println("Site A address text box is not visible in CPQ, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
				}
			
//			Backup Bandwidth
			if(!L3_Resilience_Type.equalsIgnoreCase("Multi-ISP Primary & Backup")) {
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipBackUpBandWidthLst, 120)) {
				waitForpageloadmask();
				WebInteractUtil.selectByValue(CPQ_Objects.ipBackUpBandWidthLst, BackUp_Bandwidth);
				waitForpageloadmask();
				WebInteractUtil.scrollIntoView(CPQ_Objects.cpeIPAcessNextBtn);
				WebInteractUtil.click(CPQ_Objects.cpeIPAcessNextBtn);
				for (int i = 1; i < 4; i++) {waitForpageloadmask(); }
				WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipFeaturesLnk, 20);
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "BackupBandwidth in CPQ is not visible, Please verify");
				System.out.println("serviceBandwidthLst in CPQ is not visible, Please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
				}  
			}
			
//			ipFetaures
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipFeaturesLnk, 120)) {
				WebInteractUtil.scrollIntoView(CPQ_Objects.cpeIPAcessNextBtn);
				WebInteractUtil.click(CPQ_Objects.cpeIPAcessNextBtn);
				for (int i = 1; i < 4; i++) {waitForpageloadmask(); }
				if(!L3_Resilience_Type.equalsIgnoreCase("Multi-ISP Primary & Backup")) {
					WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.cpeIPAcessNextBtn, 20);
					WebInteractUtil.click(CPQ_Objects.cpeIPAcessNextBtn);
					for (int i = 1; i < 4; i++) {waitForpageloadmask(); }
				}
				
				int times=0;
				while(!WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.ipDiversitySelector, 20)||times==3) {
					WebInteractUtil.scrollIntoView(CPQ_Objects.cpeIPAcessNextBtn);
					WebInteractUtil.click(CPQ_Objects.cpeIPAcessNextBtn);
					for (int i = 1; i < 4; i++) {waitForpageloadmask(); }
					times++;
				}
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "BackupBandwidth in CPQ is not visible, Please verify");
				System.out.println("serviceBandwidthLst in CPQ is not visible, Please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}  
			
			return "True";
			
		}
		
		public String offnetEntriesWave(String file_name, String Sheet_Name, String iScript, String iSubScript, String UI_Type, String TransactionID, String ColName) throws IOException, InterruptedException, ParseException {
			/*----------------------------------------------------------------------
			Method Name : offnetEntries
			Purpose     : This method is to configure offnet features of the respective iterations
			Designer    : Kashyap D
			Created on  : 24th June 2020 
			Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
			Output      : True/False
			 ----------------------------------------------------------------------*/ 
			
//			Initializing the Variable
			String Product_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
			String Explore_Options = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Explore_Options");	
			String Priority = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Priority");
			String Connection_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Connection_Type");
			String FlowType = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Flow_Type");
			WebElement offnetCheckBtn = null; WebElement manualEngagementBtn = null; WebElement exploreOptionsLsb = null;
			String StartTime = null; String EndTime = null; String TimeDiff = null;
			
			String[] sTransactionsID = TransactionID.split("\\|");
			String tfile_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
			
			switch (Sheet_Name) {
			
			case "A_End":
				offnetCheckBtn = CPQ_Objects.offnetCheckAEndBtn;
				manualEngagementBtn = CPQ_Objects.manualEngagementAEndBtn;
				exploreOptionsLsb = CPQ_Objects.exploreActionsAEndLst;
				break;
				
			case "B_End":
				offnetCheckBtn = CPQ_Objects.offnetCheckBEndBtn;
				manualEngagementBtn = CPQ_Objects.manualEngagementBEndBtn;
				exploreOptionsLsb = CPQ_Objects.exploreActionsBEndLst;
				break;
			}
				if (WebInteractUtil.waitForElementToBeVisible(manualEngagementBtn,75)) {
					WebInteractUtil.click(manualEngagementBtn);
					// Capturing Start point of Transaction Capture
					StartTime = dateTimeUtil.fnGetCurrentTime();
					waitForpageloadmask();
					// Capturing End point of Transaction Capture
					EndTime = dateTimeUtil.fnGetCurrentTime();
					// Computing Difference between Transactions Capture
					TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
					System.out.println("TimeDiff is "+TimeDiff);
					//Entering the Values to the Data sheet
					dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[1], ColName, TimeDiff);
					if (WebInteractUtil.waitForElementToBeVisible(exploreOptionsLsb,75)) {
						WebInteractUtil.isEnabled(exploreOptionsLsb);
						waitForpageloadmask();
						WebInteractUtil.selectByText(exploreOptionsLsb, Explore_Options);
						// Capturing End point of Transaction Capture
						EndTime = dateTimeUtil.fnGetCurrentTime();
		//				WebInteractUtil.selectByValue(exploreOptionsLsb, Explore_Options);
						WebInteractUtil.switchToFrame("exploreEngagementComponent");
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "offnet check button Wave is still visible in CPQ even after click, please verify");
						System.out.println("offnet check button Wave is still visible in CPQ even after click, please verify");
						ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
						return "False";
					}
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "offnetCheckBtn Wave is not visible in Sitedetails page in CPQ, please verify");
					System.out.println("offnetCheckBtn Wave is not visible in Sitedetails page in CPQ, please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}
				

				if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.priorityTxb,75)) {
					// Capturing End point of Transaction Capture
					EndTime = dateTimeUtil.fnGetCurrentTime();
					// Computing Difference between Transactions Capture
					TimeDiff = dateTimeUtil.fnGetElapsedTime(StartTime, EndTime);
					System.out.println("TimeDiff is "+TimeDiff);
					//Entering the Values to the Data sheet
					dataminer.fnsetTransactionValue(tfile_name, UI_Type, sTransactionsID[2], ColName, TimeDiff);
					WebInteractUtil.sendKeysWithKeys(CPQ_Objects.priorityTxb, Priority, "Enter");
					waitForpageloadmask();
					if (Connection_Type.equalsIgnoreCase("DualEntry")) { WebInteractUtil.click(CPQ_Objects.dualEntryCbx); }
					WebInteractUtil.click(CPQ_Objects.getQuoteBtn);
					WebInteractUtil.pause(4000);
					WebDriver driver = DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
					driver.switchTo().alert().accept();
					WebInteractUtil.pause(2000);
					WebInteractUtil.click(CPQ_Objects.expandArrowExploreIcn);
					if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.requestIDElem, 60)) {
						WebInteractUtil.isPresent(CPQ_Objects.requestIDElem, 30);
						String fullVal = CPQ_Objects.requestIDElem.getText();
						String[] bits = fullVal.split(":");
						String Request_ID = bits[bits.length-1].trim();
						ExtentTestManager.getTest().log(LogStatus.PASS, "Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+Sheet_Name);
						System.out.println("Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+Sheet_Name);
//						Passing this request ID to testdata sheet
						dataminer.fnsetcolvalue(file_name, Sheet_Name, iScript, iSubScript, "Request_ID", Request_ID);
						WebInteractUtil.switchToDefaultFrame();
						WebInteractUtil.click(CPQ_Objects.exploreCloseBtn);
						waitForpageloadmask();
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "requestID in OLO Cost Summary Table is not generated in Explore, please verify");
						System.out.println("requestID in OLO Cost Summary Table is not generated in Explore, please verify");
						ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
						return "False";
						}
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "offnet check button Wave is still visible in CPQ even after click, please verify");
						System.out.println("offnet check button Wave is still visible in CPQ even after click, please verify");
						ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
						return "False";
				}
			return "True";
		}
		
		public String VpnupdateSaveProductCPQ(String SaveType) throws IOException, InterruptedException {
			/*----------------------------------------------------------------------
			Method Name : updateSaveProductCPQ
			Purpose     : This method is to update and save the cpq entries
			Designer    : Kashyap D
			Created on  : 1st April 2020 
			Input       : None
			Output      : True/False
			 ----------------------------------------------------------------------*/
			
			String sResult;
			
			//Update the product before Save
			if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.vpnUpdateBtn, 120)) {
				WebInteractUtil.click(CPQ_Objects.vpnNetworkTreeLink);
				WaitforCPQloader();
				WebInteractUtil.click(CPQ_Objects.vpnUpdateBtn);
				for (int i = 1; i < 3; i++) {waitForpageloadmask(); }
//				WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.saveToQuoteBtn, 90);
//				for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Update Button is not visible in CPQ, please verify");
				System.out.println("Update Button is not visible in CPQ, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}	
			
			
			switch (SaveType.toUpperCase()) {
			
				case "SAVETOQUOTE":
					//Saving the Quote Details
					if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.vpnSaveToQuoteBtn, 120)) {
						waitForpageloadmask();
						WebInteractUtil.click(CPQ_Objects.vpnSaveToQuoteBtn);
						for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
						WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.returnToC4CBtn, 90);
						for (int i = 1; i < 2; i++) {waitForpageloadmask(); }
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "saveToQuoteBtn Button is not visible in CPQ, please verify");
						System.out.println("saveToQuoteBtn Button is not visible in CPQ, please verify");
						ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
						return "False";
					}
					break;
					
				case "SAVE":
		//			Calling the below method to save the details
					if (WebInteractUtil.waitForElementToBeVisible(CPQ_Objects.vpnSaveBtn, 60)) {
//						WebInteractUtil.scrollIntoView(sButton);
						waitForpageloadmask();
						WebInteractUtil.click(CPQ_Objects.vpnSaveBtn);
						for (int i = 1; i < 3; i++) { waitForpageloadmask(); }
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Save Button in CPQ is not visible, Please verify");
						System.out.println("Save Button in CPQ is not visible, Please verify");
						ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
						return "False";
					}
					break;
			}
			return "True";
		}
		
		public void WaitforVpnloader() throws IOException, InterruptedException {
			/*----------------------------------------------------------------------
			Method Name : WaitforCPQloader
			Purpose     : This method will wait CPQ application while loading page occurs
			Designer    : Kashyap D
			Created on  : 1st April 2020 
			Input       : None
			Output      : None
			 ----------------------------------------------------------------------*/ 
			int i = 1;
			Thread.sleep(1000);   
			WebDriver driver = DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
	          try {
	        	  while (driver.findElement(By.xpath("//oj-progress[contains(@title,'Processing')]")).isDisplayed()) {   
	        		  if (i > 60) { break; }
	                      Thread.sleep(1000);
	                      System.out.println("Waiting Vpn Page to load");
	                      i = i+1;
	                } 
	          } catch(Exception e) {
	        	  Waittilljquesryupdated();
//	            System.out.println("CPQ Page loaded fully, proceeding further");
	          }
		}
}
