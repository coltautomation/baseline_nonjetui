package com.colt.common.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class dateTimeUtil {
	
public static String fnGetCurrentTime() {
		
//		Initializing the Time format
		DateFormat timeFormat = null;
		Date time = null;
		timeFormat = new SimpleDateFormat("HH:mm:ss.SSS");
		
		time = new Date();
		String CurrentTime = timeFormat.format(time);
		System.out.println(CurrentTime);
		return CurrentTime;
		
	}
	
	public static String fnGetElapsedTime(String StartTime, String EndTime) throws ParseException {
		
//		Initializing the Time format
		DateFormat timeFormat = null;
		timeFormat = new SimpleDateFormat("HH:mm:ss.SSS");
		
		Date date1 = timeFormat.parse(StartTime);
	    Date date2 = timeFormat.parse(EndTime);
	    float difference = (date2.getTime() - date1.getTime());
	    BigDecimal bigVal = BigDecimal.valueOf(difference).divide(BigDecimal.valueOf(1000),3,RoundingMode.HALF_UP);
	    String retVal = bigVal.toString();
	    System.out.println("differnce is "+retVal);
	    return retVal;
		
	}

}
