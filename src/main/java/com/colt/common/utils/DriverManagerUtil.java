package com.colt.common.utils;



import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.imageio.ImageIO;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.SkipException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import com.qa.colt.extentreport.ExtentManager;
import com.qa.colt.extentreport.ExtentTestManager;

//import Listeners.ActionListner;

import com.qa.colt.listeners.TestListener;
import com.relevantcodes.extentreports.LogStatus;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;


public class DriverManagerUtil {
	
	public static final ThreadLocal<WebDriver> WEB_DRIVER_THREAD_LOCAL = new InheritableThreadLocal<>();
	public static TestListener Testlistener;
	public static ChromeDriver driver;
	public static int  itr;
	public static int i=0;
	public static String scriptName;
	
    /**********************************************************************************************
     * Initialize the webdriver
     * @return status {@link boolean} - true/false
     * @author Vasantharaja Chinnakannu created June 18, 2020
     * @version 1.0 May 18, 2020
     ***********************************************************************************************/
	
	@Parameters({ "SheetName","iScript","iSubScript" })
	@BeforeMethod()
	public void setup(String SheetName,String iScript,String iSubScript) throws IOException, InterruptedException
	{
		scriptName=SheetName;
		String file_name;
		if(SheetName.contains("CopyQuote")) {
			 file_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_testdata_Copy_Quote.xlsx";	
		}else { file_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_testdata.xlsx";}
		
		String Execution_Required = dataminer.fngetcolvalue(file_name, SheetName, iScript, iSubScript,"Execution_Required");
		String Scenario_Name = dataminer.fngetcolvalue(file_name, SheetName, iScript, iSubScript,"Scenario_Name");
		if(Execution_Required.equalsIgnoreCase("Yes")) {
			
			System.out.println("Scenario "+Scenario_Name+ " PickedUp for Execution");
			//Open Browser
			WebDriver driver = null;
			String targatedbrowser = "chrome";
			if(targatedbrowser.equals("chrome")) { 
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				Map<String, Object> prefs = new HashMap<String, Object>();
				// Set the notification setting it will override the default setting
				prefs.put("profile.default_content_setting_values.notifications", 2);
				prefs.put("profile.default_content_setting_values.popups", 1);
				prefs.put("download.default_directory", System.getProperty("user.dir")+"\\src\\Data\\Downloads");
				
	            // Create object of ChromeOption class
				ChromeOptions options = new ChromeOptions();
				options.setExperimentalOption("prefs", prefs);
				options.addArguments("--start-maximized");
				options.addArguments("disable-infobars");
				options.addArguments("--disable-popup-blocking");
				options.addArguments("--force-device-scale-factor=1");
				options.addArguments("--disable-dev-shm-usage--");
				options.addArguments("--allow-insecure-localhost");
				options.addArguments("--window-size=1920,1080");
				options.addArguments("enable-automation");
				options.addArguments("--disable-infobars");
				options.addArguments("--disable-gpu");
				options.addArguments("--no-sandbox");
				options.addArguments("--disable-browser-side-navigation");
				
				HashMap<String, Object> chromeLocalStatePrefs = new HashMap<String, Object>();
				List<String> experimentalFlags = new ArrayList<String>();
				 
				experimentalFlags.add("same-site-by-default-cookies@2");
				experimentalFlags.add("cookies-without-same-site-must-be-secure@1");
				experimentalFlags.add("enable-removing-all-third-party-cookies@2");
				
				chromeLocalStatePrefs.put("browser.enabled_labs_experiments", experimentalFlags);
				options.setExperimentalOption("localState", chromeLocalStatePrefs);
				
				
				capabilities.setCapability(CapabilityType.PAGE_LOAD_STRATEGY, "none");
				//capabilities.setCapability(CapabilityType.l, "none");
				LoggingPreferences logs = new LoggingPreferences(); 
			    logs.enable(LogType.DRIVER, Level.ALL); 
				capabilities.setCapability(CapabilityType.LOGGING_PREFS, logs);
				capabilities.setCapability(ChromeOptions.CAPABILITY, options);
				capabilities.setCapability("applicationCacheEnabled", false);
				String path = System.getProperty("user.dir")+"\\src\\test\\resources\\downloads\\chromedriver.exe";
				System.setProperty("webdriver.chrome.driver",path);
		        driver = new ChromeDriver(capabilities);
			}
			WEB_DRIVER_THREAD_LOCAL.set(driver);
			Thread.sleep(2000);
			waitfordriverload();
		} else {
//			System.out.println("Scenario "+Scenario_Name+ " got skipped based on user selection");
//			ExtentTestManager.endTest(); ExtentManager.getReporter().flush(); 
//			org.testng.Assert.fail("Condition Mismatch in WebInteractUtilClass, Hence failing the case");
			throw new SkipException("Skipping this test");
		}
	}
	
    /**********************************************************************************************
     * return the webdriver
     * @return status {@link boolean} - true/false
     * @author Vasantharaja Chinnakannu created May 18, 2020
     * @version 1.0 May 18, 2020
     ***********************************************************************************************/
	public WebDriver getwebdriver() {
		//Launching the driver
		WebDriver driver = WEB_DRIVER_THREAD_LOCAL.get();
		return driver;
	}
	
    /**********************************************************************************************
     * wait for driver to load
     * @return status {@link boolean} - true/false
     * @author Vasantharaja Chinnakannu created May 18, 2020
     * @version 1.0 May 18, 2020
     ***********************************************************************************************/
	public void waitfordriverload() throws InterruptedException {
		boolean Status = false;
		JavascriptExecutor js = (JavascriptExecutor) DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
		for (int i=1; i<10; i++) {
			if (js == null) {
				Thread.sleep(250);
				js = (JavascriptExecutor) DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
				continue;
			} else {
				try {
					while(!(js.executeScript("return document.readyState").equals("complete")))
					{
	//					System.out.println("dom state is" +(js.executeScript("return document.readyState")));
						Thread.sleep(500);
					}
					Status = true;
					if (Status = true) { Thread.sleep(250); break; }
				} catch (Exception e) {
					continue;
				}
			}
		}
	}
	
	   /**********************************************************************************************
     * to capture screenshot which to be attached in the extent report
     * @return status {@link boolean} - true/false
     * @author Kashyap Daibala created June 18, 2020
     * @version 1.0 June 18, 2020
     ***********************************************************************************************/
	
	public static String Capturefullscreenshot() throws IOException {
		String screenshot2;
		Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get());
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ImageIO.write(screenshot.getImage(), "jpg", bos);
		byte[] imageBytes = bos.toByteArray();
		screenshot2 = "data:image/png;base64," + Base64.getMimeEncoder().encodeToString(imageBytes);
		bos.close();
		return screenshot2;
	}
	
	  /**********************************************************************************************
     * to clear chromedriver cache
     * @return status {@link boolean} - true/false
     * @author Kashyap Daibala created June 18, 2020
     * @version 1.0 June 18, 2020
	 * @throws InterruptedException 
     ***********************************************************************************************/
	
	public static void clearChromeCache() throws IOException, InterruptedException {
		
		DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().manage().deleteAllCookies();
		
//		String path = System.getProperty("user.dir")+"\\src\\test\\resources\\downloads\\chromedriver.exe";
//		System.setProperty("webdriver.chrome.driver",path);
//		ChromeOptions chromeOptions = new ChromeOptions();
//		chromeOptions.addArguments("disable-infobars");
//		chromeOptions.addArguments("start-maximized");
//		driver = new ChromeDriver(chromeOptions);
//		driver.get("chrome://settings/clearBrowserData");
//		Thread.sleep(2000);
//		driver.findElementByXPath("//settings-ui").sendKeys(Keys.ENTER);
//		for (i = 0; i <= 60; i++) {
//			if (WebInteractUtil.isPresent(driver.findElementByXPath("//settings-ui"), 2)) {
//				Thread.sleep(1000);
//				continue;
//			} else {
//				break;
//			}
//		}
////		driver.switchTo().activeElement();
////		driver.findElement(By.cssSelector("* /deep/ #clearBrowsingDataConfirm")).click();
////		Thread.sleep(15000);
//		driver.close();
	}
	
}
