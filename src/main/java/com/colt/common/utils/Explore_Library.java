package com.colt.common.utils;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.Parameters;
import com.colt.common.utils.dataminer;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.qa.colt.extentreport.ExtentTestManager;
import com.client.cpq.uitests.pageobjects.Explore_Objects;
import com.colt.common.utils.DriverManagerUtil;
import com.colt.common.utils.WebInteractUtil;

public class Explore_Library {
	
	public WebDriver driver = DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
	
	public Explore_Objects Explore_Objects;

	public Explore_Library() {
		Explore_Objects = new Explore_Objects();
	}
	
	public String Explore_Login(String file_name, String UserType) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : Explore_Login
		Purpose     : This method will login to Explore Application
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : String file_name
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initialize the Variable
		String Explore_URL = dataminer.fngetconfigvalue(file_name, "Explore_URL");
		String Explore_Username = null;
		switch (UserType.toUpperCase()) {
		case "OFFNET":
			Explore_Username = dataminer.fngetconfigvalue(file_name, "Explore_Offnet_User");
			break;
		case "NEARNET":
			Explore_Username = dataminer.fngetconfigvalue(file_name, "Explore_Nearnet_User");
		break;
		}
		String Explore_Password = dataminer.fngetconfigvalue(file_name, "C4C_Password");
		
		WebInteractUtil.launchWebApp(Explore_URL);
		Waittilljquesryupdated();
		
//		Logout of Explore Operations if not logged in properly
		if (WebInteractUtil.waitForElementToBeVisible(Explore_Objects.menuLnk, 10)) {
			String sResult = logOutExplore();
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
		}
		
//		Entering the Credentials
		WebInteractUtil.isPresent(Explore_Objects.userNameTxb,90);
		ExtentTestManager.getTest().log(LogStatus.PASS, "Explore URL "+Explore_URL+" launch is successfull");
		System.out.println("Explore URL "+Explore_URL+" launch is successfull");
		
//		Performing the Login Operations
		WebInteractUtil.sendKeys(Explore_Objects.userNameTxb, Explore_Username);
		WebInteractUtil.sendKeys(Explore_Objects.passWordTxb, Explore_Password);
		WebInteractUtil.click(Explore_Objects.loginBtn);
		Waittilljquesryupdated();
		
//		Verify Login Successsfull or not
		if (WebInteractUtil.waitForElementToBeVisible(Explore_Objects.mapViewBtn, 120)) {
			Waittilljquesryupdated();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Login to Explore application is successfull");
			System.out.println("Login to Explore application is successfull");
		}
		else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Login to Siebel application Failed");
			System.out.println("Login to Siebel application Failed");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
		
	}
	
	public void Waittilljquesryupdated() throws InterruptedException, SocketTimeoutException {
		/*----------------------------------------------------------------------
		Method Name : Waittilljquesryupdated
		Purpose     : This method will return whether the driver steady state is completed ornot
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : None
		 ----------------------------------------------------------------------*/ 
//		JavascriptExecutor js = null;
		boolean Status = false;
		Thread.sleep(1000);
		JavascriptExecutor js = (JavascriptExecutor) DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
		for (int i=1; i<10; i++) {
			if (js == null) {
				Thread.sleep(250);
				js = (JavascriptExecutor) DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
				continue;
			} else {
				try {
					while(!(js.executeScript("return document.readyState").equals("complete")))
					{
						Thread.sleep(500);
					}
					Status = true;
					if (Status = true) { Thread.sleep(250); break; }
				} catch (Exception e) {
					continue;
				}
			}
		}	
	}
	
	public String offnetExploreOperations(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : offnetExploreOperations
		Purpose     : This method is to process the create and process the offnet cost in explore window
		Designer    : Vasantharaja C
		Created on  : 24th June 2020 
		Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String sResult = null;
		String Product_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		
//		Login to Explore
		sResult = Explore_Login(file_name,"Offnet");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }

//		To Process Offnet Cost in Explore for A Site
		sResult = processOffnetCostExplore(file_name, "A_End", iScript, iSubScript);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		To Process Offnet Cost in Explore for B Site
//		if (Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("Wave")) {
			if (Product_Name.equalsIgnoreCase("EthernetLine")) {
			sResult = processOffnetCostExplore(file_name, "B_End", iScript, iSubScript);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
		}
		
//		Logout of Explore Operations
		sResult = logOutExplore();
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		return "True";
	}
	
	public String nearnetExploreOperations(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : nearnetExploreOperations
		Purpose     : This method is to process the create and process the Nearnet cost in explore window
		Designer    : Vasantharaja C
		Created on  : 24th June 2020 
		Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String sResult = null;
		String Product_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		
//		Login to Explore
		sResult = Explore_Login(file_name, "NEARNET");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }

//		To Process Offnet Cost in Explore for A Site
		sResult = processNearnetCostExplore(file_name, "A_End", iScript, iSubScript);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		To Process Offnet Cost in Explore for B Site
		if (Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("Wave")) {
			sResult = processNearnetCostExplore(file_name, "B_End", iScript, iSubScript);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
		}
		
//		Logout of Explore Operations
		sResult = logOutExplore();
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		return "True";
	}
	
	public String processOffnetCostExplore(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : processOffnetCostExplore
		Purpose     : This method is to process the offnet cost in explore for the respective ends
		Designer    : Vasantharaja C
		Created on  : 24th June 2020 
		Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		//Initialize the Varibles
		String sResult = null;
//		String Re_Explore_Value = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Request_ID");
		
//		Search record in Explore
		sResult = SearchONQTQueue_Record(file_name, Sheet_Name, iScript, iSubScript);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		//Assigning the explore request to the logged in user
		sResult = selectActionsFromExplore("RequestAssignToMe");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		//Assigning the explore request to the logged in user
		sResult = selectActionsFromExplore("CreateCost");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		//Creating a cost for explore offnet record
		sResult = createCostExploreOffnet(file_name, Sheet_Name, iScript, iSubScript);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		return "True";
	}
	
	public String processNearnetCostExplore(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : processOffnetCostExplore
		Purpose     : This method is to process the offnet cost in explore for the respective ends
		Designer    : Vasantharaja C
		Created on  : 24th June 2020 
		Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		//Initialize the Varibles
		String sResult = null;
		
//		Search record in fibreplanning work queue
		sResult = SearchFibrePlanningWork_Record(file_name, Sheet_Name, iScript, iSubScript);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		//Assigning the explore request to the logged in user
		sResult = selectActionsFromExplore("AssignToMe");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		//to enter cost for nearnet
		sResult = createNearnetCostExplore(file_name, Sheet_Name, iScript, iSubScript);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }		
		
		return "True";
	}
	
	public String SearchONQTQueue_Record(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : SearchONQTQueue_Record
		Purpose     : This method is to search ONQT record from explore
		Designer    : Vasantharaja C
		Created on  : 24th June 2020 
		Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String Request_ID = null;
		String Additional_Request_ID = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Additional_Request_ID");
		
		if (Additional_Request_ID.equals("")) {
			Request_ID = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Request_ID");	
		} else {
			Request_ID = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Additional_Request_ID");
		}
		
//		Verify Login Successsfull or not
		if (WebInteractUtil.waitForElementToBeVisible(Explore_Objects.onqtWorkQueueBtn, 120)) {
			Waittilljquesryupdated();
			WebInteractUtil.click(Explore_Objects.onqtWorkQueueBtn);
			WebInteractUtil.pause(4000);
			if (WebInteractUtil.waitForElementToBeVisible(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//a[text()='"+Request_ID+"']")), 120)) {
				WebInteractUtil.click(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//a[text()='"+Request_ID+"']")));
				Waittilljquesryupdated();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Record "+Request_ID+" Search in ONQT Work Queue was successfull");
				System.out.println("Record "+Request_ID+" Search in ONQT Work Queue was successfull");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Request ID "+Request_ID+" is not visible in Explore, Please Verify");
				System.out.println("Request ID "+Request_ID+" is not visible in Explore, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "onqtWorkQueueBtn is not visible in Explore, Please Verify");
			System.out.println("onqtWorkQueueBtn is not visible in Explore, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
		
	}
	
	public String SearchFibrePlanningWork_Record(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : SearchFibrePlanningWork_Record
		Purpose     : This method is to search SearchFibrePlanningWork_Record from explore
		Designer    : Vasantharaja C
		Created on  : 24th June 2020 
		Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String Request_ID = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Request_ID");
		
		
//		Verify Login Successsfull or not
		if (WebInteractUtil.waitForElementToBeVisible(Explore_Objects.fibrePlanningQueueBtn, 120)) {
			Waittilljquesryupdated();
			WebInteractUtil.click(Explore_Objects.fibrePlanningQueueBtn);
			Waittilljquesryupdated();
			Waittilljquesryupdated();
			if (WebInteractUtil.waitForElementToBeVisible(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//a[text()='"+Request_ID+"']")), 120)) {
				WebInteractUtil.click(DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//a[text()='"+Request_ID+"']")));
				Waittilljquesryupdated();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Record "+Request_ID+" Search in Fibre Planning Work Queue was successfull");
				System.out.println("Record "+Request_ID+" Search in Fibre Planning Work Queue was successfull");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Request ID "+Request_ID+" is not visible in Explore, Please Verify");
				System.out.println("Request ID "+Request_ID+" is not visible in Explore, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "fibrePlanningQueueBtn is not visible in Explore, Please Verify");
			System.out.println("fibrePlanningQueueBtn is not visible in Explore, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
		
	}
	
	public String selectActionsFromExplore(String ActionType) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : selectActionsFromExplore
		Purpose     : This method is to select the action from Explore
		Designer    : Vasantharaja C
		Created on  : 24th June 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		if (WebInteractUtil.waitForElementToBeVisible(Explore_Objects.actionsBtn, 120)) {
			WebInteractUtil.click(Explore_Objects.actionsBtn);
			WebInteractUtil.pause(2000);
			Waittilljquesryupdated();
			switch (ActionType.toUpperCase()) {
				case "REQUESTASSIGNTOME":
					WebInteractUtil.isPresent(Explore_Objects.assignRequestToMeLnk, 60);
					WebInteractUtil.click(Explore_Objects.assignRequestToMeLnk);
					break;
				case "CREATECOST":
					WebInteractUtil.isPresent(Explore_Objects.createCostLnk, 60);
					WebInteractUtil.click(Explore_Objects.createCostLnk);
					break;
				case "ASSIGNTOME":
					WebInteractUtil.isPresent(Explore_Objects.assignToMeLnk, 60);
					WebInteractUtil.click(Explore_Objects.assignToMeLnk);
					break;
			}
			Waittilljquesryupdated();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Initializing "+ActionType+" Under Actions in Explore window is was successfull");
			System.out.println("Initializing "+ActionType+" Under Actions in Explore window is was successfull");
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Assign request to the user in Explore is was not successfull, Please Verify");
			System.out.println("Assign request to the user in Explore is was not successfull, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
	}
	
	public String createCostExploreOffnet(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : createCostExploreOffnet
		Purpose     : This method is to create cost for explore offnet
		Designer    : Vasantharaja C
		Created on  : 24th June 2020 
		Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String Carrier = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Carrier");
		String Node = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Node");
		String MRC = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"MRC");
		String NRC = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"NRC");
		String Terms = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Terms");
		String Currency = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Currency");
		String Price_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Price_Type");
		String Connector_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Connector_Type");
		String Connection_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Connection_Type");
		String Dual_Entry_From_OLO = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Dual_Entry_From_OLO");
		String Request_ID = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Request_ID");
		
		if (WebInteractUtil.waitForElementToBeVisible(Explore_Objects.carrierTxb, 60)) {
			Waittilljquesryupdated();
			WebInteractUtil.sendKeysWithKeys(Explore_Objects.carrierTxb, Carrier, "Enter");
			WebInteractUtil.sendKeysWithKeys(Explore_Objects.nodeTxb, Node, "Enter");
			WebInteractUtil.sendKeys(Explore_Objects.nrcTxb, NRC);
			WebInteractUtil.sendKeys(Explore_Objects.mrcTxb, MRC);
			WebInteractUtil.sendKeysWithKeys(Explore_Objects.termsTxb, Terms, "Enter");
			WebInteractUtil.sendKeysWithKeys(Explore_Objects.currenctTxb, Currency, "Enter");
			WebInteractUtil.sendKeysWithKeys(Explore_Objects.priceTypeTxb, Price_Type, "Enter");
			WebInteractUtil.sendKeysWithKeys(Explore_Objects.connectorTxb, Connector_Type, "Enter");
			if (Connection_Type.equalsIgnoreCase("DualEntry")) { WebInteractUtil.sendKeysWithKeys(Explore_Objects.dualEntryFromOLOTxb, Dual_Entry_From_OLO, "Enter"); };
			WebInteractUtil.click(Explore_Objects.createCostBtn);
			WebInteractUtil.pause(2000);
			Waittilljquesryupdated();
			if (WebInteractUtil.waitForElementToBeVisible(Explore_Objects.saveCostBtn, 60)) {
				WebInteractUtil.pause(1000);
				WebInteractUtil.click(Explore_Objects.approveQuote);
				Waittilljquesryupdated();
				if (WebInteractUtil.waitForInvisibilityOfElement(Explore_Objects.approveQuote, 25)) {
					ExtentTestManager.getTest().log(LogStatus.PASS, "Explore Offnet Cost have been Created for the request "+Request_ID+" successfully");
					System.out.println("Explore Offnet Cost have been Created for the request "+Request_ID+" successfully");
					WebInteractUtil.click(Explore_Objects.mapViewBtn);
					Waittilljquesryupdated();
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to Approve Explore offnet Cost for the request "+Request_ID+" , Please Verify");
					System.out.println("Unable to Approve Explore offnet Cost for the request "+Request_ID+" , Please Verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "saveCostBtn in Explore Create Cost Window was not visible, Please Verify");
				System.out.println("saveCostBtn Explore Create Cost Window was not visible, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}			
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Explore Create Cost Window is not visible, Please Verify");
			System.out.println("Explore Create Cost Window is not visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		
		return "True";
	}
	
	public String createNearnetCostExplore(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : createNearnetCostExplore
		Purpose     : This method is to create Nearnet Cost in Explore
		Designer    : Vasantharaja C
		Created on  : 24th June 2020 
		Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String Request_ID = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Request_ID");
		String Distance_From_Colt = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Distance_From_Colt");
		String Terms = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Terms");
		String Currency = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Currency");
		String Cost_Category = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Cost_Category");
		String Cost_SubCategory = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Cost_SubCategory");
		String Unit_Cost = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Unit_Cost");
		String Quantity = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Quantity");
		String Connection_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Connection_Type");
		String Product_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		String sResult;

		if (!Connection_Type.equalsIgnoreCase("OnnetDualEntry")) {
			if (WebInteractUtil.waitForElementToBeVisible(Explore_Objects.distanceFromColtTxb, 60)) {
				Waittilljquesryupdated();
				WebInteractUtil.sendKeys(Explore_Objects.distanceFromColtTxb, Distance_From_Colt);
				WebInteractUtil.sendKeys(Explore_Objects.quoteValidityUpToMonthTxb, Terms);
				WebInteractUtil.sendKeysWithKeys(Explore_Objects.nearnetCurrencyTxb, Currency, "Enter");
				WebInteractUtil.click(Explore_Objects.addCostBtn);
				if (WebInteractUtil.waitForElementToBeVisible(Explore_Objects.nearnetCostTable, 60)) {
					sResult = WebTableCellAction(Explore_Objects.nearnetCostHeaderTable, Explore_Objects.nearnetCostTable, "Cost Category", "1", "Select", Cost_Category);
					if (sResult.equalsIgnoreCase("False")){ return "False"; }
					sResult = WebTableCellAction(Explore_Objects.nearnetCostHeaderTable, Explore_Objects.nearnetCostTable, "Cost Subcategory", "1", "Select", Cost_SubCategory);
					if (sResult.equalsIgnoreCase("False")){ return "False"; }
					sResult = WebTableCellAction(Explore_Objects.nearnetCostHeaderTable, Explore_Objects.nearnetCostTable, "Unit Cost", "1", "Edit", Unit_Cost);
					if (sResult.equalsIgnoreCase("False")){ return "False"; }
					sResult = WebTableCellAction(Explore_Objects.nearnetCostHeaderTable, Explore_Objects.nearnetCostTable, "Quantity", "1", "Edit", Quantity);
					if (sResult.equalsIgnoreCase("False")){ return "False"; }
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Explore nearnetCostTable is not visible, Please Verify");
					System.out.println("Explore nearnetCostTable is not visible, Please Verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
					return "False";
				}	
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Explore Create Cost Window is not visible, Please Verify");
				System.out.println("Explore Create Cost Window is not visible, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
				return "False";
			}
		}
		
		
//		Secondary Cost Details Entry
		if (Connection_Type.equalsIgnoreCase("OnnetDualEntry") || Connection_Type.equalsIgnoreCase("New Building Dual Entry")) {
			switch (Connection_Type) {
				case "New Building Dual Entry":
					WebInteractUtil.click(Explore_Objects.dualEntryDigCostTab);
					Waittilljquesryupdated();
					WebInteractUtil.sendKeys(Explore_Objects.distanceFromColtSecondaryTxb, Distance_From_Colt);
					WebInteractUtil.click(Explore_Objects.addSecondaryCostBtn);
					break;
				case "OnnetDualEntry":
					WebInteractUtil.isPresent(Explore_Objects.distanceFromColtSecondaryTxb, 60);
					WebInteractUtil.sendKeys(Explore_Objects.distanceFromColtSecondaryTxb, Distance_From_Colt);
					WebInteractUtil.sendKeys(Explore_Objects.quoteValidityUpToMonthSecondaryTxb, Terms);
					WebInteractUtil.sendKeysWithKeys(Explore_Objects.nearnetCurrencySecondaryTxb, Currency, "Enter");
						WebInteractUtil.click(Explore_Objects.addSecondaryCostBtn);
			}
			Waittilljquesryupdated();
			WebInteractUtil.isPresent(Explore_Objects.nearnetCostHeaderSecondaryTable, 60);
			sResult = WebTableCellAction(Explore_Objects.nearnetCostHeaderSecondaryTable, Explore_Objects.nearnetCostSecondaryTable, "Cost Category", "1", "Select", Cost_Category);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			sResult = WebTableCellAction(Explore_Objects.nearnetCostHeaderSecondaryTable, Explore_Objects.nearnetCostSecondaryTable, "Cost Subcategory", "1", "Select", Cost_SubCategory);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			sResult = WebTableCellAction(Explore_Objects.nearnetCostHeaderSecondaryTable, Explore_Objects.nearnetCostSecondaryTable, "Unit Cost", "1", "Edit", Unit_Cost);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			sResult = WebTableCellAction(Explore_Objects.nearnetCostHeaderSecondaryTable, Explore_Objects.nearnetCostSecondaryTable, "Quantity", "1", "Edit", Quantity);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
		}
		
//		Saving the Details
		WebInteractUtil.isPresent(Explore_Objects.exploreSendToSalesBtn, 60);
		WebInteractUtil.click(Explore_Objects.exploreSendToSalesBtn);
		Waittilljquesryupdated();
		WebInteractUtil.pause(2000);
		if (Explore_Objects.nearnetStatusElem.getText().trim().equalsIgnoreCase("Completed")) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Nearnet Request with ID "+Request_ID+" for Site "+Sheet_Name+" processed succesfully");
			System.out.println("Nearnet Request with ID "+Request_ID+" for Site "+Sheet_Name+" processed succesfully");
			WebInteractUtil.click(Explore_Objects.mapViewBtn);
			Waittilljquesryupdated();
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to Process Nearnet Request with ID "+Request_ID+" for Site "+Sheet_Name+" succesfully, Please Verify");
			System.out.println("Unable to Process Nearnet Request with ID "+Request_ID+" for Site "+Sheet_Name+" succesfully, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		
		return "True";
	}
	
	public String logOutExplore() throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : logOutExplore
		Purpose     : This method is to Logout Explore Window
		Designer    : Vasantharaja C
		Created on  : 24th June 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		if (WebInteractUtil.waitForElementToBeVisible(Explore_Objects.menuLnk, 25)) {
			WebInteractUtil.click(Explore_Objects.menuLnk);
			Waittilljquesryupdated();
			WebInteractUtil.isPresent(Explore_Objects.exploreLogoutLnk, 20);
			WebInteractUtil.click(Explore_Objects.exploreLogoutLnk);
			Waittilljquesryupdated();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Explore logout was done successfully");
			System.out.println("Explore logout was done successfully");
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Explore Logout was not Successfull , Please Verify");
			System.out.println("Explore Logout was not Successfull , Please Verify , Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		return "True";
	}
	
	public String WebTableCellAction(WebElement hTable, WebElement sTable, String actColumn, String row_number, String ActionType, String ActionValue) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : WebTableCellAction
		Purpose     : This method is to logout form CPQ for the proxy user
		Designer    : Vasantharaja C
		Created on  : 1st April 2020 
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		String tXpath, sXpath;
		
		if (WebInteractUtil.waitForElementToBeVisible(hTable, 75)) {
			WebInteractUtil.scrollIntoView(hTable);
			sXpath = sTable.toString();
			sXpath = sXpath.substring(sXpath.indexOf("xpath:")+"xpath:".length(), sXpath.indexOf("]]")+1).trim();
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "WebTable "+hTable.toString()+" was not Visible, Please Verify");
			System.out.println("WebTable +"+hTable.toString()+" was not Visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
//		getting the row and column number
		List<WebElement> rows = hTable.findElements(By.tagName("tr"));
		List<WebElement> columns = rows.get(0).findElements(By.tagName("th"));
		int tot_col = columns.size();
		int iCol, aColumn_number = 0;
		
		//Reading the actual column number ff table and set the column number with use of reference
		for(iCol = 1; iCol <= tot_col-1; iCol++){
			String Col_Val = columns.get(iCol).getText().trim();
			if (Col_Val.contains(actColumn)){ 
				aColumn_number = iCol+1; 
				break; 
			}
		}
//			Returns the function of column names are not matched
		if (iCol >= tot_col) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Column Name "+actColumn+" is not found in the webtable, Please Verify ");
			System.out.println("Column Name "+actColumn+" is not found in the webtable, Please Verify ");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(DriverManagerUtil.Capturefullscreenshot()));
			return "False";
		}
		
		WebElement Cell; String sOut = null;
		Actions actions = new Actions(driver);
		switch (ActionType) {
			case "Edit":
				Cell = driver.findElement(By.xpath(sXpath+"/tr["+row_number+"]/td["+aColumn_number+"]"));
				WebInteractUtil.scrollIntoView(Cell);
				actions.doubleClick(Cell).perform(); 
				WebElement edit_Box = driver.findElement(By.xpath(sXpath+"/tr["+row_number+"]/td["+aColumn_number+"]//input"));
				actions.sendKeys(ActionValue).perform();
				WebInteractUtil.pause(1000);
				WebInteractUtil.click(hTable);
				break;
			case "Select":
				Cell = driver.findElement(By.xpath(sXpath+"/tr["+row_number+"]/td["+aColumn_number+"]"));
				WebInteractUtil.scrollIntoView(Cell);
				actions.doubleClick(Cell).perform(); 
				WebElement dropdown = driver.findElement(By.xpath(sXpath+"/tr["+row_number+"]/td["+aColumn_number+"]//input"));
				WebInteractUtil.click(dropdown); 
				actions.sendKeys(ActionValue).perform();
				WebInteractUtil.pause(1000);
				actions.sendKeys(Keys.ARROW_DOWN).perform();
				WebInteractUtil.pause(1000);
				actions.sendKeys(Keys.ENTER).perform();
				WebInteractUtil.pause(1000);
				WebInteractUtil.click(hTable);
				break;
			case "Store":
				Cell = driver.findElement(By.xpath(sXpath+"/tr["+row_number+"]/td["+aColumn_number+"]//child::span"));
				WebInteractUtil.scrollIntoView(Cell);
				sOut = Cell.getAttribute("title");
				break;
				
			case "Click":
				Cell = driver.findElement(By.xpath(sXpath+"/tr["+row_number+"]/td["+aColumn_number+"]"));
				WebInteractUtil.scrollIntoView(Cell);
				WebInteractUtil.click(Cell);
				break;
		}
		
		return "True";
	}
}

