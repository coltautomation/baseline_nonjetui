package com.qa.colt.listeners;

import com.relevantcodes.extentreports.LogStatus;
import com.colt.common.utils.DriverManagerUtil;
import com.qa.colt.extentreport.ExtentManager;
import com.qa.colt.extentreport.ExtentTestManager;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;


public class TestListener implements ITestListener  {
 
    private static String getTestMethodName(ITestResult iTestResult) {
	/*----------------------------------------------------------------------
	Method Name : getTestMethodName
	Purpose     : to get the method name of the particular test which runs
	Designer    : Vasantharaja C
	Created on  : 1st April 2020 
	Input       : iTestResult
	Output      : methodname
	 ----------------------------------------------------------------------*/ 
        return iTestResult.getMethod().getConstructorOrMethod().getName();
    }
   
    //Before starting all tests, below method runs.
    public void onStart(ITestContext iTestContext) {
	/*----------------------------------------------------------------------
	Method Name : onStart
	Purpose     : To display the current test which picked to run on the console
	Designer    : Vasantharaja C
	Created on  : 1st April 2020 
	Input       : iTestContext
	Output      : none
	 ----------------------------------------------------------------------*/ 
    	
//    	System.out.println("Starting Execution of the test " + iTestContext.getName());
    }
 
    //
    public void onFinish(ITestContext iTestContext) {
	/*----------------------------------------------------------------------
	Method Name : onFinish
	Purpose     : To End the test when the test gets completed
	Designer    : Vasantharaja C
	Created on  : 1st April 2020 
	Input       : iTestContext
	Output      : none
	 ----------------------------------------------------------------------*/ 
    	
//    	System.out.println("Ending Execution of the test " + iTestContext.getName());
        //Do tier down operations for extentreports reporting!
        ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
    }
 
    public void onTestStart(ITestResult iTestResult) {
	/*----------------------------------------------------------------------
	Method Name : onTestStart
	Purpose     : This will act like beforetest annotation where it will set up utilities before running test
	Designer    : Vasantharaja C
	Created on  : 1st April 2020 
	Input       : iTestResult
	Output      : none
	 ----------------------------------------------------------------------*/ 
        
    	//Start operation for extentreports in Extent Test Manager
    	
        DateFormat df = new SimpleDateFormat("yyyyMMdd-HHmm");
        String Filename = iTestResult.getName()+ "-" + df.format(new Date());
//        System.out.println("Name of the Extent report for this Execution is : "+Filename);
        String ScenarioName = iTestResult.getTestContext().getCurrentXmlTest().getParameter("ScenarioName");
//        System.out.println("Executing the Scenario "+ScenarioName);
        ExtentTestManager.startTest(ScenarioName,"");
    }
 
    public void onTestSuccess(ITestResult iTestResult) {
	/*----------------------------------------------------------------------
	Method Name : onTestStart
	Purpose     : This method will print the log as Pass and terminate the extent report to generate report
	Designer    : Vasantharaja C
	Created on  : 1st April 2020 
	Input       : iTestResult
	Output      : none
	 ----------------------------------------------------------------------*/ 
    	
    	String ScenarioName = iTestResult.getTestContext().getCurrentXmlTest().getParameter("ScenarioName");
    	System.out.println("Scenario " +  ScenarioName + " got executed successfully");
//    	DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().close();
        //Extentreports log operation for passed tests.
    	ExtentTestManager.getTest().log(LogStatus.PASS, ScenarioName+" : Scenario has been passed");
    	ExtentTestManager.endTest();
    	ExtentManager.getReporter().flush();
    }
 
    public void onTestFailure(ITestResult iTestResult) {
	/*----------------------------------------------------------------------
	Method Name : onTestFailure
	Purpose     : This method will print the log as Fail and terminate the extent report to generate report
	Designer    : Vasantharaja C
	Created on  : 1st April 2020 
	Input       : iTestResult
	Output      : none
	 ----------------------------------------------------------------------*/ 
    	String ScenarioName = iTestResult.getTestContext().getCurrentXmlTest().getParameter("ScenarioName");
    	String Err_Msg = iTestResult.getThrowable().getMessage();
    	System.out.println("Scenario " +  ScenarioName + " got failed, with the exception "+Err_Msg);
    	
    	try {
			String screenShot =DriverManagerUtil.Capturefullscreenshot();
			ExtentTestManager.getTest().log(LogStatus.FAIL,ScenarioName +" : with the exception "+Err_Msg+ExtentTestManager.getTest().addBase64ScreenShot(screenShot));
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
		} catch (IOException e) {
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
		}
    }
 
    public void onTestError(ITestResult iTestResult) {
	/*----------------------------------------------------------------------
	Method Name : onTestError
	Purpose     : This method will print the log as Error and terminate the extent report to generate report
	Designer    : Vasantharaja C
	Created on  : 1st April 2020 
	Input       : iTestResult
	Output      : none
	 ----------------------------------------------------------------------*/ 
    	
    	String ScenarioName = iTestResult.getTestContext().getCurrentXmlTest().getParameter("ScenarioName");
    	System.out.println("Error while Executing the scenario " +  ScenarioName + " ,Please verify");
        //Extentreports log operation for passed tests.
    	ExtentTestManager.getTest().log(LogStatus.ERROR, "Error while Executing the Scenario" + ScenarioName + " ,Please Verify");     
    	try {
			String screenShot =DriverManagerUtil.Capturefullscreenshot();
			ExtentTestManager.getTest().addBase64ScreenShot(screenShot);
			DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get().close();
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
		} catch (IOException e) {
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
		}
    }

    public void onTestSkipped(ITestResult iTestResult) {
	/*----------------------------------------------------------------------
	Method Name : onTestSkipped
	Purpose     : This method will print the log as Skipped and terminate the extent report to generate report
	Designer    : Vasantharaja C
	Created on  : 1st April 2020 
	Input       : iTestResult
	Output      : none
	 ----------------------------------------------------------------------*/ 
    	
    	String ScenarioName = iTestResult.getTestContext().getCurrentXmlTest().getParameter("ScenarioName");
//    	System.out.println("Scenario "+  ScenarioName + " got Skipped from execution");
        ExtentTestManager.getTest().log(LogStatus.SKIP, "Scenario "+  ScenarioName + " got Skipped from execution");
        ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
        
    }
 
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
	/*----------------------------------------------------------------------
	Method Name : onTestSkipped
	Purpose     : This method will print the log as Skipped and terminate the extent report to generate report
	Designer    : Vasantharaja C
	Created on  : 1st April 2020 
	Input       : iTestResult
	Output      : none
	 ----------------------------------------------------------------------*/ 
    	
    	String ScenarioName = iTestResult.getTestContext().getCurrentXmlTest().getParameter("ScenarioName");
    	System.out.println("Scenario " +ScenarioName+" failed but it is in defined success ratio ");
    	ExtentTestManager.getTest().log(LogStatus.INFO, "Scenario "+ScenarioName+" failed but it is in defined success ratio ");
        ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
    }
 
}
